﻿$(function () {
    $("input[type!='submit'], input[type!='button'], textarea").focusin(function () {
        $(this).css({ 'background-color': 'khaki' });
    });

    $("input[type!='submit'], input[type!='button'], textarea").blur(function () {
        $(this).css({ 'background-color': '' });
    });
});