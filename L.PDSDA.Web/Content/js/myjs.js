﻿var root;

/* String Function */
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}

function trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g, "");
}
function ltrim(stringToTrim) {
    return stringToTrim.replace(/^\s+/, "");
}
function rtrim(stringToTrim) {
    return stringToTrim.replace(/\s+$/, "");
}
/* String Function */

function initialized(_root) {
    if (_root == "/") {
        _root = "";
    }
    this.root = _root;
}

$.ajaxSetup({ cache: false });

var dialogLoading;

function fnLoading() {
    //    alert(root);
    var loading = $('<center><img src="' + root + '/Content/images/ajaxLoading.gif" height="150" width = "200"  alt="Loading..." /></center>');
    dialogLoading = $('<div style="overflow:hidden"></div>');
    dialogLoading.append(loading);
    dialogLoading.dialog({ closeOnEscape: false, modal: true }).dialog("open");
}

function fnLoadingClose() {
    dialogLoading.dialog("close");
}

// LOAD WITH EVENT
function loadDialogWithEvent(tag, event, target) {
    event.preventDefault();
    var $loading = $('<img src="/Content/images/ajaxLoading.gif" height="20" width = "20"  alt="loading" class="ui-loading-icon">');
    var $url = tag.href.replace(/ /g, '%20'); // $(tag).attr('href').replace(/ /g, '%20');
    var $title = tag.title; // $(tag).attr('title');
    var $dialog = $('<div></div>');
    //    var $table = $("table.display").dataTable();
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    autoOpen: false
			, title: $title
			, width: 500
	        , modal: true
			, minHeight: 200
	        	, show: 'fade'
		    //	        	, hide: 'fade'
		});

    $dialog.dialog("option", "buttons", {
        "Tutup": function () {
            $(this).dialog("close");
            $(this).empty();

        },
        "Simpan": function () {
            var dlg = $(this);
            $.ajax({
                url: $url,
                type: 'POST',
                data: $("#target").serialize(),
                success: function (response) {
                    dlg.dialog('close');
                    //		                console.log($table);
                    //		                $table.fnDraw(false);
                    $(target).html(response);
                    dlg.empty();
                    //		                $("#ajaxResult").hide().html('Record saved.').fadeIn(300, function () {
                    //		                    var e = this;
                    //		                    setTimeout(function () { $(e).fadeOut(400); }, 2500);
                    //		                });
                    parent.location.reload(true);
                },
                error: function (xhr) {
                    if (xhr.status == 400)
                        dlg.html(xhr.responseText, xhr.status);     /* display validation errors in this dialog */
                    else {
                        console.log(xhr);
                        alert(xhr);
                        //		                    displayError(xhr.responseText, xhr.status); /* display other errors in separate dialog */
                    }
                    parent.location.reload(true);
                }
            });
        }
    });

    //    $dialog.dialog('open');
};


function loadDialog(tag, target) {
    //    var $loading =     $('<img src="../images/ajaxLoading.gif" height="20" width = "20"  alt="loading" class="ui-loading-icon">');
    //    alert(tag.href);
    var $url = tag.href.replace(/ /g, '%20'); // $(tag).attr('href').replace(/ /g, '%20');
    var $title = tag.title; // $(tag).attr('title');
    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;"></div>');
    //    var $table = $("table.display").dataTable();
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: 500
	        	, modal: true
			, minHeight: 200
	        	, show: 'fade'
		    //                ,height:$(window).height()
			, maxHeight: $(window).height()

		    //	        	, hide: 'fade'
		});

    $dialog.dialog("option", "buttons", [{
        text: "Simpan",
        click: function () {
            $("#target").validate();
            if (!$("#target").valid()) {
                return false;
            }
            var dlg = $(this);
            $.ajax({
                url: $url,
                type: 'POST',
                data: $("#target, #targetRawaDyn").serialize(),
                success: function (response) {
                    alert(response.message);
                    if (response.success) {
                        dlg.dialog('close');
                        $(target).html(response);
                        dlg.empty();
                        parent.window.location.reload(true);
                    }
                    return false;
                },
                error: function (xhr) {
                    alert(xhr.message);
                    //                    alert(xhr.message);

                    if (xhr.status == 400)
                        dlg.html(xhr.responseText, xhr.status);     /* display validation errors in this dialog */
                    else {
                        console.log(xhr);
                        alert(xhr);
                    }
                    parent.window.location.reload(true);
                }
            });
        },
        icons: {
            primary: 'ui-icon-disk'
        }
    }
    , {
        text: "Tutup",
        click: function () {
            $(this).dialog("close");
            $(this).empty();
            parent.window.location.reload(true);
        },
        icons: {
            primary: 'ui-icon-circle-close'
        }
    }]);

    //    $dialog.dialog('open');
}



//function loadDialog(tag, target) {
//    //    var $loading =     $('<img src="../images/ajaxLoading.gif" height="20" width = "20"  alt="loading" class="ui-loading-icon">');
//    //    alert(tag.href);
//    var $url = tag.href.replace(/ /g, '%20'); // $(tag).attr('href').replace(/ /g, '%20');
//    var $title = tag.title; // $(tag).attr('title');
//    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;"></div>');
//    //    var $table = $("table.display").dataTable();
//    $dialog
//    //	.append($loading)
//		.load($url)
//		.dialog({
//		    closeOnEscape: false,
//		    position: "top",
//		    autoOpen: false
//			, title: $title
//			, width: 500
//	        	, modal: true
//			, minHeight: 200
//	        	, show: 'fade'
//		    //                ,height:$(window).height()
//			, maxHeight: $(window).height()

//		    //	        	, hide: 'fade'
//		});

//    $dialog.dialog("option", "buttons", {
//        "Tutup": function () {
//            $(this).dialog("close");
//            $(this).empty();
//            parent.window.location.reload(true);
//        },
//        "Simpan": function () {
//            $("#target").validate();
//            if (!$("#target").valid()) {
//                return false;
//            }
//            var dlg = $(this);
//            $.ajax({
//                url: $url,
//                type: 'POST',
//                data: $("#target, #targetRawaDyn").serialize(),
//                success: function (response) {
//                    alert(response.message);
//                    dlg.dialog('close');
//                    //		                console.log($table);
//                    //		                $table.fnDraw(false);
//                    $(target).html(response);
//                    dlg.empty();
//                    //		                $("#ajaxResult").hide().html('Record saved.').fadeIn(300, function () {
//                    //		                    var e = this;
//                    //		                    setTimeout(function () { $(e).fadeOut(400); }, 2500);
//                    //		                });
//                    parent.window.location.reload(true);
//                },
//                error: function (xhr) {
//                    alert(xhr.message);

//                    if (xhr.status == 400)
//                        dlg.html(xhr.responseText, xhr.status);     /* display validation errors in this dialog */
//                    else {
//                        console.log(xhr);
//                        alert(xhr);
//                        //                        displayError(xhr.responseText, xhr.status); /* display other errors in separate dialog */
//                    }
//                    parent.window.location.reload(true);
//                }
//            });
//        }
//    });

//    $dialog.dialog('open');
//};





function loadDialogAssign(tag, target) {
    //    var $loading = $('<img src="images/ajaxLoading.gif" height="20" width = "20"  alt="loading" class="ui-loading-icon">');
    var $url = tag.href.replace(/ /g, '%20'); // $(tag).attr('href').replace(/ /g, '%20');
    var $title = tag.title; // $(tag).attr('title');
    var $dialog = $('<div></div>');
    //    var $table = $("table.display").dataTable();
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({ closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: 1000
	        , modal: true
			, minHeight: 100
	        , show: 'fade'
		    //	        	, hide: 'fade'
		});

    $dialog.dialog("option", "buttons",
    //     {
    //         "Tutup": function () {
    //             $(this).dialog("close");
    //             $(this).empty();
    //             window.location.reload(true);
    //         }
    //     }
[{
    text: "Tutup",
    click: function () {
        try {
            $(this).dialog("close");
            $(this).empty();
        } catch (e) {
            fnReloadHalaman();
        }
        //            parent.window.location.reload(true);
        //        oTable.fnDraw(false);

    },
    icons: {
        primary: 'ui-icon-circle-close'
    }
}]
     );

    //    $dialog.dialog("option", "buttons", {
    //        "Tutup": function () {
    //            $(this).dialog("close");
    //            $(this).empty();
    //        },
    //        "Simpan": function () {
    //            var dlg = $(this);
    //            console.log($("#signing"));
    //            $.ajax({
    //                url: $url,
    //                type: 'POST',
    //                data: $("#signing").find('.display, input').serialize(),
    //                success: function (response) {
    //                    dlg.dialog('close');
    //                    //		                console.log($table);
    //                    //		                $table.fnDraw(false);
    //                    $(target).html(response);
    //                    dlg.empty();
    //                    //		                $("#ajaxResult").hide().html('Record saved.').fadeIn(300, function () {
    //                    //		                    var e = this;
    //                    //		                    setTimeout(function () { $(e).fadeOut(400); }, 2500);
    //                    //		                });
    //                    //                    parent.location.reload(true);
    //                },
    //                error: function (xhr) {
    //                    if (xhr.status == 400)
    //                        dlg.html(xhr.responseText, xhr.status);     /* display validation errors in this dialog */
    //                    else {
    //                        console.log(xhr);
    //                        displayError(xhr.responseText, xhr.status); /* display other errors in separate dialog */
    //                    }
    //                    parent.location.reload(true);
    //                }
    //            });
    //        }
    //    });

    //    $dialog.dialog('open');
};




function loadDialogAssignReloadTableClose(tag, target, oTable) {
    //    var $loading = $('<img src="images/ajaxLoading.gif" height="20" width = "20"  alt="loading" class="ui-loading-icon">');
    var $url = tag.href.replace(/ /g, '%20'); // $(tag).attr('href').replace(/ /g, '%20');
    var $title = tag.title; // $(tag).attr('title');
    var $dialog = $('<div></div>');
    //    var $table = $("table.display").dataTable();
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({ closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: 1000
	        	, modal: true
			, minHeight: 100
	        	, show: 'fade'
		    //	        	, hide: 'fade'
		});

    $dialog.dialog("option", "buttons",
    //     {
    //         "Tutup": function () {
    //             $(this).dialog("close");
    //             $(this).empty();
    //             window.location.reload(true);
    //         }
    //     }
[{
    text: "Tutup",
    click: function () {
        try {
            $(this).dialog("close");
            $(this).empty();
            oTable.fnDraw(false);
        } catch (e) {
            fnReloadHalaman();
        }

    },
    icons: {
        primary: 'ui-icon-circle-close'
    }
}]
     );

    //    $dialog.dialog("option", "buttons", {
    //        "Tutup": function () {
    //            $(this).dialog("close");
    //            $(this).empty();
    //        },
    //        "Simpan": function () {
    //            var dlg = $(this);
    //            console.log($("#signing"));
    //            $.ajax({
    //                url: $url,
    //                type: 'POST',
    //                data: $("#signing").find('.display, input').serialize(),
    //                success: function (response) {
    //                    dlg.dialog('close');
    //                    //		                console.log($table);
    //                    //		                $table.fnDraw(false);
    //                    $(target).html(response);
    //                    dlg.empty();
    //                    //		                $("#ajaxResult").hide().html('Record saved.').fadeIn(300, function () {
    //                    //		                    var e = this;
    //                    //		                    setTimeout(function () { $(e).fadeOut(400); }, 2500);
    //                    //		                });
    //                    //                    parent.location.reload(true);
    //                },
    //                error: function (xhr) {
    //                    if (xhr.status == 400)
    //                        dlg.html(xhr.responseText, xhr.status);     /* display validation errors in this dialog */
    //                    else {
    //                        console.log(xhr);
    //                        displayError(xhr.responseText, xhr.status); /* display other errors in separate dialog */
    //                    }
    //                    parent.location.reload(true);
    //                }
    //            });
    //        }
    //    });

    //    $dialog.dialog('open');
}


function loadDialogWithFormId(tag, formid, dialog, onsuccess, onerror, onclose) {
    //    var $loading = $('<img src="/Content/images/ajaxLoading.gif" height="20" width = "20"  alt="loading" class="ui-loading-icon">');
    var $url = tag.href.replace(/ /g, '%20'); // $(tag).attr('href').replace(/ /g, '%20');
    var $title = tag.title; // $(tag).attr('title');
    var $dialog = dialog;
    //    var $table = $("table.display").dataTable();
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    autoOpen: false
			, title: $title
			, width: 500
	        	, modal: true
			, minHeight: 200
	        	, show: 'fade'
		    //	        	, hide: 'fade'
		});

    $dialog.dialog("option", "buttons", {
        "Tutup": onclose,
        "Simpan": function () {
            var dlg = $(this);
            $.ajax({
                url: $url,
                type: 'POST',
                data: $("#" + formid).serialize(),
                success: onsuccess,
                error: onerror
            });
        }
    });

    //    $dialog.dialog('open');
};


function LoadDialogCustomForm(tag, target, formid) {
    //    var $loading =     $('<img src="../images/ajaxLoading.gif" height="20" width = "20"  alt="loading" class="ui-loading-icon">');
    var $url = tag.href.replace(/ /g, '%20'); // $(tag).attr('href').replace(/ /g, '%20');
    var $title = tag.title; // $(tag).attr('title');
    var $dialog = $('<div style="overflow:auto;"></div>');
    //    var $table = $("table.display").dataTable();
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: 500
	        	, modal: true
			, minHeight: 200
	        	, show: 'fade'
		    //	        	, hide: 'fade'
		});

    $dialog.dialog("option", "buttons", {
        "Tutup": function () {
            try {
                $(this).dialog("close");
                $(this).empty();
            } catch (e) {
                fnReloadHalaman();
            }
            //            parent.window.location.reload(true);
        },
        "Simpan": function () {
            var dlg = $(this);
            $.ajax({
                url: $url,
                type: 'POST',
                data: $("#" + formid).serialize(),
                success: function (response) {
                    alert(response.message);
                    dlg.dialog('close');
                    //		                console.log($table);
                    //		                $table.fnDraw(false);
                    $(target).html(response);
                    dlg.empty();
                    //		                $("#ajaxResult").hide().html('Record saved.').fadeIn(300, function () {
                    //		                    var e = this;
                    //		                    setTimeout(function () { $(e).fadeOut(400); }, 2500);
                    //		                });
                    //                    parent.window.location.reload(true);
                },
                error: function (xhr) {
                    alert(xhr.message);

                    if (xhr.status == 400)
                        dlg.html(xhr.responseText, xhr.status);     /* display validation errors in this dialog */
                    else {
                        //                        console.log(xhr);
                        alert(xhr);
                    }
                    parent.window.location.reload(true);
                }
            });
        }
    });

    //    $dialog.dialog('open');
}


function loadDialogCustom(tag, divId, funcClose, funcSave) {
    var $url = tag.href.replace(/ /g, '%20');
    var $title = tag.title;
    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;" id="' + divId + '"></div>');
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: 500
	        	, modal: true
			, minHeight: 200
	        	, show: 'fade'
			, maxHeight: $(window).height()
		});

    $dialog.dialog("option", "buttons", {
        "Tutup": funcClose,
        "Simpan": funcSave
    });

    //    $dialog.dialog('open');
};

function loadDefaultDialogCustomControl(tag, divId, width, height, formid, oTable) {
    var $url = tag.href.replace(/ /g, '%20');
    var $title = tag.title;
    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;" id="' + divId + '"></div>');

    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: width
	        	, modal: true
			, minHeight: height
	        	, show: 'fade'
			, maxHeight: $(window).height()
		                , close: function () { $(this).dialog("destroy"); oTable.fnDraw(false); }
		    //                        , open: function () { $(this).dialog('open'); }
		});
    //    alert('edit2');

    ExecuteButton($dialog, $url, formid, oTable);

    //    $dialog.dialog('open');
};

function ExecuteButton($dialog, $url, formid, oTable) {
    //    alert('masuk');
    $dialog.dialog("option", "buttons", [{
        text: "Simpan",
        click: function () {
            try {
                fnLoading();
                $("#" + formid).validate();
                if (!$("#" + formid).valid()) {
                    dialogLoading.dialog('close');
                    alert('Periksa inputan kembali');
                    return false;
                }
                var dlg = $($dialog);
                //            console.log($("#" + formid).serialize());
                $.ajax({
                    url: $url,
                    type: 'POST',
                    data: $("#" + formid).serialize(),
                    success: function (response) {
                        //                        console.log(response);
                        var strMsg = response.message;
//                        alert(strMsg.ltrim().indexOf('at NHibernate.Id.Assigned.Generate'));
                        if (strMsg.ltrim().indexOf('at NHibernate.Id.Assigned.Generate') != -1) {
                            dialogLoading.dialog('close');
                            alert('Kode / Id Tidak Boleh Kosong');
                            return false;
                        }
                        else {
                            dialogLoading.dialog('close');
                            alert(response.message);
                        }
                        if (response.success) {
                            dialogLoading.dialog('close');
                            dlg.dialog('close');
                            dlg.empty();
                            //                        $(this).remove();
                            //                        dlg.dialog().destroy();
                        }
                        return false;
                    },
                    error: function (xhr) {
                        alert(xhr.message);
                        //                        alert(xhr.message);

                        if (xhr.status == 400)
                            dlg.html(xhr.responseText, xhr.status);     /* display validation errors in this dialog */
                        else {
                            dialogLoading.dialog('close');
                            alert(xhr);
                        }
                    }
                });
            } catch (e) {
                if (e == "TypeError: $(...).validate is not a function" || e == "TypeError: this[0] is undefined") {
                    dialogLoading.dialog('close');
                    alert("Tidak dapat disimpan. Hanya dapat melihat detail saja.");
                }
                else {
                    dialogLoading.dialog('close');
                    alert(e); //                console.log(e);
                }
            }
        },
        icons: {
            primary: 'ui-icon-disk'
        }
    }
    , {
        text: "Tutup",
        click: function () {
            try {
                $(this).dialog("close");
                $(this).empty();
            } catch (e) {
                fnReloadHalaman();
            }
        },
        icons: {
            primary: 'ui-icon-circle-close'
        }
    }]);
}

function loadDefaultDialogCustomControlWithoutDefaultButton(tag, divId, width, height, formid, oTable) {
    var $url = tag.href.replace(/ /g, '%20');
    var $title = tag.title;
    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;" id="' + divId + '"></div>');
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: width
	        	, modal: true
			, minHeight: height
	        	, show: 'fade'
			, maxHeight: $(window).height()
		                , close: function () { $dialog.dialog('close'); $(this).dialog("destroy"); oTable.fnDraw(false); }
		});

    //    ExecuteButton($dialog, $url, formid, oTable);

    //    $dialog.dialog('open');
};

// Detail Dialog
function loadDetailDialogCustomControl(tag, divId, width, height, formid, oTable) {

    //    var timestamp = new Date().getTime();
    //    $(this).load('@Url.Action(MVC.FileUpload.FileUpload())' + '&t=' + timestamp);
    var $url = tag.href.replace(/ /g, '%20'); //  + '&t=' + timestamp;
    var $title = tag.title;
    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;" id="' + divId + '"></div>');
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    //		    if (status == "error") {
		    //		        var msg = "Sorry but there was an error: ";
		    //		        $("#error").html(msg + xhr.status + " " + xhr.statusText);
		    //		    }
		    //		    else 
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    //		    cache: false,
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: width
	        , modal: true
			, minHeight: height
	        , show: 'fade'
			, maxHeight: $(window).height()
		    , close: function () { $(this).dialog("destroy"); oTable.fnDraw(false); }
		});

    ExecuteDetailButton($dialog, $url, formid, oTable);
    //    $dialog.dialog("moveToTop");
};



// Detail Dialog
function loadDetailDialogFromMapCustomControl(tag, divId, width, height, formid) {
    var $url = tag.href.replace(/ /g, '%20');
    var $title = tag.title;
    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;" id="' + divId + '"></div>');
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    //		    if (status == "error") {
		    //		        var msg = "Sorry but there was an error: ";
		    //		        $("#error").html(msg + xhr.status + " " + xhr.statusText);
		    //		    }
		    //		    else 
		    if (status == "success") {
		        dialogLoading.dialog("close"); $dialog.dialog('open');
		    }
		})
		.dialog({
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: width
	        	, modal: true
			, minHeight: height
	        	, show: 'fade'
			, maxHeight: $(window).height()
		                , close: function () { $(this).dialog("destroy"); }
		});

    ExecuteDetailButton($dialog, $url, formid, "");

    //    $dialog.dialog('open');
}

function ExecuteDetailButton($dialog, $url, formid, oTable) {
    $dialog.dialog("option", "buttons", [
    {
        text: "Tutup",
        click: function () {
            try {
                $(this).dialog("close");
                $(this).empty();
            } catch (e) {
                fnReloadHalaman();
            }

        },
        icons: {
            primary: 'ui-icon-circle-close'
        }
    }]);
}



function loadDetailCalendar(tag, divId, width, height, formid, oTable) {

    //    var timestamp = new Date().getTime();
    //    $(this).load('@Url.Action(MVC.FileUpload.FileUpload())' + '&t=' + timestamp);
    var $url = tag.href.replace(/ /g, '%20'); //  + '&t=' + timestamp;
    var $title = tag.title;
    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;" id="' + divId + '"></div>').dialog();
    fnLoading();
    $dialog
		.load($url, function (response, status, xhr) {
		    //		    if (status == "error") {
		    //		        var msg = "Sorry but there was an error: ";
		    //		        $("#error").html(msg + xhr.status + " " + xhr.statusText);
		    //		    }
		    //		    else 
		    if (status == "success") {
		        dialogLoading.dialog("close"); //$dialog.dialog('open');
		        $dialog.dialog("open");
		    }
		})
		.dialog({
		    //		    cache: false,
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: width
	        , modal: true
			, minHeight: height
	        , show: 'fade'
			, maxHeight: $(window).height()
		    , close: function () { $(this).dialog("destroy"); oTable.fnDraw(false); }
		});

    ExecuteDetailButton($dialog, $url, formid, oTable);
    //    $dialog.dialog("open");
};



$(function () {
    //    $("input[type!='submit'], input[type!='button'], textarea").on({
    //        focusin: function () {
    //            $(this).css({ 'background-color': 'khaki' })
    //        },
    //        blur: function () {
    //            $(this).css({ 'background-color': '' })
    //        }
    //    });

    $("input[type!='submit'], input[type!='button'], textarea").focusin(function () {
        $(this).css({ 'background-color': 'khaki' });
    });

    $("input[type!='submit'], input[type!='button'], textarea").blur(function () {
        $(this).css({ 'background-color': '' });
    });

    $("a.clsButtonAdd").button({
        icons: {
            primary: "ui-icon-circle-plus"
        }
    }).css({ "font-weight": "normal" });

    $("a.clsButtonEdit").button({
        icons: {
            primary: "ui-icon-wrench"
        }
    }).css({ "font-weight": "normal" });

    $("a.clsButtonDelete").button({
        icons: {
            primary: "ui-icon-trash"
        }
    }).css({ "font-weight": "normal" });

    $("a.clsButtonClose").button({
        icons: {
            primary: "ui-icon-circle-close"
        }
    }).css({ "font-weight": "normal" });

    $("a.clsButtonDetail").button({
        icons: {
            primary: "ui-icon-newwin"
        }
    }).css({ "font-weight": "normal" });


});

function oLanguage() {
    return {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
            "sFirst": "Pertama",
            "sPrevious": "Sebelumnya",
            "sNext": "Selanjutnya",
            "sLast": "Terakhir"
        }
    }
}

function loadDialogMap(tag, divId, width, height) {
    var $url = tag.href.replace(/ /g, '%20');
    var $title = tag.title;
    var $dialog = $('<div style="overflow:auto;position:relative;z-index:1000;" id="' + divId + '"></div>');
    $dialog
		.load($url)
		.dialog({
		    closeOnEscape: false,
		    position: "top",
		    autoOpen: false
			, title: $title
			, width: width
	        	, modal: true
			, minHeight: height
	        	, show: 'fade'
			, maxHeight: $(window).height()
            , close: function () {
                $dialog.dialog("close");
                //                $dialog.empty();
                $(this).dialog("destroy");
            }

		});


    $dialog.dialog("option", "buttons",
[{
    text: "Tutup",
    click: function () {
        //        $dialog.dialog("close");
        //        $dialog.empty();
        $(this).dialog('close');
    },
    icons: {
        primary: 'ui-icon-circle-close'
    }
}]
     );

    $dialog.dialog('open');
};

function fnServerData(url, data, callback) {
    $.ajax({
        "url": url,
        "data": data,
        "success": callback,
        "dataType": "json",
        "type": "POST",
        "cache": false,
        "error": function () {
            //            alert("DataTables warning: JSON data from server failed to load or be parsed. " +
            //						"This is most likely to be caused by a JSON formatting error.");
            fnReloadHalaman();
        }
    });
}

function fnReloadHalaman(e) {
    //    alert("Halaman akan di reload.");
    //    location.reload();
}