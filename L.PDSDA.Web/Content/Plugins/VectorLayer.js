﻿
function onFeatureSelect(feature) {
    selectedFeature = feature;
    popup = new OpenLayers.Popup.FramedCloud("tempId", feature.geometry.getBounds().getCenterLonLat(),
                                     null,
                                     selectedFeature.attributes.salutation, // + " Position Lat:" + selectedFeature.attributes.dLat + " Lon:" + selectedFeature.attributes.dLon,
                                     null, true);
    feature.popup = popup;
    map.addPopup(popup);
}

function onFeatureUnselect(feature) {
    map.removePopup(feature.popup);
    feature.popup.destroy();
    feature.popup = null;
}

function placeMarker(lat, lon, data) {
    var Lat = lat;
    var Lon = lon;
    var dLat = lat;
    var dLon = lon;
    var LonLat = new OpenLayers.Geometry.Point(Lon, Lat);
    LonLat.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
    var externalImage = "";
    externalImage = "/Content/Plugins/lib/Openlayers/img/marker.png";
    if (data.type == "sungai") {
        externalImage = "/Content/icon/SDA/03 Sungai.jpg";
    }
    else if (data.type == "rawa") {
        externalImage = "/Content/icon/SDA/04 Rawa.jpg";
    }
    else if (data.type == "danau") {
        externalImage = "/Content/icon/SDA/05 Danau.jpg";
    }
    else if (data.type == "airtanah") {
        externalImage = "/Content/icon/SDA/06 Air Tanah.jpg";
    }
    else if (data.type == "embung") {
        externalImage = "/Content/icon/SDA/07 Embung.jpg";
    }
    else if (data.type == "embungpotensi") {
        externalImage = "/Content/icon/SDA/08 Embung Potensi.jpg";
    }
    else if (data.type == "bendungan") {
        externalImage = "/Content/icon/SDA/09 Bendungan.jpg";
    }
    else if (data.type == "bendung") {
        externalImage = "/Content/icon/SDA/10 Bendung.jpg";
    }

    else if (data.type == "poskobanjir") {
        externalImage = "/Content/icon/Bencana/01 Posko Bencana.jpg";
    }
    else if (data.type == "pospengamatbanjir") {
        externalImage = "/Content/icon/Bencana/02 Pos Pengamat Banjir.jpg";
    }
    else if (data.type == "daerahrawanbanjir") {
        externalImage = "/Content/icon/Bencana/03 Daerah Rawan Banjir.jpg";
    }
    else if (data.type == "daerahrawankekeringan") {
        externalImage = "/Content/icon/Bencana/04 Daerah Rawan Kekeringan.jpg";
    }
    else if (data.type == "daerahrawanlongsor") {
        externalImage = "/Content/icon/Bencana/05 Daerah Rawan Longsor.jpg";
    }
    else if (data.type == "daerahgunungberapi") {
        externalImage = "/Content/icon/Bencana/06 Daerah Gunung Merapi.jpg";
    }

    else if (data.type == "poshujan") {
        externalImage = "/Content/icon/Hidrologi/01 Pos Hujan.jpg";
    }
    else if (data.type == "posdugaair") {
        externalImage = "/Content/icon/Hidrologi/02 Pos Duga Air.jpg";
    }
    else if (data.type == "posklimatologi") {
        externalImage = "/Content/icon/Hidrologi/03 Pos Klimato.jpg";
    }

    //    if (paket.KdKeg == "5037") {
    //        externalImage = "/Contents/Plugins/lib/Openlayers/img/marker-blue.png";
    //    }
    //    if (paket.KdKeg == "5039") {
    //        externalImage = "/Contents/Plugins/lib/Openlayers/img/marker-green.png";
    //    }
    //    if (paket.KdKeg == "5040") {
    //        externalImage = "/Contents/Plugins/lib/Openlayers/img/marker-gold.png";
    //    }

    var Feature = new OpenLayers.Feature.Vector(LonLat,
                                    { salutation: "<table><tr><td colspan='3'>" + data.type + "</td></tr><tr><td>Name</td><td>:</td><td>" + decodeURI(data.name) + "</td></tr><tr><td colspan='4'><a href=javascript:fnDetail(this,'" + data.KEY + "','" + data.type + "')>Detail</a></td></tr></table>"
                                        , Lon: Lon
                                        , Lat: Lat

                                    }
                                        , {
                                            externalGraphic: externalImage,
                                            graphicWidth: 20,
                                            graphicHeight: 20,
                                            fillOpacity: 1
                                        }
                                    );
    //    console.log(Feature);
    vectorLayer.addFeatures(Feature);
    //    var popup = new OpenLayers.Popup.FramedCloud("tempId", new OpenLayers.LonLat(Lon, Lat).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()),
    //                               null,
    //                               Feature.attributes.salutation, // + " Position Lat:" + Feature.attributes.dLat + " Lon:" + Feature.attributes.dLon,
    //                               null, true);
    //    Feature.popup = popup;
    //    map.addPopup(popup);
}

//////////////////////////////////////////
function GetOLFeatures(url) {
    getOpenLayersFeatures(url, function (fs) {
        shpLayer.removeAllFeatures();
        // reproject features
        // this is ordinarily done by the format object, but since we're adding features manually we have to do it.

        var fsLen = fs.length;
        var inProj = new OpenLayers.Projection('EPSG:4326');
        var outProj = new OpenLayers.Projection('EPSG:3857');
        for (var i = 0; i < fsLen; i++) {
            fs[i].geometry = fs[i].geometry.transform(inProj, outProj);
        }
        shpLayer.addFeatures(fs);
    });
}

function GetOLFeaturesWorld(url, callback) {
    getOpenLayersFeatures(url, function (fs) {
        //        worldLayer.removeAllFeatures();
        //        console.log(fs);
        // reproject features
        // this is ordinarily done by the format object, but since we're adding features manually we have to do it.

        var fsLen = fs.length;
        var inProj = new OpenLayers.Projection('EPSG:4326');
        var outProj = new OpenLayers.Projection('EPSG:3857');
        for (var i = 0; i < fsLen; i++) {
            fs[i].geometry = fs[i].geometry.transform(inProj, outProj);
        }
        worldLayer.addFeatures(fs);
        if (callback) {
            callback();
        }
    });
}


//function GetOLFeaturesNewLayer(url, Name) {
//    var shpLayer1 = new OpenLayers.Layer.Vector(Name, { projection: "EPSG:4326" });
//    var selectShapeFileControl1 = new OpenLayers.Control.SelectFeature(shpLayer1, { onSelect: onFeatureSelectSF, onUnselect: onFeatureUnselectSF });
//    map.addControl(selectShapeFileControl1);

//    selectShapeFileControl1.activate();
//    map.addLayer(shpLayer1);

//    getOpenLayersFeatures(url, function (fs) {
//        // reproject features
//        // this is ordinarily done by the format object, but since we're adding features manually we have to do it.
//        var fsLen = fs.length;
//        var inProj = new OpenLayers.Projection('EPSG:4326');
//        var outProj = new OpenLayers.Projection('EPSG:3857');
//        for (var i = 0; i < fsLen; i++) {
//            fs[i].geometry = fs[i].geometry.transform(inProj, outProj);
//        }
//        shpLayer1.addFeatures(fs);
//    });
//}

function onFeatureSelectSF(feature) {
    //    console.log(feature);
    //    selectedShapeFileFeature = feature;
    popup = new OpenLayers.Popup.FramedCloud("tempId", feature.geometry.getBounds().getCenterLonLat(),
                                     null,
                                     feature.attributes.values.WS,
                                     null, true);
    feature.popup = popup;
    map.addPopup(popup);
}

function onFeatureUnselectSF(feature) {
    map.removePopup(feature.popup);
    feature.popup.destroy();
    feature.popup = null;
}


function onFeatureSelectWorld(feature) {
    //    console.log(feature);
    //    selectedShapeFileFeature = feature;
    popup = new OpenLayers.Popup.FramedCloud("tempId", feature.geometry.getBounds().getCenterLonLat(),
                                     null,
                                     feature.attributes.values.WS,
                                     null, true);
    feature.popup = popup;
    map.addPopup(popup);
}

function onFeatureUnselectWorld(feature) {
    map.removePopup(feature.popup);
    feature.popup.destroy();
    feature.popup = null;
}
