﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace L.PDSDA.Web.Areas.Sample.Controllers
{
    public class SampleController : Controller
    {
        //
        // GET: /Sample/Sample/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Sample/Sample/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Sample/Sample/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Sample/Sample/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Sample/Sample/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Sample/Sample/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Sample/Sample/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Sample/Sample/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
