﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TadahHujan.aspx.cs" Inherits="L.PDSDA.Web.Areas.DaerahIrigasi.TEMP.TadahHujan" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table style="width:100%;">
            <tr>
                <td>
                    Tahun Data</td>
                <td>
                    2000</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Luas Lahan Tadah Hujan (Ha)</td>
                <td>
                    1</td>
                <td>
                    Luas Lahan Kering (Ha)</td>
                <td>
                    2</td>
            </tr>
        </table>
    
    </div>
    <br />
    <div>
        <table style="width: 100%;">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    Padi</td>
                <td>
                    Palawija</td>
                <td>
                    Lain - lain</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Masa Tanam I</td>
                <td>
                    3</td>
                <td>
                    4</td>
                <td>
                    &nbsp;
                    5</td>
                <td style="text-align: center">
                    IP</td>
            </tr>
            <tr>
                <td>
                    Masa Tanam II</td>
                <td>
                    6</td>
                <td>
                    7</td>
                <td>
                    &nbsp;
                    8</td>
                <td>
                    12</td>
            </tr>
            <tr>
                <td>
                    Masa Tanam III</td>
                <td>
                    9</td>
                <td>
                    10</td>
                <td>
                    11</td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table style="width: 100%;">
            <tr>
                <td>
                    P3A</td>
                <td>
                    Total</td>
                <td>
                    Aktif</td>
                <td>
                    Berkembang</td>
                <td>
                    Tidak Berkembang</td>
                <td>
                    Pasif</td>
            </tr>
            <tr>
                <td>
                    Jumlah</td>
                <td>
                    13</td>
                <td>
                    14</td>
                <td>
                    15</td>
                <td>
                    16</td>
                <td>
                    17</td>
            </tr>
            <tr>
                <td>
                    Anggota</td>
                <td>
                    18</td>
                <td>
                    19</td>
                <td>
                    20</td>
                <td>
                    21</td>
                <td>
                    22</td>
            </tr>
            <tr>
                <td>
                    Berbadan Hukum</td>
                <td>
                    23</td>
                <td>
                    24</td>
                <td>
                    25</td>
                <td>
                    26</td>
                <td>
                    27</td>
            </tr>
        </table>
    </div>
    <br />
    <div>
    
        <table style="width: 100%;">
            <tr>
                <td>
                    Keterangan</td>
                <td>
                    28</td>
            </tr>
            </table>
    </div>
    </form>
</body>
</html>
