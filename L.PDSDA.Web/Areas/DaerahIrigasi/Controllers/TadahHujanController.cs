﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities.Web;
using L.Core.General;
using log4net;

namespace L.PDSDA.Web.Areas.DaerahIrigasi.Controllers
{
    public class TadahHujanController : BaseController
    {

        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;
        ILog log = LogManager.GetLogger(typeof(TadahHujanController));

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        IDefaultRepository<TADAH_HUJAN> DefaultRepository;

        public TadahHujanController()
        {
            DefaultRepository = new DefaultRepository<TADAH_HUJAN>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();

        }
        //
        // GET: /DaerahIrigasi/IrigasiTanam/

        public ActionResult Index()
        {
            string k_di = Utils.GetQueryString("id");
            return View("Index", k_di);
        }

        //
        // GET: /Bencana/TADAH_HUJAN/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                //return this.GenericViewDefault();
                return Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildPropinsi();
            BuildKabupaten(string.Empty);
            return PartialView("SaveV1");
        }

        //
        // POST: /Bencana/TADAH_HUJAN/Create

        [HttpPost]
        public ActionResult Create(TADAH_HUJAN domain)
        {
            try
            {
                if (domain.KABUPATEN == null)
                {
                    return Json(new { success = false, message = "Propinsi & Kabupaten " + Pesan.M1 });
                }
                else if (string.IsNullOrEmpty(domain.KABUPATEN.PROPINSI.K_PROPINSI))
                {
                    return Json(new { success = false, message = "Propinsi " + Pesan.M1 });
                }
                else if (string.IsNullOrEmpty(domain.KABUPATEN.K_KABUPATEN))
                {
                    return Json(new { success = false, message = "Kabupaten " + Pesan.M1 });
                }
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    domain.CreateBy = this.UserLoggedIn;
                    domain.CreateDate = DateTime.Now;
                    domain.CreateTerminal = this.UserHostAddress;
                    domain.UpdateBy = this.UserLoggedIn;
                    domain.UpdateDate = DateTime.Now;
                    domain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.Save(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/TADAH_HUJAN/Edit/5

        public ActionResult Edit(string id)
        {
            string[] arr = id.Split(',');
            if (!IsUpdateable)
            {
                //return this.GenericViewDefault();
                return Detail(id);
            }
            ViewBag.ActionName = "Edit";

            //TADAH_HUJAN domain = DefaultRepository.Get(x => x.K_KABUPATEN == arr[1] && x.K_PROPINSI == arr[0] && x.TAHUN_DATA == Convert.ToInt32(arr[2])).FirstOrDefault();
            TADAH_HUJAN domain = DefaultRepository.Get(x => (x.KABUPATEN != null && x.KABUPATEN.K_KABUPATEN == arr[1]) && (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.K_PROPINSI == arr[0]) && x.TAHUN_DATA == Convert.ToInt32(arr[2])).FirstOrDefault();
            BuildPropinsi();
            BuildKabupaten(domain!=null?domain.KABUPATEN == null ? "" : domain.KABUPATEN.PROPINSI == null ? "" : domain.KABUPATEN.PROPINSI.K_PROPINSI:"");
         
            return PartialView("SaveV1", domain);
        }


        public ActionResult Detail(string id)
        {
            string[] arr = id.Split(',');
          
            ViewBag.ActionName = "Detail";

            //TADAH_HUJAN domain = DefaultRepository.Get(x => x.K_KABUPATEN == arr[1] && x.K_PROPINSI == arr[0] && x.TAHUN_DATA == Convert.ToInt32(arr[2])).FirstOrDefault();
            TADAH_HUJAN domain = DefaultRepository.Get(x => (x.KABUPATEN != null && x.KABUPATEN.K_KABUPATEN == arr[1]) && (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.K_PROPINSI == arr[0]) && x.TAHUN_DATA == Convert.ToInt32(arr[2])).FirstOrDefault();
            BuildPropinsi();
            BuildKabupaten(domain!=null?domain.KABUPATEN == null ? "" : domain.KABUPATEN.PROPINSI == null ? "" : domain.KABUPATEN.PROPINSI.K_PROPINSI:"");
            return PartialView("Detail", domain);
        }

        //
        // POST: /Bencana/TADAH_HUJAN/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, TADAH_HUJAN domain)
        {
            if (domain.KABUPATEN == null)
            {
                return Json(new { success = false, message = "Propinsi & Kabupaten " + Pesan.M1 });
            }
            else if (string.IsNullOrEmpty(domain.KABUPATEN.PROPINSI.K_PROPINSI))
            {
                return Json(new { success = false, message = "Propinsi " + Pesan.M1 });
            }
            else if (string.IsNullOrEmpty(domain.KABUPATEN.K_KABUPATEN))
            {
                return Json(new { success = false, message = "Kabupaten " + Pesan.M1 });
            }
            string[] arr = id.Split(',');

            TADAH_HUJAN saveDomain = DefaultRepository.Get(x => (x.KABUPATEN != null && x.KABUPATEN.K_KABUPATEN == arr[1]) && (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.K_PROPINSI == arr[0]) && x.TAHUN_DATA == Convert.ToInt32(arr[2])).FirstOrDefault();
            saveDomain.KABUPATEN = domain.KABUPATEN;
            //TADAH_HUJAN saveDomain = DefaultRepository.Get(x => x.K_KABUPATEN == arr[1] && x.K_PROPINSI == arr[0] && x.TAHUN_DATA == Convert.ToInt32(arr[2])).FirstOrDefault();
            Utils.SetTProperty<TADAH_HUJAN>(saveDomain, domain);

            try
            {
                if (ModelState.IsValid)
                {
                    log.InfoFormat("TadahHujanController Update {0} by {1} at {2}", domain.TAHUN_DATA, this.UserLoggedIn, this.UserHostAddress);
                    saveDomain.UpdateBy = this.UserLoggedIn;
                    saveDomain.UpdateDate = DateTime.Now;
                    saveDomain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.Update(saveDomain);
                    log.Debug(this.DefaultRepository.Update(saveDomain));
                    return Json(new { success = true, message = Pesan.A1B });

                }
                return Json(new { success = true, message = Pesan.A2B });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/TADAH_HUJAN/Delete/5

        public ActionResult Delete(string id)
        {
            string[] arr = id.Split(',');

            if (!IsDeleteable)
            {
                //return this.GenericViewDefault();
                return Json(new { success = false, message = Pesan.A2C + ". Harap reload halaman." });
            }
            TADAH_HUJAN deleteDomain = DefaultRepository.Get(x => (x.KABUPATEN != null && x.KABUPATEN.K_KABUPATEN == arr[1]) && (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.K_PROPINSI == arr[0]) && x.TAHUN_DATA == Convert.ToInt32(arr[2])).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<TADAH_HUJAN> collTADAH_HUJAN = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collTADAH_HUJAN = collTADAH_HUJAN.Where(x => (x.TAHUN_DATA.ToString().Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch))));
            }
            Count = collTADAH_HUJAN.Count();

            var sortColumnIndex = iSortCol;
            Func<TADAH_HUJAN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.TAHUN_DATA.ToString() :
                                                                sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                c.TAHUN_DATA.ToString());


            IList<TADAH_HUJAN> collResult = new List<TADAH_HUJAN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collTADAH_HUJAN.Count() > 0)
            {
                collResult = collTADAH_HUJAN.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.KABUPATEN.PROPINSI.K_PROPINSI + "," + c.KABUPATEN.K_KABUPATEN + "," + c.TAHUN_DATA,
                             c.TAHUN_DATA,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
