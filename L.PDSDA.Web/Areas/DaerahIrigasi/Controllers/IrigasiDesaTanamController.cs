﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.Core.General;

namespace L.PDSDA.Web.Areas.DaerahIrigasi.Controllers
{
    public class IrigasiDesaTanamController : BaseController
    {
        IDefaultRepository<PID> PIDRepository;

        IDefaultRepository<PID_DYN> DefaultRepository;

        public IrigasiDesaTanamController()
        {
            DefaultRepository = new DefaultRepository<PID_DYN>();
            PIDRepository = new DefaultRepository<PID>();
        }
        //
        // GET: /DaerahIrigasi/IrigasiTanam/

        public ActionResult Index()
        {
            string k_di = Utils.GetQueryString("id");
            PID PID = this.PIDRepository.Get(x => x.K_PID == k_di).FirstOrDefault();
            if (PID != null)
            {
                if (PID.W != null && !collAssignWS.Contains(PID.W))
                {
                    ViewBag.IsViewable = false;
                    ViewBag.IsCreateable = false;
                    ViewBag.IsUpdateable = false;
                    ViewBag.IsDeleteable = false;
                    ViewBag.Method = Utils.GetPostRequest("method");
                    return PartialView("Index", k_di);
                }
            }
            ViewBag.Method = Utils.GetQueryString("method");
            return PartialView("Index", k_di);
        }

        //
        // GET: /Bencana/PID_DYN/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                //return this.GenericViewDefault();
                return Detail("");
            }
            ViewBag.ActionName = "Create";
            return PartialView("Save");
        }

        //
        // POST: /Bencana/PID_DYN/Create

        [HttpPost]
        public ActionResult Create(PID_DYN domain)
        {
            string k_di = Utils.GetQueryString("K_PID");
            PID PID = this.PIDRepository.Get(x => x.K_PID == k_di).FirstOrDefault();
            domain.PID = PID;
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Save(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/PID_DYN/Edit/5

        public ActionResult Edit(string id)
        {
            string[] arr = id.Split(',');

            if (!IsUpdateable)
            {
                //return this.GenericViewDefault();
                return Detail(id);
            }
            ViewBag.ActionName = "Edit";

            PID_DYN domain = DefaultRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[1]) && (x.PID != null && x.PID.K_PID == arr[0])).FirstOrDefault();
            return PartialView("Save", domain);
        }

        public ActionResult Detail(string id)
        {
            string[] arr = id.Split(',');


            ViewBag.ActionName = "Detail";

            PID_DYN domain = string.IsNullOrWhiteSpace(id) ? null : DefaultRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[1]) && (x.PID != null && x.PID.K_PID == arr[0])).FirstOrDefault();
            return PartialView("Detail", domain);
        }

        //
        // POST: /Bencana/PID_DYN/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, PID_DYN domain)
        {
            string[] arr = id.Split(',');

            PID_DYN saveDomain = DefaultRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[1]) && (x.PID != null && x.PID.K_PID == arr[0])).FirstOrDefault();
            Utils.SetTProperty<PID_DYN>(saveDomain, domain);

            try
            {
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Update(saveDomain);
                    return Json(new { success = true, message = "Data telah diubah." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/PID_DYN/Delete/5

        public ActionResult Delete(string id)
        {
            string[] arr = id.Split(',');

            if (!IsDeleteable)
            {
                //return this.GenericViewDefault();
                return Json(new { success = false, message = Pesan.A2C + ". Harap reload halaman." });
            }
            PID_DYN deleteDomain = DefaultRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[1]) && (x.PID != null && x.PID.K_PID == arr[0])).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (deleteDomain.PID != null && deleteDomain.PID.W != null && !collAssignWS.Contains(deleteDomain.PID.W))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<PID_DYN> collPID_DYN = this.DefaultRepository.Get(x => x.PID.K_PID == Utils.GetQueryString("id"));

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collPID_DYN = collPID_DYN.Where(x => (x.PID != null && x.PID.K_PID.Contains(param.sSearch)) || x.TAHUN_DATA.ToString().Contains(param.sSearch)
                    || (x.PID != null && x.PID.N_PID.Contains(param.sSearch)));
            }
            Count = collPID_DYN.Count();

            var sortColumnIndex = iSortCol;
            Func<PID_DYN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.PID != null ? c.PID.K_PID : string.Empty :
                                                                 sortColumnIndex == 2 ? c.PID != null ? c.PID.N_PID : string.Empty :
                                                                sortColumnIndex == 3 ? c.TAHUN_DATA < 1900 ? "" : c.TAHUN_DATA.ToString() :
                                                                c.TAHUN_DATA.ToString());


            IList<PID_DYN> collResult = new List<PID_DYN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collPID_DYN.Count() > 0)
            {
                collResult = collPID_DYN.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.PID == null ? "" : c.PID.K_PID + "," + c.TAHUN_DATA,
                             K_PID = c.PID == null ? "" : c.PID.K_PID,
                             N_PID = c.PID == null ? string.Empty : c.PID.N_PID,
                             c.TAHUN_DATA
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
