﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities.Web;
using L.Core.Utilities;
using L.PDSDA.Web.Controllers;
using L.Core.General;

namespace L.PDSDA.Web.Areas.DaerahIrigasi.Controllers
{
    public class IrigasiLintasController : BaseController
    {
        private readonly IDefaultRepository<IRIGASI_LINTAS> DefaultRepository;
        private readonly IDefaultRepository<STATUS_IRIGASI> STATUS_IRIGASIRepository;

        /// <summary>
        /// 
        /// </summary>
        public IrigasiLintasController()
        {
            this.DefaultRepository = new DefaultRepository<IRIGASI_LINTAS>();
            STATUS_IRIGASIRepository = new DefaultRepository<STATUS_IRIGASI>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult NotPermit()
        {
            return GenericView("~/Views/Shared/NotPermit");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            string key = Utils.Encrypt(this.CodeMenu + "|" + DateTime.Now.GetHashCode());
            ViewBag.Key = key;
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {
            string urlRedirect = string.Empty;
            if (!IsPermitCUD(out urlRedirect))
            {
                //return GenericView(urlRedirect);
                return Detail("");
            }

            ViewBag.ActionName = "Create";

            IQueryable<STATUS_IRIGASI> collStatusIrigasi = this.STATUS_IRIGASIRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collStatusIrigasi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_STATUS, Value = item.K_STATUS });
            }
            ViewBag.collSTATUS_IRIGASI = collSelectListItem;

            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(IRIGASI_LINTAS domain)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    //domain.STATUS_IRIGASI.K_STATUS = domain.K_STATUS;
                    if (domain.STATUS_IRIGASI != null && domain.STATUS_IRIGASI.K_STATUS == "-1")
                    {
                        domain.STATUS_IRIGASI = null;
                    }
                    this.DefaultRepository.Save(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });
                }
                return Json(new { success = false, message = "Already exists data." });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            string urlRedirect = string.Empty;
            if (!IsPermitCUD(out urlRedirect))
            {
                //return GenericView(urlRedirect);
                return Detail(id);
            }

            ViewBag.ActionName = "Edit";
            IQueryable<STATUS_IRIGASI> collStatusIrigasi = this.STATUS_IRIGASIRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collStatusIrigasi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_STATUS, Value = item.K_STATUS });
            }
            ViewBag.collSTATUS_IRIGASI = collSelectListItem;

            IRIGASI_LINTAS IRIGASI_LINTAS = DefaultRepository.Get(x => x.K_DI_LINTAS == id).FirstOrDefault();
            return PartialView("Save", IRIGASI_LINTAS);

            //return PartialView(ws);
        }

        public ActionResult Detail(string id)
        {
            string urlRedirect = string.Empty;


            ViewBag.ActionName = "Detail";
            IQueryable<STATUS_IRIGASI> collStatusIrigasi = this.STATUS_IRIGASIRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collStatusIrigasi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_STATUS, Value = item.K_STATUS });
            }
            ViewBag.collSTATUS_IRIGASI = collSelectListItem;

            IRIGASI_LINTAS IRIGASI_LINTAS = DefaultRepository.Get(x => x.K_DI_LINTAS == id).FirstOrDefault();
            return PartialView("Detail", IRIGASI_LINTAS);

            //return PartialView(ws);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, IRIGASI_LINTAS domain)
        {
            IRIGASI_LINTAS IRIGASI_LINTAS = DefaultRepository.Get(x => x.K_DI_LINTAS == id).FirstOrDefault();
            if (domain.STATUS_IRIGASI != null && domain.STATUS_IRIGASI.K_STATUS == "-1")
            {
                domain.STATUS_IRIGASI = null;
            }
            IRIGASI_LINTAS.STATUS_IRIGASI = domain.STATUS_IRIGASI;
            Utils.SetTProperty<IRIGASI_LINTAS>(IRIGASI_LINTAS, domain);

            try
            {
                // TODO: Add update logic here
                this.DefaultRepository.Update(IRIGASI_LINTAS);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = "Data telah diubah" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            if (IsDeleteable)
            {
                string urlRedirect = string.Empty;
                if (!IsPermitCUD(out urlRedirect))
                {
                    //return GenericView(urlRedirect);
                    return Json(new { success = false, message = Pesan.A2C + ". Harap reload halaman." });
                }

                IRIGASI_LINTAS IRIGASI_LINTAS = this.DefaultRepository.Get(x => x.K_DI_LINTAS == id).FirstOrDefault();
                if (IRIGASI_LINTAS != null)
                {
                    this.DefaultRepository.Delete(IRIGASI_LINTAS);
                    return Json(new { success = true, message = "Data telah dihapus" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, message = "Data tidak ditemukan" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return GenericView("~/Views/Shared/NotPermit");
            }

        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<IRIGASI_LINTAS> collIRIGASI_LINTAS = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collIRIGASI_LINTAS = collIRIGASI_LINTAS.Where(x => x.K_DI_LINTAS.Contains(param.sSearch) || x.N_DI_LINTAS.Contains(param.sSearch) || (x.STATUS_IRIGASI != null && x.STATUS_IRIGASI.N_STATUS.Contains(param.sSearch)));
            }
            Count = collIRIGASI_LINTAS.Count();

            var sortColumnIndex = iSortCol;
            Func<IRIGASI_LINTAS, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_DI_LINTAS :
                                                                sortColumnIndex == 2 ? c.N_DI_LINTAS :
                                                                sortColumnIndex == 3 ? c.STATUS_IRIGASI != null ? c.STATUS_IRIGASI.N_STATUS : string.Empty : c.K_DI_LINTAS);


            IList<IRIGASI_LINTAS> collResult = new List<IRIGASI_LINTAS>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collIRIGASI_LINTAS.Count() > 0)
            {
                collResult = collIRIGASI_LINTAS.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_DI_LINTAS,
                             c.K_DI_LINTAS,
                             c.N_DI_LINTAS,
                             N_STATUS = c.STATUS_IRIGASI == null ? "" : c.STATUS_IRIGASI.N_STATUS
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddIrigasiPemerintah()
        {
            return PartialView("AddIrigasiPemerintah", Utils.GetQueryString("IrigasiLintasID"));
        }

        public ActionResult DeleteIrigasiPemerintah()
        {
            return PartialView("SaveIrigasiPemerintah");
        }

        public ActionResult LoadIrigasiPemerintah()
        {
            return View();
        }
    }
}
