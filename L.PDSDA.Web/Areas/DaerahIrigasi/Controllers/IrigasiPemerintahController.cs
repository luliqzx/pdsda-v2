﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.Core.General;
using NHibernate.Criterion;

namespace L.PDSDA.Web.Areas.DaerahIrigasi.Controllers
{
    public class IrigasiPemerintahController : BaseController
    {
        //
        // GET: /Bencana/IRIGASI/
        IDefaultRepository<IRIGASI> DefaultRepository;
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;
        private readonly IDefaultRepository<KEWENANGAN> KEWENANGANRepository;
        private readonly IDefaultRepository<IRIGASI_LINTAS> IRIGASI_LINTASRepository;

        public IrigasiPemerintahController()
        {
            this.DefaultRepository = new DefaultRepository<IRIGASI>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            KABUPATENRepository = new DefaultRepository<KABUPATEN>();
            KEWENANGANRepository = new DefaultRepository<KEWENANGAN>();
            IRIGASI_LINTASRepository = new DefaultRepository<IRIGASI_LINTAS>();
        }

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        void BuildPropinsi()
        {
            //IQueryable<PROPINSI> collPropinsi = this.PROPINSIRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        void BuildKewenangan()
        {
            IQueryable<KEWENANGAN> collKEWENANGAN = this.KEWENANGANRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collKEWENANGAN)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_KEWENANGAN, Value = item.K_KEWENANGAN });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collKEWENANGAN = collSelectListItem;
        }

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Bencana/IRIGASI/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Bencana/IRIGASI/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                //return this.GenericViewDefault();
                return Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildDAS(string.Empty);
            BuildWS();
            BuildKabupaten(string.Empty);
            BuildPropinsi();
            BuildKewenangan();
            return PartialView("Save");
        }

        //
        // POST: /Bencana/IRIGASI/Create

        [HttpPost]
        public ActionResult Create(IRIGASI domain)
        {
            try
            {
                // TODO: Add insert logic here
                if (domain.KABUPATEN != null && domain.KABUPATEN.K_KABUPATEN == "-1")
                {
                    domain.KABUPATEN = null;
                }
                if (domain.KABUPATEN != null && domain.KABUPATEN.PROPINSI != null && domain.KABUPATEN.PROPINSI.K_PROPINSI == "-1")
                {
                    domain.KABUPATEN.PROPINSI = null;
                }
                if (domain.WS != null && domain.WS.K_WS == "-1")
                {
                    domain.WS = null;
                }
                if (domain.DAS != null && domain.DAS.K_DAS == "-1")
                {
                    domain.DAS = null;
                }
                if (domain.KEWENANGAN != null && domain.KEWENANGAN.K_KEWENANGAN == "-1")
                {
                    domain.KEWENANGAN = null;
                }

                if (ModelState.IsValid)
                {

                    domain.CreateBy = this.UserLoggedIn;
                    domain.CreateDate = DateTime.Now;
                    domain.CreateTerminal = this.UserHostAddress;
                    domain.UpdateBy = this.UserLoggedIn;
                    domain.UpdateDate = DateTime.Now;
                    domain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.Save(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/IRIGASI/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                //return this.GenericViewDefault();
                return Detail(id);
            }
            ViewBag.ActionName = "Edit";

            IRIGASI domain = DefaultRepository.Get(x => x.K_DI == id).FirstOrDefault();
            BuildDAS(domain != null ? domain.WS == null ? string.Empty : domain.WS.K_WS : "");
            BuildWS();
            BuildKabupaten(domain != null ? domain.KABUPATEN == null ? string.Empty : domain.KABUPATEN.PROPINSI == null ? string.Empty : domain.KABUPATEN.PROPINSI.K_PROPINSI : "");
            BuildPropinsi();
            BuildKewenangan();

            if (domain.WS != null && !collAssignWS.Contains(domain.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = domain.WS.N_WS, Value = domain.WS.K_WS, Selected = true });
                //ViewBag.collWS = addWStoSelectedListItem;
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", domain);
                //return Detail(id);
            }
            return PartialView("Save", domain);
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";

            IRIGASI domain = DefaultRepository.Get(x => x.K_DI == id).FirstOrDefault();
            BuildDAS(domain != null ? domain.WS == null ? string.Empty : domain.WS.K_WS : "");
            BuildWS();
            BuildPropinsi();
            BuildKabupaten(domain != null ? domain.KABUPATEN == null ? string.Empty : domain.KABUPATEN.PROPINSI == null ? string.Empty : domain.KABUPATEN.PROPINSI.K_PROPINSI : "");
            BuildKewenangan();
            return PartialView("Detail", domain);
        }

        //
        // POST: /Bencana/IRIGASI/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, IRIGASI domain)
        {

            if (domain.KABUPATEN != null && domain.KABUPATEN.K_KABUPATEN == "-1")
            {
                domain.KABUPATEN = null;
            }
            if (domain.KABUPATEN != null && domain.KABUPATEN.PROPINSI != null && domain.KABUPATEN.PROPINSI.K_PROPINSI == "-1")
            {
                domain.KABUPATEN.PROPINSI = null;
            }
            if (domain.WS != null && domain.WS.K_WS == "-1")
            {
                domain.WS = null;
            }
            if (domain.DAS != null && domain.DAS.K_DAS == "-1")
            {
                domain.DAS = null;
            }
            if (domain.KEWENANGAN != null && domain.KEWENANGAN.K_KEWENANGAN == "-1")
            {
                domain.KEWENANGAN = null;
            }

            IRIGASI saveDomain = DefaultRepository.Get(x => x.K_DI == id).FirstOrDefault();
            saveDomain.DAS = domain.DAS;
            saveDomain.WS = domain.WS;
            saveDomain.KABUPATEN = domain.KABUPATEN;
            saveDomain.KEWENANGAN = domain.KEWENANGAN;
            Utils.SetTProperty<IRIGASI>(saveDomain, domain);

            try
            {
                if (ModelState.IsValid)
                {
                    saveDomain.UpdateBy = this.UserLoggedIn;
                    saveDomain.UpdateDate = DateTime.Now;
                    saveDomain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.Update(saveDomain);
                    return Json(new { success = true, message = "Data telah diubah." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/IRIGASI/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                //return this.GenericViewDefault();
                return Json(new { success = false, message = Pesan.A2C + ". Harap reload halaman." });
            }
            IRIGASI deleteDomain = DefaultRepository.Get(x => x.K_DI == id).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (deleteDomain.WS != null && !collAssignWS.Contains(deleteDomain.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    if (deleteDomain.IRIGASI_LINTAS != null && deleteDomain.IRIGASI_LINTAS.IRIGASIs != null)
                    {
                        deleteDomain.IRIGASI_LINTAS.IRIGASIs.Clear();
                    }
                    this.DefaultRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<IRIGASI> collIRIGASI = this.DefaultRepository.GetAll();

            //if (!string.IsNullOrWhiteSpace(param.sSearch))
            //{
            //    collIRIGASI = collIRIGASI.Where(x => x.K_DI.Contains(param.sSearch) || x.N_DI.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch))
            //         || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)) || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
            //          || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)) || (x.KEWENANGAN != null && x.KEWENANGAN.N_KEWENANGAN.Contains(param.sSearch)));
            //}

            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_0")))
            {
                collIRIGASI = collIRIGASI.Where(x => x.K_DI.Contains(Utils.GetPostRequest("sSearch_0")));
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_1")))
            {
                collIRIGASI = collIRIGASI.Where(x => x.N_DI.Contains(Utils.GetPostRequest("sSearch_1")));
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_2")))
            {
                collIRIGASI = collIRIGASI.Where(x => x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(Utils.GetPostRequest("sSearch_2")));
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_3")))
            {
                collIRIGASI = collIRIGASI.Where(x => x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(Utils.GetPostRequest("sSearch_3")));
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_4")))
            {
                collIRIGASI = collIRIGASI.Where(x => x.WS != null && x.WS.N_WS.Contains(Utils.GetPostRequest("sSearch_4")));
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_5")))
            {
                collIRIGASI = collIRIGASI.Where(x => x.DAS != null && x.DAS.N_DAS.Contains(Utils.GetPostRequest("sSearch_5")));
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_6")))
            {
                collIRIGASI = collIRIGASI.Where(x => x.KEWENANGAN != null && x.KEWENANGAN.N_KEWENANGAN.Contains(Utils.GetPostRequest("sSearch_6")));
            }
            
            Count = collIRIGASI.Count();
            
            var sortColumnIndex = iSortCol;
            Func<IRIGASI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_DI :
                                                                sortColumnIndex == 2 ? c.N_DI :
                                                                 sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                sortColumnIndex == 7 ? c.KEWENANGAN == null ? string.Empty : c.KEWENANGAN.N_KEWENANGAN :
                                                                c.K_DI);
            if (collIRIGASI.Count() > 0)
            {
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collIRIGASI = collIRIGASI.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).AsQueryable();
                else
                    collIRIGASI = collIRIGASI.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).AsQueryable();
            }

            var result = from c in collIRIGASI
                         select new
                         {
                             KEY = c.K_DI,
                             c.K_DI,
                             c.N_DI,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             N_KEWENANGAN = c.KEWENANGAN == null ? string.Empty : c.KEWENANGAN.N_KEWENANGAN
                         };
            int iTotalDisplayRecords = param.iDisplayLength;// result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadAllData2(jQueryDataTableParamModel param)
        {
            int Count = 0;
            var vIRIGASI = this.DefaultRepository.GetAllWithCriteria();
            var iCountIRIGASI = this.DefaultRepository.GetAllWithCriteria();

            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_0")))
            {
                vIRIGASI.Add(Restrictions.Like("K_DI",Utils.GetPostRequest("sSearch_0"), MatchMode.Anywhere));
                iCountIRIGASI.Add(Restrictions.Like("K_DI", Utils.GetPostRequest("sSearch_0"), MatchMode.Anywhere));
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_1")))
            {
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_2")))
            {
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_3")))
            {
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_4")))
            {
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_5")))
            {
            }
            if (!string.IsNullOrWhiteSpace(Utils.GetPostRequest("sSearch_6")))
            {
            }

            Count = iCountIRIGASI.SetProjection(Projections.RowCount()).FutureValue<Int32>().Value;

            IList<IRIGASI> collIRIGASI = vIRIGASI.SetFirstResult(param.iDisplayStart).SetMaxResults(param.iDisplayLength).List<IRIGASI>();
            var result = from c in collIRIGASI
                         select new
                         {
                             KEY = c.K_DI,
                             c.K_DI,
                             c.N_DI,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             N_KEWENANGAN = c.KEWENANGAN == null ? string.Empty : c.KEWENANGAN.N_KEWENANGAN
                         };
            int iTotalDisplayRecords = param.iDisplayLength;// result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadAllDataByIrigasiLintas(string IrigasiLintasID, jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<IRIGASI> collIRIGASI = this.DefaultRepository.Get(x => x.IRIGASI_LINTAS != null && x.IRIGASI_LINTAS.K_DI_LINTAS == IrigasiLintasID);

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collIRIGASI = collIRIGASI.Where(x => x.K_DI.Contains(param.sSearch) || x.N_DI.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch))
                     || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)) || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
                      || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)) || (x.KEWENANGAN != null && x.KEWENANGAN.N_KEWENANGAN.Contains(param.sSearch)));
            }
            Count = collIRIGASI.Count();

            var sortColumnIndex = iSortCol;
            Func<IRIGASI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_DI :
                                                                sortColumnIndex == 2 ? c.N_DI :
                                                                 sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                sortColumnIndex == 7 ? c.KEWENANGAN == null ? string.Empty : c.KEWENANGAN.N_KEWENANGAN :
                                                                c.K_DI);


            IList<IRIGASI> collResult = new List<IRIGASI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collIRIGASI.Count() > 0)
            {
                collResult = collIRIGASI.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_DI,
                             c.K_DI,
                             c.N_DI,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             N_KEWENANGAN = c.KEWENANGAN == null ? string.Empty : c.KEWENANGAN.N_KEWENANGAN
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadAllDataNotInIrigasiLintas(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<IRIGASI> collIRIGASI = this.DefaultRepository.Get(x => x.IRIGASI_LINTAS == null);

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collIRIGASI = collIRIGASI.Where(x => x.K_DI.Contains(param.sSearch) || x.N_DI.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch))
                     || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)) || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
                      || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)) || (x.KEWENANGAN != null && x.KEWENANGAN.N_KEWENANGAN.Contains(param.sSearch)));
            }
            Count = collIRIGASI.Count();

            var sortColumnIndex = iSortCol;
            Func<IRIGASI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_DI :
                                                                sortColumnIndex == 2 ? c.N_DI :
                                                                 sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                sortColumnIndex == 7 ? c.KEWENANGAN == null ? string.Empty : c.KEWENANGAN.N_KEWENANGAN :
                                                                c.K_DI);


            IList<IRIGASI> collResult = new List<IRIGASI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collIRIGASI.Count() > 0)
            {
                collResult = collIRIGASI.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_DI,
                             c.K_DI,
                             c.N_DI,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             N_KEWENANGAN = c.KEWENANGAN == null ? string.Empty : c.KEWENANGAN.N_KEWENANGAN
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateIrigasiLintas(string IrigasiLintasID, string IrigasiPemerintahID)
        {
            IRIGASI_LINTAS ili = IRIGASI_LINTASRepository.Get(x => x.K_DI_LINTAS == IrigasiLintasID).FirstOrDefault();
            IRIGASI iri = this.DefaultRepository.Get(x => x.K_DI == IrigasiPemerintahID).FirstOrDefault();

            iri.IRIGASI_LINTAS = ili;
            this.DefaultRepository.Update(iri);
            if (iri.IRIGASI_LINTAS == ili)
            {
                return Json("Data sudah disimpan.", JsonRequestBehavior.AllowGet);
            }
            return Json("Data belum tersimpan.", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteIrigasiLintas(string IrigasiLintasID, string IrigasiPemerintahID)
        {
            IRIGASI iri = this.DefaultRepository.Get(x => x.K_DI == IrigasiPemerintahID && (x.IRIGASI_LINTAS != null && x.IRIGASI_LINTAS.K_DI_LINTAS == IrigasiLintasID)).FirstOrDefault();
            if (iri != null)
            {
                if (iri.IRIGASI_LINTAS != null)
                {
                    iri.IRIGASI_LINTAS = null;
                    this.DefaultRepository.Update(iri);
                }

                return Json(new { success = true, message = "Data sudah dihapus." }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, message = "Data belum tersimpan." }, JsonRequestBehavior.AllowGet);
        }
    }
}
