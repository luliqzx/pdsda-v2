﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities.Web;
using L.Core.Utilities;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.Web.Controllers;
using L.Core.General;

namespace L.PDSDA.Web.Areas.DaerahIrigasi.Controllers
{
    public class IrigasiDynController : PopUpBaseController
    {
        IDefaultRepository<IRIGASI> IrigasiRepository;

        IDefaultRepository<IRIGASI_DYN> DefaultRepository;

        void BuildTingkatan()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            {
                collSelectListItem.Add(new SelectListItem { Text = "Teknis", Value = "T" });
                collSelectListItem.Add(new SelectListItem { Text = "Semi Teknis", Value = "ST" });
                collSelectListItem.Add(new SelectListItem { Text = "Sederhana", Value = "SD" });
            }
            ViewBag.collTingkatan = collSelectListItem;
        }

        public IrigasiDynController()
        {
            DefaultRepository = new DefaultRepository<IRIGASI_DYN>();
            IrigasiRepository = new DefaultRepository<IRIGASI>();
        }
        //
        // GET: /DaerahIrigasi/IrigasiDyn/

        public ActionResult Index()
        {
            string k_di = Utils.GetQueryString("id");
            IRIGASI IRIGASI = this.IrigasiRepository.Get(x => x.K_DI == k_di).FirstOrDefault();
            if (IRIGASI != null)
            {
                if (IRIGASI.WS != null && !collAssignWS.Contains(IRIGASI.WS))
                {
                    ViewBag.IsViewable = false;
                    ViewBag.IsCreateable = false;
                    ViewBag.IsUpdateable = false;
                    ViewBag.IsDeleteable = false;
                    ViewBag.Method = Utils.GetPostRequest("method");
                    return PartialView("Index", k_di);
                }
            }
            ViewBag.Method = Utils.GetQueryString("method");
            ViewBag.IsViewable = Utils.TryParseToBoolean(Utils.GetQueryString("IsViewable"));
            ViewBag.IsCreateable = Utils.TryParseToBoolean(Utils.GetQueryString("IsCreateable"));
            ViewBag.IsUpdateable = Utils.TryParseToBoolean(Utils.GetQueryString("IsUpdateable"));
            ViewBag.IsDeleteable = Utils.TryParseToBoolean(Utils.GetQueryString("IsDeleteable"));
            return PartialView("Index", k_di);
        }

        //
        // GET: /Bencana/IRIGASI_DYN/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                //return this.GenericViewDefault();
                return Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildTingkatan();
            return PartialView("SaveV2");
        }

        //
        // POST: /Bencana/IRIGASI_DYN/Create

        [HttpPost]
        public ActionResult Create(IRIGASI_DYN domain)
        {
            string k_di = Utils.GetQueryString("K_DI");
            IRIGASI IRIGASI = this.IrigasiRepository.Get(x => x.K_DI == k_di).FirstOrDefault();
            domain.IRIGASI = IRIGASI;
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Save(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/IRIGASI_DYN/Edit/5

        public ActionResult Edit(string id)
        {
            IsUpdateable = Utils.TryParseToBoolean(Utils.GetQueryString("IsUpdateable"));
            string[] arr = id.Split(',');

            if (!IsUpdateable)
            {
                //return this.GenericViewDefault();
                return Detail(id);
            }
            ViewBag.ActionName = "Edit";

            IRIGASI_DYN domain = DefaultRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[1]) && (x.IRIGASI != null && x.IRIGASI.K_DI == arr[0])).FirstOrDefault();
            BuildTingkatan();

            return PartialView("SaveV2", domain);
        }
        public ActionResult Detail(string id)
        {
            string[] arr = id.Split(',');

            ViewBag.ActionName = "Detail";

            IRIGASI_DYN domain = string.IsNullOrWhiteSpace(id) ? null : DefaultRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[1]) && (x.IRIGASI != null && x.IRIGASI.K_DI == arr[0])).FirstOrDefault();
            BuildTingkatan();

            return PartialView("Detail", domain);
        }

        //
        // POST: /Bencana/IRIGASI_DYN/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, IRIGASI_DYN domain)
        {
            string[] arr = id.Split(',');

            IRIGASI_DYN saveDomain = DefaultRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[1]) && (x.IRIGASI != null && x.IRIGASI.K_DI == arr[0])).FirstOrDefault();
            Utils.SetTProperty<IRIGASI_DYN>(saveDomain, domain);

            try
            {
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Update(saveDomain);
                    return Json(new { success = true, message = "Data telah diubah." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/IRIGASI_DYN/Delete/5

        public ActionResult Delete(string id)
        {
            IsDeleteable = Utils.TryParseToBoolean(Utils.GetQueryString("IsDeleteable"));

            string[] arr = id.Split(',');

            if (!IsDeleteable)
            {
                //return this.GenericViewDefault();
                return Json(new { success = false, message = Pesan.A2C + ". Harap reload halaman." });
            }
            IRIGASI_DYN deleteDomain = DefaultRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[1]) && (x.IRIGASI != null && x.IRIGASI.K_DI == arr[0])).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (deleteDomain.IRIGASI != null && deleteDomain.IRIGASI.WS != null && !collAssignWS.Contains(deleteDomain.IRIGASI.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<IRIGASI_DYN> collIRIGASI_DYN = this.DefaultRepository.Get(x => x.IRIGASI.K_DI == Utils.GetPostRequest("id"));

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collIRIGASI_DYN = collIRIGASI_DYN.Where(x => (x.IRIGASI != null && x.IRIGASI.K_DI.Contains(param.sSearch)) || x.TAHUN_DATA.ToString().Contains(param.sSearch)
                    || (x.IRIGASI != null && x.IRIGASI.N_DI.Contains(param.sSearch))
                     || (x.ANGGOTA != null && x.ANGGOTA.ToString().Contains(param.sSearch)) || (x.BADAN_HUKUM != null && x.BADAN_HUKUM.ToString().Contains(param.sSearch)));
            }
            Count = collIRIGASI_DYN.Count();

            var sortColumnIndex = iSortCol;
            Func<IRIGASI_DYN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.IRIGASI != null ? c.IRIGASI.K_DI : string.Empty :
                                                                 sortColumnIndex == 2 ? c.IRIGASI != null ? c.IRIGASI.N_DI : string.Empty :
                                                                sortColumnIndex == 3 ? c.TAHUN_DATA < 1900 ? "" : c.TAHUN_DATA.ToString() :
                                                                sortColumnIndex == 4 ? c.ANGGOTA == null ? "" : c.ANGGOTA.ToString() :
                                                                sortColumnIndex == 5 ? c.BADAN_HUKUM == null ? "" : c.BADAN_HUKUM.ToString() :
                                                                c.TAHUN_DATA.ToString());


            IList<IRIGASI_DYN> collResult = new List<IRIGASI_DYN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collIRIGASI_DYN.Count() > 0)
            {
                collResult = collIRIGASI_DYN.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.IRIGASI == null ? "" : c.IRIGASI.K_DI + "," + c.TAHUN_DATA,
                             K_DI = c.IRIGASI == null ? "" : c.IRIGASI.K_DI,
                             N_DI = c.IRIGASI == null ? string.Empty : c.IRIGASI.N_DI,
                             c.TAHUN_DATA,
                             c.ANGGOTA,
                             c.BADAN_HUKUM
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
