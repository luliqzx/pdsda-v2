﻿using System.Web.Mvc;

namespace L.PDSDA.Web.Areas.DaerahIrigasi
{
    public class DaerahIrigasiAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DaerahIrigasi";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DaerahIrigasi_default",
                "DaerahIrigasi/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
