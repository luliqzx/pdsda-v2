﻿using System.Web.Mvc;

namespace L.PDSDA.Web.Areas.Hidrologi
{
    public class HidrologiAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Hidrologi";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Hidrologi_default",
                "Hidrologi/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
