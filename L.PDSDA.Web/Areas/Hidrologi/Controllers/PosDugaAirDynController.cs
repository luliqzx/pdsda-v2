﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Globalization;
using L.Core.General;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.PDSDA.Web.DTOs;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Repositories.Base;

namespace L.PDSDA.Web.Areas.Hidrologi.Controllers
{
    public class PosDugaAirDynController : BaseController
    {
        IDefaultRepository<STASIUNDEBIT> StasiunDebitRepository;

        IDefaultRepository<STASIUNDEBIT_DYN> DefaultRepository;

        public PosDugaAirDynController()
        {
            DefaultRepository = new DefaultRepository<STASIUNDEBIT_DYN>();
            StasiunDebitRepository = new DefaultRepository<STASIUNDEBIT>();
        }
        //
        // GET: /DaerahIrigasi/IrigasiDyn/

        public ActionResult Index()
        {
            string K_STASIUN = Utils.GetQueryString("id");
            STASIUNDEBIT STASIUNDEBIT = this.StasiunDebitRepository.Get(x => x.K_STASIUN == K_STASIUN).FirstOrDefault();
            if (STASIUNDEBIT != null)
            {
                if (STASIUNDEBIT.W != null && !collAssignWS.Contains(STASIUNDEBIT.W))
                {
                    ViewBag.IsViewable = false;
                    ViewBag.IsCreateable = false;
                    ViewBag.IsUpdateable = false;
                    ViewBag.IsDeleteable = false;
                    ViewBag.Method = Utils.GetPostRequest("method");
                    return PartialView("Index", K_STASIUN);
                }
            }
            return PartialView("Index", K_STASIUN);
        }

        //
        // GET: /Bencana/STASIUNDEBIT_DYN/Create

        void GetCodeAndYear()
        {
            string K_STASIUN = Utils.GetQueryString("K_STASIUN");
            ViewBag.K_STASIUN = K_STASIUN;
            IQueryable<STASIUNDEBIT_DYN> qrySTASIUNDEBIT_DYN = this.DefaultRepository.Get(x => x.STASIUNDEBIT != null && x.STASIUNDEBIT.K_STASIUN == K_STASIUN);
            IList<STASIUNDEBIT_DYN> collSTASIUNDEBIT_DYN = null;
            string sTahun = "";
            if (qrySTASIUNDEBIT_DYN != null)
            {
                collSTASIUNDEBIT_DYN = qrySTASIUNDEBIT_DYN.ToList();
            }
            if (collSTASIUNDEBIT_DYN != null && collSTASIUNDEBIT_DYN.Count > 0)
            {
                for (int i = 0; i < collSTASIUNDEBIT_DYN.Count; i++)
                {
                    if (string.IsNullOrEmpty(sTahun))
                    {
                        sTahun = collSTASIUNDEBIT_DYN[i].TAHUN.ToString();
                    }
                    else
                    {
                        sTahun = sTahun + "," + collSTASIUNDEBIT_DYN[i].TAHUN.ToString();
                    }
                }
            }
            ViewBag.collTahun = sTahun;
        }

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                //return this.GenericViewDefault();
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            GetCodeAndYear();
            this.Session.Remove("DebitAirEvents");
            this.Session.Remove("TinggiMukaAirEvents");
            return PartialView("Save");
        }

        //
        // POST: /Bencana/STASIUNDEBIT_DYN/Create

        [HttpPost]
        public ActionResult Create(STASIUNDEBIT_DYN domain)
        {
            string K_STASIUN = Utils.GetQueryString("K_STASIUN");
            STASIUNDEBIT STASIUNDEBIT = this.StasiunDebitRepository.Get(x => x.K_STASIUN == K_STASIUN).FirstOrDefault();
            domain.STASIUNDEBIT = STASIUNDEBIT;

            if (Session["DEBIT"] != null)
            {
                domain.DEBIT = Session["DEBIT"] as byte[];
            }
            if (Session["TMA"] != null)
            {
                domain.TMA = Session["TMA"] as byte[];
            }

            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Save(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/STASIUNDEBIT_DYN/Edit/5

        public ActionResult Edit(string id)
        {
            string[] arr = id.Split(',');

            if (!IsUpdateable)
            {
                //return this.GenericViewDefault();
                return this.Detail(id);
            }
            ViewBag.ActionName = "Edit";
            //GetCodeAndYear();
            ViewBag.K_STASIUN = arr[0];
            ViewBag.TAHUN = arr[1];
            STASIUNDEBIT_DYN domain = DefaultRepository.Get(x => x.TAHUN == Convert.ToInt32(arr[1]) && (x.STASIUNDEBIT != null && x.STASIUNDEBIT.K_STASIUN == arr[0])).FirstOrDefault();

            IList<Utils.NameValue> DebitAirEvents = new List<Utils.NameValue>();
            if (domain.DEBIT != null)
            {
                if (Session["DEBIT"] != null)
                {
                    this.Session.Remove("DEBIT");
                }


                this.Session.Add("DEBIT", domain.DEBIT);

                using (Stream stream = new MemoryStream(domain.DEBIT))
                {
                    StreamReader reader = new StreamReader(stream);
                    string lines = string.Empty;
                    while ((lines = reader.ReadLine()) != null)
                    {
                        //string text = reader.ReadLine();
                        string[] texts = lines.Split(',');
                        Utils.NameValue NameValue = new Utils.NameValue();
                        NameValue.Name = texts[0];
                        NameValue.Value = texts[1];
                        DebitAirEvents.Add(NameValue);
                        lines = string.Empty;
                    }
                }
            }

            IList<Utils.NameValue> TMAEvents = new List<Utils.NameValue>();

            if (domain.TMA != null)
            {
                if (Session["TMA"] != null)
                {
                    this.Session.Remove("TMA");
                }


                this.Session.Add("TMA", domain.TMA);

                using (Stream stream = new MemoryStream(domain.TMA))
                {
                    StreamReader reader = new StreamReader(stream);
                    string lines = string.Empty;
                    while ((lines = reader.ReadLine()) != null)
                    {
                        //string text = reader.ReadLine();
                        string[] texts = lines.Split(',');
                        Utils.NameValue NameValue = new Utils.NameValue();
                        NameValue.Name = texts[0];
                        NameValue.Value = texts[1];
                        TMAEvents.Add(NameValue);
                        lines = string.Empty;
                    }
                }
            }



            STASIUNDEBIT_DYNDTO STASIUNDEBIT_DYNDTO = new STASIUNDEBIT_DYNDTO();
            STASIUNDEBIT_DYNDTO.STASIUNDEBIT_DYN = domain;
            STASIUNDEBIT_DYNDTO.DebitAirEvents = new List<jQueryFullCalendarEventModel>();
            STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents = new List<jQueryFullCalendarEventModel>();
            DebitAirEvents.ToList().ForEach(delegate(Utils.NameValue nv)
            {
                if (Utils.TryParseToDouble(nv.Value) > 0)
                {

                    jQueryFullCalendarEventModel jQueryFullCalendarEventModel = new jQueryFullCalendarEventModel();
                    //jQueryFullCalendarEventModel.id = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));
                    //jQueryFullCalendarEventModel.start = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));

                    jQueryFullCalendarEventModel.id = nv.Name;
                    jQueryFullCalendarEventModel.start = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.end = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.title = nv.Value;
                    jQueryFullCalendarEventModel.allDay = true;
                    STASIUNDEBIT_DYNDTO.DebitAirEvents.Add(jQueryFullCalendarEventModel);
                }

            });

            if (this.Session["DebitAirEvents"] != null)
            {
                this.Session.Remove("DebitAirEvents");
            }
            this.Session.Add("DebitAirEvents", STASIUNDEBIT_DYNDTO.DebitAirEvents);

            TMAEvents.ToList().ForEach(delegate(Utils.NameValue nv)
            {
                if (Utils.TryParseToDouble(nv.Value) > 0)
                {
                    jQueryFullCalendarEventModel jQueryFullCalendarEventModel = new jQueryFullCalendarEventModel();
                    //jQueryFullCalendarEventModel.id = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));
                    //jQueryFullCalendarEventModel.start = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));

                    jQueryFullCalendarEventModel.id = nv.Name;
                    jQueryFullCalendarEventModel.start = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.end = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.title = nv.Value;
                    jQueryFullCalendarEventModel.allDay = true;
                    STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents.Add(jQueryFullCalendarEventModel);
                }
            });


            if (this.Session["TinggiMukaAirEvents"] != null)
            {
                this.Session.Remove("TinggiMukaAirEvents");
            }
            this.Session.Add("TinggiMukaAirEvents", STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents);


            if (domain.STASIUNDEBIT != null && domain.STASIUNDEBIT.W != null && !collAssignWS.Contains(domain.STASIUNDEBIT.W))
            {
                //IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                //addWStoSelectedListItem.Add(new SelectListItem() { Text = domain.W.N_WS, Value = domain.W.K_WS, Selected = true });
                //ViewBag.collWS = addWStoSelectedListItem;
                ViewBag.ActionName = "Detail";
                ViewBag.Method = "Detail";
                return PartialView("Detail", domain);
                //return Detail(id);
            }

            return PartialView("Save", domain);
        }

        public ActionResult Detail(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return PartialView("Detail", null);
            }
            string[] arr = id.Split(',');


            ViewBag.ActionName = "Detail";
            ViewBag.Method = "Detail";
            //GetCodeAndYear();
            ViewBag.K_STASIUN = arr[0];
            ViewBag.TAHUN = arr[1];
            STASIUNDEBIT_DYN domain = DefaultRepository.Get(x => x.TAHUN == Convert.ToInt32(arr[1]) && (x.STASIUNDEBIT != null && x.STASIUNDEBIT.K_STASIUN == arr[0])).FirstOrDefault();

            IList<Utils.NameValue> DebitAirEvents = new List<Utils.NameValue>();
            if (domain.DEBIT != null)
            {
                if (Session["DEBIT"] != null)
                {
                    this.Session.Remove("DEBIT");
                }


                this.Session.Add("DEBIT", domain.DEBIT);

                using (Stream stream = new MemoryStream(domain.DEBIT))
                {
                    StreamReader reader = new StreamReader(stream);
                    string lines = string.Empty;
                    while ((lines = reader.ReadLine()) != null)
                    {
                        //string text = reader.ReadLine();
                        string[] texts = lines.Split(',');
                        Utils.NameValue NameValue = new Utils.NameValue();
                        NameValue.Name = texts[0];
                        NameValue.Value = texts[1];
                        DebitAirEvents.Add(NameValue);
                        lines = string.Empty;
                    }
                }
            }

            IList<Utils.NameValue> TMAEvents = new List<Utils.NameValue>();

            if (domain.TMA != null)
            {
                if (Session["TMA"] != null)
                {
                    this.Session.Remove("TMA");
                }


                this.Session.Add("TMA", domain.TMA);

                using (Stream stream = new MemoryStream(domain.TMA))
                {
                    StreamReader reader = new StreamReader(stream);
                    string lines = string.Empty;
                    while ((lines = reader.ReadLine()) != null)
                    {
                        //string text = reader.ReadLine();
                        string[] texts = lines.Split(',');
                        Utils.NameValue NameValue = new Utils.NameValue();
                        NameValue.Name = texts[0];
                        NameValue.Value = texts[1];
                        TMAEvents.Add(NameValue);
                        lines = string.Empty;
                    }
                }
            }



            STASIUNDEBIT_DYNDTO STASIUNDEBIT_DYNDTO = new STASIUNDEBIT_DYNDTO();
            STASIUNDEBIT_DYNDTO.STASIUNDEBIT_DYN = domain;
            STASIUNDEBIT_DYNDTO.DebitAirEvents = new List<jQueryFullCalendarEventModel>();
            STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents = new List<jQueryFullCalendarEventModel>();
            DebitAirEvents.ToList().ForEach(delegate(Utils.NameValue nv)
            {
                if (Utils.TryParseToDouble(nv.Value) > 0)
                {
                    jQueryFullCalendarEventModel jQueryFullCalendarEventModel = new jQueryFullCalendarEventModel();
                    //jQueryFullCalendarEventModel.id = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));
                    //jQueryFullCalendarEventModel.start = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));

                    jQueryFullCalendarEventModel.id = nv.Name;
                    jQueryFullCalendarEventModel.start = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.end = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.title = nv.Value;
                    jQueryFullCalendarEventModel.allDay = true;
                    STASIUNDEBIT_DYNDTO.DebitAirEvents.Add(jQueryFullCalendarEventModel);
                }
            });

            if (this.Session["DebitAirEvents"] != null)
            {
                this.Session.Remove("DebitAirEvents");
            }
            this.Session.Add("DebitAirEvents", STASIUNDEBIT_DYNDTO.DebitAirEvents);

            TMAEvents.ToList().ForEach(delegate(Utils.NameValue nv)
            {
                if (Utils.TryParseToDouble(nv.Value) > 0)
                {
                    jQueryFullCalendarEventModel jQueryFullCalendarEventModel = new jQueryFullCalendarEventModel();
                    //jQueryFullCalendarEventModel.id = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));
                    //jQueryFullCalendarEventModel.start = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));

                    jQueryFullCalendarEventModel.id = nv.Name;
                    jQueryFullCalendarEventModel.start = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.end = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.title = nv.Value;
                    jQueryFullCalendarEventModel.allDay = true;
                    STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents.Add(jQueryFullCalendarEventModel);
                }
            });


            if (this.Session["TinggiMukaAirEvents"] != null)
            {
                this.Session.Remove("TinggiMukaAirEvents");
            }
            this.Session.Add("TinggiMukaAirEvents", STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents);

            return PartialView("Detail", domain);
        }

        //
        // POST: /Bencana/STASIUNDEBIT_DYN/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, STASIUNDEBIT_DYN domain)
        {
            string[] arr = id.Split(',');

            STASIUNDEBIT_DYN saveDomain = DefaultRepository.Get(x => x.TAHUN == Convert.ToInt32(arr[1]) && (x.STASIUNDEBIT != null && x.STASIUNDEBIT.K_STASIUN == arr[0])).FirstOrDefault();
            Utils.SetTProperty<STASIUNDEBIT_DYN>(saveDomain, domain);

            if (Session["DEBIT"] != null)
            {
                saveDomain.DEBIT = Session["DEBIT"] as byte[];
            }
            if (Session["TMA"] != null)
            {
                saveDomain.TMA = Session["TMA"] as byte[];
            }

            try
            {
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Update(saveDomain);
                    return Json(new { success = true, message = "Data telah diubah." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/STASIUNDEBIT_DYN/Delete/5

        public ActionResult Delete(string id)
        {
            string[] arr = id.Split(',');

            if (!IsDeleteable)
            {
                //return this.GenericViewDefault();
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            STASIUNDEBIT_DYN deleteDomain = DefaultRepository.Get(x => x.TAHUN == Convert.ToInt32(arr[1]) && (x.STASIUNDEBIT != null && x.STASIUNDEBIT.K_STASIUN == arr[0])).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (deleteDomain.STASIUNDEBIT != null && deleteDomain.STASIUNDEBIT.W != null && !collAssignWS.Contains(deleteDomain.STASIUNDEBIT.W))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<STASIUNDEBIT_DYN> collSTASIUNDEBIT_DYN = this.DefaultRepository.Get(x => (x.STASIUNDEBIT != null && x.STASIUNDEBIT.K_STASIUN == Utils.GetPostRequest("K_STASIUN")));

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collSTASIUNDEBIT_DYN = collSTASIUNDEBIT_DYN.Where(x => (x.STASIUNDEBIT != null && (x.STASIUNDEBIT.K_STASIUN.Contains(param.sSearch) || x.STASIUNDEBIT.K_STASIUN1.Contains(param.sSearch) || x.STASIUNDEBIT.N_STASIUN.Contains(param.sSearch))) || x.TAHUN.ToString().Contains(param.sSearch));
            }
            Count = collSTASIUNDEBIT_DYN.Count();

            var sortColumnIndex = iSortCol;
            Func<STASIUNDEBIT_DYN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.TAHUN.ToString() :
                                                                 sortColumnIndex == 2 ? c.STASIUNDEBIT != null ? c.STASIUNDEBIT.K_STASIUN : string.Empty :
                                                                 sortColumnIndex == 2 ? c.STASIUNDEBIT != null ? c.STASIUNDEBIT.N_STASIUN : string.Empty :
                                                                 sortColumnIndex == 3 ? c.STASIUNDEBIT != null ? c.STASIUNDEBIT.K_STASIUN1 : string.Empty :
                                                                c.TAHUN.ToString());


            IList<STASIUNDEBIT_DYN> collResult = new List<STASIUNDEBIT_DYN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collSTASIUNDEBIT_DYN.Count() > 0)
            {
                collResult = collSTASIUNDEBIT_DYN.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.STASIUNDEBIT == null ? "" : c.STASIUNDEBIT.K_STASIUN + "," + c.TAHUN,
                             c.TAHUN,
                             K_STASIUN = c.STASIUNDEBIT == null ? "" : c.STASIUNDEBIT.K_STASIUN,
                             N_STASIUN = c.STASIUNDEBIT == null ? string.Empty : c.STASIUNDEBIT.N_STASIUN,
                             K_STASIUN1 = c.STASIUNDEBIT == null ? string.Empty : c.STASIUNDEBIT.K_STASIUN1
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveData(IList<jQueryFullCalendarEventModel> events, string TAHUN, string K_STASIUN)
        {
            STASIUNDEBIT_DYN STASIUNDEBIT_DYN = new STASIUNDEBIT_DYN();
            STASIUNDEBIT_DYN.TAHUN = Convert.ToInt32(TAHUN);
            STASIUNDEBIT_DYN.K_STASIUN = K_STASIUN;
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream))
                {
                    // Various for loops etc as necessary that will ultimately do this:
                    foreach (var o in events)
                    {
                        int iYear = DateTime.ParseExact((string)o.id, "d-MM-yyyy", new CultureInfo("en-US")).Year;//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                        if (iYear == STASIUNDEBIT_DYN.TAHUN)
                        {
                            writer.WriteLine(o.id + "," + o.title);
                        }
                    }
                }
                STASIUNDEBIT_DYN.DEBIT = memoryStream.ToArray();
            }

            try
            {
                DefaultRepository.SaveOrUpdate(STASIUNDEBIT_DYN);
                return Json(new { success = true, message = Pesan.A1A }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult DebitAir()
        {
            ViewBag.K_STASIUN = Utils.GetQueryString("id");
            return PartialView("DebitAirSaveV2");
        }
        public ActionResult TMA()
        {
            ViewBag.K_STASIUN = Utils.GetQueryString("id");
            return PartialView("TMASaveV2");
        }

        #region Run
        //public ActionResult SetToCacheDebitAir(IList<jQueryFullCalendarEventModel> events, string TAHUN, string K_STASIUN)
        //{
        //    STASIUNDEBIT_DYN STASIUNDEBIT_DYN = new STASIUNDEBIT_DYN();
        //    STASIUNDEBIT_DYN.TAHUN = Convert.ToInt32(TAHUN);
        //    STASIUNDEBIT_DYN.K_STASIUN = K_STASIUN;
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        using (var writer = new StreamWriter(memoryStream))
        //        {
        //            // Various for loops etc as necessary that will ultimately do this:
        //            foreach (var o in events)
        //            {
        //                int iYear = DateTime.ParseExact((string)o.id, "d-MM-yyyy", new CultureInfo("en-US")).Year;//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
        //                if (iYear == STASIUNDEBIT_DYN.TAHUN)
        //                {
        //                    writer.WriteLine(o.id + "," + o.title);
        //                }
        //            }
        //        }
        //        STASIUNDEBIT_DYN.DEBIT = memoryStream.ToArray();
        //    }

        //    try
        //    {
        //        if (this.Session["DEBIT"] != null)
        //        {
        //            this.Session.Remove("DEBIT");
        //        }
        //        this.Session.Add("DEBIT", STASIUNDEBIT_DYN.DEBIT);
        //        //DefaultRepository.SaveOrUpdate(STASIUNDEBIT_DYN);
        //        return Json(new { success = true, message = Pesan.A1A }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { success = false, message = ex.StackTrace }, JsonRequestBehavior.AllowGet);

        //    }

        //}

        //public ActionResult SetToCacheTMA(IList<jQueryFullCalendarEventModel> events, string TAHUN, string K_STASIUN)
        //{
        //    STASIUNDEBIT_DYN STASIUNDEBIT_DYN = new STASIUNDEBIT_DYN();
        //    STASIUNDEBIT_DYN.TAHUN = Convert.ToInt32(TAHUN);
        //    STASIUNDEBIT_DYN.K_STASIUN = K_STASIUN;
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        using (var writer = new StreamWriter(memoryStream))
        //        {
        //            // Various for loops etc as necessary that will ultimately do this:
        //            foreach (var o in events)
        //            {
        //                int iYear = DateTime.ParseExact((string)o.id, "d-MM-yyyy", new CultureInfo("en-US")).Year;//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
        //                if (iYear == STASIUNDEBIT_DYN.TAHUN)
        //                {
        //                    writer.WriteLine(o.id + "," + o.title);
        //                }
        //            }
        //        }
        //        STASIUNDEBIT_DYN.TMA = memoryStream.ToArray();
        //    }

        //    try
        //    {
        //        if (this.Session["TMA"] != null)
        //        {
        //            this.Session.Remove("TMA");
        //        }
        //        this.Session.Add("TMA", STASIUNDEBIT_DYN.TMA);
        //        //DefaultRepository.SaveOrUpdate(STASIUNDEBIT_DYN);
        //        return Json(new { success = true, message = Pesan.A1A }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { success = false, message = ex.StackTrace }, JsonRequestBehavior.AllowGet);

        //    }

        //}
        #endregion Run

        #region TEST

        public ActionResult SetToCacheDebitAir(IList<jQueryFullCalendarEventModel> events, string TAHUN, string K_STASIUN)
        {
            STASIUNDEBIT_DYN STASIUNDEBIT_DYN = new STASIUNDEBIT_DYN();
            STASIUNDEBIT_DYN.TAHUN = Convert.ToInt32(TAHUN);
            STASIUNDEBIT_DYN.K_STASIUN = K_STASIUN;
            //using (var memoryStream = new MemoryStream())
            //{
            //    using (var writer = new StreamWriter(memoryStream))
            //    {
            //        // Various for loops etc as necessary that will ultimately do this:
            //        foreach (var o in events)
            //        {
            //            int iYear = DateTime.ParseExact((string)o.id, "d-MM-yyyy", new CultureInfo("en-US")).Year;//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
            //            if (iYear == STASIUNDEBIT_DYN.TAHUN)
            //            {
            //                writer.WriteLine(o.id + "," + o.title);
            //            }
            //        }
            //    }
            //    STASIUNDEBIT_DYN.DEBIT = memoryStream.ToArray();
            //}
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream))
                {
                    int p = 0;
                    for (int j = 1; j <= 12; j++)
                    {
                        for (int i = 1; i <= 31; i++)
                        {
                            p = i;

                            string sDate = j + "/" + p + "/" + TAHUN;
                            DateTime? dt =Utils.TryParseToDateTime(sDate);
                            if (dt != null)
                            {
                                //DateTime dt = Convert.ToDateTime(sDate);
                                if (events != null)
                                {
                                    var o = events.FirstOrDefault(x => DateTime.ParseExact((string)x.id, "d-MM-yyyy", new CultureInfo("en-US")) == dt.Value);
                                    if (o != null)
                                    {
                                        if (dt.Value.Year == STASIUNDEBIT_DYN.TAHUN)
                                        {
                                            writer.WriteLine(o.id + "," + o.title);
                                        }
                                    }
                                    else
                                    {
                                        writer.WriteLine(string.Format("{0},{1}", dt.Value.ToString("d-MM-yyyy"), ""));
                                    }
                                }
                                else
                                {
                                    writer.WriteLine(string.Format("{0},{1}", dt.Value.ToString("d-MM-yyyy"), ""));
                                }
                            }
                        }
                    }
                }
                STASIUNDEBIT_DYN.DEBIT = memoryStream.ToArray();
            }

            try
            {
                IList<Utils.NameValue> DebitAirEvents = new List<Utils.NameValue>();
                if (STASIUNDEBIT_DYN.DEBIT != null)
                {
                    if (Session["DEBIT"] != null)
                    {
                        this.Session.Remove("DEBIT");
                    }


                    this.Session.Add("DEBIT", STASIUNDEBIT_DYN.DEBIT);

                    using (Stream stream = new MemoryStream(STASIUNDEBIT_DYN.DEBIT))
                    {
                        StreamReader reader = new StreamReader(stream);
                        string lines = string.Empty;
                        while ((lines = reader.ReadLine()) != null)
                        {
                            //string text = reader.ReadLine();
                            string[] texts = lines.Split(',');
                            Utils.NameValue NameValue = new Utils.NameValue();
                            NameValue.Name = texts[0];
                            NameValue.Value = texts[1];
                            DebitAirEvents.Add(NameValue);
                            lines = string.Empty;
                        }
                    }
                }

                STASIUNDEBIT_DYNDTO STASIUNDEBIT_DYNDTO = new STASIUNDEBIT_DYNDTO();
                STASIUNDEBIT_DYNDTO.STASIUNDEBIT_DYN = STASIUNDEBIT_DYN;
                STASIUNDEBIT_DYNDTO.DebitAirEvents = new List<jQueryFullCalendarEventModel>();
                STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents = Session["TinggiMukaAirEvents"] == null ? new List<jQueryFullCalendarEventModel>() : Session["TinggiMukaAirEvents"] as IList<jQueryFullCalendarEventModel>;
                DebitAirEvents.ToList().ForEach(delegate(Utils.NameValue nv)
                {
                    jQueryFullCalendarEventModel jQueryFullCalendarEventModel = new jQueryFullCalendarEventModel();
                    //jQueryFullCalendarEventModel.id = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));
                    //jQueryFullCalendarEventModel.start = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));

                    jQueryFullCalendarEventModel.id = nv.Name;
                    jQueryFullCalendarEventModel.start = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.end = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.title = nv.Value;
                    jQueryFullCalendarEventModel.allDay = true;
                    STASIUNDEBIT_DYNDTO.DebitAirEvents.Add(jQueryFullCalendarEventModel);

                });

                if (this.Session["DebitAirEvents"] != null)
                {
                    this.Session.Remove("DebitAirEvents");
                }
                this.Session.Add("DebitAirEvents", STASIUNDEBIT_DYNDTO.DebitAirEvents);




                if (this.Session["DEBIT"] != null)
                {
                    this.Session.Remove("DEBIT");
                }
                this.Session.Add("DEBIT", STASIUNDEBIT_DYN.DEBIT);
                //DefaultRepository.SaveOrUpdate(STASIUNDEBIT_DYN);
                return Json(new { success = true, message = Pesan.A1A }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult SetToCacheTMA(IList<jQueryFullCalendarEventModel> events, string TAHUN, string K_STASIUN)
        {
            STASIUNDEBIT_DYN STASIUNDEBIT_DYN = new STASIUNDEBIT_DYN();
            STASIUNDEBIT_DYN.TAHUN = Convert.ToInt32(TAHUN);
            STASIUNDEBIT_DYN.K_STASIUN = K_STASIUN;
            //using (var memoryStream = new MemoryStream())
            //{
            //    using (var writer = new StreamWriter(memoryStream))
            //    {
            //        // Various for loops etc as necessary that will ultimately do this:
            //        foreach (var o in events)
            //        {
            //            int iYear = DateTime.ParseExact((string)o.id, "d-MM-yyyy", new CultureInfo("en-US")).Year;//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
            //            if (iYear == STASIUNDEBIT_DYN.TAHUN)
            //            {
            //                writer.WriteLine(o.id + "," + o.title);
            //            }
            //        }
            //    }
            //    STASIUNDEBIT_DYN.TMA = memoryStream.ToArray();
            //}
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream))
                {
                    int p = 0;
                    for (int j = 1; j <= 12; j++)
                    {
                        for (int i = 1; i <= 31; i++)
                        {
                            p = i;

                            string sDate = j + "/" + p + "/" + TAHUN;
                            DateTime? dt=Utils.TryParseToDateTime(sDate);
                            if (dt != null)
                            {
                                //DateTime dt = Convert.ToDateTime(sDate);
                                if (events != null)
                                {
                                    var o = events.FirstOrDefault(x => DateTime.ParseExact((string)x.id, "d-MM-yyyy", new CultureInfo("en-US")) == dt.Value);
                                    if (o != null)
                                    {
                                        if (dt.Value.Year == STASIUNDEBIT_DYN.TAHUN)
                                        {
                                            writer.WriteLine(o.id + "," + o.title);
                                        }
                                    }
                                    else
                                    {
                                        writer.WriteLine(string.Format("{0},{1}", dt.Value.ToString("d-MM-yyyy"), ""));
                                    }
                                }
                                else
                                {
                                    writer.WriteLine(string.Format("{0},{1}", dt.Value.ToString("d-MM-yyyy"), ""));
                                }
                            }
                        }
                    }
                }
                STASIUNDEBIT_DYN.TMA = memoryStream.ToArray();
            }

            try
            {
                IList<Utils.NameValue> TMAEvents = new List<Utils.NameValue>();

                if (STASIUNDEBIT_DYN.TMA != null)
                {
                    if (Session["TMA"] != null)
                    {
                        this.Session.Remove("TMA");
                    }


                    this.Session.Add("TMA", STASIUNDEBIT_DYN.TMA);

                    using (Stream stream = new MemoryStream(STASIUNDEBIT_DYN.TMA))
                    {
                        StreamReader reader = new StreamReader(stream);
                        string lines = string.Empty;
                        while ((lines = reader.ReadLine()) != null)
                        {
                            //string text = reader.ReadLine();
                            string[] texts = lines.Split(',');
                            Utils.NameValue NameValue = new Utils.NameValue();
                            NameValue.Name = texts[0];
                            NameValue.Value = texts[1];
                            TMAEvents.Add(NameValue);
                            lines = string.Empty;
                        }
                    }


                }

                STASIUNDEBIT_DYNDTO STASIUNDEBIT_DYNDTO = new STASIUNDEBIT_DYNDTO();
                STASIUNDEBIT_DYNDTO.STASIUNDEBIT_DYN = STASIUNDEBIT_DYN;
                STASIUNDEBIT_DYNDTO.DebitAirEvents = Session["DebitAirEvents"] == null ? new List<jQueryFullCalendarEventModel>() : Session["DebitAirEvents"] as IList<jQueryFullCalendarEventModel>;
                STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents = new List<jQueryFullCalendarEventModel>();
                TMAEvents.ToList().ForEach(delegate(Utils.NameValue nv)
                {
                    jQueryFullCalendarEventModel jQueryFullCalendarEventModel = new jQueryFullCalendarEventModel();
                    //jQueryFullCalendarEventModel.id = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));
                    //jQueryFullCalendarEventModel.start = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));

                    jQueryFullCalendarEventModel.id = nv.Name;
                    jQueryFullCalendarEventModel.start = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.end = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.title = nv.Value;
                    jQueryFullCalendarEventModel.allDay = true;
                    STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents.Add(jQueryFullCalendarEventModel);
                });


                if (this.Session["TinggiMukaAirEvents"] != null)
                {
                    this.Session.Remove("TinggiMukaAirEvents");
                }
                this.Session.Add("TinggiMukaAirEvents", STASIUNDEBIT_DYNDTO.TinggiMukaAirEvents);




                if (this.Session["TMA"] != null)
                {
                    this.Session.Remove("TMA");
                }
                this.Session.Add("TMA", STASIUNDEBIT_DYN.TMA);
                //DefaultRepository.SaveOrUpdate(STASIUNDEBIT_DYN);
                return Json(new { success = true, message = Pesan.A1A }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace }, JsonRequestBehavior.AllowGet);

            }

        }
        #endregion TEST
    }
}
