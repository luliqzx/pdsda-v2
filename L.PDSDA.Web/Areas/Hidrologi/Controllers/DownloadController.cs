﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Common;
using System.IO;

namespace L.PDSDA.Web.Areas.Hidrologi.Controllers
{
    public class DownloadController : Controller
    {
        //
        // GET: /Hidrologi/Download/

        string fileTemplatePath = Utils.GetAppSetting("FileTemplatePath");
        string qryString = string.Empty;
        string filePath = "";

        public ActionResult Index()
        {
            qryString = Utils.GetQueryString("x");
            return DownloadTemplate(qryString);
        }

        private FilePathResult DownloadTemplate(string qryString)
        {
            filePath = fileTemplatePath + qryString + ".xls";
            string fileDownloadName = "";
            switch (qryString.ToLower())
            {
                case "posdugaaird":
                    fileDownloadName = "PosDugaAirDTemplate";
                    break;
                case "posdugaairt":
                    fileDownloadName = "PosDugaAirTTemplate";
                    break;
                case "poshujan":
                    fileDownloadName = "PosHujanTemplate";
                    break;
                case "posklimatologi":
                    fileDownloadName = "PosKlimatologiTemplate";
                    break;
                default:
                    break;
            }
            return File(filePath, "application/vnd.ms-excel", fileDownloadName);
        }
    }
}
