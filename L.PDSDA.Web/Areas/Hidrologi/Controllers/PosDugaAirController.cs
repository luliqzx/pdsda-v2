﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Hidrologi.Controllers
{
    public class PosDugaAirController : BaseController
    {

        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildJenis()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            collSelectListItem.Add(new SelectListItem { Text = "Otomatis", Value = "1" });
            collSelectListItem.Add(new SelectListItem { Text = "Manual", Value = "2" });
            //new PROPINSI().K_PROPINSI
            ViewBag.collJENIS = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        IDefaultRepository<STASIUNDEBIT> DefaultRepository;

        public PosDugaAirController()
        {
            DefaultRepository = new DefaultRepository<STASIUNDEBIT>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();

        }
        //
        // GET: /DaerahIrigasi/IrigasiTanam/

        public ActionResult Index()
        {
            string k_di = Utils.GetQueryString("id");
            return View("Index", k_di);
        }

        //
        // GET: /Bencana/STASIUNDEBIT/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                //return this.GenericViewDefault();
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildDAS(string.Empty);
            BuildWS();
            BuildPropinsi();
            BuildKabupaten(string.Empty);
            BuildJenis();
            return PartialView("Save");
        }

        //
        // POST: /Bencana/STASIUNDEBIT/Create

        [HttpPost]
        public ActionResult Create(STASIUNDEBIT domain)
        {
            try
            {
                // TODO: Add insert logic here
                if (domain.KABUPATEN != null && domain.KABUPATEN.K_KABUPATEN == "-1")
                {
                    domain.KABUPATEN = null;
                }
                if (domain.KABUPATEN != null && domain.KABUPATEN.PROPINSI != null && domain.KABUPATEN.PROPINSI.K_PROPINSI == "-1")
                {
                    domain.KABUPATEN.PROPINSI = null;
                }
                if (domain.W != null && domain.W.K_WS == "-1")
                {
                    domain.W = null;
                }
                if (domain.DA != null && domain.DA.K_DAS == "-1")
                {
                    domain.DA = null;
                }

                if (ModelState.IsValid)
                {
                    domain.CreateBy = this.UserLoggedIn;
                    domain.CreateDate = DateTime.Now;
                    domain.CreateTerminal = this.UserHostAddress;
                    domain.UpdateBy = this.UserLoggedIn;
                    domain.UpdateDate = DateTime.Now;
                    domain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.SaveOrUpdate(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/STASIUNDEBIT/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                //return this.GenericViewDefault();
                return Detail(id);
            }
            ViewBag.ActionName = "Edit";

            STASIUNDEBIT domain = DefaultRepository.Get(x => x.K_STASIUN == id).FirstOrDefault();
            BuildDAS(domain != null ? domain.W == null ? "" : domain.W.K_WS : "");
            BuildWS();
            BuildPropinsi();
            BuildJenis();
            BuildKabupaten(domain!=null?domain.KABUPATEN == null ? "" : domain.KABUPATEN.PROPINSI == null ? "" : domain.KABUPATEN.PROPINSI.K_PROPINSI:"");
            if (domain.W != null && !collAssignWS.Contains(domain.W))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = domain.W.N_WS, Value = domain.W.K_WS, Selected = true });
                //ViewBag.collWS = addWStoSelectedListItem;
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", domain);
                //return Detail(id);
            }
            return PartialView("Save", domain);
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";

            STASIUNDEBIT domain = DefaultRepository.Get(x => x.K_STASIUN == id).FirstOrDefault();
            BuildDAS(domain != null ? domain.W == null ? "" : domain.W.K_WS : "");
            BuildWS();
            BuildPropinsi();
            BuildJenis();
            BuildKabupaten(domain!=null?domain.KABUPATEN == null ? "" : domain.KABUPATEN.PROPINSI == null ? "" : domain.KABUPATEN.PROPINSI.K_PROPINSI:"");
            return PartialView("Detail", domain);
        }

        //
        // POST: /Bencana/STASIUNDEBIT/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, STASIUNDEBIT domain)
        {
            if (domain.KABUPATEN != null && domain.KABUPATEN.K_KABUPATEN == "-1")
            {
                domain.KABUPATEN = null;
            }
            if (domain.KABUPATEN != null && domain.KABUPATEN.PROPINSI != null && domain.KABUPATEN.PROPINSI.K_PROPINSI == "-1")
            {
                domain.KABUPATEN.PROPINSI = null;
            }
            if (domain.W != null && domain.W.K_WS == "-1")
            {
                domain.W = null;
            }
            if (domain.DA != null && domain.DA.K_DAS == "-1")
            {
                domain.DA = null;
            }

            STASIUNDEBIT saveDomain = DefaultRepository.Get(x => x.K_STASIUN == id).FirstOrDefault();
            saveDomain.W = domain.W;
            saveDomain.DA = domain.DA;
            saveDomain.KABUPATEN = domain.KABUPATEN;
            saveDomain.LatLong = domain.LatLong;
            Utils.SetTProperty<STASIUNDEBIT>(saveDomain, domain);

            try
            {
                if (ModelState.IsValid)
                {
                    saveDomain.UpdateBy = this.UserLoggedIn;
                    saveDomain.UpdateDate = DateTime.Now;
                    saveDomain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.Update(saveDomain);
                    return Json(new { success = true, message = "Data telah diubah." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/STASIUNDEBIT/Delete/5

        public ActionResult Delete(string id)
        {
            string[] arr = id.Split(',');

            if (!IsDeleteable)
            {
                //return this.GenericViewDefault();
                return Json(new { success = false, message = Pesan.A2C1 });
            }
            STASIUNDEBIT deleteDomain = DefaultRepository.Get(x => x.K_STASIUN == id).FirstOrDefault();
            
            if (deleteDomain != null)
            {
                if (deleteDomain.W != null && !collAssignWS.Contains(deleteDomain.W))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    //var childRepo = new DefaultRepository<STASIUNDEBIT_DYN>();
                    //if (deleteDomain.STASIUNDEBIT_DYN != null)
                    //{
                    //    //deleteDomain.STASIUNDEBIT_DYN.Clear();
                    //    deleteDomain.STASIUNDEBIT_DYN.ToList().ForEach(delegate(STASIUNDEBIT_DYN child)
                    //    {
                    //        childRepo.Delete(child);
                    //    });
                    //}
                    this.DefaultRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = Pesan.A1C });

                }
                return Json(new { success = false, message = Pesan.A2C });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<STASIUNDEBIT> collSTASIUNDEBIT = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collSTASIUNDEBIT = collSTASIUNDEBIT.Where(x => (x.K_STASIUN.Contains(param.sSearch) || x.N_STASIUN.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)))
                    || (x.W != null && x.W.N_WS.Contains(param.sSearch))
                     || (x.DA != null && x.DA.N_DAS.Contains(param.sSearch)));
            }
            Count = collSTASIUNDEBIT.Count();

            var sortColumnIndex = iSortCol;
            Func<STASIUNDEBIT, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_STASIUN :
                                                                sortColumnIndex == 2 ? c.N_STASIUN :
                                                                sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.W != null ? c.W.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DA != null ? c.DA.N_DAS : string.Empty :
                                                                c.K_STASIUN);


            IList<STASIUNDEBIT> collResult = new List<STASIUNDEBIT>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collSTASIUNDEBIT.Count() > 0)
            {
                collResult = collSTASIUNDEBIT.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_STASIUN,
                             c.K_STASIUN,
                             c.N_STASIUN,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.W != null ? c.W.N_WS : string.Empty,
                             N_DAS = c.DA != null ? c.DA.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.N_STASIUN
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
