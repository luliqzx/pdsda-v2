﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using L.PDSDA.DAL.Entities.Sample;
using L.PDSDA.DAL.Repositories.Base;
using L.Core.Utilities.Web;
using System.Text;
using NetUtils = L.Core.Utilities.Net;

namespace L.PDSDA.Web.Areas.Hidrologi.Controllers
{
    public class SampleCalendarController : Controller
    {
        IDefaultRepository<sBlob> sBlobRepo;

        public SampleCalendarController()
        {
            sBlobRepo = new DefaultRepository<sBlob>();
        }

        //
        // GET: /Hidrologi/SampleCalendar/

        public ActionResult Index()
        {
            return View("SampleCalendar");
        }

        public ActionResult CreateSampleForm()
        {
            return View("CreateSample");
        }
        public ActionResult CreateSample(HttpPostedFileBase file)
        {
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                Stream strm = file.InputStream;
                sBlob sBlob = new sBlob();
                //sBlob.Id = 1;
                using (MemoryStream ms = new MemoryStream())
                {
                    strm.CopyTo(ms);
                    sBlob.blobData = ms.ToArray();
                }
                //sBlob.blobData = strm;
                sBlobRepo.SaveOrUpdate(sBlob);
                // extract only the fielname
                var fileName = Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/Sample"), fileName);
                file.SaveAs(path);
            }
            // redirect back to the index action to show the form once again
            return RedirectToAction("Index");
        }

        public ActionResult CreateSampleBlob()
        {
            return View("CreateSampleBlob");
        }

        [HttpPost()]
        public ActionResult CreateSampleBlob(IList<jQueryFullCalendarEventModel> events)
        {
            sBlob sBlob = new sBlob();
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream))
                {
                    // Various for loops etc as necessary that will ultimately do this:
                    foreach (var o in events)
                    {
                        writer.WriteLine(o.id + "," + o.title);
                    }
                }
                sBlob.blobData = memoryStream.ToArray();
            }

            sBlobRepo.SaveOrUpdate(sBlob);
            NetUtils.Utils.SendMail("lucky_yuditia_putra@yahoo.com", "TEST MAIL", sBlob.blobData.ToString(), credentialUserName: "luliqzx@gmail.com", credentialPassword: "pass@word123");


            return Content("Data telah di insert");
        }

    }
}
