﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using L.Core.Utilities.Web;
using L.Core.Utilities;
using System.IO;
using L.PDSDA.Web.DTOs;
using System.Globalization;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Hidrologi.Controllers
{
    public class PosKlimatologiDynController : BaseController
    {
        IDefaultRepository<STASIUNKLIMATOLOGI> STASIUNKLIMATOLOGIRepository;

        IDefaultRepository<STASIUNKLIMATOLOGI_DYN> DefaultRepository;

        public PosKlimatologiDynController()
        {
            DefaultRepository = new DefaultRepository<STASIUNKLIMATOLOGI_DYN>();
            STASIUNKLIMATOLOGIRepository = new DefaultRepository<STASIUNKLIMATOLOGI>();
        }
        //
        // GET: /DaerahIrigasi/IrigasiDyn/

        public ActionResult Index()
        {
            string K_STASIUN = Utils.GetQueryString("id");
            STASIUNKLIMATOLOGI STASIUNKLIMATOLOGI = this.STASIUNKLIMATOLOGIRepository.Get(x => x.K_STASIUN == K_STASIUN).FirstOrDefault();
            if (STASIUNKLIMATOLOGI != null)
            {
                if (STASIUNKLIMATOLOGI.W != null && !collAssignWS.Contains(STASIUNKLIMATOLOGI.W))
                {
                    ViewBag.IsViewable = false;
                    ViewBag.IsCreateable = false;
                    ViewBag.IsUpdateable = false;
                    ViewBag.IsDeleteable = false;
                    ViewBag.Method = Utils.GetPostRequest("method");
                    return PartialView("Index", K_STASIUN);
                }
            }
            return PartialView("Index", K_STASIUN);
        }

        //
        // GET: /Bencana/STASIUNKLIMATOLOGI_DYN/Create

        void GetCodeAndYear()
        {
            string K_STASIUN = Utils.GetQueryString("K_STASIUN");
            ViewBag.K_STASIUN = K_STASIUN;
            IQueryable<STASIUNKLIMATOLOGI_DYN> qrySTASIUNKLIMATOLOGI_DYN = this.DefaultRepository.Get(x => x.STASIUNKLIMATOLOGI != null && x.STASIUNKLIMATOLOGI.K_STASIUN == K_STASIUN);
            IList<STASIUNKLIMATOLOGI_DYN> collSTASIUNKLIMATOLOGI_DYN = null;
            string sTahun = "";
            if (qrySTASIUNKLIMATOLOGI_DYN != null)
            {
                collSTASIUNKLIMATOLOGI_DYN = qrySTASIUNKLIMATOLOGI_DYN.ToList();
            }
            if (collSTASIUNKLIMATOLOGI_DYN != null && collSTASIUNKLIMATOLOGI_DYN.Count > 0)
            {
                for (int i = 0; i < collSTASIUNKLIMATOLOGI_DYN.Count; i++)
                {
                    if (string.IsNullOrEmpty(sTahun))
                    {
                        sTahun = collSTASIUNKLIMATOLOGI_DYN[i].TAHUN.ToString();
                    }
                    else
                    {
                        sTahun = sTahun + "," + collSTASIUNKLIMATOLOGI_DYN[i].TAHUN.ToString();
                    }
                }
            }
            ViewBag.collTahun = sTahun;
        }

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                //return this.GenericViewDefault();
                return this.Detail();
            }
            ViewBag.ActionName = "Create";
            GetCodeAndYear();
            return PartialView("SaveV2");
        }

        //
        // POST: /Bencana/STASIUNKLIMATOLOGI_DYN/Create

        [HttpPost]
        public ActionResult Create(STASIUNKLIMATOLOGI_DYN domain)
        {
            string K_STASIUN = Utils.GetQueryString("K_STASIUN");
            STASIUNKLIMATOLOGI STASIUNKLIMATOLOGI = this.STASIUNKLIMATOLOGIRepository.Get(x => x.K_STASIUN == K_STASIUN).FirstOrDefault();
            domain.STASIUNKLIMATOLOGI = STASIUNKLIMATOLOGI;
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Save(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/STASIUNKLIMATOLOGI_DYN/Edit/5

        public ActionResult Edit(string id)
        {
            string[] arr = id.Split(',');

            if (!IsUpdateable)
            {
                //return this.GenericViewDefault();
                return this.Detail(id);

            }
            ViewBag.ActionName = "Edit";
            //GetCodeAndYear();
            ViewBag.K_STASIUN = arr[0];
            ViewBag.TAHUN = arr[1];
            STASIUNKLIMATOLOGI_DYN domain = DefaultRepository.Get(x => x.TAHUN == Convert.ToInt32(arr[1]) && (x.STASIUNKLIMATOLOGI != null && x.STASIUNKLIMATOLOGI.K_STASIUN == arr[0])).FirstOrDefault();

            IList<Utils.NameValue> Events = new List<Utils.NameValue>();
            if (domain.HARIAN != null)
            {

                using (Stream stream = new MemoryStream(domain.HARIAN))
                {
                    StreamReader reader = new StreamReader(stream);
                    string lines = string.Empty;
                    while ((lines = reader.ReadLine()) != null)
                    {
                        //string text = reader.ReadLine();
                        string[] texts = lines.Split(',');
                        Utils.NameValue NameValue = new Utils.NameValue();
                        NameValue.Name = texts[0];
                        NameValue.Value = texts[1];
                        Events.Add(NameValue);
                        lines = string.Empty;
                    }
                }
            }

            STASIUNKLIMATOLOGI_DYNDTO STASIUNKLIMATOLOGI_DYNDTO = new STASIUNKLIMATOLOGI_DYNDTO();
            STASIUNKLIMATOLOGI_DYNDTO.STASIUNKLIMATOLOGI_DYN = domain;
            STASIUNKLIMATOLOGI_DYNDTO.Events = new List<jQueryFullCalendarEventModel>();
            Events.ToList().ForEach(delegate(Utils.NameValue nv)
            {
                if (!string.IsNullOrWhiteSpace(nv.Value))
                {
                    jQueryFullCalendarEventModel jQueryFullCalendarEventModel = new jQueryFullCalendarEventModel();
                    //jQueryFullCalendarEventModel.id = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));
                    //jQueryFullCalendarEventModel.start = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));

                    jQueryFullCalendarEventModel.id = nv.Name;
                    jQueryFullCalendarEventModel.start = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.end = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    jQueryFullCalendarEventModel.title = nv.Value;
                    jQueryFullCalendarEventModel.allDay = true;
                    STASIUNKLIMATOLOGI_DYNDTO.Events.Add(jQueryFullCalendarEventModel);
                }
            });


            return PartialView("SaveV2", STASIUNKLIMATOLOGI_DYNDTO);
        }

        public ActionResult Detail(string id = "")
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return PartialView("Detail", null);
            }
            string[] arr = id.Split(',');


            ViewBag.ActionName = "Detail";
            //ViewBag.Method = "Detail";
            //GetCodeAndYear();
            ViewBag.K_STASIUN = arr[0];
            ViewBag.TAHUN = arr[1];
            STASIUNKLIMATOLOGI_DYN domain = DefaultRepository.Get(x => x.TAHUN == Convert.ToInt32(arr[1]) && (x.STASIUNKLIMATOLOGI != null && x.STASIUNKLIMATOLOGI.K_STASIUN == arr[0])).FirstOrDefault();

            IList<Utils.NameValue> Events = new List<Utils.NameValue>();
            if (domain.HARIAN != null)
            {

                using (Stream stream = new MemoryStream(domain.HARIAN))
                {
                    StreamReader reader = new StreamReader(stream);
                    string lines = string.Empty;
                    while ((lines = reader.ReadLine()) != null)
                    {
                        //string text = reader.ReadLine();
                        string[] texts = lines.Split(',');
                        Utils.NameValue NameValue = new Utils.NameValue();
                        NameValue.Name = texts[0];
                        NameValue.Value = texts[1];
                        Events.Add(NameValue);
                        lines = string.Empty;
                    }
                }
            }

            STASIUNKLIMATOLOGI_DYNDTO STASIUNKLIMATOLOGI_DYNDTO = new STASIUNKLIMATOLOGI_DYNDTO();
            STASIUNKLIMATOLOGI_DYNDTO.STASIUNKLIMATOLOGI_DYN = domain;
            STASIUNKLIMATOLOGI_DYNDTO.Events = new List<jQueryFullCalendarEventModel>();
            Events.ToList().ForEach(delegate(Utils.NameValue nv)
            {
                jQueryFullCalendarEventModel jQueryFullCalendarEventModel = new jQueryFullCalendarEventModel();
                //jQueryFullCalendarEventModel.id = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));
                //jQueryFullCalendarEventModel.start = Convert.ToDateTime(nv.Name, CultureInfo.GetCultureInfo("id-ID"));

                jQueryFullCalendarEventModel.id = nv.Name;
                jQueryFullCalendarEventModel.start = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                jQueryFullCalendarEventModel.end = DateTime.ParseExact(nv.Name, "d-MM-yyyy", new CultureInfo("en-US")).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                jQueryFullCalendarEventModel.title = nv.Value;
                jQueryFullCalendarEventModel.allDay = true;
                STASIUNKLIMATOLOGI_DYNDTO.Events.Add(jQueryFullCalendarEventModel);
            });


            return PartialView("Detail", STASIUNKLIMATOLOGI_DYNDTO);
        }

        //
        // POST: /Bencana/STASIUNKLIMATOLOGI_DYN/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, STASIUNKLIMATOLOGI_DYN domain)
        {
            string[] arr = id.Split(',');

            STASIUNKLIMATOLOGI_DYN saveDomain = DefaultRepository.Get(x => x.TAHUN == Convert.ToInt32(arr[1]) && (x.STASIUNKLIMATOLOGI != null && x.STASIUNKLIMATOLOGI.K_STASIUN == arr[0])).FirstOrDefault();
            Utils.SetTProperty<STASIUNKLIMATOLOGI_DYN>(saveDomain, domain);

            try
            {
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Update(saveDomain);
                    return Json(new { success = true, message = "Data telah diubah." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/STASIUNKLIMATOLOGI_DYN/Delete/5

        public ActionResult Delete(string id)
        {
            string[] arr = id.Split(',');

            if (!IsDeleteable)
            {
                //return this.GenericViewDefault();
                return Json(new { success = false, message = Pesan.A2C1 });
            }
            STASIUNKLIMATOLOGI_DYN deleteDomain = DefaultRepository.Get(x => x.TAHUN == Convert.ToInt32(arr[1]) && (x.STASIUNKLIMATOLOGI != null && x.STASIUNKLIMATOLOGI.K_STASIUN == arr[0])).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (deleteDomain.STASIUNKLIMATOLOGI != null && deleteDomain.STASIUNKLIMATOLOGI.W != null && !collAssignWS.Contains(deleteDomain.STASIUNKLIMATOLOGI.W))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<STASIUNKLIMATOLOGI_DYN> collSTASIUNKLIMATOLOGI_DYN = this.DefaultRepository.Get(x => (x.STASIUNKLIMATOLOGI != null && x.STASIUNKLIMATOLOGI.K_STASIUN == Utils.GetPostRequest("K_STASIUN")));

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collSTASIUNKLIMATOLOGI_DYN = collSTASIUNKLIMATOLOGI_DYN.Where(x => (x.STASIUNKLIMATOLOGI != null && (x.STASIUNKLIMATOLOGI.K_STASIUN.Contains(param.sSearch) || x.STASIUNKLIMATOLOGI.K_STASIUN1.Contains(param.sSearch) || x.STASIUNKLIMATOLOGI.N_STASIUN.Contains(param.sSearch))) || x.TAHUN.ToString().Contains(param.sSearch));
            }
            Count = collSTASIUNKLIMATOLOGI_DYN.Count();

            var sortColumnIndex = iSortCol;
            Func<STASIUNKLIMATOLOGI_DYN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.TAHUN.ToString() :
                                                                 sortColumnIndex == 2 ? c.STASIUNKLIMATOLOGI != null ? c.STASIUNKLIMATOLOGI.K_STASIUN : string.Empty :
                                                                 sortColumnIndex == 2 ? c.STASIUNKLIMATOLOGI != null ? c.STASIUNKLIMATOLOGI.N_STASIUN : string.Empty :
                                                                 sortColumnIndex == 3 ? c.STASIUNKLIMATOLOGI != null ? c.STASIUNKLIMATOLOGI.K_STASIUN1 : string.Empty :
                                                                c.TAHUN.ToString());


            IList<STASIUNKLIMATOLOGI_DYN> collResult = new List<STASIUNKLIMATOLOGI_DYN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collSTASIUNKLIMATOLOGI_DYN.Count() > 0)
            {
                collResult = collSTASIUNKLIMATOLOGI_DYN.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.STASIUNKLIMATOLOGI == null ? "" : c.STASIUNKLIMATOLOGI.K_STASIUN + "," + c.TAHUN,
                             c.TAHUN,
                             K_STASIUN = c.STASIUNKLIMATOLOGI == null ? "" : c.STASIUNKLIMATOLOGI.K_STASIUN,
                             N_STASIUN = c.STASIUNKLIMATOLOGI == null ? string.Empty : c.STASIUNKLIMATOLOGI.N_STASIUN,
                             K_STASIUN1 = c.STASIUNKLIMATOLOGI == null ? string.Empty : c.STASIUNKLIMATOLOGI.K_STASIUN1
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveData(IList<jQueryFullCalendarEventModel> events, string TAHUN, string K_STASIUN)
        {
            STASIUNKLIMATOLOGI_DYN STASIUNKLIMATOLOGI_DYN = new STASIUNKLIMATOLOGI_DYN();
            STASIUNKLIMATOLOGI_DYN.TAHUN = Convert.ToInt32(TAHUN);
            STASIUNKLIMATOLOGI_DYN.K_STASIUN = K_STASIUN;
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream))
                {
                    int p = 0;
                    for (int j = 1; j <= 12; j++)
                    {
                        for (int i = 1; i <= 31; i++)
                        {
                            p = i;

                            string sDate = j + "/" + p + "/" + TAHUN;
                            if (Utils.TryParseToDateTime(sDate) != null)
                            {
                                DateTime dt = Convert.ToDateTime(sDate);
                                if (events != null)
                                {
                                    var o = events.FirstOrDefault(x => DateTime.ParseExact((string)x.id, "d-MM-yyyy", new CultureInfo("en-US")) == dt);
                                    if (o != null)
                                    {
                                        if (dt.Year == STASIUNKLIMATOLOGI_DYN.TAHUN)
                                        {
                                            writer.WriteLine(o.id + "," + o.title);
                                        }
                                    }
                                    else
                                    {
                                        writer.WriteLine(string.Format("{0},{1}", dt.ToString("d-MM-yyyy"), ""));
                                    }
                                }
                                else
                                {
                                    writer.WriteLine(string.Format("{0},{1}", dt.ToString("d-MM-yyyy"), ""));
                                }
                                //writer.WriteLine(string.Format("{0},{1}", dt.ToString("d-MM-yyyy"), -1));
                            }
                        }
                    }

                    // Various for loops etc as necessary that will ultimately do this:
                    //foreach (var o in events)
                    //{
                    //    int iYear = DateTime.ParseExact((string)o.id, "d-MM-yyyy", new CultureInfo("en-US")).Year;//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
                    //    if (iYear == STASIUNHUJAN_DYN.TAHUN)
                    //    {
                    //        writer.WriteLine(o.id + "," + o.title);
                    //    }
                    //}
                }
                STASIUNKLIMATOLOGI_DYN.HARIAN = memoryStream.ToArray();
            }
            //{
            //    using (var writer = new StreamWriter(memoryStream))
            //    {
            //        // Various for loops etc as necessary that will ultimately do this:
            //        foreach (var o in events)
            //        {
            //            int iYear = DateTime.ParseExact((string)o.id, "d-MM-yyyy", new CultureInfo("en-US")).Year;//, new CultureInfo(""), DateTimeStyles.NoCurrentDateDefault).ToString("yyyy-MM-dd");// Convert.ToDateTime(nv.Name).ToString("yyyy-MM-dd");
            //            if (iYear == STASIUNKLIMATOLOGI_DYN.TAHUN)
            //            {
            //                writer.WriteLine(o.id + "," + o.title);
            //            }
            //        }
            //    }
            //    STASIUNKLIMATOLOGI_DYN.HARIAN = memoryStream.ToArray();
            //}

            try
            {
                DefaultRepository.SaveOrUpdate(STASIUNKLIMATOLOGI_DYN);
                return Json(new { success = true, message = Pesan.A1A }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace }, JsonRequestBehavior.AllowGet);

            }
        }
    }
}
