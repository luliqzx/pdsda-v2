﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using L.PDSDA.Web.Common;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using System.Text;

namespace L.PDSDA.Web.Areas.Hidrologi.Controllers
{
    public class ImportController : Controller
    {
        //
        // GET: /Hidrologi/Import/
        string oName = string.Empty;
        IDefaultRepository<STASIUNDEBIT> StasiunDebitRepo;
        IDefaultRepository<STASIUNDEBIT_DYN> StasiunDebitDynRepo;
        IDefaultRepository<STASIUNHUJAN> StasiunHujanRepo;
        IDefaultRepository<STASIUNHUJAN_DYN> StasiunHujanDynRepo;
        IDefaultRepository<DAS> DASRepo;
        IDefaultRepository<WS> WSRepo;
        IDefaultRepository<KABUPATEN> KabupatenRepo;
        IDefaultRepository<PROPINSI> PropinsiRepo;
        string nPropinsi = string.Empty;
        string nKabupaten = string.Empty;

        public ImportController()
        {
            this.DASRepo = new DefaultRepository<DAS>();
            this.WSRepo = new DefaultRepository<WS>();
            this.KabupatenRepo = new DefaultRepository<KABUPATEN>();
            this.PropinsiRepo = new DefaultRepository<PROPINSI>();
            this.StasiunDebitRepo = new DefaultRepository<STASIUNDEBIT>();
            this.StasiunDebitDynRepo = new DefaultRepository<STASIUNDEBIT_DYN>();
            this.StasiunHujanRepo = new DefaultRepository<STASIUNHUJAN>();
            this.StasiunHujanDynRepo = new DefaultRepository<STASIUNHUJAN_DYN>();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(FormCollection frmCollection)
        {
            oName = frmCollection["hfName"];// Utils.GetPostRequest("hfName");
            HttpFileCollectionBase hfcb = Request.Files;
            if (hfcb.Count > 0)
            {
                HttpPostedFileBase hpfb = hfcb[0];
                string path = Server.MapPath(@"~/uploads/");
                string filename = hpfb.FileName;
                string filepath = Path.Combine(path, filename);

                if (System.IO.File.Exists(filepath))
                {
                    int length = filename.Length - 4;
                    string ext = filename.Substring(length);
                    if (ext.ToLower() == ".xls")
                    {
                        filename = filename.Substring(0, length) + new Guid() + ext;
                    }
                    else if (ext.ToLower() == "xlsx")
                    {
                        filename = filename.Substring(0, length - 1) + new Guid() + "." + ext;
                    }
                    filepath = Path.Combine(path, filename);
                }
                hpfb.SaveAs(filepath);

                DataSet ds = Utils.ReadExcelAsDataSet(filepath, false);
                if (ds != null && ds.Tables.Count > 0)
                {
                    ImportToDb(ds);
                }
            }
            return View("Index");
        }

        private void ImportToDb(DataSet ds)
        {
            //string oName = "";
            DataTable dt = ds.Tables["Data Stasiun"];
            DataRowCollection drc = dt.Rows;

            if (oName.ToLower() == "poshujan")
            {
                if (IsVerifyTahunData(drc[17][3]))
                {
                    FillToPosHujan(drc);
                }
            }
            else if (oName.ToLower().Contains("posdugaair"))
            {
                if (IsVerifyTahunData(drc[26][3]))
                {
                    FillToPosDugaAir(drc);
                }
            }

        }

        private bool IsVerifyTahunData(object oTahunData)
        {
            int iTahun = 0;
            return int.TryParse(Utils.TryParseToString(oTahunData), out iTahun);
        }

        private void FillToPosHujan(DataRowCollection drc)
        {
            STASIUNHUJAN sh = this.StasiunHujanRepo.Get(x => x.K_STASIUN == (string)drc[5][3]).FirstOrDefault();
            if (Utils.TryParseToString(drc[0][0]).ToLower() == "data pos hujan")
            {
                nPropinsi = Utils.TryParseToString(drc[3][1]);
                nKabupaten = Utils.TryParseToString(drc[4][1]);
                if (sh == null)
                {
                    sh = new STASIUNHUJAN();
                }

                sh.KABUPATEN = KabupatenRepo.Get(x => nPropinsi.Contains(x.PROPINSI.N_PROPINSI) && nKabupaten.Contains(x.N_KABUPATEN)).FirstOrDefault();
                sh.K_STASIUN = Utils.TryParseToString(drc[5][3]);
                sh.N_STASIUN = Utils.TryParseToString(drc[6][3]);
                sh.K_STASIUN1 = drc[7][3] == DBNull.Value ? "" : Utils.TryParseToString(drc[7][3]);
                sh.LOKASI = Utils.TryParseToString(drc[9][3]);
                sh.KECAMATAN = Utils.TryParseToString(drc[10][3]);
                sh.KADASTER = Utils.TryParseToString(drc[11][3]);
                sh.TINGGI_MUKA_LAUT = Utils.TryParseToDouble(drc[12][3]);
                sh.W = this.WSRepo.Get(x => x.N_WS == Utils.TryParseToString(drc[13][3])).FirstOrDefault();
                sh.DA = this.DASRepo.Get(x => x.N_DAS == Utils.TryParseToString(drc[8][3])).FirstOrDefault();
                sh.INDUK_SUNGAI = Utils.TryParseToString(drc[14][3]);
                sh.DIBANGUNOLEH = Utils.TryParseToString(drc[15][3]);
                sh.THPENDIRIAN = Utils.TryParseToShort(drc[16][3]);
                //this.StasiunHujanRepo.Save(sh);

                // kurang Desa
                //sh.SUNGAI = (string)drc[0][0];
                //sh.JENIS = (string)drc[0][0];
                //sh.LUAS_DAS = (double?)drc[0][0];
                //sh.PELAKSANA = (string)drc[0][0];

                int iTahunData = Convert.ToInt32(drc[17][3]);
                StringBuilder sb = new StringBuilder();
                STASIUNHUJAN_DYN shd = this.StasiunHujanDynRepo.Get(x => x.STASIUNHUJAN == sh && x.TAHUN == iTahunData).FirstOrDefault();
                if (shd == null)
                {
                    if (sh.STASIUNHUJAN_DYN == null)
                    {
                        sh.STASIUNHUJAN_DYN = new List<STASIUNHUJAN_DYN>();
                    }
                    shd = new STASIUNHUJAN_DYN();
                    shd.STASIUNHUJAN = sh;

                    using (var memoryStream = new MemoryStream())
                    {
                        using (var writer = new StreamWriter(memoryStream))
                        {
                            // Various for loops etc as necessary that will ultimately do this:
                            int p = 0;
                            for (int j = 1; j <= 12; j++)
                            {
                                for (int i = 20; i <= 50; i++)
                                {
                                    p = p + 1;

                                    string sDate = j + "/" + p + "/" + iTahunData;
                                    if (Utils.TryParseToDateTime(sDate) != null)
                                    {
                                        DateTime dt = Convert.ToDateTime(sDate);
                                        writer.WriteLine(string.Format("{0},{1}", dt.ToString("d-MM-yyyy"), drc[i][j]));
                                    }
                                }
                                p = 0;
                            }
                        }
                        shd.HARIAN = memoryStream.ToArray();
                    }

                    shd.TAHUN = iTahunData;
                    sh.STASIUNHUJAN_DYN.Add(shd);
                }

                this.StasiunHujanRepo.SaveOrUpdate(sh);
                ViewBag.ImportMessage = "Import Success";
            }
        }

        private void FillToPosDugaAir(DataRowCollection drc)
        {
            STASIUNDEBIT sd = this.StasiunDebitRepo.Get(x => x.K_STASIUN == Utils.TryParseToString(drc[5][3])).FirstOrDefault();
            if (Utils.TryParseToString(drc[0][0]).ToLower() == "data pos duga air")
            {
                nPropinsi = Utils.TryParseToString(drc[3][1]);
                nKabupaten = Utils.TryParseToString(drc[4][1]);
                if (sd == null)
                {
                    sd = new STASIUNDEBIT();
                    sd.KABUPATEN = KabupatenRepo.Get(x => nPropinsi.Contains(x.PROPINSI.N_PROPINSI) && nKabupaten.Contains(x.N_KABUPATEN)).FirstOrDefault();
                    sd.K_STASIUN = Utils.TryParseToString(drc[5][3]);
                    sd.N_STASIUN = Utils.TryParseToString(drc[6][3]);
                    sd.INDUK_SUNGAI = drc[7][3] == DBNull.Value ? "" : Utils.TryParseToString(drc[7][3]);
                    sd.LOKASI_GEOGRAFIS = Utils.TryParseToString(drc[8][3]);
                    sd.LOKASI = Utils.TryParseToString(drc[9][3]);
                    sd.LUAS_DAS = Utils.TryParseToDouble(drc[10][3]);
                    sd.ELEVASI = Utils.TryParseToDouble(drc[11][3]);

                    sd.DIBANGUNOLEH = Utils.TryParseToString(drc[13][3]);
                    sd.TGLDIBANGUN = Utils.TryParseToDateTime(drc[14][3]);
                    //sd.PeriodePencatatan
                    sd.JENIS = Utils.TryParseToString(drc[16][3]);
                    //sd.

                }

                if (sd.STASIUNDEBIT_DYN == null)
                {
                    sd.STASIUNDEBIT_DYN = new List<STASIUNDEBIT_DYN>();
                }

                int iTahunData = Convert.ToInt32(drc[26][3]);
                STASIUNDEBIT_DYN sdd = sd.STASIUNDEBIT_DYN.FirstOrDefault(x => x.STASIUNDEBIT == sd && x.TAHUN == iTahunData);
                if (sdd == null)
                {
                    sdd = new STASIUNDEBIT_DYN();
                    sdd.STASIUNDEBIT = sd;
                    sdd.TAHUN = iTahunData;
                    sd.STASIUNDEBIT_DYN.Add(sdd);
                }

                if (Utils.TryParseToString(drc[27][0]).ToUpper().TrimStart().TrimEnd() == "TABEL TINGGI MUKA AIR HARIAN ( METER )")
                {
                    sdd.T_PERIODE_PENCATATAN = iTahunData.ToString();
                    sdd.T_CATATAN = Utils.TryParseToString(drc[16][3]);
                    sdd.T_MAKSIMUM_EKSTRIM = Utils.TryParseToString(drc[18][3]);
                    sdd.T_MINIMUM_EKSTRIM = Utils.TryParseToString(drc[19][3]);
                    sdd.T_MAKSIMUM_EKSTRIM_KUMULATIF = Utils.TryParseToString(drc[21][3]);
                    sdd.T_MINIMUM_EKSTRIM_KUMULATIF = Utils.TryParseToString(drc[22][3]);
                    sdd.T_PENENTUAN = Utils.TryParseToString(drc[23][3]);
                    sdd.T_CATATAN = Utils.TryParseToString(drc[24][3]);
                    sdd.T_PELAKSANA = Utils.TryParseToString(drc[25][3]);
                }
                else if (Utils.TryParseToString(drc[27][0]).ToUpper().TrimStart().TrimEnd() == "TABEL DEBIT AIR HARIAN ( METER )")
                {
                    sdd.D_PERIODE_PENCATATAN = iTahunData.ToString();
                    sdd.D_CATATAN = Utils.TryParseToString(drc[16][3]);
                    sdd.D_MAKSIMUM_EKSTRIM = Utils.TryParseToString(drc[18][3]);
                    sdd.D_MINIMUM_EKSTRIM = Utils.TryParseToString(drc[19][3]);
                    sdd.D_MAKSIMUM_EKSTRIM_KUMULATIF = Utils.TryParseToString(drc[21][3]);
                    sdd.D_MINIMUM_EKSTRIM_KUMULATIF = Utils.TryParseToString(drc[22][3]);
                    sdd.D_PENENTUAN = Utils.TryParseToString(drc[23][3]);
                    sdd.D_CATATAN = Utils.TryParseToString(drc[24][3]);
                    sdd.D_PELAKSANA = Utils.TryParseToString(drc[25][3]);
                }

                using (var memoryStream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(memoryStream))
                    {
                        // Various for loops etc as necessary that will ultimately do this:
                        int p = 0;
                        for (int j = 1; j <= 12; j++)
                        {
                            for (int i = 29; i <= 59; i++)
                            {
                                p = p + 1;

                                string sDate = j + "/" + p + "/" + iTahunData;
                                if (Utils.TryParseToDateTime(sDate) != null)
                                {
                                    DateTime dt = Convert.ToDateTime(sDate);
                                    writer.WriteLine(string.Format("{0},{1}", dt.ToString("d-MM-yyyy"), drc[i][j]));
                                }
                            }
                            p = 0;
                        }
                    }
                    if (Utils.TryParseToString(drc[27][0]).ToUpper().TrimStart().TrimEnd() == "TABEL TINGGI MUKA AIR HARIAN ( METER )")
                    {
                        sdd.TMA = memoryStream.ToArray();
                    }
                    else
                    {
                        sdd.DEBIT = memoryStream.ToArray();
                    }
                }

                this.StasiunDebitRepo.SaveOrUpdate(sd);
                ViewBag.ImportMessage = "Import Success";
            }
        }
    }
}
