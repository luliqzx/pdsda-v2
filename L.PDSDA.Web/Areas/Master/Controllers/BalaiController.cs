﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.PDSDA.Web.Controllers;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Master.Controllers
{
    public class BalaiController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDefaultRepository<BALAI> BALAIRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<Group> GroupRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AssignBalaiRole(string id)
        {
            BALAI balai = this.BALAIRepository.Get(x => x.KodeBalai == id).FirstOrDefault();
            IQueryable<Group> collGroup = this.GroupRepository.GetAll();
            ViewBag.Balai = balai;
            ViewBag.BalaiRole = balai.Groups;
            return PartialView("AssignBalaiRole", collGroup);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="balai"></param>
        /// <param name="group"></param>
        /// <param name="ischecked"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SetBalaiToRole(string balai, string group, bool ischecked)
        {
            string KodeBalai = balai;
            int Group_Id = Convert.ToInt32(group);
            bool assign = ischecked;
            BALAI Balai = this.BALAIRepository.Get(x => x.KodeBalai == KodeBalai).SingleOrDefault();
            Group Group = this.GroupRepository.Get(x => x.Id == Group_Id).SingleOrDefault();

            if (Balai.Groups != null)
            {
                if (Balai.Groups.Contains(Group))
                {
                    if (!ischecked)
                        Balai.Groups.Remove(Group);
                    else
                        Balai.Groups.Add(Group);
                }
                else
                {
                    Balai.Groups.Add(Group);
                }
            }
            else
            {
                Balai.Groups = new List<Group>();
                Balai.Groups.Add(Group);
            }

            try
            {
                this.BALAIRepository.SaveOrUpdate(Balai);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSuccess = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AssignBalai(string id)
        {
            BALAI balai = this.BALAIRepository.Get(x => x.KodeBalai == id).FirstOrDefault();
            IQueryable<PROPINSI> collPropinsi = this.PROPINSIRepository.GetAll();
            ViewBag.Balai = balai;
            ViewBag.BalaiPropinsi = balai.PROPINSIs;
            return PartialView("AssignBalai", collPropinsi);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="balai"></param>
        /// <param name="propinsi"></param>
        /// <param name="ischecked"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SetBalaiToPropinsi(string balai, string propinsi, bool ischecked)
        {
            string KodeBalai = balai;
            string K_PROPINSI = propinsi;
            bool assign = ischecked;
            BALAI Balai = this.BALAIRepository.Get(x => x.KodeBalai == KodeBalai).SingleOrDefault();
            PROPINSI Propinsi = this.PROPINSIRepository.Get(x => x.K_PROPINSI == K_PROPINSI).SingleOrDefault();

            if (Balai.PROPINSIs != null)
            {
                if (Balai.PROPINSIs.Contains(Propinsi))
                {
                    if (!ischecked)
                        Balai.PROPINSIs.Remove(Propinsi);
                    else
                        Balai.PROPINSIs.Add(Propinsi);
                }
                else
                {
                    Balai.PROPINSIs.Add(Propinsi);
                }
            }
            else
            {
                Balai.PROPINSIs = new List<PROPINSI>();
                Balai.PROPINSIs.Add(Propinsi);
            }

            try
            {
                this.BALAIRepository.SaveOrUpdate(Balai);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSuccess = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AssignBalaiWS(string id)
        {
            BALAI balai = this.BALAIRepository.Get(x => x.KodeBalai == id).FirstOrDefault();
            IQueryable<WS> collWS = this.WSRepository.GetAll();
            ViewBag.Balai = balai;
            ViewBag.BalaiWS = balai.WSs;
            return PartialView("AssignBalaiWs", collWS);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="balai"></param>
        /// <param name="ws"></param>
        /// <param name="ischecked"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SetBalaiToWS(string balai, string ws, bool ischecked)
        {
            string KodeBalai = balai;
            string K_WS = ws;
            bool assign = ischecked;
            BALAI Balai = this.BALAIRepository.Get(x => x.KodeBalai == KodeBalai).SingleOrDefault();
            WS Ws = this.WSRepository.Get(x => x.K_WS == K_WS).SingleOrDefault();

            if (Balai.WSs != null)
            {
                if (Balai.WSs.Contains(Ws))
                {
                    if (!ischecked)
                        Balai.WSs.Remove(Ws);
                    else
                        Balai.WSs.Add(Ws);
                }
                else
                {
                    Balai.WSs.Add(Ws);
                }
            }
            else
            {
                Balai.WSs = new List<WS>();
                Balai.WSs.Add(Ws);
            }

            try
            {
                this.BALAIRepository.SaveOrUpdate(Balai);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSuccess = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="balai"></param>
        /// <param name="ws"></param>
        /// <returns></returns>
        public ActionResult SetAllBalaiToWS(string balai, string[] ws)
        {
            string KodeBalai = balai;
            BALAI Balai = this.BALAIRepository.Get(x => x.KodeBalai == KodeBalai).SingleOrDefault();
            IList<WS> collWS = this.WSRepository.GetAll().ToList();

            for (int i = 0; i < ws.Length; i++)
            {
                if (Balai.WSs != null)
                {
                    WS WS = collWS.SingleOrDefault(x => x.K_WS == ws[i]);
                    if (!Balai.WSs.Contains(WS))
                    {
                        Balai.WSs.Add(WS);
                    }
                }
            }

            try
            {
                this.BALAIRepository.SaveOrUpdate(Balai);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSuccess = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="balai"></param>
        /// <param name="ws"></param>
        /// <returns></returns>
        public ActionResult UnSetAllBalaiToWS(string balai, string[] ws)
        {
            string KodeBalai = balai;
            BALAI Balai = this.BALAIRepository.Get(x => x.KodeBalai == KodeBalai).SingleOrDefault();
            IList<WS> collWS = this.WSRepository.GetAll().ToList();

            for (int i = 0; i < ws.Length; i++)
            {
                if (Balai.WSs != null)
                {
                    WS WS = collWS.SingleOrDefault(x => x.K_WS == ws[i]);
                    if (Balai.WSs.Contains(WS))
                    {
                        Balai.WSs.Remove(WS);
                    }
                }
            }

            try
            {
                this.BALAIRepository.SaveOrUpdate(Balai);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSuccess = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        public BalaiController()
        {
            BALAIRepository = new DefaultRepository<BALAI>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            GroupRepository = new DefaultRepository<Group>();
            WSRepository = new DefaultRepository<WS>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            ViewBag.ActionName = "Create";
            return PartialView("Save");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="formdomain"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(BALAI formdomain)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    this.BALAIRepository.SaveOrUpdate(formdomain);
                    return Json(new { success = true, message = "Data telah disimpan." });
                }
                return Json(new { success = false, message = "Invalid Model." + Pesan.A2A });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            ViewBag.ActionName = "Edit";
            BALAI balai = this.BALAIRepository.Get(x => x.KodeBalai == id).SingleOrDefault();
            return PartialView("Save", balai);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="formDomain"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(string id, BALAI formDomain)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BALAI saveDomain = this.BALAIRepository.Get(x => x.KodeBalai == id).SingleOrDefault();
                    Utils.SetTProperty(saveDomain, formDomain);
                    this.BALAIRepository.Update(saveDomain);
                    return Json(new { success = true, message = "Data telah diubah" });
                }
                return Json(new { success = false, message = "Invalid Model." + Pesan.A2B });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";
            BALAI balai = this.BALAIRepository.Get(x => x.KodeBalai == id).SingleOrDefault();
            return PartialView("Detail", balai);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            BALAI BALAI = this.BALAIRepository.Get(x => x.KodeBalai == id).SingleOrDefault();
            if (BALAI != null)
            {
                this.BALAIRepository.Delete(BALAI);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        /// <summary>
        /// 
        /// </summary>
        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        /// <summary>
        /// 
        /// </summary>
        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<BALAI> collBalai = this.BALAIRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collBalai = collBalai.Where(x => x.KodeBalai.Contains(param.sSearch) || x.NamaBalai.Contains(param.sSearch)
                    //|| (x.PROPINSIs != null && x.PROPINSIs.FirstOrDefault(p => p.N_PROPINSI.Contains(param.sSearch)) != null)
                    || (x.WSs.FirstOrDefault(p => p.N_WS.Contains(param.sSearch)) != null)
                    || (x.Groups.FirstOrDefault(p => p.Name.Contains(param.sSearch)) != null)
                    );
            }
            Count = collBalai.Count();

            var sortColumnIndex = iSortCol;
            Func<BALAI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.KodeBalai :
                                                                sortColumnIndex == 2 ? c.NamaBalai : c.KodeBalai);

            IList<BALAI> collBALAI = new List<BALAI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collBalai.Count() > 0)
            {
                collBALAI = collBalai.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collBALAI = collBALAI.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collBALAI = collBALAI.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }

            var result = from c in collBALAI
                         select new
                         {
                             KEY = c.KodeBalai,
                             KodeBalai = c.KodeBalai,
                             NamaBalai = c.NamaBalai,
                             Propinsi = this.GetPropinsiName(c),//sPropinsi
                             Role = this.GetRoleName(c),
                             Ws = this.GetWsName(c)
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewIndex()
        {
            return View();
        }

        private string GetPropinsiName(BALAI balai)
        {
            string sPropinsi = string.Empty;
            foreach (PROPINSI propinsi in balai.PROPINSIs.OrderBy(x => x.N_PROPINSI))
            {
                if (string.IsNullOrWhiteSpace(sPropinsi))
                {
                    sPropinsi = propinsi.N_PROPINSI;
                }
                else
                {
                    sPropinsi = sPropinsi + ", " + propinsi.N_PROPINSI;
                }
            }
            return sPropinsi;
        }

        private string GetWsName(BALAI balai)
        {
            string sWs = string.Empty;
            if (balai.WSs.Count > 0)
            {
                foreach (WS ws in balai.WSs)
                {
                    if (ws != null)
                    {
                        if (string.IsNullOrWhiteSpace(sWs))
                        {
                            sWs = ws.N_WS;
                        }
                        else
                        {
                            sWs = sWs + ", " + ws.N_WS;
                        }
                    }
                   
                }
            }
            return sWs;
        }

        private string GetRoleName(BALAI balai)
        {
            string sRole = string.Empty;
            if (balai.Groups.Count > 0)
            {
                foreach (Group Group in balai.Groups)
                {
                    if (Group != null)
                    {
                        if (string.IsNullOrWhiteSpace(sRole))
                        {
                            sRole = Group.Name;
                        }
                        else
                        {
                            sRole = sRole + ", " + Group.Name;
                        }
                    }
                    
                }
            }
            return sRole;
        }
    }
}
