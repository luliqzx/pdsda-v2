﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Master.Controllers
{
    public class StatusIrigasiController : BaseController
    {

        private readonly IDefaultRepository<STATUS_IRIGASI> DefaultRepository;
        public StatusIrigasiController()
        {
            this.DefaultRepository = new DefaultRepository<STATUS_IRIGASI>();
        }
        //
        // GET: /Master/StatusWs/

        public ActionResult Index()
        {
            //IQueryable<STATUS_IRIGASI> collStatusWs = this.DefaultRepository.GetAll();
            //return View(collStatusWs);
            return View();
        }

        //
        // GET: /Master/StatusWs/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Master/StatusWs/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(STATUS_IRIGASI domain)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Save(domain);
                }
                return Json(new { success = true, message = "Data telah disimpan." });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Detail(string id="")
        {
            ViewBag.ActionName = "Detail";
            STATUS_IRIGASI domain = this.DefaultRepository.Get(x => x.K_STATUS == id).FirstOrDefault();
            return PartialView("Detail", domain);
        }


        public ActionResult Edit(string id)
        {

            if (!IsUpdateable)
            {
                return this.Detail(id);
            }
            ViewBag.ActionName = "Edit";
            STATUS_IRIGASI domain = this.DefaultRepository.Get(x => x.K_STATUS == id).FirstOrDefault();
            return PartialView("Save", domain);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, STATUS_IRIGASI domain)
        {
            STATUS_IRIGASI saveDomain = this.DefaultRepository.Get(x => x.K_STATUS == id).FirstOrDefault();
            Utils.SetTProperty<STATUS_IRIGASI>(saveDomain, domain);

            try
            {
                // TODO: Add update logic here
                this.DefaultRepository.Update(saveDomain);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = "Data telah diubah" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Master/StatusWs/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            STATUS_IRIGASI domain = this.DefaultRepository.Get(x => x.K_STATUS == id).FirstOrDefault();
            if (domain != null)
            {
                if (domain.IRIGASI_LINTAS != null && domain.IRIGASI_LINTAS.Count > 0)
                {
                    domain.IRIGASI_LINTAS.ToList().ForEach(x => x.STATUS_IRIGASI = null);
                }
                this.DefaultRepository.Delete(domain);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<STATUS_IRIGASI> collDomain = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDomain = collDomain.Where(x => x.K_STATUS.Contains(param.sSearch) || x.N_STATUS.Contains(param.sSearch));
            }
            Count = collDomain.Count();

            var sortColumnIndex = iSortCol;
            Func<STATUS_IRIGASI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_STATUS :
                                                                sortColumnIndex == 2 ? c.N_STATUS :
                                                                c.K_STATUS);

            IList<STATUS_IRIGASI> collResult = new List<STATUS_IRIGASI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_STATUS,
                             c.K_STATUS,
                             c.N_STATUS
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
