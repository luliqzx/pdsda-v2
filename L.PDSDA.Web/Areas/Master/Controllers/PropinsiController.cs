﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using System.Data;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Master.Controllers
{
    public class PropinsiController : BaseController
    {
        //
        // GET: /Master/Propinsi/
        private readonly IDefaultRepository<PROPINSI> DefaultRepository;

        public PropinsiController()
        {
            this.DefaultRepository = new DefaultRepository<PROPINSI>();
        }

        public ActionResult Index()
        {
            IQueryable<PROPINSI> collProp = this.DefaultRepository.GetAll();
            return View(collProp);
        }

        //
        // GET: /Master/Propinsi/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Master/Propinsi/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("");
            }
            return PartialView();
        }

        //
        // POST: /Master/Propinsi/Create

        [HttpPost]
        public ActionResult Create(PROPINSI propinsi)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.SaveOrUpdate(propinsi);
                    return Json(new { success = true, message = "OK" });
                }
            }
            catch (DataException)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return Json(new { success = false, message = "ERROR" });

        }

        //
        // GET: /Master/Propinsi/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }

            PROPINSI propinsi = this.DefaultRepository.GetById(id.ToString());
            return PartialView(propinsi);
        }

        public ActionResult Detail(string id)
        {
            PROPINSI propinsi = this.DefaultRepository.GetById(id.ToString());
            return PartialView("Detail",propinsi);
        }

        //
        // POST: /Master/Propinsi/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, PROPINSI propinsi)
        {
            if (propinsi.K_PROPINSI.Equals(id.ToString()))
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        this.DefaultRepository.SaveOrUpdate(propinsi);
                        return Json(new { success = true, message = Pesan.A1B });
                    }
                }
                catch (DataException)
                {
                    //Log the error (add a variable name after DataException)
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                }
            }
            else
            {
                return Json(new { success = false, message = "Not valid Data" });
            }
            return Json(new { success = false, message = "ERROR" });
        }

        //
        // GET: /Master/Propinsi/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            PROPINSI propinsi = this.DefaultRepository.GetById(id);
            if (propinsi != null)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        this.DefaultRepository.Delete(propinsi);
                        return Json(new { success = true, message = "Data telah dihapus." });
                    }
                    return Json(new { success = true, message = "Data tidak valid." });
                }
                catch (DataException)
                {
                    //Log the error (add a variable name after DataException)
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                }
            }
            else
            {
                return Json(new { success = false, message = "Not valid Data" });
            }
            return Json(new { success = false, message = "ERROR" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<PROPINSI> collDomain = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDomain = collDomain.Where(x => x.K_PROPINSI.Contains(param.sSearch) || x.N_PROPINSI.Contains(param.sSearch));
            }
            Count = collDomain.Count();

            var sortColumnIndex = iSortCol;
            Func<PROPINSI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_PROPINSI :
                                                                sortColumnIndex == 2 ? c.N_PROPINSI :
                                                                c.K_PROPINSI);

            IList<PROPINSI> collResult = new List<PROPINSI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_PROPINSI,
                             c.K_PROPINSI,
                             c.N_PROPINSI
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
