﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using System.Data;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.Core.General;
using log4net;

namespace L.PDSDA.Web.Areas.Master.Controllers
{
    public class KabupatenController : BaseController
    {
        private readonly IDefaultRepository<KABUPATEN> DefaultRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        ILog log = LogManager.GetLogger(typeof(KabupatenController));
        public KabupatenController()
        {
            this.DefaultRepository = new DefaultRepository<KABUPATEN>();
            this.PROPINSIRepository = new DefaultRepository<PROPINSI>();
        }

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = this.PROPINSIRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        //
        // GET: /Master/Kabupaten/


        public ActionResult Index()
        {
            IQueryable<KABUPATEN> collKab = this.DefaultRepository.GetAll();
            return View(collKab);
        }

        //
        // GET: /Master/Kabupaten/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Master/Kabupaten/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("", "");
            }
            BuildPropinsi();
            return PartialView();
        }

        //
        // POST: /Master/Kabupaten/Create

        [HttpPost]
        public ActionResult Create(KABUPATEN kab)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.SaveOrUpdate(kab);
                    return Json(new { success = true, message = "OK" });
                }
            }
            catch (DataException)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return Json(new { success = false, message = "ERROR" });
        }

        //
        // GET: /Master/Kabupaten/Edit/5

        public ActionResult Edit(string propinsiid, string kabupatenid)
        {

            if (!IsUpdateable)
            {
                return this.Detail(propinsiid, kabupatenid);
            }
            //KABUPATEN kab = this.DefaultRepository.GetById(id);
            KABUPATEN kab = this.DefaultRepository.Get(x => x.K_KABUPATEN == kabupatenid && (x.PROPINSI != null && x.PROPINSI.K_PROPINSI == propinsiid)).FirstOrDefault();
            BuildPropinsi();
            return PartialView(kab);
        }


        public ActionResult Detail(string propinsiid, string kabupatenid)
        {


            //KABUPATEN kab = this.DefaultRepository.GetById(id);
            KABUPATEN kab = this.DefaultRepository.Get(x => x.K_KABUPATEN == kabupatenid && (x.PROPINSI != null && x.PROPINSI.K_PROPINSI == propinsiid)).FirstOrDefault();
            BuildPropinsi();
            return PartialView("Detail", kab);
        }


        //
        // POST: /Master/Kabupaten/Edit/5

        [HttpPost]
        public ActionResult Edit(string propinsiid, string kabupatenid, KABUPATEN kab)
        {
            //if (kab.K_KABUPATEN.Equals(id.ToString()))
            //{
            try
            {
                KABUPATEN domainSave = this.DefaultRepository.Get(x => x.K_KABUPATEN == kabupatenid && (x.PROPINSI != null && x.PROPINSI.K_PROPINSI == propinsiid)).FirstOrDefault();

                if (kab.PROPINSI != null && kab.PROPINSI.K_PROPINSI == "-1")
                {
                    kab.PROPINSI = null;
                }

                domainSave.PROPINSI = kab.PROPINSI;

                if (ModelState.IsValid)
                {
                    Utils.SetTProperty<KABUPATEN>(domainSave, kab);
                    this.DefaultRepository.Update(domainSave);
                    return Json(new { success = true, message = "Data telah diubah." });
                }
            }
            catch (DataException)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            //}
            //else
            //{
            //    return Json(new { success = false, message = "Not valid Data" });
            //}
            return Json(new { success = false, message = "ERROR" });
        }

        //
        // GET: /Master/Kabupaten/Delete/5

        public ActionResult Delete(string propinsiid, string kabupatenid)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            //KABUPATEN kab = this.DefaultRepository.GetById(id);
            KABUPATEN kab = this.DefaultRepository.Get(x => x.K_KABUPATEN == kabupatenid && (x.PROPINSI != null && x.PROPINSI.K_PROPINSI == propinsiid)).FirstOrDefault();
            if (kab != null)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        if (kab.BENDUNGs != null && kab.BENDUNGs.Count > 0)
                        {
                            kab.BENDUNGs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.AIRTANAHs != null && kab.AIRTANAHs.Count > 0)
                        {
                            kab.AIRTANAHs.ToList().ForEach(x => x.KABUPATEN = null);
                        }
                        if (kab.BENDUNGANs != null && kab.BENDUNGANs.Count > 0)
                        {
                            kab.BENDUNGANs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.EMBUNG_POTENSI != null && kab.EMBUNG_POTENSI.Count > 0)
                        {
                            kab.EMBUNG_POTENSI.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.EMBUNGs != null && kab.EMBUNGs.Count > 0)
                        {
                            kab.EMBUNGs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.IRIGASIs != null && kab.IRIGASIs.Count > 0)
                        {
                            kab.IRIGASIs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.PIDs != null && kab.PIDs.Count > 0)
                        {
                            kab.PIDs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.RAWAs != null && kab.RAWAs.Count > 0)
                        {
                            kab.RAWAs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.STASIUNDEBITs != null && kab.STASIUNDEBITs.Count > 0)
                        {
                            kab.STASIUNDEBITs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.STASIUNHUJANs != null && kab.STASIUNHUJANs.Count > 0)
                        {
                            kab.STASIUNHUJANs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.STASIUNKLIMATOLOGIs != null && kab.STASIUNKLIMATOLOGIs.Count > 0)
                        {
                            kab.STASIUNKLIMATOLOGIs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        if (kab.DANAUs != null && kab.DANAUs.Count > 0)
                        {
                            kab.DANAUs.ToList().ForEach(x => x.KABUPATEN = null);
                        }

                        log.InfoFormat("KabupatenController Delete {0} by {1} at {2}", kab.N_KABUPATEN, this.UserLoggedIn, this.UserHostAddress);
                        this.DefaultRepository.Delete(kab);
                        return Json(new { success = true, message = Pesan.A1C });
                    }
                    return Json(new { success = true, message = Pesan.A2C });
                }
                catch (DataException)
                {
                    //Log the error (add a variable name after DataException)
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                }
            }
            else
            {
                return Json(new { success = false, message = "Not valid Data" });
            }
            return Json(new { success = false, message = "ERROR" });
        }

        public ActionResult GetKabupatenByPropinsi(string propinsiid)
        {
            IQueryable<KABUPATEN> qryCollKABUPATEN = this.DefaultRepository.Get(x => x.PROPINSI.K_PROPINSI == propinsiid);
            IList<KABUPATEN> collKABUPATEN = qryCollKABUPATEN == null ? new List<KABUPATEN>() : qryCollKABUPATEN.ToList();

            return Json(new SelectList(collKABUPATEN, "K_KABUPATEN", "N_KABUPATEN"), JsonRequestBehavior.AllowGet);
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<KABUPATEN> collDomain = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDomain = collDomain.Where(x => x.K_KABUPATEN.Contains(param.sSearch) || x.N_KABUPATEN.Contains(param.sSearch)
                     || (x.PROPINSI != null && x.PROPINSI.N_PROPINSI.Contains(param.sSearch)));
            }
            Count = collDomain.Count();

            var sortColumnIndex = iSortCol;
            Func<KABUPATEN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_KABUPATEN :
                                                                sortColumnIndex == 2 ? c.N_KABUPATEN :
                                                                sortColumnIndex == 3 ? c.PROPINSI == null ? "" : c.PROPINSI.N_PROPINSI :
                                                                c.K_KABUPATEN);

            IList<KABUPATEN> collResult = new List<KABUPATEN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_KABUPATEN,
                             c.K_KABUPATEN,
                             c.N_KABUPATEN,
                             K_PROPINSI = c.PROPINSI == null ? "" : c.PROPINSI.K_PROPINSI,
                             N_PROPINSI = c.PROPINSI == null ? "" : c.PROPINSI.N_PROPINSI
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
