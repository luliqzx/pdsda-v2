﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using L.Core.Utilities;
using L.PDSDA.Web.Controllers;
using log4net;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Master.Controllers
{
    public class KewenanganController : BaseController
    {
        private readonly IDefaultRepository<KEWENANGAN> DefaultRepository;
        ILog log = LogManager.GetLogger(typeof(KewenanganController));

        public KewenanganController()
        {
            this.DefaultRepository = new DefaultRepository<KEWENANGAN>();
            log.InfoFormat("KewenanganController Initialize by {0} at {1}", this.UserLoggedIn, this.UserHostAddress);
        }


        //
        // GET: /Master/Kewenangan/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(KEWENANGAN domain)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    log.InfoFormat("KewenanganController Save {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, domain.N_KEWENANGAN);
                    this.DefaultRepository.Save(domain);
                    return Json(new { success = true, message = Pesan.A1A });

                }
                return Json(new { success = true, message = Pesan.A2A });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }

            ViewBag.ActionName = "Edit";
            KEWENANGAN domain = DefaultRepository.Get(x => x.K_KEWENANGAN == id).FirstOrDefault();
            return PartialView("Save", domain);
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";
            KEWENANGAN domain = DefaultRepository.Get(x => x.K_KEWENANGAN == id).FirstOrDefault();
            return PartialView("Detail", domain);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, KEWENANGAN domain)
        {
            KEWENANGAN saveDomain = DefaultRepository.Get(x => x.K_KEWENANGAN == id).FirstOrDefault();
            Utils.SetTProperty<KEWENANGAN>(saveDomain, domain);

            try
            {
                if (ModelState.IsValid)
                {
                    // TODO: Add update logic here
                    log.InfoFormat("KewenanganController Update {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, domain.N_KEWENANGAN);
                    this.DefaultRepository.Update(saveDomain);
                    //return RedirectToAction("Index");
                    return Json(new { success = true, message = Pesan.A1B });
                }
                return Json(new { success = true, message = Pesan.A2B });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }

            KEWENANGAN domain = DefaultRepository.Get(x => x.K_KEWENANGAN == id).FirstOrDefault();

            if (domain != null)
            {

                if (domain.IRIGASIs != null)
                {
                    domain.IRIGASIs.ToList().ForEach(x => x.KEWENANGAN = null);
                }

                log.InfoFormat("KewenanganController Delete {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, domain.N_KEWENANGAN);
                this.DefaultRepository.Delete(domain);
                return Json(new { success = true, message = Pesan.A1C });
            }
            return Json(new { success = false, message = Pesan.A2C });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<KEWENANGAN> collDomain = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDomain = collDomain.Where(x => x.K_KEWENANGAN.Contains(param.sSearch) || x.N_KEWENANGAN.Contains(param.sSearch));
            }
            Count = collDomain.Count();

            var sortColumnIndex = iSortCol;
            Func<KEWENANGAN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_KEWENANGAN :
                                                                sortColumnIndex == 2 ? c.N_KEWENANGAN :
                                                                c.K_KEWENANGAN);

            IList<KEWENANGAN> collResult = new List<KEWENANGAN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_KEWENANGAN,
                             c.K_KEWENANGAN,
                             c.N_KEWENANGAN
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
