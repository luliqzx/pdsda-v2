﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities.Web;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Bencana.Controllers
{
    public class DaerahRawanKekeringanController : BaseController
    {
        //
        // GET: /Bencana/DaerahRawanBanjir/
        IDefaultRepository<DAERAHRAWANKEKERINGAN> DefaultRepository;
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;

        public DaerahRawanKekeringanController()
        {
            this.DefaultRepository = new DefaultRepository<DAERAHRAWANKEKERINGAN>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
        }

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        void BuildPropinsi()
        {
            //IQueryable<PROPINSI> collPropinsi = this.PROPINSIRepository.GetAll();
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        public ActionResult Index()
        {
            IQueryable<DAERAHRAWANKEKERINGAN> collDAERAHRAWANKEKERINGAN = this.DefaultRepository.GetAll();
            return View(collDAERAHRAWANKEKERINGAN);
        }

        //
        // GET: /Bencana/DaerahRawanBanjir/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Bencana/DaerahRawanBanjir/Create

        public ActionResult Create()
        {

            if (!IsCreateable)
            {
                return Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildDAS("");
            BuildWS();
            BuildPropinsi();
            return PartialView("Save");
        }

        //
        // POST: /Bencana/DaerahRawanBanjir/Create

        [HttpPost]
        public ActionResult Create(DAERAHRAWANKEKERINGAN domain)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (domain.PROPINSI != null && domain.PROPINSI.K_PROPINSI == "-1")
                    {
                        domain.PROPINSI = null;
                    }
                    if (domain.W != null && domain.W.K_WS == "-1")
                    {
                        domain.W = null;
                    }
                    if (domain.DA != null && domain.DA.K_DAS == "-1")
                    {
                        domain.DA = null;
                    }

                    domain.CreateBy = this.UserLoggedIn;
                    domain.CreateDate = DateTime.Now;
                    domain.CreateTerminal = this.UserHostAddress;
                    domain.UpdateBy = this.UserLoggedIn;
                    domain.UpdateDate = DateTime.Now;
                    domain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.SaveOrUpdate(domain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/DaerahRawanBanjir/Edit/5

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";
            DAERAHRAWANKEKERINGAN domain = DefaultRepository.Get(x => x.K_DAERAHRAWANKEKERINGAN == id).FirstOrDefault();
                BuildDAS(domain != null ? domain.W == null ? "" : domain.W.K_WS : "");

                BuildWS();
                BuildPropinsi();
            return PartialView("Detail", domain);
        }
        public ActionResult Edit(string id)
        {
            ViewBag.ActionName = "Edit";
            DAERAHRAWANKEKERINGAN domain = DefaultRepository.Get(x => x.K_DAERAHRAWANKEKERINGAN == id).FirstOrDefault();
            BuildDAS(domain != null ? domain.W == null ? "" : domain.W.K_WS : "");

            BuildWS();
            BuildPropinsi();

            if (!IsUpdateable)
            {
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", domain);
            }
            if (domain.W != null && !collAssignWS.Contains(domain.W))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = domain.W.N_WS, Value = domain.W.K_WS, Selected = true });
                //ViewBag.collWS = addWStoSelectedListItem;
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", domain);
                //return Detail(id);
            }
            return PartialView("Save", domain);
        }

        //
        // POST: /Bencana/DaerahRawanBanjir/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, DAERAHRAWANKEKERINGAN formDomain)
        {
            if (formDomain.PROPINSI != null && formDomain.PROPINSI.K_PROPINSI == "-1")
            {
                formDomain.PROPINSI = null;
            }
            if (formDomain.W != null && formDomain.W.K_WS == "-1")
            {
                formDomain.W = null;
            }
            if (formDomain.DA != null && formDomain.DA.K_DAS == "-1")
            {
                formDomain.DA = null;
            }
            DAERAHRAWANKEKERINGAN saveDomain = DefaultRepository.Get(x => x.K_DAERAHRAWANKEKERINGAN == id).FirstOrDefault();
            saveDomain.DA = formDomain.DA;
            saveDomain.W = formDomain.W;
            saveDomain.PROPINSI = formDomain.PROPINSI;
            saveDomain.LatLong = formDomain.LatLong;
            Utils.SetTProperty<DAERAHRAWANKEKERINGAN>(saveDomain, formDomain);

            try
            {
                if (ModelState.IsValid)
                {
                    saveDomain.UpdateBy = this.UserLoggedIn;
                    saveDomain.UpdateDate = DateTime.Now;
                    saveDomain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.SaveOrUpdate(saveDomain);
                    return Json(new { success = true, message = "Data telah diubah." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Bencana/DaerahRawanBanjir/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C + ". Harap reload halaman." });
            }
            DAERAHRAWANKEKERINGAN deleteDomain = DefaultRepository.Get(x => x.K_DAERAHRAWANKEKERINGAN == id).FirstOrDefault();

          
            if (deleteDomain != null)
            {
                if (deleteDomain.W != null && !collAssignWS.Contains(deleteDomain.W))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    this.DefaultRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<DAERAHRAWANKEKERINGAN> collDAERAHRAWANKEKERINGAN = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDAERAHRAWANKEKERINGAN = collDAERAHRAWANKEKERINGAN.Where(x => x.K_DAERAHRAWANKEKERINGAN.Contains(param.sSearch) || x.N_DAERAHRAWANKEKERINGAN.Contains(param.sSearch)
                    || (x.PROPINSI != null && x.PROPINSI.N_PROPINSI.Contains(param.sSearch))
                    || (x.W != null && x.W.N_WS.Contains(param.sSearch))
                     || (x.DA != null && x.DA.N_DAS.Contains(param.sSearch)));
            }
            Count = collDAERAHRAWANKEKERINGAN.Count();

            var sortColumnIndex = iSortCol;
            Func<DAERAHRAWANKEKERINGAN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_DAERAHRAWANKEKERINGAN :
                                                                sortColumnIndex == 2 ? c.N_DAERAHRAWANKEKERINGAN :
                                                                sortColumnIndex == 3 ? c.PROPINSI != null ? c.PROPINSI.N_PROPINSI : string.Empty :
                                                                sortColumnIndex == 4 ? c.W != null ? c.W.N_WS : string.Empty :
                                                                sortColumnIndex == 5 ? c.DA != null ? c.DA.N_DAS : string.Empty :
                                                                c.K_DAERAHRAWANKEKERINGAN);


            IList<DAERAHRAWANKEKERINGAN> collResult = new List<DAERAHRAWANKEKERINGAN>();// = collRawaDyn == null ? new List<DAERAHRAWANKEKERINGAN_DYN>() : collRawaDyn.ToList();
            if (collDAERAHRAWANKEKERINGAN.Count() > 0)
            {
                collResult = collDAERAHRAWANKEKERINGAN.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_DAERAHRAWANKEKERINGAN,
                             c.K_DAERAHRAWANKEKERINGAN,
                             c.N_DAERAHRAWANKEKERINGAN,
                             N_PROPINSI = c.PROPINSI != null ? c.PROPINSI.N_PROPINSI : string.Empty,
                             N_WS = c.W != null ? c.W.N_WS : string.Empty,
                             N_DAS = c.DA != null ? c.DA.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.N_DAERAHRAWANKEKERINGAN

                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
