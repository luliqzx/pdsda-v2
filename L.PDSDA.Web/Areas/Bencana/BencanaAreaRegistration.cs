﻿using System.Web.Mvc;

namespace L.PDSDA.Web.Areas.Bencana
{
    public class BencanaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Bencana";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Bencana_default",
                "Bencana/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
