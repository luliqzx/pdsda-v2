﻿using System.Web.Mvc;

namespace L.PDSDA.Web.Areas.Peta
{
    public class PetaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Peta";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Peta_default",
                "Peta/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
