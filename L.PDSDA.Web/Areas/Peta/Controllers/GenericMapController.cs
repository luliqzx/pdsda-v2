﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.DTOs;

namespace L.PDSDA.Web.Areas.Peta.Controllers
{
    public class GenericMapController : Controller
    {
        //
        // GET: /Peta/GenericMap/

        public ActionResult Index(MapDTO MapDTO)
        {
            return View(MapDTO);
        }

        public ActionResult IndexWithClick()
        {
            return View();
        }

    }
}
