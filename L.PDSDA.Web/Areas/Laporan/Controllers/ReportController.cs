﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using L.Core.Utilities.Net;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.Web.SDADataSetTableAdapters;
using L.PDSDA.Web.DIDataSetTableAdapters;
using L.PDSDA.Web.BencanaDataSetTableAdapters;

namespace L.PDSDA.Web.Areas.Laporan.Controllers
{
    public class ReportController : Controller
    {
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;
        private readonly IDefaultRepository<IRIGASI> irigasiRepo;
        private readonly IDefaultRepository<RAWA> rawaRepo;

        /// <summary>
        /// ReportController
        /// </summary>
        public ReportController()
        {
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();
            irigasiRepo = new DefaultRepository<IRIGASI>();
            rawaRepo = new DefaultRepository<RAWA>();
        }

        /// <summary>
        /// GetKabupatenByPropinsi(string propinsiid)
        /// </summary>
        /// <param name="propinsiid"></param>
        /// <returns></returns>
        public ActionResult GetKabupatenByPropinsi(string propinsiid)
        {
            IQueryable<KABUPATEN> qryCollKABUPATEN = this.KABUPATENRepository.Get(x => x.PROPINSI.K_PROPINSI == propinsiid);
            IList<KABUPATEN> collKABUPATEN = qryCollKABUPATEN == null ? new List<KABUPATEN>() : qryCollKABUPATEN.ToList();

            return Json(new SelectList(collKABUPATEN, "K_KABUPATEN", "N_KABUPATEN"), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// BuildPropinsi()
        /// </summary>
        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        /// <summary>
        /// BuildKabupaten(string K_PROPINSI)
        /// </summary>
        /// <param name="K_PROPINSI"></param>
        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }
        //
        // GET: /Laporan/Report/

        /// <summary>
        /// Index()
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            BuildPropinsi();
            BuildKabupaten("");
            return View();
        }

        public ActionResult Test(FormCollection formCollection)
        {
            return View("Index");
        }

        /// <summary>
        /// collReportParameter
        /// </summary>
        IList<ReportParameter> collReportParameter = new List<ReportParameter>();

        /// <summary>
        /// Current in used
        /// </summary>
        /// <param name="formCollection"></param>
        /// 

        string N_KABUPATEN = "";
        string K_PROPINSI = "";
        string K_KABUPATEN = "";
        int TAHUN = DateTime.Now.Year;
        bool bIsNull = false;

        /// <summary>
        /// GeneralReport(FormCollection formCollection)
        /// </summary>
        /// <param name="formCollection"></param>
        [HttpPost]
        public void GeneralReport(FormCollection formCollection)
        {
            //Common.Utils.PosHujanGenerator("Libok", 2009);
            string reportPath = string.Empty;
            object Model = null;
            string dsName = string.Empty;
            LocalReport localReport = new LocalReport();

            ReportParameter rp1 = new ReportParameter();
            rp1.Name = "TAHUN";
            rp1.Values.Add(Common.Utils.TryParseToInt32(formCollection["txtYear"]) == 0 ? DateTime.Now.Year.ToString() : formCollection["txtYear"]);
            TAHUN = Common.Utils.TryParseToInt32(formCollection["txtYear"]);

            PROPINSI prop = this.PROPINSIRepository.Get(x => x.K_PROPINSI == formCollection["ddlPropinsi"]).FirstOrDefault();
            string N_PROPINSI = prop == null ? "" : prop.N_PROPINSI;

            KABUPATEN kab = this.KABUPATENRepository.Get(x => x.K_KABUPATEN == formCollection["ddlKabupaten"] && x.PROPINSI != null && x.PROPINSI.K_PROPINSI == formCollection["ddlPropinsi"]).FirstOrDefault();
            N_KABUPATEN = kab == null ? "" : kab.N_KABUPATEN;
            K_KABUPATEN = formCollection["ddlKabupaten"];

            ReportParameter rp2 = new ReportParameter();
            rp2.Name = "N_PROPINSI";
            rp2.Values.Add(N_PROPINSI);

            ReportParameter rp3 = new ReportParameter();
            rp3.Name = "K_PROPINSI";
            rp3.Values.Add(formCollection["ddlPropinsi"]);
            K_PROPINSI = formCollection["ddlPropinsi"];

            collReportParameter.Add(rp1);
            collReportParameter.Add(rp2);
            collReportParameter.Add(rp3);

            if (formCollection["data"] == "sda")
            {
                SDAReport(formCollection, out dsName, out Model, out reportPath, localReport);
            }
            else if (formCollection["data"] == "di")
            {
                DIReport(formCollection, out dsName, out Model, out reportPath, localReport);
            }
            else if (formCollection["data"] == "hi")
            {
                HDReport(formCollection);
                return;
            }
            else if (formCollection["data"] == "bc")
            {
                BCReport(formCollection, out dsName, out Model, out reportPath);
            }

            if (string.IsNullOrWhiteSpace(reportPath))
            {
                throw new Exception("Report Not Found!!!");
            }

            localReport.ReportPath = reportPath;
            //if (!bIsNull)
            //{
            Utils.RenderReport(localReport, Model, dsName, OutputFormat: "Excel", collParameterReport: collReportParameter);
            //}
        }

        #region Bencana
        private void BCReport(FormCollection formCollection, out string dsName, out object Model, out string reportPath)
        {
            dsName = "";
            Model = null;
            reportPath = "";
            // Belum gunakan parameter
            if (formCollection["kriteria"] == "pb")
            {
                reportPath = Server.MapPath("~/Reports/Bencana/PoskoBanjirReport.rdlc");
                dsName = "PoskoBanjirDataSet";
                Model = new PoskoBanjirTableAdapter().GetData();
            }
            if (formCollection["kriteria"] == "ppb")
            {
                reportPath = Server.MapPath("~/Reports/Bencana/PoskoPengamatReport.rdlc");
                dsName = "PoskoPengamatDataSet";
                Model = new PoskoPengamatTableAdapter().GetData();
            }
            if (formCollection["kriteria"] == "drb")
            {

            }
            if (formCollection["kriteria"] == "drk")
            {
                reportPath = Server.MapPath("~/Reports/Bencana/DaerahRawanKekeringanReport.rdlc");
                dsName = "DaerahRawanKekeringanDataSet";
                Model = new daerahrawankekeringanTableAdapter().GetData();
            }
            if (formCollection["kriteria"] == "drl")
            {
                reportPath = Server.MapPath("~/Reports/Bencana/DaerahRawanLongsorReport.rdlc");
                dsName = "DaerahRawanLongsorDataSet";
                Model = new daerahrawanlongsorTableAdapter().GetData();
            }
            if (formCollection["kriteria"] == "drgb")
            {
                reportPath = Server.MapPath("~/Reports/Bencana/DaerahRawanGunungBerapiReport.rdlc");
                dsName = "DaerahRawanGunungBerapiDataSet";
                Model = new gunungberapiTableAdapter().GetData();
            }
        }
        #endregion Bencana

        #region Hidrologi
        /// <summary>
        /// 
        /// </summary>
        /// <param name="formCollection"></param>
        /// <param name="dsName"></param>
        /// <param name="Model"></param>
        /// <param name="reportPath"></param>
        private void HDReport(FormCollection formCollection)
        {
            string sNamaPos = formCollection["NamaPos"];
            string sTahun = formCollection["txtYear"];

            if (formCollection["kriteria"] == "poshujan")
            {
                Common.HidrologiUtils.PosHujanGenerator(sNamaPos, Common.Utils.TryParseToInt32(sTahun));
            }
            else if (formCollection["kriteria"] == "posdugaair")
            {
                string tipe = formCollection["ddlAir"];
                Common.HidrologiUtils.PosDugaAirGenerator(sNamaPos, Common.Utils.TryParseToInt32(sTahun), tipe);
            }
            else if (formCollection["kriteria"] == "posklimatologi")
            {
                string tipe = formCollection["ddlAir"];
                Common.HidrologiUtils.PosKlimatologiGenerator(sNamaPos, Common.Utils.TryParseToInt32(sTahun));
            }
        }
        #endregion Hidrologi

        #region Daerah Irigasi

        /// <summary>
        /// DIReport(FormCollection formCollection, out string dsName, out object Model, out string reportPath)
        /// </summary>
        /// <param name="formCollection"></param>
        /// <param name="dsName"></param>
        /// <param name="Model"></param>
        /// <param name="reportPath"></param>
        private void DIReport(FormCollection formCollection, out string dsName, out object Model, out string reportPath)
        {
            DIReport(formCollection, out  dsName, out  Model, out  reportPath, null);
        }

        /// <summary>
        /// DIReport(FormCollection formCollection, out string dsName, out object Model, out string reportPath, LocalReport localReport)
        /// </summary>
        /// <param name="formCollection"></param>
        /// <param name="dsName"></param>
        /// <param name="Model"></param>
        /// <param name="reportPath"></param>
        /// <param name="localReport"></param>
        private void DIReport(FormCollection formCollection, out string dsName, out object Model, out string reportPath, LocalReport localReport)
        {
            dsName = "";
            Model = null;
            reportPath = "";
            // Belum gunakan parameter
            if (formCollection["kriteria"] == "ip")
            {
                //localReport = new LocalReport();
                ReportParameter rp4 = new ReportParameter();
                rp4.Name = "N_KABUPATEN";
                rp4.Values.Add(N_KABUPATEN);

                ReportParameter rp5 = new ReportParameter();
                rp5.Name = "K_KABUPATEN";
                rp5.Values.Add(formCollection["ddlKabupaten"]);

                collReportParameter.Add(rp4);
                collReportParameter.Add(rp5);

                reportPath = Server.MapPath("~/Reports/DaerahIrigasi/DIPReport.rdlc");
                dsName = "DataSet1";
                localReport.SubreportProcessing += new SubreportProcessingEventHandler(IPReport_SubreportProcessing);
            }

            if (formCollection["kriteria"] == "id")
            {
                ReportParameter rp4 = new ReportParameter();
                rp4.Name = "N_KABUPATEN";
                rp4.Values.Add(N_KABUPATEN);

                ReportParameter rp5 = new ReportParameter();
                rp5.Name = "K_KABUPATEN";
                rp5.Values.Add(formCollection["ddlKabupaten"]);

                collReportParameter.Add(rp4);
                collReportParameter.Add(rp5);

                reportPath = Server.MapPath("~/Reports/DaerahIrigasi/PIDReport.rdlc");
                dsName = "PIDDataSet";
                //Model = new PIDTableAdapter().GetData();
                Model = new PIDTableAdapter().GetDataBy(formCollection["ddlKabupaten"], formCollection["ddlPropinsi"], Web.Common.Utils.TryParseToInt32(formCollection["txtYear"]));
            }

            // Belum gunakan parameter
            if (formCollection["kriteria"] == "tdlk")
            {
                reportPath = Server.MapPath("~/Reports/DaerahIrigasi/DITadahHujanDanLahanKeringReport.rdlc");
                dsName = "TadahHujanDataSet";
                Model = new tadah_hujanTableAdapter().GetDataBy(formCollection["ddlPropinsi"]);
                //Model = new tadah_hujanTableAdapter().GetData();
            }

            // daftar rawa
            if (formCollection["kriteria"] == "dr")
            {
                //localReport = new LocalReport();
                ReportParameter rp4 = new ReportParameter();
                rp4.Name = "N_KABUPATEN";
                rp4.Values.Add(N_KABUPATEN);

                ReportParameter rp5 = new ReportParameter();
                rp5.Name = "K_KABUPATEN";
                rp5.Values.Add(formCollection["ddlKabupaten"]);

                collReportParameter.Add(rp4);
                collReportParameter.Add(rp5);

                reportPath = Server.MapPath("~/Reports/DaerahIrigasi/DaftarRawaReport.rdlc");
                dsName = "DataSet1";
                localReport.SubreportProcessing += new SubreportProcessingEventHandler(DaftarRawaReport_SubreportProcessing);
            }
        }
        #endregion Daerah Irigasi

        #region SDA
        /// <summary>
        /// SDAReport
        /// </summary>
        /// <param name="formCollection"></param>
        /// <param name="dsName"></param>
        /// <param name="Model"></param>
        /// <param name="reportPath"></param>
        void SDAReport(FormCollection formCollection, out string dsName, out object Model, out string reportPath, LocalReport localReport)
        {
            dsName = "";
            Model = null;
            reportPath = "";
            // Belum gunakan parameter
            if (formCollection["kriteria"] == "sg")
            {
                //reportPath = Server.MapPath("~/Reports/SDA/SungaiReport.rdlc");
                //dsName = "SungaiDataSet";
                //Model = new SungaiTableAdapter().GetData();

                reportPath = Server.MapPath("~/Reports/SDA/Sungai2Report.rdlc");
                dsName = "SungaiDataSet";
                Model = new SungaiTableAdapter().GetData();
            }
            // Belum gunakan parameter
            else if (formCollection["kriteria"] == "at")
            {

                ReportParameter rp4 = new ReportParameter();
                rp4.Name = "N_KABUPATEN";
                rp4.Values.Add(N_KABUPATEN);

                ReportParameter rp5 = new ReportParameter();
                rp5.Name = "K_KABUPATEN";
                rp5.Values.Add(formCollection["ddlKabupaten"]);

                collReportParameter.Add(rp4);
                collReportParameter.Add(rp5);

                reportPath = Server.MapPath("~/Reports/SDA/AirTanahReport.rdlc");
                dsName = "AirTanahDataSet";
                Model = new AirTanahTableAdapter().GetDataByPropKab(formCollection["ddlPropinsi"], formCollection["ddlKabupaten"]);

            }
            else if (formCollection["kriteria"] == "das")
            {
                reportPath = Server.MapPath("~/Reports/SDA/DASReport.rdlc");
                dsName = "DASDataSet";
                Model = new DASTableAdapter().GetData();
            }
            else if (formCollection["kriteria"] == "dn")
            {
                reportPath = Server.MapPath("~/Reports/SDA/DanauReport.rdlc");
                dsName = "DanauDataSet";
                Model = new DanauTableAdapter().GetDataByPropinsi(formCollection["ddlPropinsi"]);
            }
            else if (formCollection["kriteria"] == "bdgn")
            {
                reportPath = Server.MapPath("~/Reports/SDA/BendunganReport.rdlc");
                dsName = "BendunganDataSet";
                Model = new BendunganTableAdapter().GetDataByProp(formCollection["ddlPropinsi"]);//, formCollection["ddlKabupaten"]);

                localReport.SubreportProcessing += new SubreportProcessingEventHandler(localReport_SubreportProcessing);

            }
            else if (formCollection["kriteria"] == "bdg")
            {
                reportPath = Server.MapPath("~/Reports/SDA/BendungReport.rdlc");
                dsName = "BendungDataSet";
                Model = new BendungTableAdapter().GetDataByProp(formCollection["ddlPropinsi"]);
            }
            else if (formCollection["kriteria"] == "eb")
            {
                reportPath = Server.MapPath("~/Reports/SDA/EmbungReport.rdlc");
                dsName = "EmbungDataSet";
                Model = new EmbungTableAdapter().GetDataByPropKab(formCollection["ddlPropinsi"], formCollection["ddlKabupaten"]);
            }
            else if (formCollection["kriteria"] == "ebp")
            {
                reportPath = Server.MapPath("~/Reports/SDA/EmbungPotensiReport.rdlc");
                dsName = "EmbungPotensiDataSet";
                Model = new Embung_PotensiTableAdapter().GetDataByPropKab(formCollection["ddlPropinsi"], formCollection["ddlKabupaten"]);
            }
        }
        #endregion SDA

        /// <summary>
        /// localReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void localReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            string dsName = "BendunganDataSet";
            var Model = new BendunganTableAdapter().GetDataByProp(K_PROPINSI);

            if (Model == null || Model.Rows.Count <= 0)
            {
                Model = new SDADataSet.BendunganDataTableDataTable();
                SDADataSet.BendunganDataTableRow BendunganDataTableRow = Model.NewBendunganDataTableRow();
                BendunganDataTableRow.K_BENDUNGAN = "";
                Model.Rows.Add(BendunganDataTableRow);
            }

            ReportDataSource rds1 = new ReportDataSource();
            rds1.Name = dsName;
            rds1.Value = Model;
            e.DataSources.Add(rds1);
            //ReportDataSource rds2 = new ReportDataSource();
            //rds2.Name = dsName;
            //rds2.Value = Model;
            //e.DataSources.Add(rds2);
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Current not in used
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reportFilePath"></param>
        /// <param name="dataSourceName"></param>
        /// <param name="collData"></param>
        /// <param name="collParameter"></param>
        /// <returns></returns>
        public ActionResult GenerateReport<T>(string reportFilePath, string dataSourceName, IList<T> collData, ReportParameter[] collParameter = null)
        {
            LocalReport localReport = new LocalReport();
            localReport.ReportPath = Server.MapPath(reportFilePath);
            ReportDataSource reportDataSource = new ReportDataSource(dataSourceName, collData);
            localReport.DataSources.Add(reportDataSource);
            if (collParameter != null)
            {
                localReport.SetParameters(collParameter);
            }

            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType

            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return File(renderedBytes, mimeType);
        }

        /// <summary>
        ///  GetNamaPos(string hi)
        /// </summary>
        /// <param name="hi"></param>
        /// <returns></returns>
        public ActionResult GetNamaPos(string hi)
        {
            if (hi == "poshujan")
            {
                IDefaultRepository<STASIUNHUJAN> hujanRepo = new DefaultRepository<STASIUNHUJAN>();
                IQueryable<STASIUNHUJAN> qrySH = hujanRepo.GetAll();
                if (qrySH != null)
                {
                    IList<STASIUNHUJAN> collSH = qrySH.ToList();
                    return Json(new SelectList(collSH, "K_STASIUN", "N_STASIUN"), JsonRequestBehavior.AllowGet);
                }
            }
            else if (hi == "posdugaair")
            {
                IDefaultRepository<STASIUNDEBIT> debitREpo = new DefaultRepository<STASIUNDEBIT>();
                IQueryable<STASIUNDEBIT> qrySH = debitREpo.GetAll();
                if (qrySH != null)
                {
                    IList<STASIUNDEBIT> collSH = qrySH.ToList();
                    return Json(new SelectList(collSH, "K_STASIUN", "N_STASIUN"), JsonRequestBehavior.AllowGet);
                }
            }

            else if (hi == "posklimatologi")
            {
                string k_kabupaten = Common.Utils.GetQueryString("k_kabupaten");
                string k_propinsi = Common.Utils.GetQueryString("k_propinsi");
                KABUPATEN kab = new DefaultRepository<KABUPATEN>().Get(x => x.K_KABUPATEN == k_kabupaten && x.PROPINSI.K_PROPINSI == k_propinsi).FirstOrDefault();

                IDefaultRepository<STASIUNKLIMATOLOGI> kliREpo = new DefaultRepository<STASIUNKLIMATOLOGI>();
                IQueryable<STASIUNKLIMATOLOGI> qrySH = kliREpo.GetAll().Where(x => x.KABUPATEN == kab).AsQueryable();
                if (qrySH != null)
                {
                    IList<STASIUNKLIMATOLOGI> collSH = qrySH.ToList();
                    return Json(new SelectList(collSH, "K_STASIUN", "N_STASIUN"), JsonRequestBehavior.AllowGet);
                }
            }
            return null;
        }

        #region Irigasi Pemerintah
        /// <summary>
        /// IPReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void IPReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            #region DI
            IList<DTOs.DIPemerintahDTO> lstDIPDto = new List<DTOs.DIPemerintahDTO>();
            IQueryable<IRIGASI> qryIrigasi = irigasiRepo.Get(x => x.KABUPATEN.K_KABUPATEN == K_KABUPATEN && x.KABUPATEN.PROPINSI.K_PROPINSI == K_PROPINSI);
            IList<IRIGASI> lstIrigasi = qryIrigasi == null ? new List<IRIGASI>() : qryIrigasi.ToList();
            for (int i = 0; i < lstIrigasi.Count; i++)
            {
                IRIGASI irigasi = lstIrigasi[i];
                this.PortingEntityToDTOIrigasi(irigasi, lstDIPDto);
            }
            DTOs.DTOCollection DTOCollection = new DTOs.DTOCollection();
            #endregion DI

            if (lstDIPDto.Count > 0)
            {

            }
            else
            {
                bIsNull = true;
            }
            string dsName = "DataSet1";
            var Model = DTOCollection.GetDIPemerintahDTO(lstDIPDto);
            ReportDataSource rds1 = new ReportDataSource();
            rds1.Name = dsName;
            rds1.Value = Model;
            e.DataSources.Add(rds1);
            //ReportDataSource rds2 = new ReportDataSource();
            //rds2.Name = dsName;
            //rds2.Value = Model;
            //e.DataSources.Add(rds2);
            //throw new NotImplementedException();
        }

        #region Irigasi
        private void PortingEntityToDTOIrigasi(IRIGASI irigasi, IList<DTOs.DIPemerintahDTO> lstDIPDto)
        {
            IList<IRIGASI_DYN> lstIrigasiDyn = irigasi.IRIGASI_DYN == null ? null : irigasi.IRIGASI_DYN.Where(x => x.TAHUN_DATA == TAHUN).ToList();
            if (lstIrigasiDyn != null && lstIrigasiDyn.Count > 0)
            {
                for (int i = 0; i < lstIrigasiDyn.Count; i++)
                {
                    DTOs.DIPemerintahDTO DIPemerintahDTO = new DTOs.DIPemerintahDTO();
                    this.PortIrigasiToDTO(irigasi, DIPemerintahDTO);
                    this.PortIrigasiDynToDTO(lstIrigasiDyn[i], DIPemerintahDTO);
                    lstDIPDto.Add(DIPemerintahDTO);
                }
            }
            else
            {
                DTOs.DIPemerintahDTO DIPemerintahDTO = new DTOs.DIPemerintahDTO();
                this.PortIrigasiToDTO(irigasi, DIPemerintahDTO);
                lstDIPDto.Add(DIPemerintahDTO);
            }

            #region Irigasi Tanam
            IList<IRIGASI_TANAM> lstIrigasiTanam = irigasi.IRIGASI_TANAM == null ? null : irigasi.IRIGASI_TANAM.Where(x => x.TAHUN_DATA == TAHUN).ToList();
            if (lstIrigasiTanam != null && lstIrigasiTanam.Count > 0)
            {
                for (int i = 0; i < lstIrigasiTanam.Count; i++)
                {
                    IRIGASI_TANAM it = lstIrigasiTanam[i];
                    DTOs.DIPemerintahDTO DIPemerintahDTO = lstDIPDto.FirstOrDefault(x => x.K_DI == it.IRIGASI.K_DI && x.TAHUN_DATA == it.TAHUN_DATA);
                    if (DIPemerintahDTO == null)
                    {
                        DIPemerintahDTO = new DTOs.DIPemerintahDTO();
                        this.PortIrigasiToDTO(irigasi, DIPemerintahDTO);
                    }
                    else
                    {
                        lstDIPDto.Remove(DIPemerintahDTO);
                    }
                    this.PortIrigasiTanamToDTO(it, DIPemerintahDTO);
                    lstDIPDto.Add(DIPemerintahDTO);
                }
            }
            #endregion Irigasi Tanam
        }

        private void PortIrigasiTanamToDTO(IRIGASI_TANAM it, DTOs.DIPemerintahDTO DIPemerintahDTO)
        {
            DIPemerintahDTO.IP = it.IP;
            DIPemerintahDTO.MT1_LAIN = it.MT1_LAIN;
            DIPemerintahDTO.MT1_PD = it.MT1_PD;
            DIPemerintahDTO.MT1_PL = it.MT1_PL;
            DIPemerintahDTO.MT2_LAIN = it.MT2_LAIN;
            DIPemerintahDTO.MT2_PD = it.MT2_PD;
            DIPemerintahDTO.MT2_PL = it.MT2_PL;
            DIPemerintahDTO.MT3_LAIN = it.MT3_LAIN;
            DIPemerintahDTO.MT3_PD = it.MT3_PD;
            DIPemerintahDTO.MT3_PL = it.MT3_PL;
            DIPemerintahDTO.PRED_IP = it.PRED_IP;
            DIPemerintahDTO.PRED_MT1_LAIN = it.PRED_MT1_LAIN;
            DIPemerintahDTO.PRED_MT1_PD = it.PRED_MT1_PD;
            DIPemerintahDTO.PRED_MT1_PL = it.PRED_MT1_PL;
            DIPemerintahDTO.PRED_MT2_LAIN = it.PRED_MT2_LAIN;
            DIPemerintahDTO.PRED_MT2_PD = it.PRED_MT2_PD;
            DIPemerintahDTO.PRED_MT2_PL = it.PRED_MT2_PL;
            DIPemerintahDTO.PRED_MT3_LAIN = it.PRED_MT3_LAIN;
            DIPemerintahDTO.PRED_MT3_PD = it.PRED_MT3_PD;
            DIPemerintahDTO.PRED_MT3_PL = it.PRED_MT3_PL;
            DIPemerintahDTO.TAHUN_DATA = it.TAHUN_DATA;
        }

        private void PortIrigasiDynToDTO(IRIGASI_DYN iRIGASI_DYN, DTOs.DIPemerintahDTO DIPemerintahDTO)
        {
            DIPemerintahDTO.ANGGOTA = iRIGASI_DYN.ANGGOTA;
            DIPemerintahDTO.BADAN_HUKUM = iRIGASI_DYN.BADAN_HUKUM;
            DIPemerintahDTO.BAGI_B = iRIGASI_DYN.BAGI_B;
            DIPemerintahDTO.BAGI_JML = iRIGASI_DYN.BAGI_JML;
            DIPemerintahDTO.BAGI_RB = iRIGASI_DYN.BAGI_RB;
            DIPemerintahDTO.BAGI_RR = iRIGASI_DYN.BAGI_RR;

            DIPemerintahDTO.BAGIP_B = iRIGASI_DYN.BAGIP_B;
            DIPemerintahDTO.BAGIP_JML = iRIGASI_DYN.BAGIP_JML;
            DIPemerintahDTO.BAGIP_RB = iRIGASI_DYN.BAGIP_RB;
            DIPemerintahDTO.BAGIP_RR = iRIGASI_DYN.BAGIP_RR;

            DIPemerintahDTO.BEBAS_B = iRIGASI_DYN.BEBAS_B;
            DIPemerintahDTO.BEBAS_JML = iRIGASI_DYN.BEBAS_JML;
            DIPemerintahDTO.BEBAS_RB = iRIGASI_DYN.BEBAS_RB;
            DIPemerintahDTO.BEBAS_RR = iRIGASI_DYN.BEBAS_RR;

            DIPemerintahDTO.BEBASP_B = iRIGASI_DYN.BEBASP_B;
            DIPemerintahDTO.BEBASP_JML = iRIGASI_DYN.BEBASP_JML;
            DIPemerintahDTO.BEBASP_RB = iRIGASI_DYN.BEBASP_RB;
            DIPemerintahDTO.BEBASP_RR = iRIGASI_DYN.BEBASP_RR;

            DIPemerintahDTO.BG_B = iRIGASI_DYN.BG_B;
            DIPemerintahDTO.BG_JML = iRIGASI_DYN.BG_JML;
            DIPemerintahDTO.BG_RB = iRIGASI_DYN.BG_RB;
            DIPemerintahDTO.BG_RR = iRIGASI_DYN.BG_RR;

            DIPemerintahDTO.BGP_B = iRIGASI_DYN.BGP_B;
            DIPemerintahDTO.BGP_JML = iRIGASI_DYN.BGP_JML;
            DIPemerintahDTO.BGP_RB = iRIGASI_DYN.BGP_RB;
            DIPemerintahDTO.BGP_RR = iRIGASI_DYN.BGP_RR;

            DIPemerintahDTO.BJAR_BSAWAH = iRIGASI_DYN.BJAR_BSAWAH;
            DIPemerintahDTO.BJAR_SAWAH = iRIGASI_DYN.BJAR_SAWAH;
            DIPemerintahDTO.BSADAP_B = iRIGASI_DYN.BSADAP_B;
            DIPemerintahDTO.BSADAP_JML = iRIGASI_DYN.BSADAP_JML;
            DIPemerintahDTO.BSADAP_RB = iRIGASI_DYN.BSADAP_RB;
            DIPemerintahDTO.BSADAP_RR = iRIGASI_DYN.BSADAP_RR;

            DIPemerintahDTO.BSADAPP_B = iRIGASI_DYN.BSADAPP_B;
            DIPemerintahDTO.BSADAPP_JML = iRIGASI_DYN.BSADAPP_JML;
            DIPemerintahDTO.BSADAPP_RB = iRIGASI_DYN.BSADAPP_RB;
            DIPemerintahDTO.BSADAPP_RR = iRIGASI_DYN.BSADAPP_RR;

            DIPemerintahDTO.BT_B = iRIGASI_DYN.BT_B;
            DIPemerintahDTO.BT_JML = iRIGASI_DYN.BT_JML;
            DIPemerintahDTO.BT_RB = iRIGASI_DYN.BT_RB;
            DIPemerintahDTO.BT_RR = iRIGASI_DYN.BT_RR;

            DIPemerintahDTO.BTP_B = iRIGASI_DYN.BTP_B;
            DIPemerintahDTO.BTP_JML = iRIGASI_DYN.BTP_JML;
            DIPemerintahDTO.BTP_RB = iRIGASI_DYN.BTP_RB;
            DIPemerintahDTO.BTP_RR = iRIGASI_DYN.BTP_RR;

            DIPemerintahDTO.DEBIT_KENYATAAN = iRIGASI_DYN.DEBIT_KENYATAAN;
            DIPemerintahDTO.DEBIT_RENCANA = iRIGASI_DYN.DEBIT_RENCANA;

            DIPemerintahDTO.GEND_GOR_B = iRIGASI_DYN.GEND_GOR_B;
            DIPemerintahDTO.GEND_GOR_JML = iRIGASI_DYN.GEND_GOR_JML;
            DIPemerintahDTO.GEND_GOR_RB = iRIGASI_DYN.GEND_GOR_RB;
            DIPemerintahDTO.GEND_GOR_RR = iRIGASI_DYN.GEND_GOR_RR;

            DIPemerintahDTO.GEND_LAIN_B = iRIGASI_DYN.GEND_LAIN_B;
            DIPemerintahDTO.GEND_LAIN_JML = iRIGASI_DYN.GEND_LAIN_JML;
            DIPemerintahDTO.GEND_LAIN_RB = iRIGASI_DYN.GEND_LAIN_RB;
            DIPemerintahDTO.GEND_LAIN_RR = iRIGASI_DYN.GEND_LAIN_RR;

            DIPemerintahDTO.GEND_LAINP_B = iRIGASI_DYN.GEND_LAINP_B;
            DIPemerintahDTO.GEND_LAINP_JML = iRIGASI_DYN.GEND_LAINP_JML;
            DIPemerintahDTO.GEND_LAINP_RB = iRIGASI_DYN.GEND_LAINP_RB;
            DIPemerintahDTO.GEND_LAINP_RR = iRIGASI_DYN.GEND_LAINP_RR;

            DIPemerintahDTO.GEND_TERJUN_B = iRIGASI_DYN.GEND_TERJUN_B;
            DIPemerintahDTO.GEND_TERJUN_JML = iRIGASI_DYN.GEND_TERJUN_JML;
            DIPemerintahDTO.GEND_TERJUN_RB = iRIGASI_DYN.GEND_TERJUN_RB;
            DIPemerintahDTO.GEND_TERJUN_RR = iRIGASI_DYN.GEND_TERJUN_RR;

            DIPemerintahDTO.GOR_B = iRIGASI_DYN.GOR_B;
            DIPemerintahDTO.GOR_JML = iRIGASI_DYN.GOR_JML;
            DIPemerintahDTO.GOR_PEMB_B = iRIGASI_DYN.GOR_PEMB_B;
            DIPemerintahDTO.GOR_PEMB_JML = iRIGASI_DYN.GOR_PEMB_JML;
            DIPemerintahDTO.GOR_PEMB_RB = iRIGASI_DYN.GOR_PEMB_RB;
            DIPemerintahDTO.GOR_PEMB_RR = iRIGASI_DYN.GOR_PEMB_RR;

            DIPemerintahDTO.GOR_PEMBP_B = iRIGASI_DYN.GOR_PEMBP_B;
            DIPemerintahDTO.GOR_PEMBP_JML = iRIGASI_DYN.GOR_PEMBP_JML;
            DIPemerintahDTO.GOR_PEMBP_RB = iRIGASI_DYN.GOR_PEMBP_RB;
            DIPemerintahDTO.GOR_PEMBP_RR = iRIGASI_DYN.GOR_PEMBP_RR;

            DIPemerintahDTO.GOT_B = iRIGASI_DYN.GOT_B;
            DIPemerintahDTO.GOT_JML = iRIGASI_DYN.GOT_JML;
            DIPemerintahDTO.GOT_RB = iRIGASI_DYN.GOT_RB;
            DIPemerintahDTO.GOT_RR = iRIGASI_DYN.GOT_RR;

            DIPemerintahDTO.GOTP_B = iRIGASI_DYN.GOTP_B;
            DIPemerintahDTO.GOTP_JML = iRIGASI_DYN.GOTP_JML;
            DIPemerintahDTO.GOTP_RB = iRIGASI_DYN.GOTP_RB;
            DIPemerintahDTO.GOTP_RR = iRIGASI_DYN.GOTP_RR;


        }

        private void PortIrigasiToDTO(IRIGASI irigasi, DTOs.DIPemerintahDTO DIPemerintahDTO)
        {
            DIPemerintahDTO.DESA = irigasi.DESA;
            DIPemerintahDTO.K_DI = irigasi.K_DI;
            DIPemerintahDTO.KECAMATAN = irigasi.KECAMATAN;
            DIPemerintahDTO.N_DI = irigasi.N_DI;
        }
        #endregion Irigasi
        #endregion Irigasi Pemerintah

        #region Daftar Rawa
        /// <summary>
        /// DaftarRawaReport_SubreportProcessing
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">SubreportProcessingEventArgs</param>
        void DaftarRawaReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            #region DI
            IList<DTOs.RawaDTO> lstDRDto = new List<DTOs.RawaDTO>();
            IQueryable<RAWA> qryRawa = rawaRepo.Get(x => x.KABUPATEN.K_KABUPATEN == K_KABUPATEN && x.KABUPATEN.PROPINSI.K_PROPINSI == K_PROPINSI);
            IList<RAWA> lstRawa = qryRawa == null ? new List<RAWA>() : qryRawa.ToList();
            for (int i = 0; i < lstRawa.Count; i++)
            {
                RAWA rawa = lstRawa[i];
                this.PortingEntityToDTORawa(rawa, lstDRDto);
            }
            DTOs.DTOCollection DTOCollection = new DTOs.DTOCollection();
            #endregion DI

            if (lstDRDto.Count > 0)
            {

            }
            else
            {
                bIsNull = true;
            }
            string dsName = "DataSet1";
            var Model = DTOCollection.GetRawaDTO(lstDRDto);
            ReportDataSource rds1 = new ReportDataSource();
            rds1.Name = dsName;
            rds1.Value = Model;
            e.DataSources.Add(rds1);
        }

        private void PortingEntityToDTORawa(RAWA rawa, IList<DTOs.RawaDTO> lstRAWADTO)
        {
            IList<RAWA_DYN> lstRawaDyn = rawa.RAWA_DYNs.ToList();
            if (lstRawaDyn != null && lstRawaDyn.Count > 0)
            {
                for (int i = 0; i < lstRawaDyn.Count; i++)
                {
                    DTOs.RawaDTO RawaDTO = new DTOs.RawaDTO();
                    this.PortRawaToDTO(rawa, RawaDTO);
                    this.PortRawaDynToDTO(lstRawaDyn[i], RawaDTO);
                    lstRAWADTO.Add(RawaDTO);
                }
            }
            else
            {
                DTOs.RawaDTO RawaDTO = new DTOs.RawaDTO();
                this.PortRawaToDTO(rawa, RawaDTO);
                lstRAWADTO.Add(RawaDTO);
            }
        }

        private void PortRawaDynToDTO(RAWA_DYN rAWA_DYN, DTOs.RawaDTO RawaDTO)
        {
            RawaDTO.BANGUN_ATUR = rAWA_DYN.BANGUN_ATUR;
            RawaDTO.BANGUN_ATUR2 = rAWA_DYN.BANGUN_ATUR2;
            RawaDTO.BANGUN_LAIN = rAWA_DYN.BANGUN_LAIN;
            RawaDTO.BANGUN_LAIN2 = rAWA_DYN.BANGUN_LAIN2;
            RawaDTO.BIAYA_INVESTASI1 = rAWA_DYN.BIAYA_INVESTASI1;
            RawaDTO.BIAYA_INVESTASI2 = rAWA_DYN.BIAYA_INVESTASI2;
            RawaDTO.BLM_DESAIN = rAWA_DYN.BLM_DESAIN;
            RawaDTO.JML_LAHAN1 = rAWA_DYN.JML_LAHAN1;
            RawaDTO.JML_LAHAN2 = rAWA_DYN.JML_LAHAN2;
            RawaDTO.JML_TANI1 = rAWA_DYN.JML_TANI1;
            RawaDTO.JML_TANI2 = rAWA_DYN.JML_TANI2;
            //RawaDTO.K_RAWA = rAWA_DYN.K_RAWA;
            RawaDTO.LAIN_LAIN = rAWA_DYN.LAIN_LAIN;
            RawaDTO.LRAWA_DESAIN = rAWA_DYN.LRAWA_DESAIN;
            RawaDTO.LastModifiedOn = rAWA_DYN.LastModifiedOn;
            RawaDTO.LRAWA_FUNGSIONAL = rAWA_DYN.LRAWA_FUNGSIONAL;
            RawaDTO.LRAWA_POTENSIAL = rAWA_DYN.LRAWA_POTENSIAL;
            RawaDTO.LRAWA_REKLAMASI = rAWA_DYN.LRAWA_REKLAMASI;
            RawaDTO.LTOTALRAWA_FUNGSIMANFAAT = rAWA_DYN.LTOTALRAWA_FUNGSIMANFAAT;
            RawaDTO.LTOTALRAWA_PEMERINTAH = rAWA_DYN.LTOTALRAWA_PEMERINTAH;
            RawaDTO.LTOTALRAWA_SWASTA = rAWA_DYN.LTOTALRAWA_SWASTA;
            RawaDTO.LUASLAHAN_SWASTA = rAWA_DYN.LUASLAHAN_SWASTA;
            RawaDTO.NON_FUNGSI = rAWA_DYN.NON_FUNGSI;
            RawaDTO.NON_POTENSIAL = rAWA_DYN.NON_POTENSIAL;
            RawaDTO.PERIKANAN = rAWA_DYN.PERIKANAN;
            RawaDTO.PERKEBUNAN = rAWA_DYN.PERKEBUNAN;
            RawaDTO.PERTANIAN = rAWA_DYN.PERTANIAN;
            RawaDTO.S_IKAN1 = rAWA_DYN.S_IKAN1;
            RawaDTO.S_IKAN2 = rAWA_DYN.S_IKAN2;
            RawaDTO.S_LAIN1 = rAWA_DYN.S_LAIN1;
            RawaDTO.S_LAIN2 = rAWA_DYN.S_LAIN2;
            RawaDTO.S_KEBUN1 = rAWA_DYN.S_KEBUN1;
            RawaDTO.S_KEBUN2 = rAWA_DYN.S_KEBUN2;
            RawaDTO.S_TANI1 = rAWA_DYN.S_TANI1;
            RawaDTO.S_TANI2 = rAWA_DYN.S_TANI2;
            RawaDTO.SAL_PRIMER_SWASTA = rAWA_DYN.SAL_PRIMER_SWASTA;
            RawaDTO.SALURAN_NAVIGASI_MGUNA1 = rAWA_DYN.SALURAN_NAVIGASI_MGUNA1;
            RawaDTO.SALURAN_NAVIGASI_MGUNA2 = rAWA_DYN.SALURAN_NAVIGASI_MGUNA2;
            RawaDTO.SALURAN_PRI_TAMBAK1 = rAWA_DYN.SALURAN_PRI_TAMBAK1;
            RawaDTO.SALURAN_PRI_TAMBAK2 = rAWA_DYN.SALURAN_PRI_TAMBAK2;
            RawaDTO.SALURAN_PRIMER1 = rAWA_DYN.SALURAN_PRIMER1;
            RawaDTO.SALURAN_PRIMER2 = rAWA_DYN.SALURAN_PRIMER2;
            RawaDTO.SALURAN_SEKUNDER1 = rAWA_DYN.SALURAN_SEKUNDER1;
            RawaDTO.SALURAN_SEKUNDER2 = rAWA_DYN.SALURAN_SEKUNDER2;
            RawaDTO.SALURAN_TERSIER1 = rAWA_DYN.SALURAN_TERSIER1;
            RawaDTO.SALURAN_TERSIER2 = rAWA_DYN.SALURAN_TERSIER2;
            RawaDTO.SIAP_REKLAMASI = rAWA_DYN.SIAP_REKLAMASI;
            RawaDTO.SUMBER_DANA1 = rAWA_DYN.SUMBER_DANA1;
            RawaDTO.SUMBER_DANA2 = rAWA_DYN.SUMBER_DANA2;
            RawaDTO.TAHUN_DATA = rAWA_DYN.TAHUN_DATA;
            RawaDTO.TANGGUL = rAWA_DYN.TANGGUL;
            RawaDTO.TANGGUL2 = rAWA_DYN.TANGGUL2;
            RawaDTO.TANI_GARAP_SWASTA = rAWA_DYN.TANI_GARAP_SWASTA;
            RawaDTO.THN_BANGUN_SWASTA = rAWA_DYN.THN_BANGUN_SWASTA;
            RawaDTO.THN_BANGUN1 = rAWA_DYN.THN_BANGUN1;
            RawaDTO.THN_BANGUN2 = rAWA_DYN.THN_BANGUN2;
            RawaDTO.TOTAL_TAHAP1 = rAWA_DYN.TOTAL_TAHAP1;
            RawaDTO.TOTAL_TAHAP2 = rAWA_DYN.TOTAL_TAHAP2;
            RawaDTO.TRANS_LOKALLAHAN1 = rAWA_DYN.TRANS_LOKALLAHAN1;
            RawaDTO.TRANS_LOKALLAHAN2 = rAWA_DYN.TRANS_LOKALLAHAN2;
            RawaDTO.TRANS_LOKALTANI1 = rAWA_DYN.TRANS_LOKALTANI1;
            RawaDTO.TRANS_LOKALTANI2 = rAWA_DYN.TRANS_LOKALTANI2;
            RawaDTO.TRANS_UMUMLAHAN1 = rAWA_DYN.TRANS_UMUMLAHAN1;
            RawaDTO.TRANS_UMUMLAHAN2 = rAWA_DYN.TRANS_UMUMLAHAN2;
            RawaDTO.TRANS_UMUMTANI1 = rAWA_DYN.TRANS_UMUMTANI1;
            RawaDTO.TRANS_UMUMTANI2 = rAWA_DYN.TRANS_UMUMTANI2;


            //throw new NotImplementedException(); // Arif Edit
        }

        private void PortRawaToDTO(RAWA rawa, DTOs.RawaDTO RawaDTO)
        {
            // Arif Edit
            RawaDTO.DAS = rawa.DAS == null ? "" : rawa.DAS.N_DAS;
            RawaDTO.DESA = rawa.DESA;
            RawaDTO.WS = rawa.WS == null ? "" : rawa.WS.N_WS;

            RawaDTO.JENIS = rawa.JENIS;
            RawaDTO.K_RAWA = rawa.K_RAWA;
            RawaDTO.KABUPATEN = rawa.KABUPATEN.N_KABUPATEN;
            RawaDTO.KECAMATAN = rawa.KECAMATAN;
            RawaDTO.N_RAWA = rawa.N_RAWA;

        }
        #endregion Daftar Rawa
    }
}
