﻿using System.Web.Mvc;

namespace L.PDSDA.Web.Areas.Laporan
{
    public class LaporanAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Laporan";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Laporan_default",
                "Laporan/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
