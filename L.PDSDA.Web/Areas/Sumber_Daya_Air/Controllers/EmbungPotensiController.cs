﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class EmbungPotensiController : BaseController
    {
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        private void BuildPotensi()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "Embung Baru / Pemerintah", Value = "1" });
            collSelectListItem.Add(new SelectListItem { Text = "Embung Pemerintah Rehabilitasi", Value = "2" });
            collSelectListItem.Add(new SelectListItem { Text = "Embung Desa Rehabilitasi", Value = "3" });
            collSelectListItem.Add(new SelectListItem { Text = "Embung Rakyat Rehabilitasi", Value = "4" });
            //new EMBUNG_POTENSI().JENIS_PENANGANAN
            ViewBag.collJENIS_PENANGANAN = collSelectListItem;
        }


        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }
        //
        // GET: /Sumber_Daya_Air/EMBUNG_POTENSIPotensi/

        private readonly IDefaultRepository<EMBUNG_POTENSI> DefaultRepository;

        public EmbungPotensiController()
        {
            this.DefaultRepository = new DefaultRepository<EMBUNG_POTENSI>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();
        }

        public ActionResult Index()
        {
            //IQueryable<EMBUNG_POTENSI> collRawa = this.DefaultRepository.GetAll();
            //return View(collRawa);
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {

            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildDAS("");
            BuildWS();
            BuildPropinsi();
            BuildKabupaten("");
            BuildPotensi();
            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(EMBUNG_POTENSI formDomain)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (formDomain.KABUPATEN != null && formDomain.KABUPATEN.K_KABUPATEN == "-1")
                    {
                        formDomain.KABUPATEN = null;
                    }
                    if (formDomain.PROPINSI != null && formDomain.PROPINSI.K_PROPINSI == "-1")
                    {
                        formDomain.PROPINSI = null;
                    }
                    if (formDomain.WS != null && formDomain.WS.K_WS == "-1")
                    {
                        formDomain.WS = null;
                    }
                    if (formDomain.DAS != null && formDomain.DAS.K_DAS == "-1")
                    {
                        formDomain.DAS = null;
                    }
                    formDomain.CreateBy = this.UserLoggedIn;
                    formDomain.CreateDate = DateTime.Now;
                    formDomain.CreateTerminal = this.UserHostAddress;
                    formDomain.UpdateBy = this.UserLoggedIn;
                    formDomain.UpdateDate = DateTime.Now;
                    formDomain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.SaveOrUpdate(formDomain);

                }
                return Json(new { success = true, message = "Data telah disimpan." });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }
            ViewBag.ActionName = "Edit";
            EMBUNG_POTENSI EMBUNG_POTENSI = DefaultRepository.Get(x => x.K_EMBUNG_POTENSI == id).FirstOrDefault();
            BuildWS();
            BuildDAS(EMBUNG_POTENSI.WS == null ? "" : EMBUNG_POTENSI.WS.K_WS);
            BuildPropinsi();
            BuildKabupaten(EMBUNG_POTENSI.KABUPATEN == null ? "" : EMBUNG_POTENSI.KABUPATEN.PROPINSI == null ? "" : EMBUNG_POTENSI.KABUPATEN.PROPINSI.K_PROPINSI);
            BuildPotensi();
            if (EMBUNG_POTENSI.WS != null && !collAssignWS.Contains(EMBUNG_POTENSI.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = EMBUNG_POTENSI.WS.N_WS, Value = EMBUNG_POTENSI.WS.K_WS, Selected = true });
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", EMBUNG_POTENSI);
                //return Detail(id);
            }
          
            return PartialView("Save", EMBUNG_POTENSI);
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";
            EMBUNG_POTENSI EMBUNG_POTENSI = DefaultRepository.Get(x => x.K_EMBUNG_POTENSI == id).FirstOrDefault();
            BuildDAS(EMBUNG_POTENSI!=null?EMBUNG_POTENSI.WS == null ? "" : EMBUNG_POTENSI.WS.K_WS:"");
            BuildWS();
            BuildPropinsi();
            BuildKabupaten(EMBUNG_POTENSI!=null?EMBUNG_POTENSI.KABUPATEN == null ? "" : EMBUNG_POTENSI.KABUPATEN.PROPINSI == null ? "" : EMBUNG_POTENSI.KABUPATEN.PROPINSI.K_PROPINSI:"");
            BuildPotensi();
            return PartialView("Detail", EMBUNG_POTENSI);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, EMBUNG_POTENSI formDomain)
        {
            if (formDomain.KABUPATEN != null && formDomain.KABUPATEN.K_KABUPATEN == "-1")
            {
                formDomain.KABUPATEN = null;
            }
            if (formDomain.PROPINSI != null && formDomain.PROPINSI.K_PROPINSI == "-1")
            {
                formDomain.PROPINSI = null;
            }
            if (formDomain.WS != null && formDomain.WS.K_WS == "-1")
            {
                formDomain.WS = null;
            }
            if (formDomain.DAS != null && formDomain.DAS.K_DAS == "-1")
            {
                formDomain.DAS = null;
            }
            EMBUNG_POTENSI EMBUNG_POTENSI = DefaultRepository.Get(x => x.K_EMBUNG_POTENSI == id).FirstOrDefault();
            EMBUNG_POTENSI.DAS = formDomain.DAS;
            EMBUNG_POTENSI.WS = formDomain.WS;
            EMBUNG_POTENSI.PROPINSI = formDomain.PROPINSI;
            EMBUNG_POTENSI.KABUPATEN = formDomain.KABUPATEN;
            Utils.SetTProperty<EMBUNG_POTENSI>(EMBUNG_POTENSI, formDomain);
            EMBUNG_POTENSI.LatLong = formDomain.LatLong;

            try
            {
                // TODO: Add update logic here
                EMBUNG_POTENSI.UpdateBy = this.UserLoggedIn;
                EMBUNG_POTENSI.UpdateDate = DateTime.Now;
                EMBUNG_POTENSI.UpdateTerminal = this.UserHostAddress;
                this.DefaultRepository.SaveOrUpdate(EMBUNG_POTENSI);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = "Data telah diubah" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            EMBUNG_POTENSI deleteDomain = DefaultRepository.Get(x => x.K_EMBUNG_POTENSI == id).FirstOrDefault();
            
            if (deleteDomain != null)
            {
                if (deleteDomain.WS != null && !collAssignWS.Contains(deleteDomain.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                this.DefaultRepository.Delete(deleteDomain);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<EMBUNG_POTENSI> collEMBUNG_POTENSI = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collEMBUNG_POTENSI = collEMBUNG_POTENSI.Where(x => x.K_EMBUNG_POTENSI.Contains(param.sSearch) || x.N_EMBUNG_POTENSI.Contains(param.sSearch)
                    || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)))
                    || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
                     || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)));
            }
            Count = collEMBUNG_POTENSI.Count();

            var sortColumnIndex = iSortCol;
            Func<EMBUNG_POTENSI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_EMBUNG_POTENSI :
                                                                sortColumnIndex == 2 ? c.N_EMBUNG_POTENSI :
                                                                sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                c.K_EMBUNG_POTENSI);


            IList<EMBUNG_POTENSI> collResult = new List<EMBUNG_POTENSI>();// = collRawaDyn == null ? new List<EMBUNG_POTENSI_DYN>() : collRawaDyn.ToList();
            if (collEMBUNG_POTENSI.Count() > 0)
            {
                collResult = collEMBUNG_POTENSI.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_EMBUNG_POTENSI,
                             c.K_EMBUNG_POTENSI,
                             c.N_EMBUNG_POTENSI,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.N_EMBUNG_POTENSI
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
