﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using log4net;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Repositories.Data.SumberDayaAir;
using L.PDSDA.DAL.Repositories.Data.SumberDayaAir.Implements;
using NHibernate;
using NHibernate.Exceptions;
using L.Core.Utilities;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class RawaDYNController : BaseController
    {
        private readonly IRawaDynRepository RawaDynRepository;
        private readonly IDefaultRepository<RAWA> RAWARepository;

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Request["iSortCol_0"]); }
        }

        protected string iSortDir
        {
            get { return Request["sSortDir_0"]; }
        }

        ILog log = LogManager.GetLogger(typeof(RawaDYNController));

        public RawaDYNController()
        {

            this.RawaDynRepository = new RawaDynRepository();
            RAWARepository = new DefaultRepository<RAWA>();
        }


        //
        // GET: /Sumber_Daya_Air/RawaDYN/

        string k_rawa = string.Empty;
        public ActionResult Index()
        {
            k_rawa = this.HttpContext.Request.QueryString["id"];
            ViewData["hfK_RAWA"] = k_rawa;

            RAWA RAWA = this.RAWARepository.Get(x => x.K_RAWA == k_rawa).FirstOrDefault();
            if (RAWA != null)
            {
                if (RAWA.WS != null && !collAssignWS.Contains(RAWA.WS))
                {
                    ViewBag.IsViewable = "False";
                    ViewBag.IsCreateable = "False";
                    ViewBag.IsUpdateable = "False";
                    ViewBag.IsDeleteable = "False";
                    ViewBag.Method = Utils.GetPostRequest("method");
                    return PartialView();
                }
            }

            ViewBag.IsViewable = Utils.GetPostRequest("IsViewable");
            ViewBag.IsCreateable = Utils.GetPostRequest("IsCreateable");
            ViewBag.IsUpdateable = Utils.GetPostRequest("IsUpdateable");
            ViewBag.IsDeleteable = Utils.GetPostRequest("IsDeleteable");
            ViewBag.Method = Utils.GetPostRequest("method");
            return PartialView();

        }

        public ActionResult LoadData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<RAWA_DYN> collRawaDyn = this.RawaDynRepository.Get(x => x.RAWA.K_RAWA == this.HttpContext.Request.QueryString["k_rawa"]);
            Count = collRawaDyn.Count();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collRawaDyn = collRawaDyn.Where(x => x.RAWA.N_RAWA.Contains(param.sSearch) || x.TAHUN_DATA.ToString().Contains(param.sSearch));
            }

            var sortColumnIndex = iSortCol;
            Func<RAWA_DYN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.RAWA.K_RAWA :
                                                                sortColumnIndex == 2 ? c.RAWA.N_RAWA :
                                                                sortColumnIndex == 3 ? c.TAHUN_DATA.ToString() : c.TAHUN_DATA.ToString());


            IList<RAWA_DYN> collRAWA_DYN = new List<RAWA_DYN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collRawaDyn.Count() > 0)
            {
                collRAWA_DYN = collRawaDyn.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collRAWA_DYN = collRAWA_DYN.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collRAWA_DYN = collRAWA_DYN.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collRAWA_DYN
                         select new
                         {
                             KEY = c.TAHUN_DATA.ToString() + "," + c.RAWA.K_RAWA,
                             TAHUN_DATA = c.TAHUN_DATA.ToString(),
                             K_RAWA = c.RAWA.K_RAWA,
                             N_RAWA = c.RAWA.N_RAWA
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Sumber_Daya_Air/RawaDYN/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/RawaDYN/Create

        public ActionResult Create()
        {

            if (!IsCreateable)
            {
                return this.Detail("");
            }
            string k_rawa = this.HttpContext.Request.QueryString["k_rawa"];
            RAWA_DYN RAWA_DYN = new RAWA_DYN();
            RAWA_DYN.K_RAWA = k_rawa;
            //RAWA_DYN.RAWA = this.RAWARepository.Get(x => x.K_RAWA == k_rawa).FirstOrDefault();
            return PartialView("Save", RAWA_DYN);
        }

        //
        // POST: /Sumber_Daya_Air/RawaDYN/Create

        [HttpPost]
        public ActionResult Create(RAWA_DYN formDomain)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    //formDomain.RAWA = this.RAWARepository.Get(x => x.K_RAWA == formDomain.RAWA.K_RAWA).FirstOrDefault();
                    formDomain.RAWA.K_RAWA = formDomain.K_RAWA;
                    this.RawaDynRepository.SaveOrUpdate(formDomain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/RawaDYN/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }

            string[] arr = id.Split(',');
            RAWA_DYN RAWA_DYN = this.RawaDynRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[0]) && x.RAWA.K_RAWA == arr[1]).FirstOrDefault();
            return PartialView("Save", RAWA_DYN);
            //return View();
        }
        public ActionResult Detail(string id)
        {
            string[] arr = id.Split(',');
            RAWA_DYN RAWA_DYN = string.IsNullOrWhiteSpace(id) ? null : this.RawaDynRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[0]) && x.RAWA.K_RAWA == arr[1]).FirstOrDefault();
            return PartialView("Detail", RAWA_DYN);
            //return View();
        }

        //
        // POST: /Sumber_Daya_Air/RawaDYN/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, RAWA_DYN domain)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string[] arr = id.Split(',');
                    RAWA_DYN formSave = this.RawaDynRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[0]) && x.RAWA.K_RAWA == arr[1]).FirstOrDefault();
                    Utils.SetTProperty(formSave, domain);
                    this.RawaDynRepository.Update(formSave);
                    return Json(new { success = true, message = "Data telah diubah." });
                }
            }
            catch (DataException)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return Json(new { success = false, message = "ERROR" });
        }

        //
        // GET: /Sumber_Daya_Air/RawaDYN/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            string[] arr = id.Split(',');
            RAWA_DYN deleteRAWA_DYN = this.RawaDynRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[0]) && x.RAWA.K_RAWA == arr[1]).FirstOrDefault();

            if (deleteRAWA_DYN != null)
            {
                if (deleteRAWA_DYN.RAWA != null && deleteRAWA_DYN.RAWA.WS != null && !collAssignWS.Contains(deleteRAWA_DYN.RAWA.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    this.RawaDynRepository.Delete(deleteRAWA_DYN);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        //
        // POST: /Sumber_Daya_Air/RawaDYN/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
