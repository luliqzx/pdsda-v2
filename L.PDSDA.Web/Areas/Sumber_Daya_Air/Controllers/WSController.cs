﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories;
using L.PDSDA.DAL.Repositories.Implements;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities;
using L.PDSDA.DAL.Repositories.Base;
using L.Core.Utilities.Web;
using L.Core.General;
using log4net;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class WSController : BaseController
    {
        private readonly IWSRepository WSRepository;
        private readonly IDefaultRepository<STATUS_WS> statusWsRepo;

        ILog log = LogManager.GetLogger(typeof(WSController));

        public WSController()
        {
            this.WSRepository = new WSRepository();
            this.statusWsRepo = new DefaultRepository<STATUS_WS>();
            log.InfoFormat("WSController Initialize by {0} at {1}", this.UserLoggedIn, this.UserHostAddress);
        }

        //
        // GET: /Sumber_Daya_Air/WS/

        public ActionResult NotPermit()
        {
            return GenericView("~/Views/Shared/NotPermit");
        }

        public ActionResult Index()
        {
            string key = Utils.Encrypt(this.CodeMenu + "|" + DateTime.Now.GetHashCode());
            ViewBag.Key = key;
            //IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            //return View(collWS);
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/WS/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("");
            }
            //string key = Utils.GetQueryString("key");
            //if (string.IsNullOrEmpty(key))
            //{
            //    //HttpContext.Response.Redirect("", true);
            //    return GenericView("~/Views/Shared/NotPermit");
            //}
            //else
            //{
            //    try
            //    {
            //        string plainText = Utils.Decrypt(key);
            //        string[] strArr = plainText.Split('|');
            //        if (strArr[0] != this.CodeMenu)
            //        {
            //            return GenericView("~/Views/Shared/NotPermit");
            //        }
            //    }
            //    catch (Exception)
            //    {
            //        return GenericView("~/Views/Shared/HttpError500");
            //    }

            //}
            string urlRedirect = string.Empty;
            if (!IsPermitCUD(out urlRedirect))
            {
                return GenericView(urlRedirect);
            }

            ViewBag.ActionName = "Create";

            IQueryable<STATUS_WS> collStatusWs = this.statusWsRepo.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collStatusWs)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_STATUS, Value = item.K_STATUS });
            }
            ViewBag.StatusWS = collSelectListItem;

            return PartialView("Save");



            //return PartialView("Save");

        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(WS ws)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (ws.STATUS_WS.K_STATUS == "-1")
                    {
                        ws.STATUS_WS = null;
                    }

                    WS w = WSRepository.Get(x => x.K_WS == ws.K_WS).FirstOrDefault();
                    if (w == null)
                    {
                        log.InfoFormat("WSController Save {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, ws.N_WS);
                        ws.CreateBy = this.UserLoggedIn;
                        ws.CreateDate = DateTime.Now;
                        ws.CreateTerminal = this.UserHostAddress;
                        ws.UpdateBy = this.UserLoggedIn;
                        ws.UpdateDate = DateTime.Now;
                        ws.UpdateTerminal = this.UserHostAddress;
                        this.WSRepository.Save(ws);
                        return Json(new { success = true, message = Pesan.A1A });
                    }
                }
                return Json(new { success = false, message = Pesan.A2A });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }
            string urlRedirect = string.Empty;
            if (!IsPermitCUD(out urlRedirect))
            {
                return GenericView(urlRedirect);
            }

            ViewBag.ActionName = "Edit";
            IQueryable<STATUS_WS> collStatusWs = this.statusWsRepo.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collStatusWs)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_STATUS, Value = item.K_STATUS });
            }
            ViewBag.StatusWS = collSelectListItem;

            WS ws = WSRepository.Get(x => x.K_WS == id).FirstOrDefault();
            return PartialView("Save", ws);

            //return PartialView(ws);
        }

        public ActionResult Detail(string id)
        {
            string urlRedirect = string.Empty;
            ViewBag.ActionName = "Detail";

            WS ws = WSRepository.Get(x => x.K_WS == id).FirstOrDefault();
            return PartialView("Detail", ws);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, WS ws)
        {
            WS WS = WSRepository.Get(x => x.K_WS == id).FirstOrDefault();
            if (ws.STATUS_WS != null && ws.STATUS_WS.K_STATUS == "-1")
            {
                ws.STATUS_WS = null;
            }
            WS.STATUS_WS = ws.STATUS_WS;
            Utils.SetTProperty<WS>(WS, ws);

            try
            {
                // TODO: Add update logic here
                log.InfoFormat("WSController Update {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, WS.N_WS);
                WS.UpdateBy = this.UserLoggedIn;
                WS.UpdateDate = DateTime.Now;
                WS.UpdateTerminal = this.UserHostAddress;
                this.WSRepository.Update(WS);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = Pesan.A1B });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }

            if (IsDeleteable)
            {
                string urlRedirect = string.Empty;
                if (!IsPermitCUD(out urlRedirect))
                {
                    return GenericView(urlRedirect);
                }

                WS ws = this.WSRepository.Get(x => x.K_WS == id).FirstOrDefault();
                if (ws != null)
                {
                    log.InfoFormat("WSController Delete {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, ws.N_WS);
                    SetNullRelation(ws);
                    this.WSRepository.Delete(ws);
                    return Json(new { success = true, message = Pesan.A1C }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, message = Pesan.A2C }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return GenericView("~/Views/Shared/NotPermit");
            }

        }

        private void SetNullRelation(WS ws)
        {
            if (ws.DASes != null)
                ws.DASes.ToList().ForEach(x => x.WS = null);
            if (ws.AIRTANAHs != null)
                ws.AIRTANAHs.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.BENDUNGANs != null)
                ws.BENDUNGANs.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.BENDUNGs != null)
                ws.BENDUNGs.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.DAERAHRAWANBANJIRs != null)
                ws.DAERAHRAWANBANJIRs.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.DAERAHRAWANKEKERINGANs != null)
                ws.DAERAHRAWANKEKERINGANs.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.DAERAHRAWANLONGSORs != null)
                ws.DAERAHRAWANLONGSORs.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.DANAUs != null)
                ws.DANAUs.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.EMBUNG_POTENSI != null)
                ws.EMBUNG_POTENSI.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.EMBUNGs != null)
                ws.EMBUNGs.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.GUNUNGBERAPIs != null)
                ws.GUNUNGBERAPIs.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.IRIGASIs != null)
                ws.IRIGASIs.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.PIDs != null)
                ws.PIDs.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.POS_PENGAMAT != null)
                ws.POS_PENGAMAT.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.POSKO_BANJIR != null)
                ws.POSKO_BANJIR.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.RAWAs != null)
                ws.RAWAs.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.STASIUNDEBITs != null)
                ws.STASIUNDEBITs.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.STASIUNHUJANs != null)
                ws.STASIUNHUJANs.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.STASIUNKLIMATOLOGIs != null)
                ws.STASIUNKLIMATOLOGIs.ToList().ForEach(x => { x.W = null; x.DA = null; });
            if (ws.SUNGAIs != null)
                ws.SUNGAIs.ToList().ForEach(x => { x.WS = null; x.DAS = null; });
            if (ws.BALAIs != null)
                ws.BALAIs.ToList().ForEach(x => { x.WSs = null; });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<WS> collWs = this.WSRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collWs = collWs.Where(x => x.K_WS.Contains(param.sSearch) || x.N_WS.Contains(param.sSearch) || x.KOTA.Contains(param.sSearch) || (x.STATUS_WS != null && x.STATUS_WS.N_STATUS.Contains(param.sSearch)));
            }
            Count = collWs.Count();

            var sortColumnIndex = iSortCol;
            Func<WS, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_WS :
                                                                sortColumnIndex == 2 ? c.N_WS :
                                                                sortColumnIndex == 3 ? c.STATUS_WS != null ? c.STATUS_WS.N_STATUS : string.Empty :
                                                                sortColumnIndex == 4 ? c.KOTA : c.K_WS);


            IList<WS> collWS = new List<WS>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collWs.Count() > 0)
            {
                collWS = collWs.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collWS = collWS.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collWS = collWS.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collWS
                         select new
                         {
                             KEY = c.K_WS,
                             K_WS = c.K_WS,
                             N_WS = c.N_WS,
                             N_STATUS = c.STATUS_WS == null ? "" : c.STATUS_WS.N_STATUS,
                             KOTA = c.KOTA
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
