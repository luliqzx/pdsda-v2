﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using log4net.Core;
using log4net;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class AirTanahController : BaseController
    {
        //
        // GET: /Sumber_Daya_Air/AirTanah/

        private readonly IDefaultRepository<AIRTANAH> DefaultRepository;
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;
        ILog log = LogManager.GetLogger(typeof(AirTanahController));

        public AirTanahController()
        {
            this.DefaultRepository = new DefaultRepository<AIRTANAH>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();
            log.Info("Initialize AirTanahController " + this.UserLoggedIn);
        }

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        public ActionResult Index()
        {
            //IQueryable<AIRTANAH> collRawa = this.DefaultRepository.GetAll();
            //return View(collRawa);
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {


            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildDAS("");
            BuildWS();
            BuildPropinsi();
            BuildKabupaten("");
            
            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(AIRTANAH at)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (at.KABUPATEN != null && at.KABUPATEN.K_KABUPATEN == "-1")
                    {
                        at.KABUPATEN = null;
                    }
                    if (at.PROPINSI != null && at.KABUPATEN.PROPINSI.K_PROPINSI == "-1")
                    {
                        at.PROPINSI = null;
                    }
                    if (at.WS != null && at.WS.K_WS == "-1")
                    {
                        at.WS = null;
                    }
                    if (at.DAS != null && at.DAS.K_DAS == "-1")
                    {
                        at.DAS = null;
                    }
                    at.CreateBy = this.UserLoggedIn;
                    at.CreateDate = DateTime.Now;
                    at.CreateTerminal = this.UserHostAddress;
                    at.UpdateBy = this.UserLoggedIn;
                    at.UpdateDate = DateTime.Now;
                    at.UpdateTerminal = this.UserHostAddress;
                    log.DebugFormat("Save AIRTANAH {0} by {1}", at, this.UserLoggedIn);

                    this.DefaultRepository.Save(at);

                }
                return Json(new { success = true, message = "Data telah disimpan." });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {


            if (!IsUpdateable)
            {
                return this.Detail(id);
            }

            AIRTANAH AIRTANAH = DefaultRepository.Get(x => x.K_AIRTANAH == id).FirstOrDefault();
            BuildWS();
            BuildDAS(AIRTANAH.WS == null ? "" : AIRTANAH.WS.K_WS);
            BuildPropinsi();
            BuildKabupaten(AIRTANAH.PROPINSI == null ? "" : AIRTANAH.PROPINSI.K_PROPINSI);
            if (!IsUpdateable)
            {
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", AIRTANAH);
            }
            else if (AIRTANAH.WS != null && !collAssignWS.Contains(AIRTANAH.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = AIRTANAH.WS.N_WS, Value = AIRTANAH.WS.K_WS, Selected = true });
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", AIRTANAH);
                //return Detail(id);
            }
            ViewBag.ActionName = "Edit";
            return PartialView("Save", AIRTANAH);
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";
            AIRTANAH AIRTANAH = DefaultRepository.Get(x => x.K_AIRTANAH == id).FirstOrDefault();
            BuildDAS(AIRTANAH!=null?AIRTANAH.WS == null ? "" : AIRTANAH.WS.K_WS:"");
            BuildWS();
            BuildPropinsi();
            BuildKabupaten(AIRTANAH!=null?AIRTANAH.PROPINSI == null ? "" : AIRTANAH.PROPINSI.K_PROPINSI:"");
            return PartialView("Detail", AIRTANAH);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, AIRTANAH at)
        {
            if (at.KABUPATEN != null && at.KABUPATEN.K_KABUPATEN == "-1")
            {
                at.KABUPATEN = null;
            }
            if (at.PROPINSI != null && at.KABUPATEN.PROPINSI.K_PROPINSI == "-1")
            {
                at.PROPINSI = null;
            }
            if (at.WS != null && at.WS.K_WS == "-1")
            {
                at.WS = null;
            }
            if (at.DAS != null && at.DAS.K_DAS == "-1")
            {
                at.DAS = null;
            }

            at.UpdateBy = this.UserLoggedIn;
            at.UpdateDate = DateTime.Now;
            at.UpdateTerminal = this.UserHostAddress;

            AIRTANAH AIRTANAH = DefaultRepository.Get(x => x.K_AIRTANAH == id).FirstOrDefault();
            AIRTANAH.DAS = at.DAS;
            AIRTANAH.WS = at.WS;
            AIRTANAH.PROPINSI = at.PROPINSI;
            AIRTANAH.KABUPATEN = at.KABUPATEN;
            AIRTANAH.LatLong = at.LatLong;
            Utils.SetTProperty<AIRTANAH>(AIRTANAH, at);

            try
            {
                // TODO: Add update logic here
                log.DebugFormat("Update {0} by {1}", AIRTANAH, this.UserLoggedIn);
                this.DefaultRepository.Update(AIRTANAH);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = "Data telah diubah" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            AIRTANAH deleteDomain = DefaultRepository.Get(x => x.K_AIRTANAH == id).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (deleteDomain.WS != null && !collAssignWS.Contains(deleteDomain.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                this.DefaultRepository.Delete(deleteDomain);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<AIRTANAH> collAIRTANAH = this.DefaultRepository.GetAll();

            if (PROPINSI != null)
            {
                collAIRTANAH = collAIRTANAH.Where(x => (x.KABUPATEN != null && x.KABUPATEN.PROPINSI == PROPINSI));
            }

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collAIRTANAH = collAIRTANAH.Where(x => x.K_AIRTANAH.Contains(param.sSearch) || x.NO_SUMUR.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch))
                    || (x != null && x.WS.N_WS.Contains(param.sSearch))
                     || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)));
            }
            Count = collAIRTANAH.Count();

            var sortColumnIndex = iSortCol;
            Func<AIRTANAH, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_AIRTANAH :
                                                                sortColumnIndex == 2 ? c.NO_SUMUR :
                                                               sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                c.K_AIRTANAH);

            IList<AIRTANAH> collAirTanah = new List<AIRTANAH>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collAIRTANAH.Count() > 0)
            {
                collAirTanah = collAIRTANAH.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collAirTanah = collAirTanah.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collAirTanah = collAirTanah.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collAirTanah
                         select new
                         {
                             KEY = c.K_AIRTANAH,
                             c.K_AIRTANAH,
                             c.NO_SUMUR,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.NO_SUMUR
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }


    }
}
