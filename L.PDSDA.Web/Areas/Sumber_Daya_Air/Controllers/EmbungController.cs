﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities.Web;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class EmbungController : BaseController
    {
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;
        //
        // GET: /Sumber_Daya_Air/Embung/

        private readonly IDefaultRepository<EMBUNG> DefaultRepository;

        public EmbungController()
        {
            this.DefaultRepository = new DefaultRepository<EMBUNG>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();
        }

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        private void BuildPotensi()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "Embung Baru / Pemerintah", Value = "1" });
            collSelectListItem.Add(new SelectListItem { Text = "Embung Pemerintah Rehabilitasi", Value = "2" });
            collSelectListItem.Add(new SelectListItem { Text = "Embung Desa Rehabilitasi", Value = "3" });
            collSelectListItem.Add(new SelectListItem { Text = "Embung Rakyat Rehabilitasi", Value = "4" });
            //new EMBUNG_POTENSI().JENIS_PENANGANAN
            ViewBag.collJENIS_PENANGANAN = collSelectListItem;
        }

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        public ActionResult Index()
        {
            IQueryable<EMBUNG> collRawa = this.DefaultRepository.GetAll();
            return View(collRawa);
        }



        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {

            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildWS();
            BuildDAS("");
            BuildPropinsi();
            BuildKabupaten("");
            BuildPotensi();
            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(EMBUNG formDomain)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (formDomain.KABUPATEN != null && formDomain.KABUPATEN.K_KABUPATEN == "-1")
                    {
                        formDomain.KABUPATEN = null;
                    }
                    if (formDomain.PROPINSI != null && formDomain.PROPINSI.K_PROPINSI == "-1")
                    {
                        formDomain.PROPINSI = null;
                    }
                    if (formDomain.WS != null && formDomain.WS.K_WS == "-1")
                    {
                        formDomain.WS = null;
                    }
                    if (formDomain.DAS != null && formDomain.DAS.K_DAS == "-1")
                    {
                        formDomain.DAS = null;
                    }
                    formDomain.CreateBy = this.UserLoggedIn;
                    formDomain.CreateDate = DateTime.Now;
                    formDomain.CreateTerminal = this.UserHostAddress;
                    formDomain.UpdateBy = this.UserLoggedIn;
                    formDomain.UpdateDate = DateTime.Now;
                    formDomain.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.SaveOrUpdate(formDomain);

                }
                return Json(new { success = true, message = "Data telah disimpan." });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }
            ViewBag.ActionName = "Edit";
            EMBUNG EMBUNG = DefaultRepository.Get(x => x.K_EMBUNG == id).FirstOrDefault();
            BuildWS();
            BuildDAS(EMBUNG.WS == null ? "" : EMBUNG.WS.K_WS);
            BuildPropinsi();
            BuildKabupaten(EMBUNG.KABUPATEN == null ? "" : EMBUNG.KABUPATEN.PROPINSI == null ? "" : EMBUNG.KABUPATEN.PROPINSI.K_PROPINSI);
            BuildPotensi();
            if (EMBUNG.WS != null && !collAssignWS.Contains(EMBUNG.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = EMBUNG.WS.N_WS, Value = EMBUNG.WS.K_WS, Selected = true });
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", EMBUNG);
                //return Detail(id);
            }

            return PartialView("Save", EMBUNG);
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";
            EMBUNG EMBUNG = DefaultRepository.Get(x => x.K_EMBUNG == id).FirstOrDefault();
            BuildWS();
            BuildDAS(EMBUNG != null ? EMBUNG.WS == null ? "" : EMBUNG.WS.K_WS : "");
            BuildPropinsi();
            BuildKabupaten(EMBUNG != null ? EMBUNG.KABUPATEN == null ? "" : EMBUNG.KABUPATEN.PROPINSI == null ? "" : EMBUNG.KABUPATEN.PROPINSI.K_PROPINSI : "");
            BuildPotensi();
            return PartialView("Detail", EMBUNG);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, EMBUNG formDomain)
        {
            if (formDomain.KABUPATEN != null && formDomain.KABUPATEN.K_KABUPATEN == "-1")
            {
                formDomain.KABUPATEN = null;
            }
            if (formDomain.PROPINSI != null && formDomain.PROPINSI.K_PROPINSI == "-1")
            {
                formDomain.PROPINSI = null;
            }
            if (formDomain.WS != null && formDomain.WS.K_WS == "-1")
            {
                formDomain.WS = null;
            }
            if (formDomain.DAS != null && formDomain.DAS.K_DAS == "-1")
            {
                formDomain.DAS = null;
            }
            EMBUNG EMBUNG = DefaultRepository.Get(x => x.K_EMBUNG == id).FirstOrDefault();
            EMBUNG.DAS = formDomain.DAS;
            EMBUNG.WS = formDomain.WS;
            EMBUNG.PROPINSI = formDomain.PROPINSI;
            EMBUNG.KABUPATEN = formDomain.KABUPATEN;
            Utils.SetTProperty<EMBUNG>(EMBUNG, formDomain);
            EMBUNG.LatLong = formDomain.LatLong;

            try
            {
                // TODO: Add update logic here
                EMBUNG.UpdateBy = this.UserLoggedIn;
                EMBUNG.UpdateDate = DateTime.Now;
                EMBUNG.UpdateTerminal = this.UserHostAddress;
                this.DefaultRepository.SaveOrUpdate(EMBUNG);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = "Data telah diubah" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            EMBUNG deleteDomain = DefaultRepository.Get(x => x.K_EMBUNG == id).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (deleteDomain.WS != null && !collAssignWS.Contains(deleteDomain.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                this.DefaultRepository.Delete(deleteDomain);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<EMBUNG> collEMBUNG = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collEMBUNG = collEMBUNG.Where(x => x.K_EMBUNG.Contains(param.sSearch) || x.N_EMBUNG.Contains(param.sSearch)
                    || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)))
                    || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
                     || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)));
            }
            Count = collEMBUNG.Count();

            var sortColumnIndex = iSortCol;
            Func<EMBUNG, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_EMBUNG :
                                                                sortColumnIndex == 2 ? c.N_EMBUNG :
                                                                sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                c.K_EMBUNG);


            IList<EMBUNG> collEmbung = new List<EMBUNG>();// = collRawaDyn == null ? new List<EMBUNG_DYN>() : collRawaDyn.ToList();
            if (collEMBUNG.Count() > 0)
            {
                collEmbung = collEMBUNG.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collEmbung = collEmbung.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collEmbung = collEmbung.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collEmbung
                         select new
                         {
                             KEY = c.K_EMBUNG,
                             c.K_EMBUNG,
                             c.N_EMBUNG,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.N_EMBUNG
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
