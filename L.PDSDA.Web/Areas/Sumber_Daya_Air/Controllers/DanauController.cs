﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities.Web;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class DanauController : BaseController
    {
        //
        // GET: /Sumber_Daya_Air/Danau/

        private readonly IDefaultRepository<DANAU> DefaultRepository;
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;

        public DanauController()
        {
            this.DefaultRepository = new DefaultRepository<DANAU>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();
        }

        private void BuildJenis()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "Danau", Value = "D" });
            collSelectListItem.Add(new SelectListItem { Text = "Situ", Value = "S" });
            ViewBag.collJENIS = collSelectListItem;
        }

        private void BuildSedimentasi()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "Banyak", Value = "01" });
            collSelectListItem.Add(new SelectListItem { Text = "Sedang", Value = "02" });
            collSelectListItem.Add(new SelectListItem { Text = "Sedikit", Value = "03" });
            //new DANAU().SEDIMENTASI
            ViewBag.collSEDIMENTASI = collSelectListItem;
        }

        private void BuildKualitasAir()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "Tawar", Value = "T" });
            collSelectListItem.Add(new SelectListItem { Text = "Payau", Value = "P" });
            //new DANAU().KUALITAS_AIR
            ViewBag.collKUALITAS_AIR = collSelectListItem;
        }

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        public ActionResult Index()
        {
            //IQueryable<DANAU> collDANAU = this.DefaultRepository.GetAll();
            //return View(collDANAU);
            return View();
        }


        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildDAS(string.Empty);
            BuildWS();
            BuildPropinsi();
            BuildKabupaten(string.Empty);
            BuildJenis();
            BuildKualitasAir();
            BuildSedimentasi();
            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(DANAU danau)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (danau.KABUPATEN != null && danau.KABUPATEN.K_KABUPATEN == "-1")
                    {
                        danau.KABUPATEN = null;
                    }
                    if (danau.PROPINSI != null && danau.PROPINSI.K_PROPINSI == "-1")
                    {
                        danau.PROPINSI = null;
                    }
                    if (danau.WS != null && danau.WS.K_WS == "-1")
                    {
                        danau.WS = null;
                    }
                    if (danau.DAS != null && danau.DAS.K_DAS == "-1")
                    {
                        danau.DAS = null;
                    }

                    danau.CreateBy = this.UserLoggedIn;
                    danau.CreateDate = DateTime.Now;
                    danau.CreateTerminal = this.UserHostAddress;
                    danau.UpdateBy = this.UserLoggedIn;
                    danau.UpdateDate = DateTime.Now;
                    danau.UpdateTerminal = this.UserHostAddress;

                    this.DefaultRepository.SaveOrUpdate(danau);
                    return Json(new { success = true, message = Pesan.A1A });
                }
                return Json(new { success = true, message = Pesan.A2A });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }
            ViewBag.ActionName = "Edit";
            DANAU DANAU = DefaultRepository.Get(x => x.K_DANAU == id).FirstOrDefault();
            BuildWS();
            BuildDAS(DANAU.WS == null ? string.Empty : DANAU.WS.K_WS);
            BuildPropinsi();
            BuildKabupaten(DANAU.PROPINSI == null ? string.Empty : DANAU.PROPINSI.K_PROPINSI);
            BuildJenis();
            BuildKualitasAir();
            BuildSedimentasi();
            if (DANAU.WS != null && !collAssignWS.Contains(DANAU.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = DANAU.WS.N_WS, Value = DANAU.WS.K_WS, Selected = true });
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", DANAU);
                //return Detail(id);
            }
            
            return PartialView("Save", DANAU);
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";
            DANAU DANAU = DefaultRepository.Get(x => x.K_DANAU == id).FirstOrDefault();
            BuildDAS(DANAU!= null?DANAU.WS == null ? string.Empty : DANAU.WS.K_WS:"");
            BuildWS();
            BuildPropinsi();
            BuildKabupaten(DANAU!=null?DANAU.PROPINSI == null ? string.Empty : DANAU.PROPINSI.K_PROPINSI:"");
            BuildJenis();
            BuildKualitasAir();
            BuildSedimentasi();
            return PartialView("Detail", DANAU);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, DANAU danau)
        {
            if (danau.KABUPATEN != null && danau.KABUPATEN.K_KABUPATEN == "-1")
            {
                danau.KABUPATEN = null;
            }
            if (danau.PROPINSI != null && danau.PROPINSI.K_PROPINSI == "-1")
            {
                danau.PROPINSI = null;
            }
            if (danau.WS != null && danau.WS.K_WS == "-1")
            {
                danau.WS = null;
            }
            if (danau.DAS != null && danau.DAS.K_DAS == "-1")
            {
                danau.DAS = null;
            }

            DANAU DANAU = DefaultRepository.Get(x => x.K_DANAU == id).FirstOrDefault();
            DANAU.DAS = danau.DAS;
            DANAU.WS = danau.WS;
            DANAU.PROPINSI = danau.PROPINSI;
            DANAU.KABUPATEN = danau.KABUPATEN;

            danau.CreateBy = this.UserLoggedIn;
            danau.CreateDate = DateTime.Now;
            danau.CreateTerminal = this.UserHostAddress;
            danau.UpdateBy = this.UserLoggedIn;
            danau.UpdateDate = DateTime.Now;
            danau.UpdateTerminal = this.UserHostAddress;
            DANAU.LatLong = danau.LatLong;

            Utils.SetTProperty<DANAU>(DANAU, danau);

            try
            {
                // TODO: Add update logic here
                this.DefaultRepository.SaveOrUpdate(DANAU);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = "Data telah diubah" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }

            DANAU deleteDomain = DefaultRepository.Get(x => x.K_DANAU == id).FirstOrDefault();
           
            if (deleteDomain != null)
            {
                if (deleteDomain.WS != null && !collAssignWS.Contains(deleteDomain.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                this.DefaultRepository.Delete(deleteDomain);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }


        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<DANAU> collDANAU = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDANAU = collDANAU.Where(x => x.K_DANAU.Contains(param.sSearch) || x.N_DANAU.Contains(param.sSearch)
                    || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)))
                    || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
                     || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)));
            }
            Count = collDANAU.Count();

            var sortColumnIndex = iSortCol;
            Func<DANAU, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_DANAU :
                                                                sortColumnIndex == 2 ? c.N_DANAU :
                                                                sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                c.N_DANAU);


            IList<DANAU> collDanau = new List<DANAU>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDANAU.Count() > 0)
            {
                collDanau = collDANAU.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collDanau = collDanau.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collDanau = collDanau.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collDanau
                         select new
                         {
                             KEY = c.K_DANAU,
                             c.K_DANAU,
                             c.N_DANAU,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.N_DANAU
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
