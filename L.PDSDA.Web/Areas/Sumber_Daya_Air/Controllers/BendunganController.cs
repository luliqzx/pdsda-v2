﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using log4net;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class BendunganController : BaseController
    {
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;
        //
        // GET: /Sumber_Daya_Air/Bendungan/
        ILog log = LogManager.GetLogger(typeof(BendunganController));


        private readonly IDefaultRepository<BENDUNGAN> DefaultRepository;

        public BendunganController()
        {
            this.DefaultRepository = new DefaultRepository<BENDUNGAN>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();
            log.InfoFormat("[BENDUNGAN] - Initial by {0}", this.UserLoggedIn);
        }

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }



        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        public ActionResult Index()
        {
            IQueryable<BENDUNGAN> collRawa = this.DefaultRepository.GetAll();
            return View(collRawa);
        }



        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {

            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildDAS("");
            BuildWS();
            BuildPropinsi();
            BuildKabupaten("");
            return PartialView("SaveTabs");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(BENDUNGAN formDomain)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (formDomain.KABUPATEN != null && formDomain.KABUPATEN.K_KABUPATEN == "-1")
                    {
                        formDomain.KABUPATEN = null;
                    }
                    if (formDomain.PROPINSI != null && formDomain.PROPINSI.K_PROPINSI == "-1")
                    {
                        formDomain.PROPINSI = null;
                    }
                    if (formDomain.WS != null && formDomain.WS.K_WS == "-1")
                    {
                        formDomain.WS = null;
                    }
                    if (formDomain.DAS != null && formDomain.DAS.K_DAS == "-1")
                    {
                        formDomain.DAS = null;
                    }
                    formDomain.CreateBy = this.UserLoggedIn;
                    formDomain.CreateDate = DateTime.Now;
                    formDomain.CreateTerminal = this.UserHostAddress;
                    formDomain.UpdateBy = this.UserLoggedIn;
                    formDomain.UpdateDate = DateTime.Now;
                    formDomain.UpdateTerminal = this.UserHostAddress;
                    log.InfoFormat("[BENDUNGAN] - Save by {0}", this.UserLoggedIn);
                    this.DefaultRepository.Save(formDomain);
                    return Json(new { success = true, message = Pesan.A1A });

                }
                return Json(new { success = true, message = Pesan.A2A });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }

            ViewBag.ActionName = "Edit";
            BENDUNGAN BENDUNGAN = DefaultRepository.Get(x => x.K_BENDUNGAN == id).FirstOrDefault();
            BuildWS();
            BuildDAS(BENDUNGAN.WS == null ? "" : BENDUNGAN.WS.K_WS);
            BuildPropinsi();
            BuildKabupaten(BENDUNGAN.KABUPATEN == null ? "" : BENDUNGAN.KABUPATEN.PROPINSI == null ? "" : BENDUNGAN.KABUPATEN.PROPINSI.K_PROPINSI);
          
            if (BENDUNGAN.WS != null && !collAssignWS.Contains(BENDUNGAN.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = BENDUNGAN.WS.N_WS, Value = BENDUNGAN.WS.K_WS, Selected = true });
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", BENDUNGAN);
                //return Detail(id);
            }
             return PartialView("SaveTabs", BENDUNGAN);
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";
            BENDUNGAN BENDUNGAN = DefaultRepository.Get(x => x.K_BENDUNGAN == id).FirstOrDefault();
            BuildWS();
            BuildDAS(BENDUNGAN!=null?BENDUNGAN.WS == null ? "" : BENDUNGAN.WS.K_WS:"");
            BuildPropinsi();
            BuildKabupaten(BENDUNGAN!=null?BENDUNGAN.KABUPATEN == null ? "" : BENDUNGAN.KABUPATEN.PROPINSI == null ? "" : BENDUNGAN.KABUPATEN.PROPINSI.K_PROPINSI:"");
            return PartialView("Detail", BENDUNGAN);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, BENDUNGAN formDomain)
        {
            if (formDomain.KABUPATEN != null && formDomain.KABUPATEN.K_KABUPATEN == "-1")
            {
                formDomain.KABUPATEN = null;
            }
            if (formDomain.PROPINSI != null && formDomain.PROPINSI.K_PROPINSI == "-1")
            {
                formDomain.PROPINSI = null;
            }
            if (formDomain.WS != null && formDomain.WS.K_WS == "-1")
            {
                formDomain.WS = null;
            }
            if (formDomain.DAS != null && formDomain.DAS.K_DAS == "-1")
            {
                formDomain.DAS = null;
            }
            BENDUNGAN BENDUNGAN = DefaultRepository.Get(x => x.K_BENDUNGAN == id).FirstOrDefault();
            BENDUNGAN.DAS = formDomain.DAS;
            BENDUNGAN.WS = formDomain.WS;
            BENDUNGAN.PROPINSI = formDomain.PROPINSI;
            BENDUNGAN.KABUPATEN = formDomain.KABUPATEN;
            BENDUNGAN.LatLong = formDomain.LatLong;

            formDomain.UpdateBy = this.UserLoggedIn;
            formDomain.UpdateDate = DateTime.Now;
            formDomain.UpdateTerminal = this.UserHostAddress;
            Utils.SetTProperty<BENDUNGAN>(BENDUNGAN, formDomain);

            try
            {
                // TODO: Add update logic here
                log.InfoFormat("[BENDUNGAN] - Update by {0}", this.UserLoggedIn);
                this.DefaultRepository.Update(BENDUNGAN);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = Pesan.A1B });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            BENDUNGAN deleteDomain = DefaultRepository.Get(x => x.K_BENDUNGAN == id).FirstOrDefault();
           
            if (deleteDomain != null)
            {
                if (deleteDomain.WS != null && !collAssignWS.Contains(deleteDomain.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                log.InfoFormat("Delete by {0}", this.UserLoggedIn);
                this.DefaultRepository.Delete(deleteDomain);
                return Json(new { success = true, message = Pesan.A1C });
            }
            return Json(new { success = false, message = Pesan.A2C });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<BENDUNGAN> collBENDUNGAN = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collBENDUNGAN = collBENDUNGAN.Where(x => x.K_BENDUNGAN.Contains(param.sSearch) || x.N_BENDUNGAN.Contains(param.sSearch)
                    || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)))
                    || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
                     || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)));
            }
            Count = collBENDUNGAN.Count();

            var sortColumnIndex = iSortCol;
            Func<BENDUNGAN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_BENDUNGAN :
                                                                sortColumnIndex == 2 ? c.N_BENDUNGAN :
                                                                sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                c.K_BENDUNGAN);


            IList<BENDUNGAN> collResult = new List<BENDUNGAN>();// = collRawaDyn == null ? new List<BENDUNGAN_DYN>() : collRawaDyn.ToList();
            if (collBENDUNGAN.Count() > 0)
            {
                collResult = collBENDUNGAN.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_BENDUNGAN,
                             c.K_BENDUNGAN,
                             c.N_BENDUNGAN,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.N_BENDUNGAN
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
