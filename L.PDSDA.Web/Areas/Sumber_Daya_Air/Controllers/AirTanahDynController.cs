﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using System.Data;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class AirTanahDynController : BaseController
    {
        //
        // GET: /Bencana/DaerahAirTanahnBanjir/
        IDefaultRepository<AIRTANAH_DYN> AirTanahDynRepository;
        IDefaultRepository<AIRTANAH> AirTanahRepository;
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;

        public AirTanahDynController()
        {
            this.AirTanahDynRepository = new DefaultRepository<AIRTANAH_DYN>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            AirTanahRepository = new DefaultRepository<AIRTANAH>();
        }

        void BuildDAS()
        {
            IQueryable<DAS> collWS = this.DASRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        private void BuildP3A()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "" });
            collSelectListItem.Add(new SelectListItem { Text = "Aktif", Value = "1" });
            collSelectListItem.Add(new SelectListItem { Text = "Berkembang", Value = "2" });
            collSelectListItem.Add(new SelectListItem { Text = "Tidak Berkembang", Value = "3" });
            collSelectListItem.Add(new SelectListItem { Text = "Pasif", Value = "3" });

            ViewBag.collP3A = collSelectListItem;
        }


        public ActionResult Index()
        {
            //IQueryable<AIRTANAH_DYN> collAIRTANAH_DYN = this.DefaultRepository.GetAll();
            //return View(collAIRTANAH_DYN);
            string k_airtanah = this.HttpContext.Request.QueryString["k_airtanah"];
            ViewData["hfK_AIRTANAH"] = k_airtanah;

            AIRTANAH AIRTANAH = this.AirTanahRepository.Get(x => x.K_AIRTANAH == k_airtanah).FirstOrDefault();
            if (AIRTANAH != null)
            {
                if (AIRTANAH.WS != null && !collAssignWS.Contains(AIRTANAH.WS))
                {
                    ViewBag.IsViewable = "False";
                    ViewBag.IsCreateable = "False";
                    ViewBag.IsUpdateable = "False";
                    ViewBag.IsDeleteable = "False";
                    ViewBag.Method = Utils.GetPostRequest("method");
                    return PartialView();
                }
            }

            ViewBag.IsViewable = Utils.GetPostRequest("IsViewable");
            ViewBag.IsCreateable = Utils.GetPostRequest("IsCreateable");
            ViewBag.IsUpdateable = Utils.GetPostRequest("IsUpdateable");
            ViewBag.IsDeleteable = Utils.GetPostRequest("IsDeleteable");
            ViewBag.Method = Utils.GetPostRequest("method");
            return PartialView();
        }

        //
        // GET: /Bencana/DaerahAirTanahnBanjir/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/AirTanahDYN/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("");
            }
            BuildP3A();
            string k_airtanah = this.HttpContext.Request.QueryString["k_airtanah"];
            AIRTANAH AIRTANAH = new AIRTANAH();
            AIRTANAH.K_AIRTANAH = k_airtanah;
            AIRTANAH_DYN AIRTANAH_DYN = new AIRTANAH_DYN();
            AIRTANAH_DYN.AIRTANAH = AIRTANAH;
            //AIRTANAH_DYN.AIRTANAH = this.AIRTANAHRepository.Get(x => x.K_AIRTANAH == k_rawa).FirstOrDefault();
            return PartialView("Save", AIRTANAH_DYN);
        }

        //
        // POST: /Sumber_Daya_Air/AirTanahDYN/Create

        [HttpPost]
        public ActionResult Create(AIRTANAH_DYN formDomain)
        {
            try
            {
                string k_airtanah = this.HttpContext.Request.QueryString["k_airtanah"];
                AIRTANAH AIRTANAH = new AIRTANAH();
                AIRTANAH.K_AIRTANAH = k_airtanah;
                formDomain.AIRTANAH = AIRTANAH;
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    //formDomain.AIRTANAH = this.AIRTANAHRepository.Get(x => x.K_AIRTANAH == formDomain.AIRTANAH.K_AIRTANAH).FirstOrDefault();
                    //formDomain.AIRTANAH = formDomain.AIRTANAH;
                    this.AirTanahDynRepository.SaveOrUpdate(formDomain);
                    return Json(new { success = true, message = "Data telah disimpan." });

                }
                return Json(new { success = true, message = "Invalid Model State" });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/AirTanahDYN/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }

            BuildP3A();
            string[] arr = id.Split(',');
            AIRTANAH_DYN AIRTANAH_DYN = this.AirTanahDynRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[0]) && x.AIRTANAH.K_AIRTANAH == arr[1]).FirstOrDefault();
            AIRTANAH AIRTANAH = AIRTANAH_DYN.AIRTANAH;
            if (AIRTANAH.WS != null && !collAssignWS.Contains(AIRTANAH.WS))
            {
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", AIRTANAH_DYN);
            }
            return PartialView("Save", AIRTANAH_DYN);
        }


        public ActionResult Detail(string id)
        {
            BuildP3A();
            string[] arr = id.Split(',');
            AIRTANAH_DYN AIRTANAH_DYN = string.IsNullOrWhiteSpace(id) ? null : this.AirTanahDynRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[0]) && x.AIRTANAH.K_AIRTANAH == arr[1]).FirstOrDefault();
            return PartialView("Detail", AIRTANAH_DYN);
            //return View();
        }

        //
        // POST: /Sumber_Daya_Air/AirTanahDYN/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, AIRTANAH_DYN domain)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string[] arr = id.Split(',');
                    AIRTANAH_DYN formSave = this.AirTanahDynRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[0]) && x.AIRTANAH.K_AIRTANAH == arr[1]).FirstOrDefault();
                    Utils.SetTProperty(formSave, domain);
                    this.AirTanahDynRepository.Update(formSave);
                    return Json(new { success = true, message = "Data telah diubah." });
                }
            }
            catch (DataException)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return Json(new { success = false, message = "ERROR" });
        }

        //
        // GET: /Sumber_Daya_Air/AirTanahDYN/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            string[] arr = id.Split(',');
            AIRTANAH_DYN deleteDomain = this.AirTanahDynRepository.Get(x => x.TAHUN_DATA == Convert.ToInt32(arr[0]) && x.AIRTANAH.K_AIRTANAH == arr[1]).FirstOrDefault();

            if (deleteDomain != null)
            {
                if (deleteDomain.AIRTANAH != null && deleteDomain.AIRTANAH.WS != null && !collAssignWS.Contains(deleteDomain.AIRTANAH.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                if (ModelState.IsValid)
                {
                    this.AirTanahDynRepository.Delete(deleteDomain);
                    return Json(new { success = true, message = "Data telah dihapus." });

                }
                return Json(new { success = true, message = "Invalid Model State" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }


        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            string k_airtanah = this.Request.QueryString["k_airtanah"];
            AIRTANAH AIRTANAH = this.AirTanahRepository.GetById(k_airtanah);

            IQueryable<AIRTANAH_DYN> collAIRTANAHDyn = this.AirTanahDynRepository.GetAll().Where(x => x.AIRTANAH == AIRTANAH);

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collAIRTANAHDyn = collAIRTANAHDyn.Where(x => x.TAHUN_DATA.ToString().Contains(param.sSearch) || x.JENIS_POMPA.Contains(param.sSearch)
                    || (x.AIRTANAH != null && x.AIRTANAH.NO_SUMUR.Contains(param.sSearch))
                    || (x.DEBIT_POMPA != null && x.DEBIT_POMPA.ToString().Contains(param.sSearch)));
            }
            Count = collAIRTANAHDyn.Count();

            var sortColumnIndex = iSortCol;
            Func<AIRTANAH_DYN, string> orderingFunction = (c => sortColumnIndex == 1 ? c.AIRTANAH != null ? c.AIRTANAH.NO_SUMUR : string.Empty :
                                                                sortColumnIndex == 2 ? c.TAHUN_DATA.ToString() :
                                                               sortColumnIndex == 3 ? c.JENIS_POMPA :
                                                                sortColumnIndex == 4 ? c.DEBIT_POMPA != null ? c.DEBIT_POMPA.ToString() : string.Empty :
                                                                c.TAHUN_DATA.ToString());

            IList<AIRTANAH_DYN> collResult = new List<AIRTANAH_DYN>();// = collAirTanahDyn == null ? new List<AIRTANAH_DYN>() : collAirTanahDyn.ToList();
            if (collAIRTANAHDyn.Count() > 0)
            {
                collResult = collAIRTANAHDyn.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.TAHUN_DATA + "," + c.AIRTANAH.K_AIRTANAH,
                             c.AIRTANAH.NO_SUMUR,
                             c.TAHUN_DATA,
                             c.JENIS_POMPA,
                             DEBIT_POMPA = c.DEBIT_POMPA == null ? "" : c.DEBIT_POMPA.ToString()
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
