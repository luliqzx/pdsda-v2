﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Repositories.Data.SumberDayaAir;
using L.PDSDA.DAL.Repositories.Data.SumberDayaAir.Implements;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class SungaiController : BaseController
    {
        //
        // GET: /Sumber_Daya_Air/Sungai/

        private readonly ISungaiRepository SungaiRepository;
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;

        public SungaiController()
        {
            this.SungaiRepository = new SungaiRepository();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
        }

        public ActionResult Index()
        {
            //IQueryable<SUNGAI> collSungai = this.SungaiRepository.GetAll();
            //return View(collSungai);
            return View("IndexV2");
        }


        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Request["iSortCol_0"]); }
        }

        protected string iSortDir
        {
            get { return Request["sSortDir_0"]; }
        }

        public ActionResult SelectionSungai(string KEY)
        {
            ViewBag.SungaiKey = KEY;

            string[] sOrde = KEY.Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            SUNGAI sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
                && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();

            if ((sungai.WS != null && !collAssignWS.Contains(sungai.WS)) || (!IsCreateable && !IsUpdateable && !IsDeleteable))
            {
                ViewBag.Method = "Detail";
            }
            return PartialView("SelectionSungai");
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<SUNGAI> collSungai = this.SungaiRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collSungai = collSungai.Where(x => x.N_SUNGAI.Contains(param.sSearch) || x.WS.N_WS.Contains(param.sSearch)
                     || x.K_ORDE1.ToString().Contains(param.sSearch) || x.K_ORDE2.ToString().Contains(param.sSearch) || x.K_ORDE3.ToString().Contains(param.sSearch)
                     || x.K_ORDE4.ToString().Contains(param.sSearch) || x.K_ORDE5.ToString().Contains(param.sSearch));
            }
            Count = collSungai.Count();

            var sortColumnIndex = iSortCol;
            Func<SUNGAI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.N_SUNGAI :
                                                                sortColumnIndex == 2 ? c.URUT.ToString() :
                                                                sortColumnIndex == 3 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 4 ? c.DAS != null ? c.DAS.N_DAS : string.Empty : c.URUT.ToString());


            IList<SUNGAI> collSUNGAI = new List<SUNGAI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collSungai.Count() > 0)
            {
                collSUNGAI = collSungai.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collSUNGAI = collSUNGAI.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collSUNGAI = collSUNGAI.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collSUNGAI
                         select new
                         {
                             KEY = c.URUT + "," + c.K_ORDE1 + "," + c.K_ORDE2 + "," + c.K_ORDE3 + "," + c.K_ORDE4 + "," + c.K_ORDE5,
                             ORDE = c.K_ORDE1 + "-" + c.K_ORDE2 + "-" + c.K_ORDE3 + "-" + c.K_ORDE4 + "-" + c.K_ORDE5,
                             N_SUNGAI = c.N_SUNGAI,
                             N_WS = c.WS == null ? "" : c.WS.N_WS,
                             N_DAS = c.DAS == null ? "" : c.DAS.N_DAS,
                             c.LatLong,
                             pTitle = c.N_SUNGAI
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            this.BuildWS();
            this.BuildDAS(string.Empty);
            this.BuildStatusSungai();
            return PartialView("Save");
        }

        private void BuildStatusSungai()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "Tidak Ada Data", Value = "05" });
            collSelectListItem.Add(new SelectListItem { Text = "Lintas Negara", Value = "01" });
            collSelectListItem.Add(new SelectListItem { Text = "Lintas Propinsi", Value = "02" });
            collSelectListItem.Add(new SelectListItem { Text = "Lintas Kabupaten", Value = "03" });
            collSelectListItem.Add(new SelectListItem { Text = "Non Lintas", Value = "04" });
            ViewBag.collK_STATUSSUNGAI = collSelectListItem;
        }

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(SUNGAI sungai)
        {
            double urut = this.SungaiRepository.GetMaxUrut();
            sungai.URUT = urut;
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (sungai.WS != null && sungai.WS.K_WS == "-1")
                    {
                        sungai.WS = null;
                    }
                    if (sungai.DAS != null && sungai.DAS.K_DAS == "-1")
                    {
                        sungai.DAS = null;
                    }

                    sungai.CreateBy = this.UserLoggedIn;
                    sungai.CreateDate = DateTime.Now;
                    sungai.CreateTerminal = this.UserHostAddress;
                    sungai.UpdateBy = this.UserLoggedIn;
                    sungai.UpdateDate = DateTime.Now;
                    sungai.UpdateTerminal = this.UserHostAddress;
                    this.SungaiRepository.SaveOrUpdate(sungai);

                }
                return Json(new { success = true, message = "Data telah disimpan." });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }
            ViewBag.ActionName = "Edit";

            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            SUNGAI sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
                && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();

            this.BuildWS();
            this.BuildDAS(sungai.WS == null ? string.Empty : sungai.WS.K_WS);
            this.BuildStatusSungai();
            if (sungai.WS != null && !collAssignWS.Contains(sungai.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = sungai.WS.N_WS, Value = sungai.WS.K_WS, Selected = true });
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", sungai);
                //return Detail(id);
            }

            return PartialView("Save", sungai);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, SUNGAI sungai)
        {
            if (sungai.WS != null && sungai.WS.K_WS == "-1")
            {
                sungai.WS = null;
            }
            if (sungai.DAS != null && sungai.DAS.K_DAS == "-1")
            {
                sungai.DAS = null;
            }


            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            sungai.URUT = orde[0];
            SUNGAI Sungai = SungaiRepository.Get(x => x.URUT == Convert.ToDouble(orde[0]) && x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
                && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5]).FirstOrDefault();
            Sungai.WS = sungai.WS;
            Sungai.DAS = sungai.DAS;
            Utils.SetTProperty<SUNGAI>(Sungai, sungai);
            Sungai.LatLong = sungai.LatLong;

            try
            {
                // TODO: Add update logic here
                Sungai.UpdateBy = this.UserLoggedIn;
                Sungai.UpdateDate = DateTime.Now;
                Sungai.UpdateTerminal = this.UserHostAddress;
                this.SungaiRepository.SaveOrUpdate(Sungai);
                //return RedirectToAction("Index");
                return Json(new { success = true, message = "Data telah diubah" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";

            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            if (!string.IsNullOrWhiteSpace(id))
            {
                for (int i = 0; i < orde.Length; i++)
                {
                    orde[i] = Convert.ToInt32(sOrde[i]);
                }
            }


            SUNGAI sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
                && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();

            this.BuildWS();
            this.BuildDAS(sungai != null ? sungai.WS == null ? string.Empty : sungai.WS == null ? "" : sungai.WS.K_WS : "");
            this.BuildStatusSungai();
            return PartialView("Detail", sungai);
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            SUNGAI Sungai = SungaiRepository.Get(x => x.URUT == Convert.ToDouble(orde[0]) && x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
                && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5]).FirstOrDefault();

            if (Sungai != null)
            {
                if (Sungai.WS != null && !collAssignWS.Contains(Sungai.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                this.SungaiRepository.Delete(Sungai);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

        //
        // POST: /Sumber_Daya_Air/Sungai/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
