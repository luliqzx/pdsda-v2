﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Repositories.Data.SumberDayaAir;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities.Web;
using L.Core.Utilities;
using L.Core.General;
using log4net;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class KribController : BaseController
    {

        ILog log = LogManager.GetLogger(typeof(KribController));
        //
        // GET: /Sumber_Daya_Air/Krib/

        private readonly IDefaultRepository<KRIB> KribRepository;
        private readonly IDefaultRepository<SUNGAI> SungaiRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;

        void BuildPosisi()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            collSelectListItem.Add(new SelectListItem { Text = "Kanan", Value = "1" });
            collSelectListItem.Add(new SelectListItem { Text = "Kiri", Value = "2" });
            collSelectListItem.Add(new SelectListItem { Text = "Kanan / Kiri", Value = "3" });
            ViewBag.collPOSISI = collSelectListItem;
        }

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();
            //IQueryable<PROPINSI> collPropinsi = this.PROPINSIRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        public KribController()
        {
            this.KribRepository = new DefaultRepository<KRIB>();
            SungaiRepository = new DefaultRepository<SUNGAI>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            KABUPATENRepository = new DefaultRepository<KABUPATEN>();
        }

        public ActionResult Index()
        {

            string[] sOrde = Utils.GetQueryString("KEY").Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            SUNGAI sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
             && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();

            if (sungai.WS != null && !collAssignWS.Contains(sungai.WS))
            {
                ViewBag.IsViewable = "False";
                ViewBag.IsCreateable = "False";
                ViewBag.IsUpdateable = "False";
                ViewBag.IsDeleteable = "False";
                ViewBag.Method = "Detail";
                return PartialView();
            }

            ViewBag.IsViewable = Utils.GetPostRequest("IsViewable");
            ViewBag.IsCreateable = Utils.GetPostRequest("IsCreateable");
            ViewBag.IsUpdateable = Utils.GetPostRequest("IsUpdateable");
            ViewBag.IsDeleteable = Utils.GetPostRequest("IsDeleteable");
            if (ViewBag.IsUpdateable == "False")
            {
                ViewBag.Method = "Detail";
            }
            return PartialView();
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Request["iSortCol_0"]); }
        }

        protected string iSortDir
        {
            get { return Request["sSortDir_0"]; }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            SUNGAI sungai = this.GetSungaiByKey(Utils.GetQueryString("KEY"));
            int Count = 0;
            IQueryable<KRIB> collKrib = new List<KRIB>() as IQueryable<KRIB>;// = this.KribRepository.GetAll();
            if (sungai != null)
            {
                collKrib = sungai.KRIBs.AsQueryable();
            }

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collKrib = collKrib.Where(x => x.NAMA.Contains(param.sSearch) || x.TAHUN_DATA.HasValue ? x.TAHUN_DATA.ToString().Contains(param.sSearch) : false);
            }
            Count = collKrib.Count();

            var sortColumnIndex = iSortCol;
            Func<KRIB, string> orderingFunction = (c => sortColumnIndex == 1 ? c.NAMA :
                                                                sortColumnIndex == 2 ? c.TAHUN_DATA.HasValue ? c.TAHUN_DATA.ToString() : "" : c.URUT_KRIB.ToString());

            IList<KRIB> collKRIB = new List<KRIB>();
            if (collKrib.Count() > 0)
            {
                collKRIB = collKrib.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collKRIB = collKRIB.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collKRIB = collKRIB.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }

            var result = from c in collKRIB
                         select new
                         {
                             KEY = c.SUNGAI.URUT + "," + c.SUNGAI.K_ORDE1 + ","
                                    + c.SUNGAI.K_ORDE2 + "," + c.SUNGAI.K_ORDE3 + "," + c.SUNGAI.K_ORDE4 + "," + c.SUNGAI.K_ORDE5 + "," + c.URUT_KRIB,
                             NAMA = c.NAMA,
                             c.TAHUN_DATA
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {

            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            string KEY = Utils.GetQueryString("KEY");
            SUNGAI sungai = GetSungaiByKey(KEY);
            ViewBag.NamaSungai = sungai.N_SUNGAI;
            BuildPosisi();
            BuildPropinsi();
            BuildKabupaten(string.Empty);
            return PartialView("Save");
        }

        public SUNGAI GetSungaiByKey(string id)
        {
            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            SUNGAI sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
             && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();
            return sungai;
        }

        [HttpPost]
        public ActionResult Create(KRIB domain)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SUNGAI sungai = this.GetSungaiByKey(Utils.GetQueryString("KEY"));
                    if (sungai != null)
                    {
                        domain.SUNGAI = sungai;
                    }
                    if (domain.KABUPATEN != null && domain.KABUPATEN.K_KABUPATEN == "-1")
                    {
                        domain.KABUPATEN = null;
                    }
                    if (domain.KABUPATEN != null && domain.KABUPATEN.PROPINSI != null && domain.KABUPATEN.PROPINSI.K_PROPINSI == "-1")
                    {
                        domain.KABUPATEN.PROPINSI = null;
                    }
                    if (domain.POSISI == "-1")
                    {
                        domain.POSISI = null;
                    }
                    this.KribRepository.Save(domain);
                }
                return Json(new { success = true, message = Pesan.A1A });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }


        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }
            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            BuildPosisi();
            SUNGAI sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
             && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();

            KRIB tanggul = this.KribRepository.Get(x => x.URUT_KRIB == orde[6] && x.SUNGAI == sungai).FirstOrDefault();
            if (tanggul != null)
            {
                ViewBag.NamaSungai = sungai.N_SUNGAI;
                BuildPropinsi();
                BuildKabupaten(tanggul.KABUPATEN == null ? "" : tanggul.KABUPATEN.PROPINSI.K_PROPINSI);
            }

            if (sungai.WS != null && !collAssignWS.Contains(sungai.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = sungai.WS.N_WS, Value = sungai.WS.K_WS, Selected = true });
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", tanggul);
            }

            return PartialView("Save", tanggul);
        }


        [HttpPost]
        public ActionResult Edit(string id, KRIB domain)
        {
            if (domain.KABUPATEN != null && domain.KABUPATEN.K_KABUPATEN == "-1")
            {
                domain.KABUPATEN = null;
            }
            if (domain.KABUPATEN != null && domain.KABUPATEN.PROPINSI != null && domain.KABUPATEN.PROPINSI.K_PROPINSI == "-1")
            {
                domain.KABUPATEN.PROPINSI = null;
            }
            if (domain.POSISI == "-1")
            {
                domain.POSISI = null;
            }

            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            SUNGAI sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
             && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();

            KRIB saveDomain = this.KribRepository.Get(x => x.URUT_KRIB == orde[6] && x.SUNGAI == sungai).FirstOrDefault();
            saveDomain.KABUPATEN = domain.KABUPATEN;
            Utils.SetTProperty<KRIB>(saveDomain, domain);

            try
            {
                if (ModelState.IsValid)
                {
                    log.InfoFormat("KribController Update {0} by {1} at {2}", domain.NAMA, this.UserLoggedIn, this.UserHostAddress);
                    this.KribRepository.Update(saveDomain);
                    return Json(new { success = true, message = Pesan.A1B });

                }
                return Json(new { success = true, message = Pesan.A2B });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        public ActionResult Detail(string id)
        {
            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            SUNGAI sungai = null;

            if (!string.IsNullOrWhiteSpace(id))
            {
                for (int i = 0; i < orde.Length; i++)
                {
                    orde[i] = Convert.ToInt32(sOrde[i]);
                }
                sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
            && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();
            }

            KRIB KRIB = this.KribRepository.Get(x => x.URUT_KRIB == orde[6] && x.SUNGAI == sungai).FirstOrDefault();
            ViewBag.NamaSungai = sungai != null ? sungai.N_SUNGAI : "";
            BuildPropinsi();
            BuildKabupaten(KRIB != null ? KRIB.KABUPATEN == null ? "" : KRIB.KABUPATEN.PROPINSI.K_PROPINSI : "");
            BuildPosisi();
            return PartialView("Detail", KRIB);
        }

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            string[] sOrde = id.Split(',');
            int[] orde = new int[sOrde.Length];
            for (int i = 0; i < orde.Length; i++)
            {
                orde[i] = Convert.ToInt32(sOrde[i]);
            }

            SUNGAI sungai = SungaiRepository.Get(x => x.K_ORDE1 == orde[1] && x.K_ORDE2 == orde[2]
             && x.K_ORDE3 == orde[3] && x.K_ORDE4 == orde[4] && x.K_ORDE5 == orde[5] && x.URUT == Convert.ToDouble(orde[0])).FirstOrDefault();

            KRIB tanggul = this.KribRepository.Get(x => x.URUT_KRIB == orde[6] && x.SUNGAI == sungai).FirstOrDefault();

            if (tanggul != null)
            {
                if (sungai.WS != null && !collAssignWS.Contains(sungai.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                this.KribRepository.Delete(tanggul);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }

    }
}
