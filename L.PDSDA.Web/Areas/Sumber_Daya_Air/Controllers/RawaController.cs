﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.Web.Controllers;
using L.Core.Utilities;
using L.Core.Utilities.Web;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class RawaController : BaseController
    {
        //
        // GET: /Sumber_Daya_Air/Rawa/

        private readonly IDefaultRepository<RAWA> DefaultRepository;
        private readonly IDefaultRepository<WS> WSRepository;
        private readonly IDefaultRepository<DAS> DASRepository;
        private readonly IDefaultRepository<PROPINSI> PROPINSIRepository;
        private readonly IDefaultRepository<KABUPATEN> KABUPATENRepository;

        public RawaController()
        {
            this.DefaultRepository = new DefaultRepository<RAWA>();
            WSRepository = new DefaultRepository<WS>();
            this.DASRepository = new DefaultRepository<DAS>();
            PROPINSIRepository = new DefaultRepository<PROPINSI>();
            this.KABUPATENRepository = new DefaultRepository<KABUPATEN>();
        }

        private void BuildJenis()
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "Pasang Surut", Value = "P" });
            collSelectListItem.Add(new SelectListItem { Text = "Lebak", Value = "L" });
            ViewBag.collJENIS = collSelectListItem;
        }

        void BuildDAS(string k_ws)
        {
            IQueryable<DAS> collDAS = this.DASRepository.GetAll().Where(x => x.WS.K_WS == k_ws);
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collDAS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_DAS, Value = item.K_DAS });
            }
            ViewBag.collDAS = collSelectListItem;
        }

        void BuildWS()
        {
            //IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }

        void BuildPropinsi()
        {
            IQueryable<PROPINSI> collPropinsi = collAssignPropinsi.Count > 0 ? collAssignPropinsi.AsQueryable() : this.PROPINSIRepository.GetAll();

            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collPropinsi)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.N_PROPINSI, Value = item.K_PROPINSI });
            }
            //new PROPINSI().K_PROPINSI
            ViewBag.collK_PROPINSI = collSelectListItem;
        }

        void BuildKabupaten(string K_PROPINSI)
        {
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            if (!string.IsNullOrEmpty(K_PROPINSI))
            {
                IQueryable<KABUPATEN> collKABUPATEN = this.KABUPATENRepository.GetAll().Where(x => x.PROPINSI != null && x.PROPINSI.K_PROPINSI == K_PROPINSI);
                foreach (var item in collKABUPATEN)
                {
                    string kodePropinsi = item.PROPINSI == null ? "" : item.PROPINSI.K_PROPINSI;
                    collSelectListItem.Add(new SelectListItem { Text = item.N_KABUPATEN, Value = item.K_KABUPATEN });
                }
            }

            ViewBag.collK_KABUPATEN = collSelectListItem;
        }

        public ActionResult Index()
        {
            IQueryable<RAWA> collRawa = this.DefaultRepository.GetAll();
            return View(collRawa);
        }

        //
        // GET: /Sumber_Daya_Air/Rawa/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/WS/Create

        public ActionResult Create()
        {
            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildDAS(string.Empty);
            BuildWS();
            BuildPropinsi();
            BuildKabupaten(string.Empty);
            BuildJenis();
            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/WS/Create

        [HttpPost]
        public ActionResult Create(RAWA rawa)
        {
            try
            {
                // TODO: Add insert logic here

                if (ModelState.IsValid)
                {
                    if (rawa.KABUPATEN != null && rawa.KABUPATEN.K_KABUPATEN == "-1")
                    {
                        rawa.KABUPATEN = null;
                    }

                    if (rawa.PROPINSI != null && rawa.PROPINSI.K_PROPINSI == "-1")
                    {
                        rawa.PROPINSI = null;
                    }
                    if (rawa.WS != null && rawa.WS.K_WS == "-1")
                    {
                        rawa.WS = null;
                    }
                    if (rawa.DAS != null && rawa.DAS.K_DAS == "-1")
                    {
                        rawa.DAS = null;
                    }
                    rawa.CreateBy = this.UserLoggedIn;
                    rawa.CreateDate = DateTime.Now;
                    rawa.CreateTerminal = this.UserHostAddress;
                    rawa.UpdateBy = this.UserLoggedIn;
                    rawa.UpdateDate = DateTime.Now;
                    rawa.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.SaveOrUpdate(rawa);
                }
                return Json(new { success = true, message = "Data telah disimpan." });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }

            RAWA RAWA = DefaultRepository.Get(x => x.K_RAWA == id).FirstOrDefault();
            BuildWS();
            BuildDAS(RAWA.WS == null ? string.Empty : RAWA.WS.K_WS);
            BuildPropinsi();
            BuildJenis();
            BuildKabupaten(RAWA.PROPINSI == null ? "" : RAWA.PROPINSI.K_PROPINSI);
            if (RAWA.WS != null && !collAssignWS.Contains(RAWA.WS))
            {
                IList<SelectListItem> addWStoSelectedListItem = new List<SelectListItem>(); //ViewBag.collWS = addWStoSelectedListItem;
                addWStoSelectedListItem.Add(new SelectListItem() { Text = RAWA.WS.N_WS, Value = RAWA.WS.K_WS, Selected = true });
                ViewBag.collWS = addWStoSelectedListItem;
                ViewBag.ActionName = "Detail";
                return PartialView("Detail", RAWA);
                //return Detail(id);
            }
            ViewBag.ActionName = "Edit";
            return PartialView("Save", RAWA);
        }
        public ActionResult Detail(string id)
        {
            ViewBag.ActionName = "Detail";

            RAWA RAWA = DefaultRepository.Get(x => x.K_RAWA == id).FirstOrDefault();
            BuildWS();
            BuildDAS(RAWA != null ? RAWA.WS == null ? string.Empty : RAWA.WS.K_WS : "");
            BuildPropinsi();
            BuildJenis();
            BuildKabupaten(RAWA != null ? RAWA.PROPINSI == null ? "" : RAWA.PROPINSI.K_PROPINSI : "");
            return PartialView("Detail", RAWA);
        }

        //
        // POST: /Sumber_Daya_Air/WS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, RAWA rawa)
        {
            string kodePropinsi = rawa.PROPINSI.K_PROPINSI;
            if (rawa.PROPINSI != null && rawa.PROPINSI.K_PROPINSI == "-1")
            {
                rawa.PROPINSI = null;
            }
            if (rawa.KABUPATEN != null && rawa.KABUPATEN.K_KABUPATEN == "-1")
            {
                rawa.KABUPATEN = null;
            }
            if (rawa.WS != null && rawa.WS.K_WS == "-1")
            {
                rawa.WS = null;
            }
            if (rawa.DAS != null && rawa.DAS.K_DAS == "-1")
            {
                rawa.DAS = null;
            }

            if (kodePropinsi != "-1" && rawa.KABUPATEN == null)
            {
                return Json(new { success = false, message = "Pilih Kabupaten terlebih dahulu." });
            }

            RAWA RAWA = DefaultRepository.Get(x => x.K_RAWA == id).FirstOrDefault();
            RAWA.DAS = rawa.DAS;
            RAWA.WS = rawa.WS;
            RAWA.PROPINSI = rawa.PROPINSI;
            RAWA.KABUPATEN = rawa.KABUPATEN;
            RAWA.LatLong = rawa.LatLong;
            Utils.SetTProperty<RAWA>(RAWA, rawa);

            try
            {


                if (ModelState.IsValid)
                {
                    RAWA.UpdateBy = this.UserLoggedIn;
                    RAWA.UpdateDate = DateTime.Now;
                    RAWA.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.SaveOrUpdate(RAWA);
                    //return RedirectToAction("Index");
                    return Json(new { success = true, message = "Data telah diubah" });
                }
                // TODO: Add update logic here
                return Json(new { success = false, message = "Model not valid." });




            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/WS/Delete/5

        public ActionResult Delete(string id)
        {
            RAWA RAWA = DefaultRepository.Get(x => x.K_RAWA == id).FirstOrDefault();

            if (RAWA != null)
            {
                if (RAWA.WS != null && !collAssignWS.Contains(RAWA.WS))
                {
                    return Json(new { success = false, message = "Tidak berhak hapus data." });
                }
                this.DefaultRepository.Delete(RAWA);
                return Json(new { success = true, message = "Data telah dihapus" });
            }
            return Json(new { success = false, message = "Data tidak ditemukan" });
        }



        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<RAWA> collRAWA = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collRAWA = collRAWA.Where(x => x.K_RAWA.Contains(param.sSearch) || x.N_RAWA.Contains(param.sSearch)
                    || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)))
                    || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))// || x.WS.BALAIs.FirstOrDefault(k => k.NamaBalai.Contains(param.sSearch)) != null))
                     || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch)));
            }
            Count = collRAWA.Count();

            var sortColumnIndex = iSortCol;
            Func<RAWA, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_RAWA :
                                                                sortColumnIndex == 2 ? c.N_RAWA :
                                                                sortColumnIndex == 3 ? c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty :
                                                                sortColumnIndex == 4 ? c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty :
                //sortColumnIndex == 5 ? c.WS != null && c.WS.BALAIs.FirstOrDefault() != null ? c.WS.BALAIs.FirstOrDefault().NamaBalai : string.Empty :
                                                                sortColumnIndex == 5 ? c.WS != null ? c.WS.N_WS : string.Empty :
                                                                sortColumnIndex == 6 ? c.DAS != null ? c.DAS.N_DAS : string.Empty :
                                                                c.K_RAWA);


            IList<RAWA> collResult = new List<RAWA>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collRAWA.Count() > 0)
            {
                collResult = collRAWA.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_RAWA,
                             c.K_RAWA,
                             c.N_RAWA,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.N_RAWA,
                             BALAIs = c.WS == null ? "" : this.GetNamaBalai(c.WS.BALAIs)
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadAllData2(jQueryDataTableParamModel param)
        {
            int Count = 0;
            var vRawa = this.DefaultRepository.GetAllWithCriteria();
            var iCountRawa = this.DefaultRepository.GetAllWithCriteria();


            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                vRawa.Add(NHibernate.Criterion.Restrictions.Where<RAWA>(x => x.K_RAWA.Contains(param.sSearch) || x.N_RAWA.Contains(param.sSearch)
                    || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)))
                    || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
                     || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch))));

                iCountRawa.Add(NHibernate.Criterion.Restrictions.Where<RAWA>(x => x.K_RAWA.Contains(param.sSearch) || x.N_RAWA.Contains(param.sSearch)
                    || (x.KABUPATEN != null && x.KABUPATEN.N_KABUPATEN.Contains(param.sSearch) || (x.KABUPATEN != null && x.KABUPATEN.PROPINSI != null && x.KABUPATEN.PROPINSI.N_PROPINSI.Contains(param.sSearch)))
                    || (x.WS != null && x.WS.N_WS.Contains(param.sSearch))
                     || (x.DAS != null && x.DAS.N_DAS.Contains(param.sSearch))));
            }

            Count = iCountRawa.SetProjection(NHibernate.Criterion.Projections.RowCount()).FutureValue<Int32>().Value;

            // Kurang Sort
            IList<RAWA> lstRawa = vRawa.SetFirstResult(param.iDisplayStart).SetMaxResults(param.iDisplayLength).List<RAWA>();

            var result = from c in lstRawa
                         select new
                         {
                             KEY = c.K_RAWA,
                             c.K_RAWA,
                             c.N_RAWA,
                             N_PROPINSI = c.KABUPATEN != null ? c.KABUPATEN.PROPINSI != null ? c.KABUPATEN.PROPINSI.N_PROPINSI : string.Empty : string.Empty,
                             N_KABUPATEN = c.KABUPATEN != null ? c.KABUPATEN.N_KABUPATEN : string.Empty,
                             N_WS = c.WS != null ? c.WS.N_WS : string.Empty,
                             N_DAS = c.DAS != null ? c.DAS.N_DAS : string.Empty,
                             c.LatLong,
                             pTitle = c.N_RAWA,
                             BALAIs = c.WS == null ? "" : this.GetNamaBalai(c.WS.BALAIs)
                         };

            int iTotalDisplayRecords = lstRawa.Count;

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        private string GetNamaBalai(ICollection<BALAI> iCollection)
        {
            string sBalai = "";
            if (iCollection != null && iCollection.Count > 0)
            {
                foreach (BALAI balai in iCollection)
                {
                    if (string.IsNullOrWhiteSpace(sBalai))
                    {
                        sBalai = balai.NamaBalai;
                    }
                    else
                    {
                        sBalai = sBalai + ", " + balai.NamaBalai;
                    }
                }
            }
            return sBalai;
        }
    }
}
