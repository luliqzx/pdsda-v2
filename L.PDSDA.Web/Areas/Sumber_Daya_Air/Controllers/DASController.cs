﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.Web.Controllers;
using L.PDSDA.DAL.Repositories;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Repositories.Implements;
using log4net;
using L.Core.General;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air.Controllers
{
    public class DASController : BaseController
    {
        private readonly IDefaultRepository<DAS> DefaultRepository;
        private readonly IWSRepository WSRepository;

        ILog log = LogManager.GetLogger(typeof(DASController));

        public DASController()
        {
            this.DefaultRepository = new DefaultRepository<DAS>();
            this.WSRepository = new WSRepository();
            log.InfoFormat("DASController Initialize by {0} at {1}", this.UserLoggedIn, this.UserHostAddress);

        }

        public ActionResult GetDASByWS(string k_ws)
        {
            IQueryable<DAS> qryCollDAS = this.DefaultRepository.Get(x => x.WS.K_WS == k_ws);
            IList<DAS> collDAS = qryCollDAS == null ? new List<DAS>() : qryCollDAS.ToList();

            return Json(new SelectList(collDAS, "K_DAS", "N_DAS"), JsonRequestBehavior.AllowGet);
        }

        void BuildWS()
        {
            IQueryable<WS> collWS = collAssignWS.Count > 0 ? collAssignWS.AsQueryable() : this.WSRepository.GetAll();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collWS)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.K_WS + " - " + item.N_WS, Value = item.K_WS });
            }
            ViewBag.collWS = collSelectListItem;
        }
        //
        // GET: /Sumber_Daya_Air/DAS/

        public ActionResult Index()
        {
            IQueryable<DAS> collDAS = this.DefaultRepository.GetAll();
            return View(collDAS);
        }

        //
        // GET: /Sumber_Daya_Air/DAS/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Sumber_Daya_Air/DAS/Create

        public ActionResult Create()
        {

            if (!IsCreateable)
            {
                return this.Detail("");
            }
            ViewBag.ActionName = "Create";
            BuildWS();

            return PartialView("Save");
        }

        //
        // POST: /Sumber_Daya_Air/DAS/Create

        [HttpPost]
        public ActionResult Create(DAS das)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (das.WS != null && das.WS.K_WS == "-1")
                    {
                        das.WS = null;
                    }
                    log.InfoFormat("DASController Save {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, das.N_DAS);
                    das.CreateBy = this.UserLoggedIn;
                    das.CreateDate = DateTime.Now;
                    das.CreateTerminal = this.UserHostAddress;
                    das.UpdateBy = this.UserLoggedIn;
                    das.UpdateDate = DateTime.Now;
                    das.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.Save(das);
                    return Json(new { success = true, message = Pesan.A1A });

                }
                return Json(new { success = true, message = Pesan.A2A });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/DAS/Edit/5

        public ActionResult Edit(string id)
        {
            if (!IsUpdateable)
            {
                return this.Detail(id);
            }
            BuildWS();
            DAS DAS = DefaultRepository.Get(x => x.K_DAS == id).FirstOrDefault();
            return PartialView("Save", DAS);
        }
        public ActionResult Detail(string id)
        {
            BuildWS();
            DAS DAS = DefaultRepository.Get(x => x.K_DAS == id).FirstOrDefault();
            return PartialView("Detail", DAS);
        }

        //
        // POST: /Sumber_Daya_Air/DAS/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, DAS das)
        {
            DAS DAS = DefaultRepository.Get(x => x.K_DAS == id).FirstOrDefault();
            if (das.WS != null && das.WS.K_WS == "-1")
            {
                das.WS = null;
            }
            DAS.WS = das.WS;
            Utils.SetTProperty<DAS>(DAS, das);

            try
            {
                if (ModelState.IsValid)
                {
                    // TODO: Add update logic here
                    log.InfoFormat("DASController Update {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, DAS.N_DAS);
                    DAS.UpdateBy = this.UserLoggedIn;
                    DAS.UpdateDate = DateTime.Now;
                    DAS.UpdateTerminal = this.UserHostAddress;
                    this.DefaultRepository.Update(DAS);
                    //return RedirectToAction("Index");
                    return Json(new { success = true, message = Pesan.A1B });
                }
                return Json(new { success = true, message = Pesan.A2B });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Sumber_Daya_Air/DAS/Delete/5

        public ActionResult Delete(string id)
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });

            }
            DAS deleteDomain = this.DefaultRepository.Get(x => x.K_DAS == id).FirstOrDefault();
            if (deleteDomain.WS != null && !collAssignWS.Contains(deleteDomain.WS))
            {
                return Json(new { success = false, message = "Tidak berhak hapus data." });
            }
            if (deleteDomain != null)
            {
                log.InfoFormat("DASController Delete {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, deleteDomain.N_DAS);
                SetNullRelation(deleteDomain);
                this.DefaultRepository.Delete(deleteDomain);
                return Json(new { success = true, message = Pesan.A1C });
            }
            return Json(new { success = false, message = Pesan.A2C });
        }


        public ActionResult DeleteAllDAS()
        {
            if (!IsDeleteable)
            {
                return Json(new { success = false, message = Pesan.A2C1 });
            }

            IList<DAS> collDAS = this.DefaultRepository.GetAll().ToList();

            bool IsFail = false;
            foreach (var deleteDomain in collDAS)
            {

                if (deleteDomain.WS != null && !collAssignWS.Contains(deleteDomain.WS))
                {
                    //return Json(new { success = false, message = "Tidak berhak hapus data." });
                    IsFail = true;
                    break;
                }
                if (deleteDomain != null)
                {
                    log.InfoFormat("DASController Delete {2} by {0} at {1}", this.UserLoggedIn, this.UserHostAddress, deleteDomain.N_DAS);
                    SetNullRelation(deleteDomain);
                    this.DefaultRepository.Delete(deleteDomain);
                }
            }
            if (IsFail)
            {
                return Json(new { success = false, message = Pesan.A2C }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = Pesan.A1C }, JsonRequestBehavior.AllowGet);
            }
        }



        private void SetNullRelation(DAS das)
        {
            if (das.AIRTANAHs != null)
                das.AIRTANAHs.ToList().ForEach(x => { x.DAS = null; });
            if (das.BENDUNGANs != null)
                das.BENDUNGANs.ToList().ForEach(x => { x.DAS = null; });
            if (das.BENDUNGs != null)
                das.BENDUNGs.ToList().ForEach(x => { x.DAS = null; });
            if (das.DAERAHRAWANBANJIRs != null)
                das.DAERAHRAWANBANJIRs.ToList().ForEach(x => { x.DA = null; });
            if (das.DAERAHRAWANKEKERINGANs != null)
                das.DAERAHRAWANKEKERINGANs.ToList().ForEach(x => { x.DA = null; });
            if (das.DAERAHRAWANLONGSORs != null)
                das.DAERAHRAWANLONGSORs.ToList().ForEach(x => { x.DA = null; });
            if (das.DANAUs != null)
                das.DANAUs.ToList().ForEach(x => { x.DAS = null; });
            if (das.EMBUNG_POTENSI != null)
                das.EMBUNG_POTENSI.ToList().ForEach(x => { x.DAS = null; });
            if (das.EMBUNGs != null)
                das.EMBUNGs.ToList().ForEach(x => { x.DAS = null; });
            if (das.GUNUNGBERAPIs != null)
                das.GUNUNGBERAPIs.ToList().ForEach(x => { x.DA = null; });
            if (das.IRIGASIs != null)
                das.IRIGASIs.ToList().ForEach(x => { x.DAS = null; });
            if (das.PIDs != null)
                das.PIDs.ToList().ForEach(x => { x.DA = null; });
            if (das.POS_PENGAMAT != null)
                das.POS_PENGAMAT.ToList().ForEach(x => { x.DA = null; });
            if (das.POSKO_BANJIR != null)
                das.POSKO_BANJIR.ToList().ForEach(x => { x.DA = null; });
            if (das.RAWAs != null)
                das.RAWAs.ToList().ForEach(x => { x.DAS = null; });
            if (das.STASIUNDEBITs != null)
                das.STASIUNDEBITs.ToList().ForEach(x => { x.DA = null; });
            if (das.STASIUNHUJANs != null)
                das.STASIUNHUJANs.ToList().ForEach(x => { x.DA = null; });
            if (das.STASIUNKLIMATOLOGIs != null)
                das.STASIUNKLIMATOLOGIs.ToList().ForEach(x => { x.DA = null; });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<DAS> collDas = this.DefaultRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDas = collDas.Where(x => x.K_DAS.Contains(param.sSearch) || x.N_DAS.Contains(param.sSearch) || (x.WS != null && x.WS.N_WS.Contains(param.sSearch)));
            }
            Count = collDas.Count();

            var sortColumnIndex = iSortCol;
            Func<DAS, string> orderingFunction = (c => sortColumnIndex == 1 ? c.K_DAS :
                                                                sortColumnIndex == 2 ? c.N_DAS :
                                                                sortColumnIndex == 3 ? c.WS != null ? c.WS.N_WS : string.Empty : c.K_DAS);


            IList<DAS> collDAS = new List<DAS>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDas.Count() > 0)
            {
                collDAS = collDas.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collDAS = collDAS.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collDAS = collDAS.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collDAS
                         select new
                         {
                             KEY = c.K_DAS,
                             K_DAS = c.K_DAS,
                             N_DAS = c.N_DAS,
                             N_WS = c.WS == null ? "" : c.WS.N_WS,
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
