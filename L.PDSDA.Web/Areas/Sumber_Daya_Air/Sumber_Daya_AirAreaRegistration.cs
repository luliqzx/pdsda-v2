﻿using System.Web.Mvc;

namespace L.PDSDA.Web.Areas.Sumber_Daya_Air
{
    public class Sumber_Daya_AirAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Sumber_Daya_Air";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Sumber_Daya_Air_default",
                "Sumber_Daya_Air/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
