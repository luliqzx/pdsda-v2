﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm12.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm12" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td rowspan="2">
                    Keterangan
                </td>
                <td colspan="3">
                    Masa Tanam I
                </td>
                <td colspan="3">
                    Masa Tanam II
                </td>
                <td colspan="3">
                    o Masa Tanam III
                </td>
                <td rowspan="2">
                    o IPo
                </td>
            </tr>
            <tr>
                <td>
                    Padi
                </td>
                <td>
                    Palawija
                </td>
                <td>
                    Lain - lain
                </td>
                <td>
                    Padi
                </td>
                <td>
                    Palawija
                </td>
                <td>
                    Lain - lain
                </td>
                <td>
                    Padi
                </td>
                <td>
                    Palawija
                </td>
                <td>
                    Lain - lain
                </td>
            </tr>
            <tr>
                <td>
                    Luas Tanam
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_PD)@Html.ValidationMessageFor(model => model.MT1_PD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_PL)@Html.ValidationMessageFor(model => model.MT1_PL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_LAIN)@Html.ValidationMessageFor(model => model.MT1_LAIN)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_PD)@Html.ValidationMessageFor(model => model.MT2_PD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_PL)@Html.ValidationMessageFor(model => model.MT2_PL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_LAIN)@Html.ValidationMessageFor(model => model.MT2_LAIN)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_PD)@Html.ValidationMessageFor(model => model.MT3_PD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_PL)@Html.ValidationMessageFor(model => model.MT3_PL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_LAIN)@Html.ValidationMessageFor(model => model.MT3_LAIN)
                </td>
                <td>
                    @Html.EditorFor(model => model.IP)@Html.ValidationMessageFor(model => model.IP)
                </td>
            </tr>
            <tr>
                <td>
                    Prediksi Luas Tanam
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT1_PD)@Html.ValidationMessageFor(model => model.PRED_MT1_PD)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT1_PL)@Html.ValidationMessageFor(model => model.PRED_MT1_PL)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT1_LAIN)@Html.ValidationMessageFor(model =>
                    model.PRED_MT1_LAIN)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT2_PD)@Html.ValidationMessageFor(model => model.PRED_MT2_PD)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT2_PL)@Html.ValidationMessageFor(model => model.PRED_MT2_PL)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT2_LAIN)@Html.ValidationMessageFor(model =>
                    model.PRED_MT2_LAIN)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT3_PD)@Html.ValidationMessageFor(model => model.PRED_MT3_PD)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT3_PL)@Html.ValidationMessageFor(model => model.PRED_MT3_PL)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_MT3_LAIN)@Html.ValidationMessageFor(model =>
                    model.PRED_MT3_LAIN)
                </td>
                <td>
                    @Html.EditorFor(model => model.PRED_IP)@Html.ValidationMessageFor(model => model.PRED_IP)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
