﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

namespace L.PDSDA.Web.Sample
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                L.PDSDA.Web.pdsdadbDataSetTableAdapters.airtanahTableAdapter tableadapter = new L.PDSDA.Web.pdsdadbDataSetTableAdapters.airtanahTableAdapter();
                L.PDSDA.Web.pdsdadbDataSet.airtanahDataTable datatable = tableadapter.GetData(); //new L.PDSDA.Web.pdsdadbDataSet.airtanahDataTable();
                //tableadapter.Fill(datatable);
                ReportDataSource reportds = new ReportDataSource("DataSet1", datatable as DataTable);

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/Report1.rdlc");
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(reportds);
                ReportViewer1.LocalReport.Refresh();
            }

        }
    }
}