﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PID_MASA_TANAM.aspx.cs"
    Inherits="L.PDSDA.Web.Sample.PID_MASA_TANAM" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td rowspan="2">
                    Rehabilitasi
                </td>
                <td rowspan="2">
                    Luas Lahan (Ha)
                </td>
                <td colspan="3">
                    Masa Tanam I
                </td>
                <td colspan="3">
                    Masa Tanam II
                </td>
                <td colspan="3">
                    Masa Tanam III
                </td>
                <td rowspan="2">
                    IP
                </td>
            </tr>
            <tr>
                <td>
                    Padi
                </td>
                <td>
                    Palawija
                </td>
                <td>
                    Lain - lain
                </td>
                <td>
                    Padi
                </td>
                <td>
                    Palawija
                </td>
                <td>
                    Lain - lain
                </td>
                <td>
                    Padi
                </td>
                <td>
                    Palawija
                </td>
                <td>
                    Lain - lain
                </td>
            </tr>
            <tr>
                <td>
                    Sebelum
                </td>
                <td>
                    @Html.EditorFor(model => model.LUAS_SBL)@Html.ValidationMessageFor(model => model.LUAS_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_PD_SBL)@Html.ValidationMessageFor(model => model.MT1_PD_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_PL_SBL)@Html.ValidationMessageFor(model => model.MT1_PL_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_LAIN_SBL)@Html.ValidationMessageFor(model =>
                    model.MT1_LAIN_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_PD_SBL)@Html.ValidationMessageFor(model => model.MT2_PD_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_PL_SBL)@Html.ValidationMessageFor(model => model.MT2_PL_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_LAIN_SBL)@Html.ValidationMessageFor(model =>
                    model.MT2_LAIN_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_PD_SBL)@Html.ValidationMessageFor(model => model.MT3_PD_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_PL_SBL)@Html.ValidationMessageFor(model => model.MT3_PL_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_LAIN_SBL)@Html.ValidationMessageFor(model =>
                    model.MT3_LAIN_SBL)
                </td>
                <td>
                    @Html.EditorFor(model => model.IP_SBL)@Html.ValidationMessageFor(model => model.IP_SBL)
                </td>
            </tr>
            <tr>
                <td>
                    Sesudah
                </td>
                <td>
                    @Html.EditorFor(model => model.LUAS_SSD)@Html.ValidationMessageFor(model => model.LUAS_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_PD_SSD)@Html.ValidationMessageFor(model => model.MT1_PD_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_PL_SSD)@Html.ValidationMessageFor(model => model.MT1_PL_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT1_LAIN_SSD)@Html.ValidationMessageFor(model =>
                    model.MT1_LAIN_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_PD_SSD)@Html.ValidationMessageFor(model => model.MT2_PD_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_PL_SSD)@Html.ValidationMessageFor(model => model.MT2_PL_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT2_LAIN_SSD)@Html.ValidationMessageFor(model =>
                    model.MT2_LAIN_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_PD_SSD)@Html.ValidationMessageFor(model => model.MT3_PD_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_PL_SSD)@Html.ValidationMessageFor(model => model.MT3_PL_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.MT3_LAIN_SSD)@Html.ValidationMessageFor(model =>
                    model.MT3_LAIN_SSD)
                </td>
                <td>
                    @Html.EditorFor(model => model.IP_SSD)@Html.ValidationMessageFor(model => model.IP_SSD)
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;">
            <tr>
                <td>
                    P3A
                </td>
                <td>
                    Total
                </td>
                <td>
                    Aktif
                </td>
                <td>
                    Berkembang
                </td>
                <td>
                    Tidak berkembang
                </td>
                <td>
                    Pasif
                </td>
            </tr>
            <tr>
                <td>
                    Jumlah
                </td>
                <td>
                    @Html.EditorFor(model => model.P3A)@Html.ValidationMessageFor(model => model.P3A)
</td><td>@Html.EditorFor(model => model.JML_AKTIF)@Html.ValidationMessageFor(model => model.JML_AKTIF)
</td><td>@Html.EditorFor(model => model.JML_BERKEMBANG)@Html.ValidationMessageFor(model => model.JML_BERKEMBANG)
</td><td>@Html.EditorFor(model => model.JML_TIDAKBERKEMBANG)@Html.ValidationMessageFor(model => model.JML_TIDAKBERKEMBANG)
</td><td>@Html.EditorFor(model => model.JML_PASIF)@Html.ValidationMessageFor(model => model.JML_PASIF)
</td>
            </tr>
            <tr>
                <td>
                    Anggota
                </td>
                <td>
                    @Html.EditorFor(model => model.ANGGOTA)@Html.ValidationMessageFor(model => model.ANGGOTA)
</td><td>@Html.EditorFor(model => model.JML_AKTIF_ANGGOTA)@Html.ValidationMessageFor(model => model.JML_AKTIF_ANGGOTA)
</td><td>@Html.EditorFor(model => model.JML_BERKEMBANG_ANGGOTA)@Html.ValidationMessageFor(model => model.JML_BERKEMBANG_ANGGOTA)
</td><td>@Html.EditorFor(model => model.JML_TIDAKBERKEMBANG_ANGGOTA)@Html.ValidationMessageFor(model => model.JML_TIDAKBERKEMBANG_ANGGOTA)
</td><td>@Html.EditorFor(model => model.JML_PASIF_ANGGOTA)@Html.ValidationMessageFor(model => model.JML_PASIF_ANGGOTA)
</td>
            </tr>
            <tr>
                <td>
                    Berbadan Hukum
                </td>
                <td>
                    @Html.EditorFor(model => model.BADAN_HUKUM)@Html.ValidationMessageFor(model => model.BADAN_HUKUM)
</td><td>@Html.EditorFor(model => model.JML_AKTIF_BH)@Html.ValidationMessageFor(model => model.JML_AKTIF_BH)
</td><td>@Html.EditorFor(model => model.JML_BERKEMBANG_BH)@Html.ValidationMessageFor(model => model.JML_BERKEMBANG_BH)
</td><td>@Html.EditorFor(model => model.JML_TIDAKBERKEMBANG_BH)@Html.ValidationMessageFor(model => model.JML_TIDAKBERKEMBANG_BH)
</td><td>@Html.EditorFor(model => model.JML_PASIF_BH)@Html.ValidationMessageFor(model => model.JML_PASIF_BH)
</td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
