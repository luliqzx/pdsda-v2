﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm11.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm11" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td rowspan="2">
                    Kondisi
                </td>
                <td colspan="2">
                    Kantong Lumpur
                </td>
                <td colspan="2">
                    Pengatur
                </td>
                <td colspan="2">
                    Talang
                </td>
                <td colspan="2">
                    Syphon
                </td>
                <td>
                    Jembatan
                </td>
                <td>
                    Gorong2
                </td>
                <td colspan="2">
                    Got Miring
                </td>
                <td>
                    Pelimpah
                </td>
                <td colspan="2">
                    Bangunan Pembilas
                </td>
                <td colspan="2">
                    Gorong 2 Pembuang
                </td>
                <td colspan="2">
                    Lain - lain
                </td>
            </tr>
            <tr>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
            </tr>
            <tr>
                <td>
                    Jumlah
                </td>
                <td>
                    @Html.EditorFor(model => model.KT_LUMPUR_JML)@Html.ValidationMessageFor(model =>
                    model.KT_LUMPUR_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.KT_LUMPURP_JML)@Html.ValidationMessageFor(model =>
                    model.KT_LUMPURP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PENGATUR_JML)@Html.ValidationMessageFor(model =>
                    model.PENGATUR_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PENGATURP_JML)@Html.ValidationMessageFor(model =>
                    model.PENGATURP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.TALANG_JML)@Html.ValidationMessageFor(model => model.TALANG_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.TALANGP_JML)@Html.ValidationMessageFor(model => model.TALANGP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SYPHON_JML)@Html.ValidationMessageFor(model => model.SYPHON_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SYPHONP_JML)@Html.ValidationMessageFor(model => model.SYPHONP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.JEMBATAN_JML)@Html.ValidationMessageFor(model =>
                    model.JEMBATAN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_JML)@Html.ValidationMessageFor(model => model.GOR_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOT_JML)@Html.ValidationMessageFor(model => model.GOT_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOTP_JML)@Html.ValidationMessageFor(model => model.GOTP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PELIMPAH_JML)@Html.ValidationMessageFor(model =>
                    model.PELIMPAH_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMBILAS_JML)@Html.ValidationMessageFor(model =>
                    model.PEMBILAS_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMBILASP_JML)@Html.ValidationMessageFor(model =>
                    model.PEMBILASP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_PEMB_JML)@Html.ValidationMessageFor(model =>
                    model.GOR_PEMB_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_PEMBP_JML)@Html.ValidationMessageFor(model =>
                    model.GOR_PEMBP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.LAIN_JML)@Html.ValidationMessageFor(model => model.LAIN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.LAINP_JML)@Html.ValidationMessageFor(model => model.LAINP_JML)
                </td>
            </tr>
            <tr>
                <td>
                    Baik
                </td>
                <td>
                    @Html.EditorFor(model => model.KT_LUMPUR_B)@Html.ValidationMessageFor(model => model.KT_LUMPUR_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.KT_LUMPURP_B)@Html.ValidationMessageFor(model =>
                    model.KT_LUMPURP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PENGATUR_B)@Html.ValidationMessageFor(model => model.PENGATUR_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PENGATURP_B)@Html.ValidationMessageFor(model => model.PENGATURP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.TALANG_B)@Html.ValidationMessageFor(model => model.TALANG_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.TALANGP_B)@Html.ValidationMessageFor(model => model.TALANGP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SYPHON_B)@Html.ValidationMessageFor(model => model.SYPHON_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SYPHONP_B)@Html.ValidationMessageFor(model => model.SYPHONP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.JEMBATAN_B)@Html.ValidationMessageFor(model => model.JEMBATAN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_B)@Html.ValidationMessageFor(model => model.GOR_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOT_B)@Html.ValidationMessageFor(model => model.GOT_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOTP_B)@Html.ValidationMessageFor(model => model.GOTP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PELIMPAH_B)@Html.ValidationMessageFor(model => model.PELIMPAH_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMBILAS_B)@Html.ValidationMessageFor(model => model.PEMBILAS_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMBILASP_B)@Html.ValidationMessageFor(model => model.PEMBILASP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_PEMB_B)@Html.ValidationMessageFor(model => model.GOR_PEMB_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_PEMBP_B)@Html.ValidationMessageFor(model => model.GOR_PEMBP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.LAIN_B)@Html.ValidationMessageFor(model => model.LAIN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.LAINP_B)@Html.ValidationMessageFor(model => model.LAINP_B)
                </td>
            </tr>
            <tr>
                <td>
                    Rusak Ringan
                </td>
                <td>
                    @Html.EditorFor(model => model.KT_LUMPUR_RR)@Html.ValidationMessageFor(model =>
                    model.KT_LUMPUR_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.KT_LUMPURP_RR)@Html.ValidationMessageFor(model =>
                    model.KT_LUMPURP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PENGATUR_RR)@Html.ValidationMessageFor(model => model.PENGATUR_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PENGATURP_RR)@Html.ValidationMessageFor(model =>
                    model.PENGATURP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.TALANG_RR)@Html.ValidationMessageFor(model => model.TALANG_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.TALANGP_RR)@Html.ValidationMessageFor(model => model.TALANGP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SYPHON_RR)@Html.ValidationMessageFor(model => model.SYPHON_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SYPHONP_RR)@Html.ValidationMessageFor(model => model.SYPHONP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.JEMBATAN_RR)@Html.ValidationMessageFor(model => model.JEMBATAN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_RR)@Html.ValidationMessageFor(model => model.GOR_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOT_RR)@Html.ValidationMessageFor(model => model.GOT_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOTP_RR)@Html.ValidationMessageFor(model => model.GOTP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PELIMPAH_RR)@Html.ValidationMessageFor(model => model.PELIMPAH_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMBILAS_RR)@Html.ValidationMessageFor(model => model.PEMBILAS_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMBILASP_RR)@Html.ValidationMessageFor(model =>
                    model.PEMBILASP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_PEMB_RR)@Html.ValidationMessageFor(model => model.GOR_PEMB_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_PEMBP_RR)@Html.ValidationMessageFor(model =>
                    model.GOR_PEMBP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.LAIN_RR)@Html.ValidationMessageFor(model => model.LAIN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.LAINP_RR)@Html.ValidationMessageFor(model => model.LAINP_RR)
                </td>
            </tr>
            <tr>
                <td>
                    Rusak Berat
                </td>
                <td>
                    @Html.EditorFor(model => model.KT_LUMPUR_RB)@Html.ValidationMessageFor(model =>
                    model.KT_LUMPUR_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.KT_LUMPURP_RB)@Html.ValidationMessageFor(model =>
                    model.KT_LUMPURP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.PENGATUR_RB)@Html.ValidationMessageFor(model => model.PENGATUR_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.PENGATURP_RB)@Html.ValidationMessageFor(model =>
                    model.PENGATURP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.TALANG_RB)@Html.ValidationMessageFor(model => model.TALANG_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.TALANGP_RB)@Html.ValidationMessageFor(model => model.TALANGP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SYPHON_RB)@Html.ValidationMessageFor(model => model.SYPHON_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SYPHONP_RB)@Html.ValidationMessageFor(model => model.SYPHONP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.JEMBATAN_RB)@Html.ValidationMessageFor(model => model.JEMBATAN_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_RB)@Html.ValidationMessageFor(model => model.GOR_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOT_RB)@Html.ValidationMessageFor(model => model.GOT_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOTP_RB)@Html.ValidationMessageFor(model => model.GOTP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.PELIMPAH_RB)@Html.ValidationMessageFor(model => model.PELIMPAH_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMBILAS_RB)@Html.ValidationMessageFor(model => model.PEMBILAS_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMBILASP_RB)@Html.ValidationMessageFor(model =>
                    model.PEMBILASP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_PEMB_RB)@Html.ValidationMessageFor(model => model.GOR_PEMB_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.GOR_PEMBP_RB)@Html.ValidationMessageFor(model =>
                    model.GOR_PEMBP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.LAIN_RB)@Html.ValidationMessageFor(model => model.LAIN_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.LAINP_RB)@Html.ValidationMessageFor(model => model.LAINP_RB)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
