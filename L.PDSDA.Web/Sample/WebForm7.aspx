﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm7.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm7" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td rowspan="2">
                    Jenis Bangungan
                </td>
                <td colspan="4">
                    Kondisi Bangunan
                </td>
                <td colspan="4">
                    Kondisi Pintu Bangunan
                </td>
            </tr>
            <tr>
                <td>
                    Jumlah
                </td>
                <td>
                    Baik
                </td>
                <td>
                    Rusak Ringan
                </td>
                <td>
                    Rusak Berat
                </td>
                <td>
                    Jumlah
                </td>
                <td>
                    Baik
                </td>
                <td>
                    Rusak Ringan
                </td>
                <td>
                    Rusak Berat
                </td>
            </tr>
            <tr>
                <td>
                    Jembatan
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_JEMBATAN_JML)@Html.ValidationMessageFor(model
                    => model.PEMB_JEMBATAN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_JEMBATAN_B)@Html.ValidationMessageFor(model
                    => model.PEMB_JEMBATAN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_JEMBATAN_RR)@Html.ValidationMessageFor(model
                    => model.PEMB_JEMBATAN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_JEMBATAN_RB)@Html.ValidationMessageFor(model
                    => model.PEMB_JEMBATAN_RB)
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Gorong - gorong
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_GOR_JML)@Html.ValidationMessageFor(model =>
                    model.PEMB_GOR_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_GOR_B)@Html.ValidationMessageFor(model => model.PEMB_GOR_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_GOR_RR)@Html.ValidationMessageFor(model => model.PEMB_GOR_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_GOR_RB)@Html.ValidationMessageFor(model => model.PEMB_GOR_RB)
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Pintu Klep
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_KLEP_PIN_JML)@Html.ValidationMessageFor(model
                    => model.PEMB_KLEP_PIN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_KLEP_PIN_B)@Html.ValidationMessageFor(model
                    => model.PEMB_KLEP_PIN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_KLEP_PIN_RR)@Html.ValidationMessageFor(model
                    => model.PEMB_KLEP_PIN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_KLEP_PIN_RB)@Html.ValidationMessageFor(model
                    => model.PEMB_KLEP_PIN_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_KLEP_PINP_JML)@Html.ValidationMessageFor(model
                    => model.PEMB_KLEP_PINP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_KLEP_PINP_B)@Html.ValidationMessageFor(model
                    => model.PEMB_KLEP_PINP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_KLEP_PINP_RR)@Html.ValidationMessageFor(model
                    => model.PEMB_KLEP_PINP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_KLEP_PINP_RB)@Html.ValidationMessageFor(model
                    => model.PEMB_KLEP_PINP_RB)
                </td>
            </tr>
            <tr>
                <td>
                    Terjun
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_TERJUN_JML)@Html.ValidationMessageFor(model
                    => model.PEMB_TERJUN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_TERJUN_B)@Html.ValidationMessageFor(model =>
                    model.PEMB_TERJUN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_TERJUN_RR)@Html.ValidationMessageFor(model =>
                    model.PEMB_TERJUN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_TERJUN_RB)@Html.ValidationMessageFor(model =>
                    model.PEMB_TERJUN_RB)
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Lain - lain
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_LAIN_JML)@Html.ValidationMessageFor(model =>
                    model.PEMB_LAIN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_LAIN_B)@Html.ValidationMessageFor(model => model.PEMB_LAIN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_LAIN_RR)@Html.ValidationMessageFor(model =>
                    model.PEMB_LAIN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_LAIN_RB)@Html.ValidationMessageFor(model =>
                    model.PEMB_LAIN_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_LAINP_JML)@Html.ValidationMessageFor(model =>
                    model.PEMB_LAINP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_LAINP_B)@Html.ValidationMessageFor(model =>
                    model.PEMB_LAINP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_LAINP_RR)@Html.ValidationMessageFor(model =>
                    model.PEMB_LAINP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.PEMB_LAINP_RB)@Html.ValidationMessageFor(model =>
                    model.PEMB_LAINP_RB)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
