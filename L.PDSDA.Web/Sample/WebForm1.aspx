﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="heightAuto">
        <table class="noborder">
            <tr>
                <td class="editor-label">
                    Tahun
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.TAHUN_DATA) @Html.ValidationMessageFor(model => model.TAHUN_DATA)
                </td>
                <td class="editor-label">
                    Sumber Data
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.SUMBER_DATA) @Html.ValidationMessageFor(model =>
                    model.SUMBER_DATA)
                </td>
            </tr>
        </table>
        <br />
        <table class="noborder">
            <tr>
                <td class="editor-label">
                    Tingkatan
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.K_TINGKATAN) @Html.ValidationMessageFor(model =>
                    model.K_TINGKATAN)
                </td>
                <td class="editor-label">
                    Luas Rencana (Ha)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.LUAS_RENCANA) @Html.ValidationMessageFor(model =>
                    model.LUAS_RENCANA)
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td colspan="6" style="text-align: center">
                    Luas Fungsional (Ada Jaringan Utama) Ha
                </td>
            </tr>
            <tr>
                <td colspan="3" class="style1">
                    Sudah Sawah
                </td>
                <td colspan="2" class="style1">
                    Belum Sawah
                </td>
                <td rowspan="2" class="style1">
                    Jumlah
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Jar. Tersier Terbangun
                </td>
                <td class="style1">
                    Jar. Tersier Belum Ada
                </td>
                <td class="style1">
                    Alih Fungsi
                </td>
                <td class="style1">
                    Belum Sawah
                </td>
                <td class="style1">
                    Alih Fungsi
                </td>
            </tr>
            <tr>
                <td>
                    @Html.EditorFor(model => model.JAR_OPTIMAL) @Html.ValidationMessageFor(model =>
                    model.JAR_OPTIMAL)
                </td>
                <td>
                    @Html.EditorFor(model => model.JAR_BOPTIMAL) @Html.ValidationMessageFor(model =>
                    model.JAR_BOPTIMAL)
                </td>
                <td>
                    @Html.EditorFor(model => model.JAR_ALFUNG) @Html.ValidationMessageFor(model => model.JAR_ALFUNG)
                </td>
                <td>
                    @Html.EditorFor(model => model.JAR_ALFUNGDRBS) @Html.ValidationMessageFor(model
                    => model.JAR_ALFUNGDRBS)
                </td>
                <td>
                    @Html.EditorFor(model => model.JAR_BSAWAH) @Html.ValidationMessageFor(model => model.JAR_BSAWAH)
                </td>
                <td>
                    Ini Jumlah
                </td>
            </tr>
        </table>
        <table class="noborder">
            <tr>
                <td>
                    @Html.EditorFor(model => model.JML_TIDAKBERKEMBANG_BH) @Html.ValidationMessageFor(model
                    => model.JML_TIDAKBERKEMBANG_BH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.BJAR_SAWAH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.BJAR_SAWAH) @Html.ValidationMessageFor(model => model.BJAR_SAWAH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.BJAR_BSAWAH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.BJAR_BSAWAH) @Html.ValidationMessageFor(model =>
                    model.BJAR_BSAWAH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.P3A)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.P3A) @Html.ValidationMessageFor(model => model.P3A)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.ANGGOTA) @Html.ValidationMessageFor(model => model.ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.BADAN_HUKUM)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.BADAN_HUKUM) @Html.ValidationMessageFor(model =>
                    model.BADAN_HUKUM)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.DEBIT_RENCANA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.DEBIT_RENCANA) @Html.ValidationMessageFor(model =>
                    model.DEBIT_RENCANA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.DEBIT_KENYATAAN)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.DEBIT_KENYATAAN) @Html.ValidationMessageFor(model
                    => model.DEBIT_KENYATAAN)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JALAN_PJ)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JALAN_PJ) @Html.ValidationMessageFor(model => model.JALAN_PJ)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JALAN_B)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JALAN_B) @Html.ValidationMessageFor(model => model.JALAN_B)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JALAN_RR)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JALAN_RR) @Html.ValidationMessageFor(model => model.JALAN_RR)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JALAN_RB)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JALAN_RB) @Html.ValidationMessageFor(model => model.JALAN_RB)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_AKTIF)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_AKTIF) @Html.ValidationMessageFor(model => model.JML_AKTIF)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_AKTIF_ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_AKTIF_ANGGOTA) @Html.ValidationMessageFor(model
                    => model.JML_AKTIF_ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_AKTIF_BH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_AKTIF_BH) @Html.ValidationMessageFor(model =>
                    model.JML_AKTIF_BH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_BERKEMBANG)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_BERKEMBANG) @Html.ValidationMessageFor(model
                    => model.JML_BERKEMBANG)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_BERKEMBANG_ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_BERKEMBANG_ANGGOTA) @Html.ValidationMessageFor(model
                    => model.JML_BERKEMBANG_ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_BERKEMBANG_BH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_BERKEMBANG_BH) @Html.ValidationMessageFor(model
                    => model.JML_BERKEMBANG_BH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_TIDAKBERKEMBANG)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_TIDAKBERKEMBANG) @Html.ValidationMessageFor(model
                    => model.JML_TIDAKBERKEMBANG)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_TIDAKBERKEMBANG_ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_TIDAKBERKEMBANG_ANGGOTA) @Html.ValidationMessageFor(model
                    => model.JML_TIDAKBERKEMBANG_ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_TIDAKBERKEMBANG_BH)
                </td>
                <td class="editor-field">
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_PASIF)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_PASIF) @Html.ValidationMessageFor(model => model.JML_PASIF)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_PASIF_ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_PASIF_ANGGOTA) @Html.ValidationMessageFor(model
                    => model.JML_PASIF_ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_PASIF_BH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_PASIF_BH) @Html.ValidationMessageFor(model =>
                    model.JML_PASIF_BH)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
