﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm6.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td rowspan="3">
                    Jenis Saluran
                </td>
                <td colspan="4">
                    Panjang (Km)
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                    Jumlah
                </td>
                <td colspan="3">
                    Kondisi
                </td>
            </tr>
            <tr>
                <td>
                    Baik
                </td>
                <td>
                    Rusak Ringan
                </td>
                <td>
                    Rusak Berat
                </td>
            </tr>
            <tr>
                <td>
                    Saluran Induk
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_INDUK_PJ)@Html.ValidationMessageFor(model =>
                    model.SAL_INDUK_PJ)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_INDUK_B)@Html.ValidationMessageFor(model => model.SAL_INDUK_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_INDUK_RR)@Html.ValidationMessageFor(model =>
                    model.SAL_INDUK_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_INDUK_RB)@Html.ValidationMessageFor(model =>
                    model.SAL_INDUK_RB)
                </td>
            </tr>
            <tr>
                <td>
                    Saluran Sekunder
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_SEKUNDER_PJ)@Html.ValidationMessageFor(model
                    => model.SAL_SEKUNDER_PJ)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_SEKUNDER_B)@Html.ValidationMessageFor(model =>
                    model.SAL_SEKUNDER_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_SEKUNDER_RR)@Html.ValidationMessageFor(model
                    => model.SAL_SEKUNDER_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_SEKUNDER_RB)@Html.ValidationMessageFor(model
                    => model.SAL_SEKUNDER_RB)
                </td>
            </tr>
            <tr>
                <td>
                    Saluran Pembuangan
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_PEMBUANG_PJ)@Html.ValidationMessageFor(model
                    => model.SAL_PEMBUANG_PJ)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_PEMBUANG_B)@Html.ValidationMessageFor(model =>
                    model.SAL_PEMBUANG_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_PEMBUANG_RR)@Html.ValidationMessageFor(model
                    => model.SAL_PEMBUANG_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_PEMBUANG_RB)@Html.ValidationMessageFor(model
                    => model.SAL_PEMBUANG_RB)
                </td>
            </tr>
            <tr>
                <td>
                    Saluran Suplesi
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_SUPLESI_PJ)@Html.ValidationMessageFor(model =>
                    model.SAL_SUPLESI_PJ)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_SUPLESI_B)@Html.ValidationMessageFor(model =>
                    model.SAL_SUPLESI_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_SUPLESI_RR)@Html.ValidationMessageFor(model =>
                    model.SAL_SUPLESI_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_SUPLESI_RB)@Html.ValidationMessageFor(model =>
                    model.SAL_SUPLESI_RB)
                </td>
            </tr>
            <tr>
                <td>
                    Saluran Gendong
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_GENDONG_PJ)@Html.ValidationMessageFor(model =>
                    model.SAL_GENDONG_PJ)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_GENDONG_B)@Html.ValidationMessageFor(model =>
                    model.SAL_GENDONG_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_GENDONG_RR)@Html.ValidationMessageFor(model =>
                    model.SAL_GENDONG_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SAL_GENDONG_RB)@Html.ValidationMessageFor(model =>
                    model.SAL_GENDONG_RB)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
