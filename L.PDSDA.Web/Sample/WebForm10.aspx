﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm10.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm10" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td rowspan="2">
                    Keterangan
                </td>
                <td colspan="4">
                    Bangunan
                </td>
                <td colspan="4">
                    Pintu
                </td>
            </tr>
            <tr>
                <td>
                    Jumlah
                </td>
                <td>
                    Baik
                </td>
                <td>
                    Rusak Ringan
                </td>
                <td>
                    Rusak Berat
                </td>
                <td>
                    Jumlah
                </td>
                <td>
                    Baik
                </td>
                <td>
                    Rusak Ringan
                </td>
                <td>
                    Rusak Berat
                </td>
            </tr>
            <tr>
                <td>
                    Bangunan Bagi
                </td>
                <td>
                    @Html.EditorFor(model => model.BAGI_JML)@Html.ValidationMessageFor(model => model.BAGI_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.BAGI_B)@Html.ValidationMessageFor(model => model.BAGI_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.BAGI_RR)@Html.ValidationMessageFor(model => model.BAGI_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.BAGI_RB)@Html.ValidationMessageFor(model => model.BAGI_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.BAGIP_JML)@Html.ValidationMessageFor(model => model.BAGIP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.BAGIP_B)@Html.ValidationMessageFor(model => model.BAGIP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.BAGIP_RR)@Html.ValidationMessageFor(model => model.BAGIP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.BAGIP_RB)@Html.ValidationMessageFor(model => model.BAGIP_RB)
                </td>
            </tr>
            <tr>
                <td>
                    Bangunan Bagi Sadap
                </td>
                <td>
                    @Html.EditorFor(model => model.BSADAP_JML)@Html.ValidationMessageFor(model => model.BSADAP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.BSADAP_B)@Html.ValidationMessageFor(model => model.BSADAP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.BSADAP_RR)@Html.ValidationMessageFor(model => model.BSADAP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.BSADAP_RB)@Html.ValidationMessageFor(model => model.BSADAP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.BSADAPP_JML)@Html.ValidationMessageFor(model => model.BSADAPP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.BSADAPP_B)@Html.ValidationMessageFor(model => model.BSADAPP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.BSADAPP_RR)@Html.ValidationMessageFor(model => model.BSADAPP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.BSADAPP_RB)@Html.ValidationMessageFor(model => model.BSADAPP_RB)
                </td>
            </tr>
            <tr>
                <td>
                    Bangunan Sadap
                </td>
                <td>
                    @Html.EditorFor(model => model.SADAP_JML)@Html.ValidationMessageFor(model => model.SADAP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SADAP_B)@Html.ValidationMessageFor(model => model.SADAP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SADAP_RR)@Html.ValidationMessageFor(model => model.SADAP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SADAP_RB)@Html.ValidationMessageFor(model => model.SADAP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SADAPP_JML)@Html.ValidationMessageFor(model => model.SADAPP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SADAPP_B)@Html.ValidationMessageFor(model => model.SADAPP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SADAPP_RR)@Html.ValidationMessageFor(model => model.SADAPP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SADAPP_RB)@Html.ValidationMessageFor(model => model.SADAPP_RB)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
