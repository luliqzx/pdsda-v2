﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td class="editor-label">
                    @Html.LabelFor(model => model.TAHUN_DATA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.TAHUN_DATA) @Html.ValidationMessageFor(model => model.TAHUN_DATA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.SUMBER_DATA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.SUMBER_DATA) @Html.ValidationMessageFor(model =>
                    model.SUMBER_DATA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.K_TINGKATAN)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.K_TINGKATAN) @Html.ValidationMessageFor(model =>
                    model.K_TINGKATAN)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.LUAS_RENCANA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.LUAS_RENCANA) @Html.ValidationMessageFor(model =>
                    model.LUAS_RENCANA)
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td>
                    Kata 1
                </td>
            </tr>
            <tr>
                <td>
                    Kata 2
                </td>
            </tr>
            <tr>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JAR_OPTIMAL)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JAR_BOPTIMAL)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JAR_ALFUNG)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JAR_ALFUNGDRBS)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JAR_BSAWAH)
                </td>
                <td>
                    Jumlah
                </td>
                <!-- disini 1 -->
            </tr>
            <tr>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JAR_OPTIMAL) @Html.ValidationMessageFor(model =>
                    model.JAR_OPTIMAL)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JAR_BOPTIMAL) @Html.ValidationMessageFor(model =>
                    model.JAR_BOPTIMAL)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JAR_ALFUNG) @Html.ValidationMessageFor(model => model.JAR_ALFUNG)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JAR_ALFUNGDRBS) @Html.ValidationMessageFor(model
                    => model.JAR_ALFUNGDRBS)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JAR_BSAWAH) @Html.ValidationMessageFor(model => model.JAR_BSAWAH)
                </td>
                <td>
                    Jumlah
                </td>
                <!-- Disini -->
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td class="editor-label">
                    @Html.LabelFor(model => model.BJAR_SAWAH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.BJAR_SAWAH) @Html.ValidationMessageFor(model => model.BJAR_SAWAH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.BJAR_BSAWAH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.BJAR_BSAWAH) @Html.ValidationMessageFor(model =>
                    model.BJAR_BSAWAH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.P3A)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.P3A) @Html.ValidationMessageFor(model => model.P3A)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.ANGGOTA) @Html.ValidationMessageFor(model => model.ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.BADAN_HUKUM)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.BADAN_HUKUM) @Html.ValidationMessageFor(model =>
                    model.BADAN_HUKUM)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.DEBIT_RENCANA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.DEBIT_RENCANA) @Html.ValidationMessageFor(model =>
                    model.DEBIT_RENCANA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.DEBIT_KENYATAAN)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.DEBIT_KENYATAAN) @Html.ValidationMessageFor(model
                    => model.DEBIT_KENYATAAN)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JALAN_PJ)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JALAN_PJ) @Html.ValidationMessageFor(model => model.JALAN_PJ)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JALAN_B)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JALAN_B) @Html.ValidationMessageFor(model => model.JALAN_B)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JALAN_RR)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JALAN_RR) @Html.ValidationMessageFor(model => model.JALAN_RR)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JALAN_RB)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JALAN_RB) @Html.ValidationMessageFor(model => model.JALAN_RB)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_AKTIF)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_AKTIF) @Html.ValidationMessageFor(model => model.JML_AKTIF)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_AKTIF_ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_AKTIF_ANGGOTA) @Html.ValidationMessageFor(model
                    => model.JML_AKTIF_ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_AKTIF_BH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_AKTIF_BH) @Html.ValidationMessageFor(model =>
                    model.JML_AKTIF_BH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_BERKEMBANG)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_BERKEMBANG) @Html.ValidationMessageFor(model
                    => model.JML_BERKEMBANG)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_BERKEMBANG_ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_BERKEMBANG_ANGGOTA) @Html.ValidationMessageFor(model
                    => model.JML_BERKEMBANG_ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_BERKEMBANG_BH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_BERKEMBANG_BH) @Html.ValidationMessageFor(model
                    => model.JML_BERKEMBANG_BH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_TIDAKBERKEMBANG)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_TIDAKBERKEMBANG) @Html.ValidationMessageFor(model
                    => model.JML_TIDAKBERKEMBANG)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_TIDAKBERKEMBANG_ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_TIDAKBERKEMBANG_ANGGOTA) @Html.ValidationMessageFor(model
                    => model.JML_TIDAKBERKEMBANG_ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_TIDAKBERKEMBANG_BH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_TIDAKBERKEMBANG_BH) @Html.ValidationMessageFor(model
                    => model.JML_TIDAKBERKEMBANG_BH)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_PASIF)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_PASIF) @Html.ValidationMessageFor(model => model.JML_PASIF)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_PASIF_ANGGOTA)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_PASIF_ANGGOTA) @Html.ValidationMessageFor(model
                    => model.JML_PASIF_ANGGOTA)
                </td>
                <td class="editor-label">
                    @Html.LabelFor(model => model.JML_PASIF_BH)
                </td>
                <td class="editor-field">
                    @Html.EditorFor(model => model.JML_PASIF_BH) @Html.ValidationMessageFor(model =>
                    model.JML_PASIF_BH)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
