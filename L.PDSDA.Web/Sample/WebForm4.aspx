﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        div
        {
            float: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="a0">
        <table style="width: 100%;">
            <tr>
                <td>
                    &nbsp; Tingkatan
                </td>
                <td>
                    &nbsp; Dropdown
                </td>
                <td>
                    &nbsp; Luas Rencana
                </td>
                <td>
                    TextBox
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div id="a1" style="width: 50%;">
        <table>
            <tr>
                <td colspan="6">
                    Luas Fungsional (Ada Jaringan Utama) Ha
                </td>
            </tr>
            <tr>
                <td class="style1" colspan="3">
                    Sudah Sawah
                </td>
                <td colspan="2">
                    Belum Sawah
                </td>
                <td rowspan="2">
                    JumlahJumlah
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Jar. Tersier Terbangun
                </td>
                <td>
                    Jar. Tersier Belum Ada
                </td>
                <td>
                    Alih Fungsi
                </td>
                <td>
                    Belum Sawah
                </td>
                <td>
                    Alih Fungsi
                </td>
            </tr>
            <tr>
                <td class="style1">
                    1
                </td>
                <td>
                    2
                </td>
                <td>
                    3
                </td>
                <td>
                    4
                </td>
                <td>
                    5
                </td>
                <td>
                    15
                </td>
            </tr>
        </table>
    </div>
    <div id="a2" style="width: 30;">
        <table>
            <tr>
                <td colspan="3">
                    &nbsp; Luas Belum Ada Jar. Utama (Ha)&nbsp; &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; Sudah Sawah
                </td>
                <td>
                    Belum Sawah
                </td>
                <td>
                    Jumlah
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; 6
                </td>
                <td>
                    7
                </td>
                <td>
                    13
                </td>
            </tr>
        </table>
    </div>
    <div id="a3">
        <table style="width: 100%;">
            <tr>
                <td colspan="2">
                    &nbsp; Jalan Inspeksi&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; Panjang
                </td>
                <td>
                    &nbsp; 8
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; Baik
                </td>
                <td>
                    &nbsp; 9
                </td>
            </tr>
            <tr>
                <td>
                    Rusak Ringan
                </td>
                <td>
                    10
                </td>
            </tr>
            <tr>
                <td>
                    Rusak Berat
                </td>
                <td>
                    11
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div id="a4" style="width: 70%;">
        <table style="width: 100%;">
            <tr>
                <td>
                    &nbsp; P3A
                </td>
                <td>
                    Total
                </td>
                <td>
                    Aktif
                </td>
                <td>
                    Berkembang
                </td>
                <td>
                    Tidak Berkembang
                </td>
                <td>
                    Pasif
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; Jumlah
                </td>
                <td>
                    12
                </td>
                <td>
                    13
                </td>
                <td>
                    14
                </td>
                <td>
                    15
                </td>
                <td>
                    16
                </td>
            </tr>
            <tr>
                <td>
                    Anggota
                </td>
                <td>
                    17
                </td>
                <td>
                    18
                </td>
                <td>
                    19
                </td>
                <td>
                    20
                </td>
                <td>
                    21
                </td>
            </tr>
            <tr>
                <td>
                    Berbadan Hukum
                </td>
                <td>
                    22
                </td>
                <td>
                    23
                </td>
                <td>
                    24
                </td>
                <td>
                    25
                </td>
                <td>
                    26
                </td>
            </tr>
        </table>
    </div>
    <div id="a5">
        <table style="width: 100%;">
            <tr>
                <td>
                    &nbsp; Rencana Debit Maks dalam 1 tahun (m3/detik)
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; 27
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; Realisasi Debit Maks dalam 1 tahun (m3/detik)
                </td>
            </tr>
            <tr>
                <td>
                    28
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
