﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm5.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td rowspan="3">
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    Jenis Bangunan</td>
                <td colspan="4">
                    Bangunan</td>
                <td colspan="4">
                    &nbsp;
                    Pintu Bangunan</td>
            </tr>
            <tr>
                <td rowspan="2">
                    Jumlah</td>
                <td colspan="3">
                    &nbsp;
                    Kondisi</td>
                <td rowspan="2">
                    Jumlah</td>
                <td colspan="3">
                    &nbsp;
                    Kondisi</td>
            </tr>
            <tr>
                <td>
                    Baik</td>
                <td>
                    Rusak Ringan</td>
                <td>
                    Rusak Berat</td>
                <td>
                    Baik</td>
                <td>
                    Rusak Ringan</td>
                <td>
                    Rusak Berat</td>
            </tr>
            <tr>
                <td>
                    Waduk</td>
                <td>
                     @Html.EditorFor(model => model.WADUK_JML) @Html.ValidationMessageFor(WADUK_JML)
</td>
                <td>
                     @Html.EditorFor(model => model.WADUK_B) @Html.ValidationMessageFor(WADUK_B)
</td>
                <td>
                     @Html.EditorFor(model => model.WADUK_RR) @Html.ValidationMessageFor(WADUK_RR)
</td>
                <td>
                     @Html.EditorFor(model => model.WADUK_RB) @Html.ValidationMessageFor(WADUK_RB)
</td>
                <td>
                     @Html.EditorFor(model => model.WADUKP_JML) @Html.ValidationMessageFor(WADUKP_JML)

</td>
                <td>
                    @Html.EditorFor(model => model.WADUKP_B) @Html.ValidationMessageFor(WADUKP_B)</td>
                <td>
                    
 @Html.EditorFor(model => model.WADUKP_RR) @Html.ValidationMessageFor(WADUKP_RR)</td>
                <td>
                   
 @Html.EditorFor(model => model.WADUKP_RB) @Html.ValidationMessageFor(WADUKP_RB)</td>
            </tr>
            <tr>
                <td>
                    Bendung Tetap</td>
                <td>
                    @Html.EditorFor(model => model.BT_JML) @Html.ValidationMessageFor(BT_JML)

</td>
                <td>
                    @Html.EditorFor(model => model.BT_B) @Html.ValidationMessageFor(BT_B)</td>
                <td>
                    
 @Html.EditorFor(model => model.BT_RR) @Html.ValidationMessageFor(BT_RR)</td>
                <td>
                    
 @Html.EditorFor(model => model.BT_RB) @Html.ValidationMessageFor(BT_RB)</td>
                <td>
                   
 @Html.EditorFor(model => model.BTP_JML) @Html.ValidationMessageFor(BTP_JML)</td>
                <td>
                    
 @Html.EditorFor(model => model.BTP_B) @Html.ValidationMessageFor(BTP_B)</td>
                <td>
                    
 @Html.EditorFor(model => model.BTP_RR) @Html.ValidationMessageFor(BTP_RR)</td>
                <td>
                     @Html.EditorFor(model => model.BTP_RB) @Html.ValidationMessageFor(BTP_RB)
</td>
            </tr>
            <tr>
                <td>
                    Bendung Gerak</td>
                <td>
                     @Html.EditorFor(model => model.BG_JML) @Html.ValidationMessageFor(BG_JML)
</td>
                <td>
                     @Html.EditorFor(model => model.BG_B) @Html.ValidationMessageFor(BG_B)
</td>
                <td>
                     @Html.EditorFor(model => model.BG_RR) @Html.ValidationMessageFor(BG_RR)
</td>
                <td>
                     @Html.EditorFor(model => model.BG_RB) @Html.ValidationMessageFor(BG_RB)
</td>
                <td>
                    @Html.EditorFor(model => model.BGP_JML) @Html.ValidationMessageFor(BGP_JML)
</td>
                <td>
                   @Html.EditorFor(model => model.BGP_B) @Html.ValidationMessageFor(BGP_B)
</td>
                <td>
                     @Html.EditorFor(model => model.BGP_RR) @Html.ValidationMessageFor(BGP_RR)
</td>
                <td>
                    @Html.EditorFor(model => model.BGP_RB) @Html.ValidationMessageFor(BGP_RB)
</td>
            </tr>
            <tr>
                <td>
                    Pompa</td>
                <td>
                   @Html.EditorFor(model => model.POMPA_JML) @Html.ValidationMessageFor(POMPA_JML)
</td>
                <td>
                     @Html.EditorFor(model => model.POMPA_B) @Html.ValidationMessageFor(POMPA_B)
</td>
                <td>
                     @Html.EditorFor(model => model.POMPA_RR) @Html.ValidationMessageFor(POMPA_RR)
</td>
                <td>
                     @Html.EditorFor(model => model.POMPA_RB) @Html.ValidationMessageFor(POMPA_RB)
</td>
                <td>
                    @Html.EditorFor(model => model.POMPAP_JML) @Html.ValidationMessageFor(POMPAP_JML)
</td>
                <td>
                     @Html.EditorFor(model => model.POMPAP_B) @Html.ValidationMessageFor(POMPAP_B)
</td>
                <td>
                    @Html.EditorFor(model => model.POMPAP_RR) @Html.ValidationMessageFor(POMPAP_RR)
</td>
                <td>
                     @Html.EditorFor(model => model.POMPAP_RB) @Html.ValidationMessageFor(POMPAP_RB)
</td>
            </tr>
            <tr>
                <td>
                    Pengambilan Bebas</td>
                <td>
                     @Html.EditorFor(model => model.BEBAS_JML) @Html.ValidationMessageFor(BEBAS_JML)
</td>
                <td>
                    @Html.EditorFor(model => model.BEBAS_B) @Html.ValidationMessageFor(BEBAS_B)
</td>
                <td>
                     @Html.EditorFor(model => model.BEBAS_RR) @Html.ValidationMessageFor(BEBAS_RR)
</td>
                <td>
                     @Html.EditorFor(model => model.BEBAS_RB) @Html.ValidationMessageFor(BEBAS_RB)
</td>
                <td>
                     @Html.EditorFor(model => model.BEBASP_JML) @Html.ValidationMessageFor(BEBASP_JML)
</td>
                <td>
                    @Html.EditorFor(model => model.BEBASP_B) @Html.ValidationMessageFor(BEBASP_B)
</td>
                <td>
                     @Html.EditorFor(model => model.BEBASP_RR) @Html.ValidationMessageFor(BEBASP_RR)
</td>
                <td>
                    @Html.EditorFor(model => model.BEBASP_RB) @Html.ValidationMessageFor(BEBASP_RB)
</td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
