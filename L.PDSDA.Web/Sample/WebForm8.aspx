﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm8.aspx.cs" Inherits="L.PDSDA.Web.Sample.WebForm8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td rowspan="2">
                    Kondisi
                </td>
                <td colspan="2">
                    Talang
                </td>
                <td colspan="2">
                    Syphon
                </td>
                <td>
                    Jembatan
                </td>
                <td>
                    Gorong2
                </td>
                <td colspan="2">
                    Got Miring
                </td>
                <td>
                    Terjun
                </td>
                <td colspan="2">
                    Suplesi
                </td>
                <td colspan="2">
                    Lain - lain
                </td>
            </tr>
            <tr>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
                <td>
                    Bang
                </td>
                <td>
                    Pintu
                </td>
            </tr>
            <tr>
                <td>
                    Jumlah
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TALANG_JML)@Html.ValidationMessageFor(model
                    => model.SUPL_TALANG_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TALANGP_JML)@Html.ValidationMessageFor(model
                    => model.SUPL_TALANGP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SYPHON_JML)@Html.ValidationMessageFor(model
                    => model.SUPL_SYPHON_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SYPHONP_JML)@Html.ValidationMessageFor(model
                    => model.SUPL_SYPHONP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_JEMBATAN_JML)@Html.ValidationMessageFor(model
                    => model.SUPL_JEMBATAN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOR_JML)@Html.ValidationMessageFor(model =>
                    model.SUPL_GOR_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOT_JML)@Html.ValidationMessageFor(model =>
                    model.SUPL_GOT_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOTP_JML)@Html.ValidationMessageFor(model =>
                    model.SUPL_GOTP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TERJUN_JML)@Html.ValidationMessageFor(model
                    => model.SUPL_TERJUN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SUPLESI_JML)@Html.ValidationMessageFor(model
                    => model.SUPL_SUPLESI_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SUPLESIP_JML)@Html.ValidationMessageFor(model
                    => model.SUPL_SUPLESIP_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_LAIN_JML)@Html.ValidationMessageFor(model =>
                    model.SUPL_LAIN_JML)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_LAINP_JML)@Html.ValidationMessageFor(model =>
                    model.SUPL_LAINP_JML)
                </td>
            </tr>
            <tr>
                <td>
                    Baik
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TALANG_B)@Html.ValidationMessageFor(model =>
                    model.SUPL_TALANG_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TALANGP_B)@Html.ValidationMessageFor(model =>
                    model.SUPL_TALANGP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SYPHON_B)@Html.ValidationMessageFor(model =>
                    model.SUPL_SYPHON_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SYPHONP_B)@Html.ValidationMessageFor(model =>
                    model.SUPL_SYPHONP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_JEMBATAN_B)@Html.ValidationMessageFor(model
                    => model.SUPL_JEMBATAN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOR_B)@Html.ValidationMessageFor(model => model.SUPL_GOR_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOT_B)@Html.ValidationMessageFor(model => model.SUPL_GOT_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOTP_B)@Html.ValidationMessageFor(model => model.SUPL_GOTP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TERJUN_B)@Html.ValidationMessageFor(model =>
                    model.SUPL_TERJUN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SUPLESI_B)@Html.ValidationMessageFor(model =>
                    model.SUPL_SUPLESI_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SUPLESIP_B)@Html.ValidationMessageFor(model
                    => model.SUPL_SUPLESIP_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_LAIN_B)@Html.ValidationMessageFor(model => model.SUPL_LAIN_B)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_LAINP_B)@Html.ValidationMessageFor(model =>
                    model.SUPL_LAINP_B)
                </td>
            </tr>
            <tr>
                <td>
                    Rusak Ringan
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TALANG_RR)@Html.ValidationMessageFor(model =>
                    model.SUPL_TALANG_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TALANGP_RR)@Html.ValidationMessageFor(model
                    => model.SUPL_TALANGP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SYPHON_RR)@Html.ValidationMessageFor(model =>
                    model.SUPL_SYPHON_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SYPHONP_RR)@Html.ValidationMessageFor(model
                    => model.SUPL_SYPHONP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_JEMBATAN_RR)@Html.ValidationMessageFor(model
                    => model.SUPL_JEMBATAN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOR_RR)@Html.ValidationMessageFor(model => model.SUPL_GOR_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOT_RR)@Html.ValidationMessageFor(model => model.SUPL_GOT_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOTP_RR)@Html.ValidationMessageFor(model =>
                    model.SUPL_GOTP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TERJUN_RR)@Html.ValidationMessageFor(model =>
                    model.SUPL_TERJUN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SUPLESI_RR)@Html.ValidationMessageFor(model
                    => model.SUPL_SUPLESI_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SUPLESIP_RR)@Html.ValidationMessageFor(model
                    => model.SUPL_SUPLESIP_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_LAIN_RR)@Html.ValidationMessageFor(model =>
                    model.SUPL_LAIN_RR)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_LAINP_RR)@Html.ValidationMessageFor(model =>
                    model.SUPL_LAINP_RR)
                </td>
            </tr>
            <tr>
                <td>
                    Rusak Berat
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TALANG_RB)@Html.ValidationMessageFor(model =>
                    model.SUPL_TALANG_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TALANGP_RB)@Html.ValidationMessageFor(model
                    => model.SUPL_TALANGP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SYPHON_RB)@Html.ValidationMessageFor(model =>
                    model.SUPL_SYPHON_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SYPHONP_RB)@Html.ValidationMessageFor(model
                    => model.SUPL_SYPHONP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_JEMBATAN_RB)@Html.ValidationMessageFor(model
                    => model.SUPL_JEMBATAN_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOR_RB)@Html.ValidationMessageFor(model => model.SUPL_GOR_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOT_RB)@Html.ValidationMessageFor(model => model.SUPL_GOT_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_GOTP_RB)@Html.ValidationMessageFor(model =>
                    model.SUPL_GOTP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_TERJUN_RB)@Html.ValidationMessageFor(model =>
                    model.SUPL_TERJUN_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SUPLESI_RB)@Html.ValidationMessageFor(model
                    => model.SUPL_SUPLESI_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_SUPLESIP_RB)@Html.ValidationMessageFor(model
                    => model.SUPL_SUPLESIP_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_LAIN_RB)@Html.ValidationMessageFor(model =>
                    model.SUPL_LAIN_RB)
                </td>
                <td>
                    @Html.EditorFor(model => model.SUPL_LAINP_RB)@Html.ValidationMessageFor(model =>
                    model.SUPL_LAINP_RB)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
