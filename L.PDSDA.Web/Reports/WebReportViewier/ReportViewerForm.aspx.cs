﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using Microsoft.Reporting.WebForms;

namespace L.PDSDA.Web.Reports.WebReportViewier
{
    public partial class ReportViewerForm : System.Web.UI.Page
    {
        private IDefaultRepository<IRIGASI> irigasiRepo;
        private IDefaultRepository<RAWA> rawaRepo;
        public ReportViewerForm()
        {
            irigasiRepo = new DefaultRepository<IRIGASI>();
            rawaRepo = new DefaultRepository<RAWA>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region DI
                IList<DTOs.DIPemerintahDTO> lstDIPDto = new List<DTOs.DIPemerintahDTO>();
                IQueryable<IRIGASI> qryIrigasi = irigasiRepo.GetAll().Take(10);
                IList<IRIGASI> lstIrigasi = qryIrigasi == null ? new List<IRIGASI>() : qryIrigasi.ToList();
                for (int i = 0; i < lstIrigasi.Count; i++)
                {
                    IRIGASI irigasi = lstIrigasi[i];
                    this.PortingEntityToDTOIrigasi(irigasi, lstDIPDto);
                }

                DTOs.DTOCollection DTOCollection = new DTOs.DTOCollection();

                ReportDataSource rds = new ReportDataSource();
                rds.Name = "DataSet1";
                rds.Value = DTOCollection.GetDIPemerintahDTO(lstDIPDto);
                
                //ReportViewer1.LocalReport.ReportPath = Server.MapPath(@"~/Reports/DaerahIrigasi/DIP1-19Report.rdlc");
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                //ReportViewer1.LocalReport.Refresh();
                #endregion DI

                #region Rawa
                //IList<DTOs.RawaDTO> lstRAWADTO = new List<DTOs.RawaDTO>();
                //IQueryable<RAWA> qryRAWA = rawaRepo.GetAll().Take(10);
                //IList<RAWA> lstRawa = qryRAWA == null ? new List<RAWA>() : qryRAWA.ToList();
                //for (int i = 0; i < lstRawa.Count; i++)
                //{
                //    RAWA rawa = lstRawa[i];
                //    this.PortingEntityToDTORawa(rawa, lstRAWADTO);
                //}

                //DTOs.DTOCollection DTOCollection = new DTOs.DTOCollection();

                //ReportDataSource rds = new ReportDataSource();
                //rds.Name = "DataSet1";
                //rds.Value = lstRAWADTO;// DTOCollection.GetRawaDTO(lstRAWADTO);
                ////ReportViewer1.LocalReport.ReportPath = Server.MapPath(@"~/Reports/DaerahIrigasi/DIP1-19Report.rdlc");
                //ReportViewer1.LocalReport.DataSources.Clear();
                //ReportViewer1.LocalReport.DataSources.Add(rds);
                ////ReportViewer1.LocalReport.Refresh();
                #endregion Rawa
            }
        }

        #region Rawa
        private void PortingEntityToDTORawa(RAWA rawa, IList<DTOs.RawaDTO> lstRAWADTO)
        {
            IList<RAWA_DYN> lstRawaDyn = rawa.RAWA_DYNs.ToList();
            if (lstRawaDyn != null && lstRawaDyn.Count > 0)
            {
                for (int i = 0; i < lstRawaDyn.Count; i++)
                {
                    DTOs.RawaDTO RawaDTO = new DTOs.RawaDTO();
                    this.PortRawaToDTO(rawa, RawaDTO);
                    this.PortRawaDynToDTO(lstRawaDyn[i], RawaDTO);
                    lstRAWADTO.Add(RawaDTO);
                }
            }
            else
            {
                DTOs.RawaDTO RawaDTO = new DTOs.RawaDTO();
                this.PortRawaToDTO(rawa, RawaDTO);
                lstRAWADTO.Add(RawaDTO);
            }
        }

        private void PortRawaDynToDTO(RAWA_DYN rAWA_DYN, DTOs.RawaDTO RawaDTO)
        {
            RawaDTO.BANGUN_ATUR = rAWA_DYN.BANGUN_ATUR;
            RawaDTO.BANGUN_ATUR2 = rAWA_DYN.BANGUN_ATUR2;
            RawaDTO.BANGUN_LAIN = rAWA_DYN.BANGUN_LAIN;
            RawaDTO.BANGUN_LAIN2 = rAWA_DYN.BANGUN_LAIN2;
            RawaDTO.BIAYA_INVESTASI1 = rAWA_DYN.BIAYA_INVESTASI1;
            RawaDTO.BIAYA_INVESTASI2 = rAWA_DYN.BIAYA_INVESTASI2;
            RawaDTO.BLM_DESAIN = rAWA_DYN.BLM_DESAIN;
            RawaDTO.JML_LAHAN1 = rAWA_DYN.JML_LAHAN1;
            RawaDTO.JML_LAHAN2 = rAWA_DYN.JML_LAHAN2;
            RawaDTO.JML_TANI1 = rAWA_DYN.JML_TANI1;
            RawaDTO.JML_TANI2 = rAWA_DYN.JML_TANI2;
            RawaDTO.K_RAWA = rAWA_DYN.K_RAWA;
            RawaDTO.LAIN_LAIN = rAWA_DYN.LAIN_LAIN;
            RawaDTO.LRAWA_DESAIN = rAWA_DYN.LRAWA_DESAIN;
            RawaDTO.LastModifiedOn = rAWA_DYN.LastModifiedOn;
            RawaDTO.LRAWA_FUNGSIONAL = rAWA_DYN.LRAWA_FUNGSIONAL;
            RawaDTO.LRAWA_POTENSIAL = rAWA_DYN.LRAWA_POTENSIAL;
            RawaDTO.LRAWA_REKLAMASI = rAWA_DYN.LRAWA_REKLAMASI;
            RawaDTO.LTOTALRAWA_FUNGSIMANFAAT = rAWA_DYN.LTOTALRAWA_FUNGSIMANFAAT;
            RawaDTO.LTOTALRAWA_PEMERINTAH = rAWA_DYN.LTOTALRAWA_PEMERINTAH;
            RawaDTO.LTOTALRAWA_SWASTA = rAWA_DYN.LTOTALRAWA_SWASTA;
            RawaDTO.LUASLAHAN_SWASTA = rAWA_DYN.LUASLAHAN_SWASTA;
            RawaDTO.NON_FUNGSI = rAWA_DYN.NON_FUNGSI;
            RawaDTO.NON_POTENSIAL = rAWA_DYN.NON_POTENSIAL;
            RawaDTO.PERIKANAN = rAWA_DYN.PERIKANAN;
            RawaDTO.PERKEBUNAN = rAWA_DYN.PERKEBUNAN;
            RawaDTO.PERTANIAN = rAWA_DYN.PERTANIAN;
            RawaDTO.S_IKAN1 = rAWA_DYN.S_IKAN1;
            RawaDTO.S_IKAN2 = rAWA_DYN.S_IKAN2;
            RawaDTO.S_LAIN1 = rAWA_DYN.S_LAIN1;
            RawaDTO.S_LAIN2 = rAWA_DYN.S_LAIN2;
            RawaDTO.S_KEBUN1 = rAWA_DYN.S_KEBUN1;
            RawaDTO.S_KEBUN2 = rAWA_DYN.S_KEBUN2;
            RawaDTO.S_TANI1 = rAWA_DYN.S_TANI1;
            RawaDTO.S_TANI2 = rAWA_DYN.S_TANI2;
            RawaDTO.SAL_PRIMER_SWASTA = rAWA_DYN.SAL_PRIMER_SWASTA;
            RawaDTO.SALURAN_NAVIGASI_MGUNA1 = rAWA_DYN.SALURAN_NAVIGASI_MGUNA1;
            RawaDTO.SALURAN_NAVIGASI_MGUNA2 = rAWA_DYN.SALURAN_NAVIGASI_MGUNA2;
            RawaDTO.SALURAN_PRI_TAMBAK1 = rAWA_DYN.SALURAN_PRI_TAMBAK1;
            RawaDTO.SALURAN_PRI_TAMBAK2 = rAWA_DYN.SALURAN_PRI_TAMBAK2;
            RawaDTO.SALURAN_PRIMER1 = rAWA_DYN.SALURAN_PRIMER1;
            RawaDTO.SALURAN_PRIMER2 = rAWA_DYN.SALURAN_PRIMER2;
            RawaDTO.SALURAN_SEKUNDER1 = rAWA_DYN.SALURAN_SEKUNDER1;
            RawaDTO.SALURAN_SEKUNDER2 = rAWA_DYN.SALURAN_SEKUNDER2;
            RawaDTO.SALURAN_TERSIER1 = rAWA_DYN.SALURAN_TERSIER1;
            RawaDTO.SALURAN_TERSIER2 = rAWA_DYN.SALURAN_TERSIER2;
            RawaDTO.SIAP_REKLAMASI = rAWA_DYN.SIAP_REKLAMASI;
            RawaDTO.SUMBER_DANA1 = rAWA_DYN.SUMBER_DANA1;
            RawaDTO.SUMBER_DANA2 = rAWA_DYN.SUMBER_DANA2;
            RawaDTO.TAHUN_DATA = rAWA_DYN.TAHUN_DATA;
            RawaDTO.TANGGUL = rAWA_DYN.TANGGUL;
            RawaDTO.TANGGUL2 = rAWA_DYN.TANGGUL2;
            RawaDTO.TANI_GARAP_SWASTA = rAWA_DYN.TANI_GARAP_SWASTA;
            RawaDTO.THN_BANGUN_SWASTA = rAWA_DYN.THN_BANGUN_SWASTA;
            RawaDTO.THN_BANGUN1 = rAWA_DYN.THN_BANGUN1;
            RawaDTO.THN_BANGUN2 = rAWA_DYN.THN_BANGUN2;
            RawaDTO.TOTAL_TAHAP1 = rAWA_DYN.TOTAL_TAHAP1;
            RawaDTO.TOTAL_TAHAP2 = rAWA_DYN.TOTAL_TAHAP2;
            RawaDTO.TRANS_LOKALLAHAN1 = rAWA_DYN.TRANS_LOKALLAHAN1;
            RawaDTO.TRANS_LOKALLAHAN2 = rAWA_DYN.TRANS_LOKALLAHAN2;
            RawaDTO.TRANS_LOKALTANI1 = rAWA_DYN.TRANS_LOKALTANI1;
            RawaDTO.TRANS_LOKALTANI2 = rAWA_DYN.TRANS_LOKALTANI2;
            RawaDTO.TRANS_UMUMLAHAN1 = rAWA_DYN.TRANS_UMUMLAHAN1;
            RawaDTO.TRANS_UMUMLAHAN2 = rAWA_DYN.TRANS_UMUMLAHAN2;
            RawaDTO.TRANS_UMUMTANI1 = rAWA_DYN.TRANS_UMUMTANI1;
            RawaDTO.TRANS_UMUMTANI2 = rAWA_DYN.TRANS_UMUMTANI2;


            //throw new NotImplementedException(); // Arif Edit
        }

        private void PortRawaToDTO(RAWA rawa, DTOs.RawaDTO RawaDTO)
        {
            // Arif Edit
            RawaDTO.DAS = rawa.DAS == null ? "" : rawa.DAS.N_DAS;
            RawaDTO.DESA = rawa.DESA;
            RawaDTO.WS = rawa.WS == null ? "" : rawa.WS.N_WS;

            RawaDTO.JENIS = rawa.JENIS;
            RawaDTO.K_RAWA = rawa.K_RAWA;
            RawaDTO.KABUPATEN = rawa.KABUPATEN.N_KABUPATEN;
            RawaDTO.KECAMATAN = rawa.KECAMATAN;
            RawaDTO.N_RAWA = rawa.N_RAWA;

        }
        #endregion Rawa

        #region Irigasi
        private void PortingEntityToDTOIrigasi(IRIGASI irigasi, IList<DTOs.DIPemerintahDTO> lstDIPDto)
        {
            IList<IRIGASI_DYN> lstIrigasiDyn = irigasi.IRIGASI_DYN.ToList();
            if (lstIrigasiDyn != null && lstIrigasiDyn.Count > 0)
            {
                for (int i = 0; i < lstIrigasiDyn.Count; i++)
                {
                    DTOs.DIPemerintahDTO DIPemerintahDTO = new DTOs.DIPemerintahDTO();
                    this.PortIrigasiToDTO(irigasi, DIPemerintahDTO);
                    this.PortIrigasiDynToDTO(lstIrigasiDyn[i], DIPemerintahDTO);
                    lstDIPDto.Add(DIPemerintahDTO);
                }
            }
            else
            {
                DTOs.DIPemerintahDTO DIPemerintahDTO = new DTOs.DIPemerintahDTO();
                this.PortIrigasiToDTO(irigasi, DIPemerintahDTO);
                lstDIPDto.Add(DIPemerintahDTO);
            }
        }

        private void PortIrigasiDynToDTO(IRIGASI_DYN iRIGASI_DYN, DTOs.DIPemerintahDTO DIPemerintahDTO)
        {
            DIPemerintahDTO.ANGGOTA = iRIGASI_DYN.ANGGOTA;
            DIPemerintahDTO.BADAN_HUKUM = iRIGASI_DYN.BADAN_HUKUM;
            DIPemerintahDTO.BAGI_B = iRIGASI_DYN.BAGI_B;
            DIPemerintahDTO.BAGI_JML = iRIGASI_DYN.BAGI_JML;
            DIPemerintahDTO.BAGI_RB = iRIGASI_DYN.BAGI_RB;
            DIPemerintahDTO.BAGI_RR = iRIGASI_DYN.BAGI_RR;

            DIPemerintahDTO.BAGIP_B = iRIGASI_DYN.BAGIP_B;
            DIPemerintahDTO.BAGIP_JML = iRIGASI_DYN.BAGIP_JML;
            DIPemerintahDTO.BAGIP_RB = iRIGASI_DYN.BAGIP_RB;
            DIPemerintahDTO.BAGIP_RR = iRIGASI_DYN.BAGIP_RR;

            DIPemerintahDTO.BEBAS_B = iRIGASI_DYN.BEBAS_B;
            DIPemerintahDTO.BEBAS_JML = iRIGASI_DYN.BEBAS_JML;
            DIPemerintahDTO.BEBAS_RB = iRIGASI_DYN.BEBAS_RB;
            DIPemerintahDTO.BEBAS_RR = iRIGASI_DYN.BEBAS_RR;

            DIPemerintahDTO.BEBASP_B = iRIGASI_DYN.BEBASP_B;
            DIPemerintahDTO.BEBASP_JML = iRIGASI_DYN.BEBASP_JML;
            DIPemerintahDTO.BEBASP_RB = iRIGASI_DYN.BEBASP_RB;
            DIPemerintahDTO.BEBASP_RR = iRIGASI_DYN.BEBASP_RR;

            DIPemerintahDTO.BG_B = iRIGASI_DYN.BG_B;
            DIPemerintahDTO.BG_JML = iRIGASI_DYN.BG_JML;
            DIPemerintahDTO.BG_RB = iRIGASI_DYN.BG_RB;
            DIPemerintahDTO.BG_RR = iRIGASI_DYN.BG_RR;

            DIPemerintahDTO.BGP_B = iRIGASI_DYN.BGP_B;
            DIPemerintahDTO.BGP_JML = iRIGASI_DYN.BGP_JML;
            DIPemerintahDTO.BGP_RB = iRIGASI_DYN.BGP_RB;
            DIPemerintahDTO.BGP_RR = iRIGASI_DYN.BGP_RR;

            DIPemerintahDTO.BJAR_BSAWAH = iRIGASI_DYN.BJAR_BSAWAH;
            DIPemerintahDTO.BJAR_SAWAH = iRIGASI_DYN.BJAR_SAWAH;
            DIPemerintahDTO.BSADAP_B = iRIGASI_DYN.BSADAP_B;
            DIPemerintahDTO.BSADAP_JML = iRIGASI_DYN.BSADAP_JML;
            DIPemerintahDTO.BSADAP_RB = iRIGASI_DYN.BSADAP_RB;
            DIPemerintahDTO.BSADAP_RR = iRIGASI_DYN.BSADAP_RR;

            DIPemerintahDTO.BSADAPP_B = iRIGASI_DYN.BSADAPP_B;
            DIPemerintahDTO.BSADAPP_JML = iRIGASI_DYN.BSADAPP_JML;
            DIPemerintahDTO.BSADAPP_RB = iRIGASI_DYN.BSADAPP_RB;
            DIPemerintahDTO.BSADAPP_RR = iRIGASI_DYN.BSADAPP_RR;

            DIPemerintahDTO.BT_B = iRIGASI_DYN.BT_B;
            DIPemerintahDTO.BT_JML = iRIGASI_DYN.BT_JML;
            DIPemerintahDTO.BT_RB = iRIGASI_DYN.BT_RB;
            DIPemerintahDTO.BT_RR = iRIGASI_DYN.BT_RR;

            DIPemerintahDTO.BTP_B = iRIGASI_DYN.BTP_B;
            DIPemerintahDTO.BTP_JML = iRIGASI_DYN.BTP_JML;
            DIPemerintahDTO.BTP_RB = iRIGASI_DYN.BTP_RB;
            DIPemerintahDTO.BTP_RR = iRIGASI_DYN.BTP_RR;

            DIPemerintahDTO.DEBIT_KENYATAAN = iRIGASI_DYN.DEBIT_KENYATAAN;
            DIPemerintahDTO.DEBIT_RENCANA = iRIGASI_DYN.DEBIT_RENCANA;

            DIPemerintahDTO.GEND_GOR_B = iRIGASI_DYN.GEND_GOR_B;
            DIPemerintahDTO.GEND_GOR_JML = iRIGASI_DYN.GEND_GOR_JML;
            DIPemerintahDTO.GEND_GOR_RB = iRIGASI_DYN.GEND_GOR_RB;
            DIPemerintahDTO.GEND_GOR_RR = iRIGASI_DYN.GEND_GOR_RR;

            DIPemerintahDTO.GEND_LAIN_B = iRIGASI_DYN.GEND_LAIN_B;
            DIPemerintahDTO.GEND_LAIN_JML = iRIGASI_DYN.GEND_LAIN_JML;
            DIPemerintahDTO.GEND_LAIN_RB = iRIGASI_DYN.GEND_LAIN_RB;
            DIPemerintahDTO.GEND_LAIN_RR = iRIGASI_DYN.GEND_LAIN_RR;

            DIPemerintahDTO.GEND_LAINP_B = iRIGASI_DYN.GEND_LAINP_B;
            DIPemerintahDTO.GEND_LAINP_JML = iRIGASI_DYN.GEND_LAINP_JML;
            DIPemerintahDTO.GEND_LAINP_RB = iRIGASI_DYN.GEND_LAINP_RB;
            DIPemerintahDTO.GEND_LAINP_RR = iRIGASI_DYN.GEND_LAINP_RR;

            DIPemerintahDTO.GEND_TERJUN_B = iRIGASI_DYN.GEND_TERJUN_B;
            DIPemerintahDTO.GEND_TERJUN_JML = iRIGASI_DYN.GEND_TERJUN_JML;
            DIPemerintahDTO.GEND_TERJUN_RB = iRIGASI_DYN.GEND_TERJUN_RB;
            DIPemerintahDTO.GEND_TERJUN_RR = iRIGASI_DYN.GEND_TERJUN_RR;

            DIPemerintahDTO.GOR_B = iRIGASI_DYN.GOR_B;
            DIPemerintahDTO.GOR_JML = iRIGASI_DYN.GOR_JML;
            DIPemerintahDTO.GOR_PEMB_B = iRIGASI_DYN.GOR_PEMB_B;
            DIPemerintahDTO.GOR_PEMB_JML = iRIGASI_DYN.GOR_PEMB_JML;
            DIPemerintahDTO.GOR_PEMB_RB = iRIGASI_DYN.GOR_PEMB_RB;
            DIPemerintahDTO.GOR_PEMB_RR = iRIGASI_DYN.GOR_PEMB_RR;

            DIPemerintahDTO.GOR_PEMBP_B = iRIGASI_DYN.GOR_PEMBP_B;
            DIPemerintahDTO.GOR_PEMBP_JML = iRIGASI_DYN.GOR_PEMBP_JML;
            DIPemerintahDTO.GOR_PEMBP_RB = iRIGASI_DYN.GOR_PEMBP_RB;
            DIPemerintahDTO.GOR_PEMBP_RR = iRIGASI_DYN.GOR_PEMBP_RR;

            DIPemerintahDTO.GOT_B = iRIGASI_DYN.GOT_B;
            DIPemerintahDTO.GOT_JML = iRIGASI_DYN.GOT_JML;
            DIPemerintahDTO.GOT_RB = iRIGASI_DYN.GOT_RB;
            DIPemerintahDTO.GOT_RR = iRIGASI_DYN.GOT_RR;

            DIPemerintahDTO.GOTP_B = iRIGASI_DYN.GOTP_B;
            DIPemerintahDTO.GOTP_JML = iRIGASI_DYN.GOTP_JML;
            DIPemerintahDTO.GOTP_RB = iRIGASI_DYN.GOTP_RB;
            DIPemerintahDTO.GOTP_RR = iRIGASI_DYN.GOTP_RR;


        }

        private void PortIrigasiToDTO(IRIGASI irigasi, DTOs.DIPemerintahDTO DIPemerintahDTO)
        {
            DIPemerintahDTO.DESA = irigasi.DESA;
            DIPemerintahDTO.K_DI = irigasi.K_DI;
            DIPemerintahDTO.KECAMATAN = irigasi.KECAMATAN;
            DIPemerintahDTO.N_DI = irigasi.N_DI;
        }
        #endregion Irigasi
    }
}