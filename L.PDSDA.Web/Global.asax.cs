﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net.Config;
using log4net;
using NHibernate.Cfg;
using L.Core.Modules;
//using L.PDSDA.DAL.Interceptors.SDA;

namespace L.PDSDA.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        ILog logger = LogManager.GetLogger("Global");

        protected void Application_Start()
        {
            XmlConfigurator.Configure();
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            logger.Info("application start");

            //new Configuration().SetInterceptor(new DanauInterceptor());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //bool IsError = L.Core.Modules.NHibernateSessionPerRequest.IsError;
            //string ErrorMessage = L.Core.Modules.NHibernateSessionPerRequest.ErrorMessage;

            bool IsError = NHibernateSessionFactoriesPerRequest.IsError;
            string ErrorMessage = NHibernateSessionFactoriesPerRequest.ErrorMessage;

            if (IsError)
            {
                Response.Redirect(String.Format("~/Error/ErrorDefault/?message={0}", ErrorMessage), true);
                return;
            }

            Exception exception = Server.GetLastError();
            Response.Clear();

            HttpException httpException = exception as HttpException;

            if (httpException != null)
            {
                string action;

                switch (httpException.GetHttpCode())
                {
                    case 404:
                        // page not found
                        action = "HttpError404";
                        break;
                    case 500:
                        // server error
                        action = "HttpError500";
                        break;
                    default:
                        action = "General";
                        break;
                }

                // clear error on server
                Server.ClearError();

                Response.Redirect(String.Format("~/Error/{0}/?message={1}", action, exception.Message), true);
            }
            else
            {
                Response.Redirect(String.Format("~/Error/HttpError500/?message={0}", exception.Message), true);
            }
        }
    }
}