﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.Web.DTOs
{
    public class RawaDTO
    {
        public virtual string DAS
        {
            get;
            set;
        }

        public virtual string KABUPATEN
        {
            get;
            set;
        }

        public virtual string WS
        {
            get;
            set;
        }

        #region Primitive Properties

        public virtual string K_RAWA
        {
            get;
            set;
        }

        public virtual string N_RAWA
        {
            get;
            set;
        }

        public virtual string PROPINSI
        {
            get;
            set;
        }

        public virtual string KECAMATAN
        {
            get;
            set;
        }

        public virtual string DESA
        {
            get;
            set;
        }

        public virtual string JENIS
        {
            get;
            set;
        }



        #endregion

        #region Primitive Properties

        public virtual int TAHUN_DATA
        {
            get;
            set;
        }


        public virtual Nullable<double> LTOTALRAWA_FUNGSIMANFAAT
        {
            get;
            set;
        }

        public virtual Nullable<double> LRAWA_POTENSIAL
        {
            get;
            set;
        }

        public virtual Nullable<double> LRAWA_DESAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> LRAWA_REKLAMASI
        {
            get;
            set;
        }

        public virtual Nullable<double> LRAWA_FUNGSIONAL
        {
            get;
            set;
        }

        public virtual Nullable<double> LTOTALRAWA_PEMERINTAH
        {
            get;
            set;
        }

        public virtual Nullable<double> TOTAL_TAHAP1
        {
            get;
            set;
        }

        public virtual Nullable<double> S_TANI1
        {
            get;
            set;
        }

        public virtual Nullable<double> S_KEBUN1
        {
            get;
            set;
        }

        public virtual Nullable<double> S_IKAN1
        {
            get;
            set;
        }

        public virtual Nullable<double> JML_LAHAN1
        {
            get;
            set;
        }

        public virtual Nullable<double> TRANS_UMUMLAHAN1
        {
            get;
            set;
        }

        public virtual Nullable<double> TRANS_LOKALLAHAN1
        {
            get;
            set;
        }

        public virtual Nullable<double> JML_TANI1
        {
            get;
            set;
        }

        public virtual Nullable<double> TRANS_UMUMTANI1
        {
            get;
            set;
        }

        public virtual Nullable<double> TRANS_LOKALTANI1
        {
            get;
            set;
        }

        public virtual Nullable<double> S_LAIN1
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_PRIMER1
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_SEKUNDER1
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_TERSIER1
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_NAVIGASI_MGUNA1
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_PRI_TAMBAK1
        {
            get;
            set;
        }

        public virtual Nullable<double> BIAYA_INVESTASI1
        {
            get;
            set;
        }

        public virtual string SUMBER_DANA1
        {
            get;
            set;
        }

        public virtual string THN_BANGUN1
        {
            get;
            set;
        }

        public virtual Nullable<double> TOTAL_TAHAP2
        {
            get;
            set;
        }

        public virtual Nullable<double> S_TANI2
        {
            get;
            set;
        }

        public virtual Nullable<double> S_KEBUN2
        {
            get;
            set;
        }

        public virtual Nullable<double> S_IKAN2
        {
            get;
            set;
        }

        public virtual Nullable<double> JML_LAHAN2
        {
            get;
            set;
        }

        public virtual Nullable<double> TRANS_UMUMLAHAN2
        {
            get;
            set;
        }

        public virtual Nullable<double> TRANS_LOKALLAHAN2
        {
            get;
            set;
        }

        public virtual Nullable<double> JML_TANI2
        {
            get;
            set;
        }

        public virtual Nullable<double> TRANS_UMUMTANI2
        {
            get;
            set;
        }

        public virtual Nullable<double> TRANS_LOKALTANI2
        {
            get;
            set;
        }

        public virtual Nullable<double> S_LAIN2
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_PRIMER2
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_SEKUNDER2
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_TERSIER2
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_NAVIGASI_MGUNA2
        {
            get;
            set;
        }

        public virtual Nullable<double> SALURAN_PRI_TAMBAK2
        {
            get;
            set;
        }

        public virtual Nullable<double> TANGGUL2
        {
            get;
            set;
        }

        public virtual Nullable<double> BANGUN_ATUR2
        {
            get;
            set;
        }

        public virtual Nullable<double> BANGUN_LAIN2
        {
            get;
            set;
        }

        public virtual Nullable<double> BIAYA_INVESTASI2
        {
            get;
            set;
        }

        public virtual string SUMBER_DANA2
        {
            get;
            set;
        }

        public virtual string THN_BANGUN2
        {
            get;
            set;
        }

        public virtual Nullable<double> LTOTALRAWA_SWASTA
        {
            get;
            set;
        }

        public virtual Nullable<double> PERTANIAN
        {
            get;
            set;
        }

        public virtual Nullable<double> PERKEBUNAN
        {
            get;
            set;
        }

        public virtual Nullable<double> PERIKANAN
        {
            get;
            set;
        }

        public virtual Nullable<double> LUASLAHAN_SWASTA
        {
            get;
            set;
        }

        public virtual Nullable<double> TANI_GARAP_SWASTA
        {
            get;
            set;
        }

        public virtual Nullable<double> LAIN_LAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_PRIMER_SWASTA
        {
            get;
            set;
        }

        public virtual Nullable<double> TANGGUL
        {
            get;
            set;
        }

        public virtual Nullable<double> BANGUN_ATUR
        {
            get;
            set;
        }

        public virtual Nullable<double> BANGUN_LAIN
        {
            get;
            set;
        }

        public virtual string THN_BANGUN_SWASTA
        {
            get;
            set;
        }

        public virtual Nullable<double> NON_FUNGSI
        {
            get;
            set;
        }

        public virtual Nullable<double> SIAP_REKLAMASI
        {
            get;
            set;
        }

        public virtual Nullable<double> BLM_DESAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> NON_POTENSIAL
        {
            get;
            set;
        }

        public virtual DateTime LastModifiedOn { get; set; }

        #endregion
    }
}
