﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.Web.DTOs
{
    public class STASIUNDEBIT_DYNDTO
    {
        public IList<jQueryFullCalendarEventModel> DebitAirEvents { get; set; }
        public IList<jQueryFullCalendarEventModel> TinggiMukaAirEvents { get; set; }
        public STASIUNDEBIT_DYN STASIUNDEBIT_DYN { get; set; }
    }
}