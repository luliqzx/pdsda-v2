﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace L.PDSDA.Web.DTOs
{
    public  class DIPemerintahDTO
    {
        public DIPemerintahDTO()
        {
        }

        #region Primitive Properties

        public virtual string K_DI
        {
            get;
            set;
        }

        public virtual string N_DI
        {
            get;
            set;
        }

        public virtual string KECAMATAN
        {
            get;
            set;
        }

        public virtual Nullable<int> DESA
        {
            get;
            set;
        }


        #endregion

        #region Primitive Properties

        public virtual Nullable<double> MT1_PD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT1_PL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT1_LAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_PD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_PL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_LAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_PD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_PL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_LAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> IP
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT1_PD
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT1_PL
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT1_LAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT2_PD
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT2_PL
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT2_LAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT3_PD
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT3_PL
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_MT3_LAIN
        {
            get;
            set;
        }

        public virtual Nullable<double> PRED_IP
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties

        public  string DAS
        {
            get;
            set;
        }

        public  string IRIGASI_LINTAS
        {
            get;
            set;
        }

        public  string KEWENANGAN
        {
            get;
            set;
        }

        public  string KABUPATEN
        {
            get;
            set;
        }

        public  string IRIGASI_TANAM
        { get; set; }

        public  string WS
        {
            get;
            set;
        }

        #endregion

        #region Primitive Properties

        //public virtual string K_DI
        //{
        //    get;
        //    set;
        //}

        public virtual int TAHUN_DATA
        {
            get;
            set;
        }

        public virtual string SUMBER_DATA
        {
            get;
            set;
        }

        public virtual string K_TINGKATAN
        {
            get { return _k_tingkatan; }
            set
            {
                if (value == "-1")
                {
                    _k_tingkatan = null;
                }
                else
                {
                    _k_tingkatan = value;
                }
            }
        }

        private string _k_tingkatan { get; set; }

        public virtual Nullable<double> LUAS_RENCANA
        {
            get;
            set;
        }

        public virtual Nullable<double> JAR_OPTIMAL
        {
            get;
            set;
        }

        public virtual Nullable<double> JAR_BOPTIMAL
        {
            get;
            set;
        }

        public virtual Nullable<double> JAR_ALFUNG
        {
            get;
            set;
        }

        public virtual Nullable<double> JAR_ALFUNGDRBS
        {
            get;
            set;
        }

        public virtual Nullable<double> JAR_BSAWAH
        {
            get;
            set;
        }

        public virtual Nullable<double> BJAR_SAWAH
        {
            get;
            set;
        }

        public virtual Nullable<double> BJAR_BSAWAH
        {
            get;
            set;
        }

        public virtual Nullable<int> P3A
        {
            get;
            set;
        }

        public virtual Nullable<int> ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> BADAN_HUKUM
        {
            get;
            set;
        }

        public virtual string STATUS
        {
            get;
            set;
        }

        public virtual Nullable<int> WADUK_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> WADUK_B
        {
            get;
            set;
        }

        public virtual Nullable<int> WADUK_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> WADUK_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> WADUKP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> WADUKP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> WADUKP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> WADUKP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BT_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BT_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BT_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BT_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BTP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BTP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BTP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BTP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BG_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BG_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BG_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BG_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BGP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BGP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BGP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BGP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> POMPA_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> POMPA_B
        {
            get;
            set;
        }

        public virtual Nullable<int> POMPA_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> POMPA_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> POMPAP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> POMPAP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> POMPAP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> POMPAP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BEBAS_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BEBAS_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BEBAS_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BEBAS_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BEBASP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BEBASP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BEBASP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BEBASP_RB
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_INDUK_PJ
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_INDUK_B
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_INDUK_RR
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_INDUK_RB
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_SEKUNDER_PJ
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_SEKUNDER_B
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_SEKUNDER_RR
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_SEKUNDER_RB
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_PEMBUANG_PJ
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_PEMBUANG_B
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_PEMBUANG_RR
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_PEMBUANG_RB
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_SUPLESI_PJ
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_SUPLESI_B
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_SUPLESI_RR
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_SUPLESI_RB
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_GENDONG_PJ
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_GENDONG_B
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_GENDONG_RR
        {
            get;
            set;
        }

        public virtual Nullable<double> SAL_GENDONG_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BAGI_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BAGI_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BAGI_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BAGI_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BAGIP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BAGIP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BAGIP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BAGIP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BSADAP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BSADAP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BSADAP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BSADAP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> BSADAPP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> BSADAPP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> BSADAPP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> BSADAPP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SADAP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SADAP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SADAP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SADAP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SADAPP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SADAPP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SADAPP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SADAPP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> KT_LUMPUR_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> KT_LUMPUR_B
        {
            get;
            set;
        }

        public virtual Nullable<int> KT_LUMPUR_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> KT_LUMPUR_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> KT_LUMPURP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> KT_LUMPURP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> KT_LUMPURP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> KT_LUMPURP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PENGATUR_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PENGATUR_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PENGATUR_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PENGATUR_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PENGATURP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PENGATURP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PENGATURP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PENGATURP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> TALANG_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> TALANG_B
        {
            get;
            set;
        }

        public virtual Nullable<int> TALANG_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> TALANG_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> TALANGP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> TALANGP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> TALANGP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> TALANGP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SYPHON_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SYPHON_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SYPHON_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SYPHON_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SYPHONP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SYPHONP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SYPHONP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SYPHONP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> JEMBATAN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> JEMBATAN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> JEMBATAN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> JEMBATAN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GOT_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GOT_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GOT_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> GOT_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GOTP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GOTP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GOTP_RR
        {
            get;
            set;
        }

        public virtual string GOTP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> TERJUN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> TERJUN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> TERJUN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> TERJUN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PELIMPAH_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PELIMPAH_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PELIMPAH_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PELIMPAH_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMBILAS_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMBILAS_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMBILAS_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMBILAS_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMBILASP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMBILASP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMBILASP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMBILASP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_PEMB_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_PEMB_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_PEMB_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_PEMB_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_PEMBP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_PEMBP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_PEMBP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> GOR_PEMBP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> LAIN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> LAIN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> LAIN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> LAIN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> LAINP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> LAINP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> LAINP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> LAINP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_JEMBATAN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_JEMBATAN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_JEMBATAN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_JEMBATAN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_GOR_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_GOR_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_GOR_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_GOR_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_KLEP_PIN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_KLEP_PIN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_KLEP_PIN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_KLEP_PIN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_KLEP_PINP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_KLEP_PINP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_KLEP_PINP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_KLEP_PINP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_TERJUN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_TERJUN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_TERJUN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_TERJUN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_LAIN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_LAIN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_LAIN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_LAIN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_LAINP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_LAINP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_LAINP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> PEMB_LAINP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TALANG_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TALANG_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TALANG_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TALANG_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TALANGP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TALANGP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TALANGP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TALANGP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SYPHON_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SYPHON_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SYPHON_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SYPHON_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SYPHONP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SYPHONP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SYPHONP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SYPHONP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_JEMBATAN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_JEMBATAN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_JEMBATAN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_JEMBATAN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOR_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOR_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOR_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOR_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOT_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOT_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOT_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOT_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOTP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOTP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOTP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_GOTP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TERJUN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TERJUN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TERJUN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_TERJUN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SUPLESI_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SUPLESI_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SUPLESI_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SUPLESI_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SUPLESIP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SUPLESIP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SUPLESIP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_SUPLESIP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_LAIN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_LAIN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_LAIN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_LAIN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_LAINP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_LAINP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_LAINP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> SUPL_LAINP_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_GOR_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_GOR_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_GOR_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_GOR_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_TERJUN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_TERJUN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_TERJUN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_TERJUN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_LAIN_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_LAIN_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_LAIN_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_LAIN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_LAINP_JML
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_LAINP_B
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_LAINP_RR
        {
            get;
            set;
        }

        public virtual Nullable<int> GEND_LAINP_RB
        {
            get;
            set;
        }

        public virtual Nullable<double> DEBIT_RENCANA
        {
            get;
            set;
        }

        public virtual Nullable<double> DEBIT_KENYATAAN
        {
            get;
            set;
        }

        public virtual Nullable<double> JALAN_PJ
        {
            get;
            set;
        }

        public virtual Nullable<double> JALAN_B
        {
            get;
            set;
        }

        public virtual Nullable<double> JALAN_RR
        {
            get;
            set;
        }

        public virtual Nullable<double> JALAN_RB
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_AKTIF
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_AKTIF_ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_AKTIF_BH
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_BERKEMBANG
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_BERKEMBANG_ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_BERKEMBANG_BH
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_TIDAKBERKEMBANG
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_TIDAKBERKEMBANG_ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_TIDAKBERKEMBANG_BH
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_PASIF
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_PASIF_ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_PASIF_BH
        {
            get;
            set;
        }

        #endregion
    }
}