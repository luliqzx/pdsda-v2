﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.Web.DTOs
{
    public partial class DTOCollection
    {
        public DTOCollection()
        {
            DIPemerintahDTO = new List<DIPemerintahDTO>();
            RawaDTO = new List<RawaDTO>();
        }

        private IList<DIPemerintahDTO> DIPemerintahDTO { get; set; }

        public IList<DIPemerintahDTO> GetDIPemerintahDTO(IList<DIPemerintahDTO> lstDIPemerintahDTO)
        {
            if (lstDIPemerintahDTO.Count > 0)
            {
                return lstDIPemerintahDTO;
            }
            else
            {
                DIPemerintahDTO.Add(new DIPemerintahDTO() { });
                return DIPemerintahDTO;
            }
            //return DIPemerintahDTO;
        }

        private IList<RawaDTO> RawaDTO { get; set; }

        public IList<RawaDTO> GetRawaDTO(IList<RawaDTO> lstRawaDTO)
        {
            if (lstRawaDTO.Count > 0)
            {
                return lstRawaDTO;
            }
            else
            {
                RawaDTO.Add(new RawaDTO() { });
                return RawaDTO;
            }
            //return RawaDTO;
            //return DIPemerintahDTO;
        }


    }
}