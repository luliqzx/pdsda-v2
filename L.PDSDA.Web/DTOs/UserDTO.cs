﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace L.PDSDA.Web.DTOs
{
    public class UserDTO
    {
        public Int32 Id { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Groups { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}