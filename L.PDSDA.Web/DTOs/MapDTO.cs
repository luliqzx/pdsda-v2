﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.Web.DTOs
{
    public class MapDTO : LatLong
    {
        public virtual string Title { get; set; }
    }
}