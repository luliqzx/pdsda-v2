﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.Web.DTOs
{
    public class STASIUNKLIMATOLOGI_DYNDTO
    {
        public IList<jQueryFullCalendarEventModel> Events { get; set; }
        public STASIUNKLIMATOLOGI_DYN STASIUNKLIMATOLOGI_DYN { get; set; }
    }
}