﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.Web.DTOs
{
    public class MapsDTO
    {
        public string Title { get; set; }
        public LatLong LatLong { get; set; }
        public string Kode { get; set; }
    }
}