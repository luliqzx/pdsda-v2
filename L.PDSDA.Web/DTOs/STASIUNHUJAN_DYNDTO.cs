﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.Web.DTOs
{
    public class STASIUNHUJAN_DYNDTO
    {
        public IList<jQueryFullCalendarEventModel> Events { get; set; }
        public STASIUNHUJAN_DYN STASIUNHUJAN_DYN { get; set; }
    }
}