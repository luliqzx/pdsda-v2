﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using L.PDSDA.DAL.Repositories.Base;
using L.PDSDA.DAL.Entities;
using System.IO;
using NPOI.SS.UserModel;
using System.Globalization;

namespace L.PDSDA.Web.Common
{
    public class HidrologiUtils : Common.Utils
    {
        /// <summary>
        /// Pos Hujan
        /// </summary>
        /// <param name="sKodeStasiun"></param>
        /// <param name="iTahun"></param>
        public static void PosHujanGenerator(string sKodeStasiun, int iTahun)
        {
            IDefaultRepository<STASIUNHUJAN> hujanRepo = new DefaultRepository<STASIUNHUJAN>();
            STASIUNHUJAN STASIUNHUJAN = hujanRepo.Get(x => x.K_STASIUN == sKodeStasiun).FirstOrDefault();
            STASIUNHUJAN_DYN STASIUNHUJAN_DYN = STASIUNHUJAN.STASIUNHUJAN_DYN.FirstOrDefault(x => x.TAHUN == iTahun && x.STASIUNHUJAN == STASIUNHUJAN);
            IList<Utils.NameValue> dataHarian = new List<Utils.NameValue>();

            if (STASIUNHUJAN_DYN == null)
            {
                STASIUNHUJAN_DYN = new STASIUNHUJAN_DYN();
            }

            if (STASIUNHUJAN_DYN.HARIAN != null)
            {
                using (Stream stream = new MemoryStream(STASIUNHUJAN_DYN.HARIAN))
                {
                    StreamReader reader = new StreamReader(stream);
                    string lines = string.Empty;
                    while ((lines = reader.ReadLine()) != null)
                    {
                        //string text = reader.ReadLine();
                        string[] texts = lines.Split(',');
                        Utils.NameValue NameValue = new Utils.NameValue();
                        NameValue.Name = texts[0];
                        NameValue.Value = texts[1];
                        dataHarian.Add(NameValue);
                        lines = string.Empty;
                    }
                }
            }

            InitializeWorkbook();

            ISheet sheet1 = hssfworkbook.GetSheet("Data Stasiun");
            //create cell on rows, since rows do already exist,it's not necessary to create rows again.

            #region Set Data

            sheet1.GetRow(3).GetCell(1).SetCellValue(STASIUNHUJAN.N_STASIUN); // Nama pos Hujan
            sheet1.GetRow(3).GetCell(6).SetCellValue(STASIUNHUJAN.K_STASIUN); // No pos Hujan
            sheet1.GetRow(3).GetCell(12).SetCellValue(iTahun); // Nama pos Hujan
            sheet1.GetRow(5).GetCell(2).SetCellValue(STASIUNHUJAN.DA == null ? "" : STASIUNHUJAN.DA.N_DAS); // DAS
            sheet1.GetRow(5).GetCell(10).SetCellValue(Utils.TryParseToString(STASIUNHUJAN.THPENDIRIAN)); // Tahun Pendirian
            sheet1.GetRow(6).GetCell(2).SetCellValue(STASIUNHUJAN.W == null ? "" : STASIUNHUJAN.W.N_WS); // WS
            sheet1.GetRow(6).GetCell(10).SetCellValue(""); // Elevasi
            sheet1.GetRow(7).GetCell(2).SetCellValue(STASIUNHUJAN.LOKASI); // Lokasi
            sheet1.GetRow(7).GetCell(10).SetCellValue(STASIUNHUJAN.DIBANGUNOLEH); // Di Bangun Oleh
            sheet1.GetRow(8).GetCell(2).SetCellValue(""); // Geografi
            sheet1.GetRow(8).GetCell(10).SetCellValue(STASIUNHUJAN.KABUPATEN == null ? "" : STASIUNHUJAN.KABUPATEN.PROPINSI.N_PROPINSI); // Propinsi
            sheet1.GetRow(9).GetCell(2).SetCellValue(STASIUNHUJAN.KABUPATEN == null ? "" : STASIUNHUJAN.KABUPATEN.N_KABUPATEN + " / " + STASIUNHUJAN.KECAMATAN); // Kabupaten
            sheet1.GetRow(9).GetCell(10).SetCellValue(""); // Pelaksana
            sheet1.GetRow(60).GetCell(7).SetCellValue(string.Format("CURAH HUJAN HARIAN {0} TAHUN {1}", STASIUNHUJAN.N_STASIUN.ToUpper(), iTahun)); // Pelaksana

            #endregion

            DateTime dtStart = new DateTime(iTahun, 1, 1);
            DateTime dtEnd = new DateTime(iTahun, 12, 31);
            int iPosisiDay = dtStart.DayOfYear;
            int iLastDay = dtEnd.DayOfYear;

            int iGraphicDate = 61; // Mulai Grafik

            for (int i = 1; i <= 12; i++) // Looping Per Bulan
            {
                DateTime dt1 = new DateTime(iTahun, i, 1);
                DateTime dt2 = dt1.AddMonths(1).AddDays(-1);

                int p = (int)(dt2 - dt1).TotalDays;
                //sheet1.GetRow(45).GetCell(i).SetCellValue(p);
                for (int j = 0; j <= p; j++) // Looping Per Hari
                {
                    int iDay = 1 + j;
                    sheet1.GetRow(iDay + 12).GetCell(i).SetCellValue(GetValueFromData(iDay, i, dataHarian));//Convert.ToDouble(iDay + "" + i));
                    if (iGraphicDate > 61)
                    {
                        CopyRow(hssfworkbook, sheet1, 61, iGraphicDate);
                        sheet1.GetRow(iGraphicDate).GetCell(1).SetCellValue(new DateTime(iTahun, i, iDay));
                        sheet1.GetRow(iGraphicDate).GetCell(2).SetCellValue(GetValueFromData(iDay, i, dataHarian));
                    }
                    else
                    {
                        sheet1.GetRow(iGraphicDate).GetCell(1).SetCellValue(new DateTime(iTahun, i, iDay));
                        sheet1.GetRow(iGraphicDate).GetCell(2).SetCellValue(GetValueFromData(iDay, i, dataHarian));
                    }
                    iGraphicDate++;
                }
            }

            int iPer = 0;
            IList<NameValue> nvs = dataHarian;
            for (int i = 61; i < 72; i++)
            {
                nvs = nvs.OrderByDescending(x => TryParseToDouble(x.Value)).ToList();
                if (nvs.Count > 0)
                {
                    DateTime date = DateTime.ParseExact(nvs.FirstOrDefault().Name, "d-MM-yyyy", new CultureInfo("en-US"));
                    string sValue = nvs.FirstOrDefault().Value;

                    sheet1.GetRow(i).GetCell(3).SetCellValue(iPer);
                    sheet1.GetRow(i).GetCell(4).SetCellValue(date);
                    sheet1.GetRow(i).GetCell(5).SetCellValue(nvs.FirstOrDefault().Value);
                    iPer = iPer + 10;

                    List<NameValue> deletenvs = nvs.Where(x => x.Name.Contains(date.ToString("MM-yyyy"))).ToList();
                    deletenvs.ToList().ForEach(delegate(NameValue nv)
                    {
                        nvs.Remove(nv);
                    });
                }
            }

            //Force excel to recalculate all the formula while open
            sheet1.ForceFormulaRecalculation = true;

            WriteToFile();
        }

        /// <summary>
        /// Get Value
        /// </summary>
        /// <param name="iDay"></param>
        /// <param name="iMonth"></param>
        /// <param name="dataHarian"></param>
        /// <returns></returns>
        private static double GetValueFromData(int iDay, int iMonth, IList<NameValue> dataHarian)
        {
            string sMonth = iMonth < 10 ? "0" + iMonth : iMonth.ToString();
            NameValue nv = dataHarian.FirstOrDefault(x => x.Name.Contains(iDay + "-" + sMonth));
            return nv == null ? 0 : TryParseToDouble(nv.Value) == null ? 0 : Convert.ToDouble(nv.Value);
            //throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sKodeStasiun"></param>
        /// <param name="iTahun"></param>
        /// <param name="tipe"></param>
        internal static void PosDugaAirGenerator(string sKodeStasiun, int iTahun, string tipe)
        {
            IDefaultRepository<STASIUNDEBIT> debitRepo = new DefaultRepository<STASIUNDEBIT>();
            STASIUNDEBIT STASIUNDEBIT = debitRepo.Get(x => x.K_STASIUN == sKodeStasiun).FirstOrDefault();
            STASIUNDEBIT_DYN STASIUNDEBIT_DYN = STASIUNDEBIT.STASIUNDEBIT_DYN.FirstOrDefault(x => x.TAHUN == iTahun && x.STASIUNDEBIT == STASIUNDEBIT);
            IList<Utils.NameValue> dataHarian = new List<Utils.NameValue>();

            if (STASIUNDEBIT_DYN == null)
            {
                STASIUNDEBIT_DYN = new STASIUNDEBIT_DYN();
            }
            if (tipe == "1")
            {
                if (STASIUNDEBIT_DYN.DEBIT != null)
                {
                    using (Stream stream = new MemoryStream(STASIUNDEBIT_DYN.DEBIT))
                    {
                        StreamReader reader = new StreamReader(stream);
                        string lines = string.Empty;
                        while ((lines = reader.ReadLine()) != null)
                        {
                            //string text = reader.ReadLine();
                            string[] texts = lines.Split(',');
                            Utils.NameValue NameValue = new Utils.NameValue();
                            NameValue.Name = texts[0];
                            NameValue.Value = texts[1];
                            dataHarian.Add(NameValue);
                            lines = string.Empty;
                        }
                    }
                }
                InitializeWorkbook("~/templates/debit.xls");
            }
            else
            {
                if (STASIUNDEBIT_DYN.TMA != null)
                {
                    using (Stream stream = new MemoryStream(STASIUNDEBIT_DYN.TMA))
                    {
                        StreamReader reader = new StreamReader(stream);
                        string lines = string.Empty;
                        while ((lines = reader.ReadLine()) != null)
                        {
                            //string text = reader.ReadLine();
                            string[] texts = lines.Split(',');
                            Utils.NameValue NameValue = new Utils.NameValue();
                            NameValue.Name = texts[0];
                            NameValue.Value = texts[1];
                            dataHarian.Add(NameValue);
                            lines = string.Empty;
                        }
                    }
                }
                InitializeWorkbook("~/templates/tinggimukaair.xls");
            }


            ISheet sheet1 = hssfworkbook.GetSheet("Data Stasiun");
            //create cell on rows, since rows do already exist,it's not necessary to create rows again.

            #region Set Data
            //sheet1.GetRow(0).GetCell(0).SetCellValue(STASIUNDEBIT); // 

            sheet1.GetRow(3).GetCell(0).SetCellValue(STASIUNDEBIT.SUNGAI); // Nama Sungai
            sheet1.GetRow(3).GetCell(6).SetCellValue(STASIUNDEBIT.K_STASIUN); // No pos duga air
            sheet1.GetRow(3).GetCell(12).SetCellValue(iTahun); // Tahun
            sheet1.GetRow(5).GetCell(3).SetCellValue(STASIUNDEBIT.N_STASIUN); // 
            sheet1.GetRow(6).GetCell(3).SetCellValue(STASIUNDEBIT.INDUK_SUNGAI); // 
            sheet1.GetRow(7).GetCell(3).SetCellValue(STASIUNDEBIT.LOKASI_GEOGRAFIS); // 
            sheet1.GetRow(8).GetCell(3).SetCellValue(STASIUNDEBIT.LOKASI); // 
            sheet1.GetRow(9).GetCell(3).SetCellValue(STASIUNDEBIT.KABUPATEN == null ? "" : STASIUNDEBIT.KABUPATEN.PROPINSI.N_PROPINSI + ", Kab/Kota : " + STASIUNDEBIT.KABUPATEN.N_KABUPATEN); // 
            sheet1.GetRow(10).GetCell(3).SetCellValue(STASIUNDEBIT.LUAS_DAS == null ? "" : Convert.ToDouble(STASIUNDEBIT.LUAS_DAS) + ", Elevasi : " + STASIUNDEBIT.ELEVASI); // 
            sheet1.GetRow(13).GetCell(3).SetCellValue(STASIUNDEBIT.TGLDIBANGUN == null ? "" : Convert.ToDateTime(STASIUNDEBIT.TGLDIBANGUN).ToString("dd-MMM-yyyy") + " oleh : " + STASIUNDEBIT.DIBANGUNOLEH); // 

            if (tipe == "1")
            {
                sheet1.GetRow(14).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_PERIODE_PENCATATAN); // 
                sheet1.GetRow(15).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_JENIS_ALAT); // 
                sheet1.GetRow(18).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_MAKSIMUM_EKSTRIM); // 
                sheet1.GetRow(19).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_MINIMUM_EKSTRIM); // 
                sheet1.GetRow(22).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_MAKSIMUM_EKSTRIM_KUMULATIF); // 
                sheet1.GetRow(23).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_MINIMUM_EKSTRIM_KUMULATIF); // 
                sheet1.GetRow(24).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_PENENTUAN); // 
                sheet1.GetRow(25).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_CATATAN); // 
                sheet1.GetRow(26).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.D_PELAKSANA); // 
            }
            else
            {
                sheet1.GetRow(14).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_PERIODE_PENCATATAN); // 
                sheet1.GetRow(15).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_JENIS_ALAT); // 
                sheet1.GetRow(18).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_MAKSIMUM_EKSTRIM); // 
                sheet1.GetRow(19).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_MINIMUM_EKSTRIM); // 
                sheet1.GetRow(22).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_MAKSIMUM_EKSTRIM_KUMULATIF); // 
                sheet1.GetRow(23).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_MINIMUM_EKSTRIM_KUMULATIF); // 
                sheet1.GetRow(24).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_PENENTUAN); // 
                sheet1.GetRow(25).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_CATATAN); // 
                sheet1.GetRow(26).GetCell(3).SetCellValue(STASIUNDEBIT_DYN.T_PELAKSANA); // 
            }

            #endregion

            DateTime dtStart = new DateTime(iTahun, 1, 1);
            DateTime dtEnd = new DateTime(iTahun, 12, 31);
            int iPosisiDay = dtStart.DayOfYear;
            int iLastDay = dtEnd.DayOfYear;

            int iGraphicDate = 72; // Mulai Grafik
            IList<NameValue> nvs = dataHarian;

            if (nvs.Count > 0)
            {
                for (int i = 1; i <= 12; i++) // Looping Per Bulan
                {
                    DateTime dt1 = new DateTime(iTahun, i, 1);
                    DateTime dt2 = dt1.AddMonths(1).AddDays(-1);

                    int p = (int)(dt2 - dt1).TotalDays;
                    for (int j = 0; j <= p; j++) // Looping Per Hari
                    {
                        int iDay = 1 + j;
                        sheet1.GetRow(iDay + 29).GetCell(i).SetCellValue(GetValueFromData(iDay, i, dataHarian));//Convert.ToDouble(iDay + "" + i));
                        if (iGraphicDate > 72)
                        {
                            CopyRow(hssfworkbook, sheet1, 72, iGraphicDate);
                            sheet1.GetRow(iGraphicDate).GetCell(1).SetCellValue(new DateTime(iTahun, i, iDay));
                            sheet1.GetRow(iGraphicDate).GetCell(2).SetCellValue(GetValueFromData(iDay, i, dataHarian));
                        }
                        else
                        {
                            sheet1.GetRow(iGraphicDate).GetCell(1).SetCellValue(new DateTime(iTahun, i, iDay));
                            sheet1.GetRow(iGraphicDate).GetCell(2).SetCellValue(GetValueFromData(iDay, i, dataHarian));
                        }
                        iGraphicDate++;
                    }
                }

                int iPer = 0;
                int k = 57;
                for (int i = 72; i < 83; i++)
                {
                    nvs = nvs.OrderByDescending(x => TryParseToDouble(x.Value)).ToList();
                    if (nvs.Count > 0)
                    {
                        DateTime date = DateTime.ParseExact(nvs.FirstOrDefault().Name, "d-MM-yyyy", new CultureInfo("en-US"));
                        string sValue = nvs.FirstOrDefault().Value;

                        sheet1.GetRow(i).GetCell(3).SetCellValue(iPer);
                        sheet1.GetRow(i).GetCell(4).SetCellValue(date);
                        sheet1.GetRow(i).GetCell(5).SetCellValue(nvs.FirstOrDefault().Value);

                        sheet1.GetRow(k).GetCell(17).SetCellValue(iPer);
                        sheet1.GetRow(k).GetCell(18).SetCellValue(date);
                        sheet1.GetRow(k).GetCell(19).SetCellValue(nvs.FirstOrDefault().Value);
                        k++;
                        iPer = iPer + 10;

                        List<NameValue> deletenvs = nvs.Where(x => x.Name.Contains(date.ToString("MM-yyyy"))).ToList();
                        deletenvs.ToList().ForEach(delegate(NameValue nv)
                        {
                            nvs.Remove(nv);
                        });
                    }
                }
            }

            sheet1.GetRow(71).GetCell(7).SetCellValue(string.Format("HIDROGRAF DEBIT SUNGAI {0} TAHUN {1}", STASIUNDEBIT.N_STASIUN, iTahun));
            sheet1.GetRow(71).GetCell(8).SetCellValue(string.Format("LENGKUNG DURASI DEBIT HARIAN {0} TAHUN {1}", STASIUNDEBIT.N_STASIUN, iTahun));
            sheet1.GetRow(71).GetCell(10).SetCellValue(Utils.TryParseToDouble(STASIUNDEBIT.LUAS_DAS) == null ? 0 : Convert.ToDouble(STASIUNDEBIT.LUAS_DAS));

            //Force excel to recalculate all the formula while open
            sheet1.ForceFormulaRecalculation = true;

            WriteToFile();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sKodeStasiun"></param>
        /// <param name="iTahun"></param>
        public static void PosKlimatologiGenerator(string sKodeStasiun, int iTahun)
        {
            IDefaultRepository<STASIUNKLIMATOLOGI> klimatologiRepo = new DefaultRepository<STASIUNKLIMATOLOGI>();
            STASIUNKLIMATOLOGI STASIUNKLIMATOLOGI = klimatologiRepo.Get(x => x.K_STASIUN == sKodeStasiun).FirstOrDefault();
            STASIUNKLIMATOLOGI_DYN STASIUNKLIMATOLOGI_DYN = STASIUNKLIMATOLOGI.STASIUNKLIMATOLOGI_DYN.FirstOrDefault(x => x.TAHUN == iTahun && x.STASIUNKLIMATOLOGI == STASIUNKLIMATOLOGI);
            IList<Utils.NameValue> dataHarian = new List<Utils.NameValue>();

            if (STASIUNKLIMATOLOGI_DYN == null)
            {
                STASIUNKLIMATOLOGI_DYN = new STASIUNKLIMATOLOGI_DYN();
            }

            if (STASIUNKLIMATOLOGI_DYN.HARIAN != null)
            {
                using (Stream stream = new MemoryStream(STASIUNKLIMATOLOGI_DYN.HARIAN))
                {
                    StreamReader reader = new StreamReader(stream);
                    string lines = string.Empty;
                    while ((lines = reader.ReadLine()) != null)
                    {
                        //string text = reader.ReadLine();
                        string[] texts = lines.Split(',');
                        Utils.NameValue NameValue = new Utils.NameValue();
                        NameValue.Name = texts[0];
                        NameValue.Value = texts[1];
                        dataHarian.Add(NameValue);
                        lines = string.Empty;
                    }
                }
            }

            InitializeWorkbook("~/templates/template-klimatologi.xls");

            for (int i = 1; i <= 12; i++)
            {
                IList<NameValue> nvs = dataHarian.Where(x => DateTime.ParseExact(x.Name, "d-MM-yyyy", new CultureInfo("en-US")) != null && DateTime.ParseExact(x.Name, "d-MM-yyyy", new CultureInfo("en-US")).Month == i).ToList();
                ISheet sheet1 = hssfworkbook.GetSheet(GetSheetNameKlimatologi(i));

                #region Set Data
                sheet1.GetRow(1).GetCell(0).SetCellValue("TAHUN : " + iTahun); // PROPINSI
                sheet1.GetRow(3).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.KABUPATEN == null ? ": " : ": " + STASIUNKLIMATOLOGI.KABUPATEN.PROPINSI.N_PROPINSI); // PROPINSI
                sheet1.GetRow(4).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.KABUPATEN == null ? ": " : ": " + STASIUNKLIMATOLOGI.KABUPATEN.N_KABUPATEN); // KABUPATEN
                sheet1.GetRow(5).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.K_STASIUN);
                sheet1.GetRow(6).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.N_STASIUN);
                sheet1.GetRow(7).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.KADASTER);
                sheet1.GetRow(8).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.LOKASI);
                sheet1.GetRow(9).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.KECAMATAN);
                sheet1.GetRow(10).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.DESA);
                sheet1.GetRow(11).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.TINGGI_MUKA_LAUT == null ? 0 : Convert.ToDouble(STASIUNKLIMATOLOGI.TINGGI_MUKA_LAUT));
                sheet1.GetRow(12).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.W == null ? "" : STASIUNKLIMATOLOGI.W.N_WS);
                sheet1.GetRow(13).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.DA == null ? "" : STASIUNKLIMATOLOGI.DA.N_DAS);
                sheet1.GetRow(14).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.DIBANGUNOLEH);
                sheet1.GetRow(15).GetCell(2).SetCellValue(STASIUNKLIMATOLOGI.THPENDIRIAN == null ? "" : Convert.ToInt16(STASIUNKLIMATOLOGI.THPENDIRIAN).ToString());
                sheet1.GetRow(16).GetCell(2).SetCellValue(iTahun);
                #endregion

                for (int j = 0; j < nvs.Count; j++)
                {
                    int date = DateTime.ParseExact(nvs[j].Name, "d-MM-yyyy", new CultureInfo("en-US")).Day;// Convert.ToDateTime(nvs[j].Name).Day;
                    string[] values = nvs[j].Value.Split(';');
                    //CopyRow(hssfworkbook, sheet1, j + 20, j + 21);
                    sheet1.GetRow(j + 20).GetCell(0).SetCellValue(date);
                    if (values.Count() == 11)
                    {
                        sheet1.GetRow(j + 20).GetCell(1).SetCellValue(values[0]);
                        sheet1.GetRow(j + 20).GetCell(2).SetCellValue(values[1]);
                        sheet1.GetRow(j + 20).GetCell(3).SetCellValue(values[2]);
                        sheet1.GetRow(j + 20).GetCell(4).SetCellValue(values[3]);
                        sheet1.GetRow(j + 20).GetCell(5).SetCellValue(values[4]);
                        sheet1.GetRow(j + 20).GetCell(6).SetCellValue(values[5]);
                        sheet1.GetRow(j + 20).GetCell(7).SetCellValue(values[6]);
                        sheet1.GetRow(j + 20).GetCell(8).SetCellValue(values[7]);
                        sheet1.GetRow(j + 20).GetCell(9).SetCellValue(values[8]);
                        sheet1.GetRow(j + 20).GetCell(10).SetCellValue(values[9]);
                        sheet1.GetRow(j + 20).GetCell(11).SetCellValue(values[10]);
                    }
                }

                if (i == 2 && nvs.Count < 29)
                {
                    sheet1.RemoveRow(sheet1.GetRow(48));
                }

                //Force excel to recalculate all the formula while open
                sheet1.ForceFormulaRecalculation = true;
                //break;
            }

            WriteToFile();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private static string GetSheetNameKlimatologi(int i)
        {
            switch (i)
            {
                case 1:
                    return "Januari";
                case 2:
                    return "Februari";
                case 3:
                    return "Maret";
                case 4:
                    return "April";
                case 5:
                    return "Mei";
                case 6:
                    return "Juni";
                case 7:
                    return "Juli";
                case 8:
                    return "Agustus";
                case 9:
                    return "September";
                case 10:
                    return "Oktober";
                case 11:
                    return "November";
                case 12:
                    return "Desember";
                default:
                    return "";
            }
        }
    }
}