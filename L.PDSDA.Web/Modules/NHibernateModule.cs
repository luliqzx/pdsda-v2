﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace L.PDSDA.Web.Modules
{
    using L.PDSDA.DAL.Config;
    using NHibernate;
    using NHibernate.Linq;
    using NHibernate.Context;
    using System.Web.UI;
    using System.Web;
    using L.PDSDA.DAL.Entities;
    using log4net;
    using log4net.Config;
    using L.Core.Modules;
    using L.Core.Utilities.Enums;
    using L.Core.Utilities;

    public class NHibernateModule : NHibernateConfig, IHttpModule
    {
        #region IHttpModule Members

        ILog log = LogManager.GetLogger(typeof(NHibernateModule));

        private string errMsgData = "";
        private string errMsgApp = "";

        public void Init(HttpApplication context)
        {
            context.BeginRequest += BeginRequest;
            context.EndRequest += EndRequest;
        }

        // Opens the session, begins the transaction, and binds the session
        private void BeginRequest(object sender, EventArgs e)
        {
            XmlConfigurator.Configure();
            BuildSessionFactory();
            try
            {
                if (GetFactories != null && GetFactories.Count > 0)
                {
                    #region Comments
                    //ISessionFactory SessionFactoryMsSql = GetFactories["mssqlserver"];
                    //if (SessionFactoryMsSql == null)
                    //{
                    //    GetFactories.Remove("mssqlserver");
                    //}
                    //else
                    //{
                    //    if (!CurrentSessionContext.HasBind(GetFactories["mssqlserver"]))
                    //    {
                    //        ISession session = GetFactories["mssqlserver"].OpenSession();
                    //        session.BeginTransaction();
                    //        CurrentSessionContext.Bind(session);
                    //    }
                    //}
                    #endregion

                    IList<string> factoryKeys = GetFactories.Keys.ToList();
                    for (int i = 0; i < factoryKeys.Count; i++)
                    {
                        ISessionFactory SessionFactoryMsSql = GetFactories[factoryKeys[i]];
                        if (SessionFactoryMsSql == null)
                        {
                            GetFactories.Remove(factoryKeys[i]);
                        }
                        else
                        {
                            if (!CurrentSessionContext.HasBind(GetFactories[factoryKeys[i]]))
                            {
                                ISession session = GetFactories[factoryKeys[i]].OpenSession();
                                //session.BeginTransaction();
                                //WebSessionContext.Bind(session);
                                WebSessionContext.Bind(session);
                            }
                        }
                    }
                }
            }
            catch (HibernateException he)
            {
                errMsgData = he.InnerException == null ? he.Message : he.InnerException.InnerException == null ? he.InnerException.Message : he.InnerException.InnerException.Message;
                log.ErrorFormat("ERROR HibernateException, Cannot bind Session, Please check database first {0}", he.StackTrace);
            }
            catch (Exception ex)
            {
                errMsgApp = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                log.ErrorFormat("ERROR Exception, Please check error {0}", ex.Message);
            }

            if (IsError)
            {
                log.Error(ErrorMessage);
            }
        }

        private object lockObject = new object();

        private void BuildSessionFactory()
        {
            lock (lockObject)
            {
                if (NHibernateSessionFactoriesPerRequest.GetFactories == null || NHibernateSessionFactoriesPerRequest.GetFactories.Count == 0)
                {
                    if (_allFactories == null || _allFactories.Count == 0)
                    {
                        bool IsBuildSchema = false;
                        bool.TryParse(Utils.GetAppSetting("IsBuildSchema"), out IsBuildSchema);

                        _allFactories = new Dictionary<string, ISessionFactory>();
                        #region SQL SERVER - REGISTER
                        ISessionFactory sf = CreateSessionFactory(DefaultDB.MySQL, RunIn.Web, IsBuildSchema);
                        if (sf != null)
                        {
                            _allFactories.Add("mysqlfactories", sf);//, "sqlite"));
                        }
                        else
                        {
                            log.FatalFormat(@"Failed to create session factory MySQL");
                            _allFactories = null;
                            return;
                        }
                        #endregion

                        //_allFactories.Add("mssqlserverreturn", CreateSessionFactory(DefaultDB.None, RunIn.Web, false, "mssqlserverreturn"));//, "sqlite"));
                    }
                    NHibernateSessionFactoriesPerRequest.SetFactories(_allFactories);
                }
            }
        }

        // Unbinds the session, commits the transaction, and closes the session
        private void EndRequest(object sender, EventArgs e)
        {
            if (GetFactories == null || GetFactories.Count == 0)// || _allFactories["access"] == null)
            {
                log.Error("[CUSTOM] SessionFactory null");
            }
            else
            {
                if (GetFactories != null && GetFactories.Count > 0)
                {
                    IList<string> factoryKeys = GetFactories.Keys.ToList();
                    for (int i = 0; i < factoryKeys.Count; i++)
                    {
                        ISession sessionmssqlserver = CurrentSessionContext.Unbind(GetFactories[factoryKeys[i]]);
                        if (sessionmssqlserver == null) return;// || sessionAccess == null) return;
                        try
                        {
                            //if (sessionmssqlserver.Transaction != null && sessionmssqlserver.Transaction.IsActive)
                            //    sessionmssqlserver.Transaction.Commit();
                            ErrorMessage = "";
                            IsError = false;
                        }
                        catch (Exception ex)
                        {
                            log.ErrorFormat("[C] - ERROR Exception on End Request, Please check error {0}"
                                //, ex.InnerException == null ? ex.Message : ex.InnerException.InnerException == null ? ex.InnerException.Message : ex.InnerException.InnerException.Message);
                                , ex.GetFullMessage());
                            ErrorMessage = ex.Message;
                            IsError = true;
                            //if (sessionmssqlserver.Transaction != null && sessionmssqlserver.Transaction.IsActive)
                            //    sessionmssqlserver.Transaction.();
                        }
                        finally
                        {
                            if (sessionmssqlserver != null)
                            {
                                sessionmssqlserver.Close();
                                sessionmssqlserver.Dispose();
                            }
                        }
                    }
                }
            }
            if (IsError)
            {
                log.Error(ErrorMessage);
            }
        }


        //private static void BeginRequest(object sender, EventArgs e)
        //{
        //    ILog log = LogManager.GetLogger(typeof(NHibernateModule));


        //    //MvcApplication mvcApp = sender as MvcApplication;
        //    //string url = mvcApp.Context.Request.AppRelativeCurrentExecutionFilePath;
        //    try
        //    {
        //        if (!CurrentSessionContext.HasBind(SessionFactory))
        //        {
        //            ISession session = SessionFactory.OpenSession();
        //            session.BeginTransaction();
        //            CurrentSessionContext.Bind(session);
        //            log.Info("Success Bind Session");
        //            //Module module = session.Query<Module>().FirstOrDefault(x => x.Path.Contains(url));
        //            //if (module == null)
        //            //{
        //            //    HttpContext.Current.Response.Redirect("~/Global/NotFound/");
        //            //}
        //        }
        //    }
        //    catch (HibernateException he)
        //    {
        //        log.ErrorFormat("ERROR HibernateException, Cannot bind Session, Please check database first {0}", he.StackTrace);
        //        //HttpContext.Current.Response.Redirect("~/Views/Shared/HttpError500.cshtml?msg=" + he.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.ErrorFormat("ERROR Exception, Please check error {0}", ex.Message);
        //        //HttpContext.Current.Response.Redirect("~/Views/Shared/HttpError500.cshtml?msg=" + ex.Message);
        //        //HttpContext.Current.Response.Redirect("~/");
        //    }

        //    if (IsError)
        //    {
        //        log.Error(ErrorMessage);
        //        //HttpContext.Current.Response.Redirect("~/Views/Shared/HttpError500.cshtml?msg=" + ErrorMessage);
        //        //return;
        //    }
        //}

        // Unbinds the session, commits the transaction, and closes the session
        //private static void EndRequest(object sender, EventArgs e)
        //{
        //    ILog log = LogManager.GetLogger(typeof(NHibernateModule));

        //    if (SessionFactory == null)
        //    {
        //        log.Error("[EndRequest] SessionFactory null");
        //        //HttpContext.Current.Response.Redirect("~/Views/Shared/HttpError500.cshtml?msg=" + "[CUSTOM] SessionFactory null, Please check connection first");
        //    }
        //    else
        //    {
        //        ISession session = CurrentSessionContext.Unbind(SessionFactory);

        //        if (session == null) return;

        //        try
        //        {
        //            session.Transaction.Commit();
        //        }
        //        catch (Exception)
        //        {
        //            session.Transaction.Rollback();
        //        }
        //        finally
        //        {
        //            session.Close();
        //            session.Dispose();
        //        }
        //    }

        //    if (IsError)
        //    {
        //        log.Error(ErrorMessage);
        //        //HttpContext.Current.Response.Redirect("~/Views/Shared/HttpError500.cshtml?msg=" + ErrorMessage);
        //        //return;
        //    }
        //}

        #endregion


    }
}