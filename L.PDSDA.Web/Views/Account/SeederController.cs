﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories;
using L.PDSDA.DAL.Repositories.Implements;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.Web.Controllers
{
    public class SeederController : Controller
    {
        //
        // GET: /Seeder/


        private readonly IDepartmentRepository DepartmentRepository;
        private readonly IUserRepository UserRepository;
        public SeederController()
        {
            this.DepartmentRepository = new DepartmentRepository();
            this.UserRepository = new UserRepository();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Department()
        {
            var Department1 = new Department { Name = "Finance", Code = "FN" };
            var Department2 = new Department { Name = "HR", Code = "HR" };
            var Department3 = new Department { Name = "Sales", Code = "SL" };

            this.DepartmentRepository.SaveOrUpdateAll(Department1, Department2, Department3);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult User()
        {
            var User1 = new User
            {
                Username = "luckyp1",
                Firstname = "Lucky",
                Department = this.DepartmentRepository.Get(x => x.Code == "HR").FirstOrDefault()
            };
            var User2 = new User
            {
                Username = "luckyp2",
                Firstname = "Lucky",
                Department = this.DepartmentRepository.Get(x => x.Code == "HR").FirstOrDefault()
            };
            var User3 = new User
            {
                Username = "luckyp3",
                Firstname = "Lucky",
                Department = this.DepartmentRepository.Get(x => x.Code == "SL").FirstOrDefault()
            };

            this.UserRepository.SaveOrUpdateAll(User1, User2, User3);
            
            //User User1 = this.UserRepository.GetById(1);
            //User1.Department = this.DepartmentRepository.Get(x => x.Code == "SL").FirstOrDefault();
            //this.UserRepository.SaveOrUpdateAll(User1);

            return RedirectToAction("Index");
        }
    }
}
