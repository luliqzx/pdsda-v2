﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<script runat="server">
    private void Page_Load(object sender, System.EventArgs e)
    {

        //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/Report1.rdlc");
        //ReportViewer1.LocalReport.Refresh();

        L.PDSDA.Web.pdsdadbDataSetTableAdapters.airtanahTableAdapter tableadapter = new L.PDSDA.Web.pdsdadbDataSetTableAdapters.airtanahTableAdapter();
        L.PDSDA.Web.pdsdadbDataSet.airtanahDataTable datatable = new L.PDSDA.Web.pdsdadbDataSet.airtanahDataTable();
        tableadapter.Fill(datatable);
        ReportDataSource reportds = new ReportDataSource("DataSet1", datatable as System.Data.DataTable);

        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/Report1.rdlc");
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(reportds);
        //ReportViewer1.LocalReport.Refresh();
    }
</script>
<form id="Form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
</asp:ScriptManager>
<rsweb:ReportViewer ID="ReportViewer1" runat="server" >
</rsweb:ReportViewer>
</form>
