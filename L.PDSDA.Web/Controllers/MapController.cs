﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using L.Core.Utilities;

namespace L.PDSDA.Web.Controllers
{
    public class MapController : DefaultBaseController
    {
        //
        // GET: /Map/
        IDefaultRepository<SUNGAI> SungaiRepo;
        IDefaultRepository<RAWA> RawaRepo;
        IDefaultRepository<BENDUNG> BendungRepo;
        IDefaultRepository<AIRTANAH> AirTanahRepo;

        public MapController()
        {
            SungaiRepo = new DefaultRepository<SUNGAI>();
            RawaRepo = new DefaultRepository<RAWA>();
            BendungRepo = new DefaultRepository<BENDUNG>();
            AirTanahRepo = new DefaultRepository<AIRTANAH>();
        }


        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AllMap()
        {
            string action = Utils.GetPostRequest("action");

            #region SDA
            if (action.ToLower().Equals("sda"))
            {
                JsonResult jr = this.SungaiMap();
            }
            else if (action.ToLower().Equals("sungai"))
            {
                return this.SungaiMap();
            }
            else if (action.ToLower().Equals("airtanah"))
            {
                return this.AirTanahMap();
            }
            else if (action.ToLower().Equals("rawa"))
            {
                return this.RawaMap();
            }
            else if (action.ToLower().Equals("bendung"))
            {
                return this.BendungMap();
            }
            else if (action.ToLower().Equals("bendungan"))
            {
                return this.BendunganMap();
            }
            else if (action.ToLower().Equals("danau"))
            {
                return this.DanauMap();
            }
            else if (action.ToLower().Equals("embung"))
            {
                return this.EmbungMap();
            }
            else if (action.ToLower().Equals("embungpotensi"))
            {
                return this.EmbungPotensiMap();
            }
            #endregion SDA

            #region Bencana
            else if (action.ToLower().Equals("poskobanjir"))
            {
                return this.PoskoBanjirMap();
            }
            else if (action.ToLower().Equals("pospengamatbanjir"))
            {
                return this.PosPengamatBanjirMap();
            }
            else if (action.ToLower().Equals("daerahrawanbanjir"))
            {
                return this.DaerahRawanBanjirMap();
            }
            else if (action.ToLower().Equals("daerahrawankekeringan"))
            {
                return this.DaerahRawanKekeringanMap();
            }
            else if (action.ToLower().Equals("daerahrawanlongsor"))
            {
                return this.DaerahRawanLongsorMap();
            }
            else if (action.ToLower().Equals("daerahgunungberapi"))
            {
                return this.DaerahGunungBerapiMap();
            }
            #endregion Bencana

            #region Hidrologi
            else if (action.ToLower().Equals("poshujan"))
            {
                return this.PosHujanMap();
            }
            else if (action.ToLower().Equals("posdugaair"))
            {
                return this.PosDugaAirMap();
            }
            else if (action.ToLower().Equals("posklimatologi"))
            {
                return this.PosKlimatologiMap();
            }
            #endregion Hidrologi

            return null;
        }

        #region SDA
        private JsonResult BendungMap()
        {
            IQueryable<BENDUNG> collDomain = this.BendungRepo.GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<BENDUNG> collResult = new List<BENDUNG>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_BENDUNG,
                             name = c.N_BENDUNG,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "bendung"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult RawaMap()
        {
            IQueryable<RAWA> collDomain = this.RawaRepo.GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<RAWA> collResult = new List<RAWA>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_RAWA,
                             name = c.N_RAWA,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "rawa"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult SungaiMap()
        {
            IQueryable<SUNGAI> collDomain = this.SungaiRepo.GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<SUNGAI> collResult = new List<SUNGAI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.URUT + "," + c.K_ORDE1 + "," + c.K_ORDE2 + "," + c.K_ORDE3 + "," + c.K_ORDE4 + "," + c.K_ORDE5,
                             name = c.N_SUNGAI,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "sungai"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult AirTanahMap()
        {
            IQueryable<AIRTANAH> collDomain = this.AirTanahRepo.GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<AIRTANAH> collResult = new List<AIRTANAH>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_AIRTANAH,
                             name = c.NO_SUMUR,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "airtanah"

                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult BendunganMap()
        {
            IQueryable<BENDUNGAN> collDomain = new DefaultRepository<BENDUNGAN>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<BENDUNGAN> collResult = new List<BENDUNGAN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_BENDUNGAN,
                             name = c.N_BENDUNGAN,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "bendungan"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult EmbungMap()
        {
            IQueryable<EMBUNG> collDomain = new DefaultRepository<EMBUNG>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<EMBUNG> collResult = new List<EMBUNG>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_EMBUNG,
                             name = c.N_EMBUNG,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "embung"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult EmbungPotensiMap()
        {
            IQueryable<EMBUNG_POTENSI> collDomain = new DefaultRepository<EMBUNG_POTENSI>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<EMBUNG_POTENSI> collResult = new List<EMBUNG_POTENSI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_EMBUNG_POTENSI,
                             name = c.N_EMBUNG_POTENSI,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "embungpotensi"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult DanauMap()
        {
            IQueryable<DANAU> collDomain = new DefaultRepository<DANAU>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<DANAU> collResult = new List<DANAU>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_DANAU,
                             name = c.N_DANAU,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "danau"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }
        #endregion SDA

        #region Bencana
        private JsonResult PoskoBanjirMap()
        {
            IQueryable<POSKO_BANJIR> collDomain = new DefaultRepository<POSKO_BANJIR>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<POSKO_BANJIR> collResult = new List<POSKO_BANJIR>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_POSKOBANJIR,
                             name = c.N_POSKOBANJIR,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "poskobanjir"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult DaerahGunungBerapiMap()
        {
            IQueryable<GUNUNGBERAPI> collDomain = new DefaultRepository<GUNUNGBERAPI>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<GUNUNGBERAPI> collResult = new List<GUNUNGBERAPI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_GUNUNGBERAPI,
                             name = c.N_GUNUNGBERAPI,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "daerahgunungberapi"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult DaerahRawanLongsorMap()
        {
            IQueryable<DAERAHRAWANLONGSOR> collDomain = new DefaultRepository<DAERAHRAWANLONGSOR>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<DAERAHRAWANLONGSOR> collResult = new List<DAERAHRAWANLONGSOR>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_DAERAHRAWANLONGSOR,
                             name = c.N_DAERAHRAWANLONGSOR,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "daerahrawanlongsor"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult DaerahRawanKekeringanMap()
        {
            IQueryable<DAERAHRAWANKEKERINGAN> collDomain = new DefaultRepository<DAERAHRAWANKEKERINGAN>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<DAERAHRAWANKEKERINGAN> collResult = new List<DAERAHRAWANKEKERINGAN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_DAERAHRAWANKEKERINGAN,
                             name = c.N_DAERAHRAWANKEKERINGAN,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "daerahrawankekeringan"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult DaerahRawanBanjirMap()
        {
            IQueryable<DAERAHRAWANBANJIR> collDomain = new DefaultRepository<DAERAHRAWANBANJIR>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<DAERAHRAWANBANJIR> collResult = new List<DAERAHRAWANBANJIR>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_DAERAHRAWANBANJIR,
                             name = c.N_DAERAHRAWANBANJIR,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "daerahrawanbanjir"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult PosPengamatBanjirMap()
        {
            IQueryable<POS_PENGAMAT> collDomain = new DefaultRepository<POS_PENGAMAT>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<POS_PENGAMAT> collResult = new List<POS_PENGAMAT>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_POSPENGAMAT,
                             name = c.N_POSPENGAMAT,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "pospengamatbanjir"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }

        #endregion Bencana

        #region Hidrologi
        public JsonResult PosHujanMap()
        {
            IQueryable<STASIUNHUJAN> collDomain = new DefaultRepository<STASIUNHUJAN>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<STASIUNHUJAN> collResult = new List<STASIUNHUJAN>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_STASIUN,
                             name = c.N_STASIUN,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "poshujan"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PosDugaAirMap()
        {
            IQueryable<STASIUNDEBIT> collDomain = new DefaultRepository<STASIUNDEBIT>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<STASIUNDEBIT> collResult = new List<STASIUNDEBIT>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_STASIUN,
                             name = c.N_STASIUN,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "posdugaair"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PosKlimatologiMap()
        {
            IQueryable<STASIUNKLIMATOLOGI> collDomain = new DefaultRepository<STASIUNKLIMATOLOGI>().GetAll();
            collDomain = collDomain.Where(x => x.LatLong != null && x.LatLong.Latitude.Trim() != string.Empty && x.LatLong.Longitude.Trim() != string.Empty);

            IList<STASIUNKLIMATOLOGI> collResult = new List<STASIUNKLIMATOLOGI>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
            }

            var result = from c in collResult
                         select new
                         {
                             KEY = c.K_STASIUN,
                             name = c.N_STASIUN,
                             lat = c.LatLong == null ? "" : c.LatLong.Latitude,
                             lng = c.LatLong == null ? "" : c.LatLong.Longitude
                             ,
                             type = "posklimatologi"
                         };

            return Json(new { Locations = result }, JsonRequestBehavior.AllowGet);
        }
        #endregion Hidrologi

    }
}
