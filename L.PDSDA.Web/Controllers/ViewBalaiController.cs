﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;
using L.Core.Utilities;

namespace L.PDSDA.Web.Controllers
{
    public class ViewBalaiController : Controller
    {
        //
        // GET: /ViewBalai/

        private readonly IDefaultRepository<BALAI> BALAIRepository;
        public ViewBalaiController()
        {
            BALAIRepository = new DefaultRepository<BALAI>();
        }

        /// <summary>
        /// 
        /// </summary>
        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        /// <summary>
        /// 
        /// </summary>
        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        private string GetWsName(BALAI balai)
        {
            string sWs = string.Empty;
            //foreach (WS ws in balai.WSs.OrderBy(x => x.N_WS))
            foreach (WS ws in balai.WSs)
            {
                if (ws != null)
                {
                    if (string.IsNullOrWhiteSpace(sWs))
                    {
                        sWs = ws.N_WS;
                    }
                    else
                    {
                        sWs = sWs + ", " + ws.N_WS;
                    }
                }
            }
            return sWs;
        }

        public ActionResult ViewIndex()
        {
            return PartialView();
        }

        public ActionResult LoadAllDataView(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<BALAI> collBalai = this.BALAIRepository.Get(x => x.KodeBalai == Utils.GetPostRequest("kodebalai"));

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collBalai = collBalai.Where(x => x.KodeBalai.Contains(param.sSearch) || x.NamaBalai.Contains(param.sSearch)
                    || (x.WSs.FirstOrDefault(p => p.N_WS.Contains(param.sSearch)) != null));
            }
            Count = collBalai.Count();

            var sortColumnIndex = iSortCol;
            Func<BALAI, string> orderingFunction = (c => sortColumnIndex == 1 ? c.KodeBalai
                                                                : sortColumnIndex == 2 ? c.NamaBalai
                                                                : sortColumnIndex == 3 ? c.WSs != null && c.WSs.FirstOrDefault() != null ? c.WSs.FirstOrDefault().N_WS : ""
                                                                : c.KodeBalai);

            IList<BALAI> collBALAI = new List<BALAI>();
            if (collBalai.Count() > 0)
            {
                collBALAI = collBalai.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collBALAI = collBALAI.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collBALAI = collBALAI.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }

            var result = from c in collBALAI
                         select new
                         {
                             KEY = c.KodeBalai,
                             KodeBalai = c.KodeBalai,
                             NamaBalai = c.NamaBalai,
                             Ws = this.GetWsName(c)
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
