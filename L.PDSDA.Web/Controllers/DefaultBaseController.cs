﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace L.PDSDA.Web.Controllers
{
    using L.PDSDA.DAL.Entities;
    using L.PDSDA.DAL.Repositories;
    using L.PDSDA.DAL.Repositories.Implements;
    using log4net;
    using L.Core.Utilities;
    using L.Core.General;
    using System.Web.Routing;
    using System.Collections.Specialized;
    public abstract class DefaultBaseController : Controller
    {
        //
        // GET: /Base/
        protected bool IsViewable = false;
        protected bool IsCreateable = false;
        protected bool IsUpdateable = false;
        protected bool IsDeleteable = false;
        protected string UserLoggedIn { get; private set; }
        protected string UserHostAddress { get; private set; }
        protected virtual string KodePropinsi { get; set; }
        protected virtual string NamaPropinsi { get; set; }
        protected virtual PROPINSI PROPINSI { get; set; }
        protected int MenuID { get; set; }
        protected string CodeMenu { get; set; }
        protected bool IsDetail { get; set; }
        protected bool IsVerifiedUser { get; set; }
        protected NameValueCollection QueryString { get; set; }
        ILog log = LogManager.GetLogger(typeof(BaseController));
        protected string Method { get; set; }

        protected override void Execute(System.Web.Routing.RequestContext requestContext)
        {
            IsVerifiedUser = false;
            UserHostAddress = requestContext.HttpContext.Request.UserHostAddress;
            string url = requestContext.HttpContext.Request.AppRelativeCurrentExecutionFilePath.Trim();
            QueryString = requestContext.HttpContext.Request.QueryString;
            if (QueryString.HasKeys())
            {
                Method = QueryString["method"];
                if (!string.IsNullOrWhiteSpace(Method))
                {
                    switch (Method)
                    {
                        case "Detail":
                            IsDetail = true;
                            break;
                        default:
                            IsDetail = false;
                            break;
                    }
                }
            }

            ViewBag.BreadCrumbs = "";


            Module mod = this.ModuleRepository.GetByUrlRelativePath(url);
            string modName = string.Empty;
            bool IsPublic = false;
            if (mod != null)
            {
                IsPublic = mod.IsPublic;
                if ((url.Contains("Create") || url.Contains("Edit") || url.Contains("Delete")))
                {
                    IsPublic = false;
                }
                modName = mod.Name;
                BuildBreadCrumbs(mod);
                ViewBag.Title = modName;
                MenuID = mod.Id;
                CodeMenu = mod.Code;
            }

            CheckDefaultPage(url, modName, requestContext);

            if (IsPublic)
            {
                IsViewable = IsPublic;
            }
            if (!IsViewable && !IsDetail)
            {
                //requestContext.HttpContext.Response.Redirect("~/Global/NotPermit/" + modName, true);
            }

            ViewBag.IsViewable = IsViewable;
            ViewBag.IsCreateable = IsCreateable;
            ViewBag.IsUpdateable = IsUpdateable;
            ViewBag.IsDeleteable = IsDeleteable;

            base.Execute(requestContext);
        }

        void CheckDefaultPage(string url, string modName, RequestContext requestContext)
        {
            {
                string DefaultAllowPath = Utils.GetAppSetting("DefaultAllowPath");
                string[] arrAllowPath = DefaultAllowPath.Split(',');
                //bool IsAllow = false;
                if (arrAllowPath.Contains(url))
                {
                    //for (int i = 0; i < arrAllowPath.Length; i++)
                    //{
                    //    if (url.Contains(arrAllowPath[i]))
                    //    {
                    //IsAllow = true;
                    IsViewable = true;
                    //break;
                    //    }
                    //}
                    //if (!IsAllow)
                    //{
                    //    requestContext.HttpContext.Response.Redirect("~/Global/NotPermit/" + modName, true);
                    //}
                }
                else
                {
                    for (int i = 0; i < arrAllowPath.Length; i++)
                    {
                        if (url.Contains(arrAllowPath[i]))
                        {
                            //IsAllow = true;
                            IsViewable = true;
                            break;
                        }
                    }
                    //if (!IsAllow)
                    //{
                    //    requestContext.HttpContext.Response.Redirect("~/Global/NotPermit/" + modName, true);
                    //}
                }
            }
        }

        private void BuildBreadCrumbs(Module mod)
        {
            string modName = " > " + mod.Name;
            string breadCrumbName = modName;
            if (mod.MainModule != null)
            {
                BuildParentBreadCrumbs(modName, mod.MainModule, out breadCrumbName);
            }
            ViewBag.BreadCrumbs = breadCrumbName;
        }

        private void BuildParentBreadCrumbs(string modName, Module module, out string breadCrumbName)
        {
            Module Parent = this.ModuleRepository.GetById(module.Id);
            string parentModName = " > " + Parent.Name + modName;
            breadCrumbName = parentModName;
            if (Parent.MainModule != null)
            {
                BuildParentBreadCrumbs(parentModName, Parent.MainModule, out breadCrumbName);
            }
        }

        private readonly IUserRepository UserRepository;
        private readonly IModuleRepository ModuleRepository;

        public DefaultBaseController()
        {
            this.UserRepository = new UserRepository();
            this.ModuleRepository = new ModuleRepository();
        }

        protected ActionResult GenericView(string urlView, string extension = "cshtml")
        {
            return View(urlView + "." + extension);
        }

        protected ActionResult GenericViewDefault()
        {
            return View("~/Views/Shared/NotPermit.cshtml");
        }

        protected bool IsPermitCUD(out string urlRedirect)
        {
            string key = Utils.GetQueryString("key");
            if (string.IsNullOrEmpty(key))
            {
                urlRedirect = "~/Views/Shared/NotPermit";
                return false;
            }
            else
            {
                try
                {
                    string plainText = Utils.Decrypt(key);
                    string[] strArr = plainText.Split('|');
                    if (strArr[0] != this.CodeMenu)
                    {
                        urlRedirect = "~/Views/Shared/NotPermit";
                        return false;
                    }
                }
                catch (Exception)
                {
                    urlRedirect = "~/Views/Shared/HttpError500";
                    return false;
                }
            }
            urlRedirect = string.Empty;
            return true;
        }

    }
}
