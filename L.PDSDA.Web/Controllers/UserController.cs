﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories;
using L.PDSDA.DAL.Repositories.Implements;
using L.PDSDA.DAL.Entities;
using System.Data;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using LNet = L.Core.Utilities.Net;
using L.PDSDA.Web.DTOs;
using System.Text;
using L.Core.General;
using log4net;
using L.PDSDA.DAL.Repositories.Base;

namespace L.PDSDA.Web.Controllers
{
    public class UserController : BaseController
    {
        ILog log = LogManager.GetLogger(typeof(UserController));

        //
        // GET: /User/
        private readonly IUserRepository UserRepository;
        private readonly IDepartmentRepository DepartmentRepository;
        private readonly IGroupRepository GroupRepository;
        public UserController()
        {
            this.UserRepository = new UserRepository();
            this.DepartmentRepository = new DepartmentRepository();
            this.GroupRepository = new GroupRepository();
            log.Info("Initialize UserController " + this.UserLoggedIn);
        }

        public void LogOff()
        {
            User usr = Session["UserLoggedIn"] as User;
            if (usr != null)
            {
                log.Info(string.Format("User logout : {0}", usr.Username));
                Session.Abandon();
            }

            //return View();
            //return new RedirectResult("~/");
            //this.gilterContext.Result = new RedirectResult(“our url”);

            //Response.Redirect("~/");
            RedirectToAction("Index", "Home", new { area = "" }).ExecuteResult(ControllerContext);

            //return Json(new { success = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            IQueryable<User> collUser = this.UserRepository.GetAll();
            return View(collUser);
        }

        public ActionResult AssignRole(int id)
        {
            User usr = this.UserRepository.GetById(id);
            IQueryable<Group> collGroup = this.GroupRepository.GetAll();
            ViewBag.User = usr;
            ViewBag.UserGroups = usr.Groups;
            return PartialView("AssignRole", collGroup);
        }

        public ActionResult SetUserToGroup(int userid, int groupid, bool ischecked)
        {
            User usr = this.UserRepository.GetById(userid);
            Group grp = this.GroupRepository.GetById(groupid);
            if (usr.Groups == null)
            {
                usr.Groups = new List<Group>();
            }
            if (ischecked)
            {
                if (grp != null && !usr.Groups.Contains(grp))
                {
                    usr.Groups.Add(grp);
                }
            }
            else
            {
                if (grp != null && usr.Groups.Contains(grp))
                {
                    usr.Groups.Remove(grp);
                }
            }
            log.Info(string.Format("User SetUserToGroup : {0}", usr.Username));
            this.UserRepository.SaveOrUpdate(usr);
            return Json(new { success = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(User user)
        {
            ViewBag.ErrMessage = "";
            string username = user.Username;
            if (Utils.VerifyingUserLogin(user.Username, user.Password))
            {
                user.Firstname = "Super Administrator";
                Session.Add("UserLoggedIn", user);
            }

            else
            {
                User usr = this.UserRepository.Get(x => x.Email == username || x.Username == username).FirstOrDefault();// this.UserRepository.GetByUsername(username);
                if (usr != null)
                {
                    string password = user.Password;    // original plaintext
                    string cipherText = usr.Password;

                    string passPhrase = Utils.GetAppSetting("passPhrase");        // can be any string
                    string saltValue = Utils.GetAppSetting("saltValue");             // can be any string
                    string hashAlgorithm = Utils.GetAppSetting("hashAlgorithm");                   // can be "MD5"
                    int passwordIterations = Convert.ToInt32(Utils.GetAppSetting("passwordIterations"));                   // can be any number
                    string initVector = Utils.GetAppSetting("initVector");         // must be 16 bytes
                    int keySize = Convert.ToInt32(Utils.GetAppSetting("keySize"));                 // can be 192 or 128

                    string plainPassword = RijndaelSimple.Decrypt(cipherText,
                                                    passPhrase,
                                                    saltValue,
                                                    hashAlgorithm,
                                                    passwordIterations,
                                                    initVector,
                                                    keySize);

                    if (!string.IsNullOrWhiteSpace(password) && password.Equals(plainPassword))
                    {
                        // True
                        Session.Add("UserLoggedIn", usr);
                        Session.Add("UserGroup", usr.Groups);
                        log.Info(string.Format("User Logged In : {0}", usr.Username));
                    }
                    else
                    {
                        //return Json(new { success = false, message = "password not match" });
                        ViewBag.ErrMessage = "Password tidak sesuai";
                        return View();
                        // false
                    }
                }
                else
                {
                    //return Json(new { success = false, message = "user tidak terdaftar" });
                    ViewBag.ErrMessage = "User tidak terdaftar";
                    return View();
                }
                //if (user.Username == "luliqzx" && user.Password == "luliqzx-ok")
                //{
                //    Session.Add("UserLoggedIn", user);
                //}
                // false
            }

            return Redirect("~/Home/");
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id)
        {
            var user = this.UserRepository.GetById(id);

            return View(user);
        }

        //
        // GET: /User/Create

        private void LoadDepartment()
        {
            IList<Department> collDepartment = this.DepartmentRepository.GetAll().ToList();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            foreach (var item in collDepartment)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
            }
            ViewBag.Departments = collSelectListItem;
        }

        private void LoadBalai()
        {
            IList<BALAI> collBalai = new DefaultRepository<BALAI>().GetAll().ToList();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            foreach (var item in collBalai)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.NamaBalai, Value = item.KodeBalai });
            }
            ViewBag.Balai = collSelectListItem;
        }

        public ActionResult Create()
        {
            //LoadDepartment();
            LoadBalai();
            return PartialView();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User user)
        {
            string txtConfirmPassword = Utils.GetPostRequest("txtConfirmPassword");
            string pwdcheck = string.IsNullOrEmpty(user.Password) ? "" : user.Password;
            try
            {
                if (ModelState.IsValid)
                {
                    if (pwdcheck != txtConfirmPassword)
                    {
                        return Json(new { success = false, message = Pesan.D1B });
                    }

                    bool IsPasswordNull = string.IsNullOrEmpty(user.Password) ? true : false;

                    string plainText = IsPasswordNull ? Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 8) : user.Password;    // original plaintext

                    string passPhrase = Utils.GetAppSetting("passPhrase");        // can be any string
                    string saltValue = Utils.GetAppSetting("saltValue");             // can be any string
                    string hashAlgorithm = Utils.GetAppSetting("hashAlgorithm");                   // can be "MD5"
                    int passwordIterations = Convert.ToInt32(Utils.GetAppSetting("passwordIterations"));                   // can be any number
                    string initVector = Utils.GetAppSetting("initVector");         // must be 16 bytes
                    int keySize = Convert.ToInt32(Utils.GetAppSetting("keySize"));                 // can be 192 or 128

                    string cipherText = RijndaelSimple.Encrypt(plainText,
                                                                passPhrase,
                                                                saltValue,
                                                                hashAlgorithm,
                                                                passwordIterations,
                                                                initVector,
                                                                keySize);


                    //plainText = RijndaelSimple.Decrypt(cipherText,
                    //                                            passPhrase,
                    //                                            saltValue,
                    //                                            hashAlgorithm,
                    //                                            passwordIterations,
                    //                                            initVector,
                    //                                            keySize);

                    user.Password = cipherText;
                    //user.Groups = new List<Group>() { new Group { Id = 2 } };

                    User User = this.UserRepository.Get(x => x.Email == user.Email).FirstOrDefault();
                    if (User != null)
                    {
                        return Json(new { success = false, message = Pesan.D3A });
                    }

                    if (IsCreateable)
                    {
                        log.Info(string.Format("User Save : {0}", user.Username));
                        user.CreateBy = this.UserLoggedIn;
                        user.CreateDate = DateTime.Now;
                        user.CreateTerminal = this.UserHostAddress;
                        user.UpdateBy = this.UserLoggedIn;
                        user.UpdateDate = DateTime.Now;
                        user.UpdateTerminal = this.UserHostAddress;
                        this.UserRepository.SaveOrUpdate(user);
                        if (!string.IsNullOrEmpty(user.Email))
                        {

                            bool IsSendEmail = Convert.ToBoolean(Utils.GetPostRequest("IsSendEmail"));
                            if (IsSendEmail)
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.AppendFormat("Your Login:{0}/{1}\n", user.Username, user.Email);
                                sb.AppendFormat("Your Password:{0}\n", plainText);
                                sb.AppendLine("Please dont reply this email!!!");

                                LNet.Utils.SendMail(user.Email, "Login PDSDA", sb.ToString(), credentialUserName: Utils.Decrypt(Utils.Email), credentialPassword: Utils.Decrypt(Utils.EmailPassword));
                            }
                        }
                    }

                    return Json(new { success = true, message = Pesan.A1A });
                }
            }
            catch (Exception ex)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                return Json(new { success = false, message = ex.StackTrace });

            }
            return PartialView(user);

        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id)
        {
            //LoadDepartment();            
            LoadBalai();
            User usr = this.UserRepository.GetById(id);
            return PartialView(usr);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        //public ActionResult Edit(int id, User usr)
        public JsonResult Edit(int id, User usr)
        {
            bool isSuccess = false;
            string msg = "";
            User user = this.UserRepository.GetById(usr.Id);
            string currentPassword = user.Password;
            Utils.SetTProperty<User>(user, usr);
            string txtConfirmPassword = Utils.GetPostRequest("txtConfirmPassword");
            string txtPassword = Utils.GetPostRequest("txtPassword");

            try
            {
                if (ModelState.IsValid)
                {
                    if (txtPassword != txtConfirmPassword)
                    {
                        return Json(new { success = false, message = Pesan.D1B });
                    }

                    bool IsPasswordNull = string.IsNullOrEmpty(txtPassword) ? true : false;

                    string plainText = IsPasswordNull ? Utils.Decrypt(currentPassword) : txtPassword;    // original plaintext
                    //string plainText = user.Password;    // original plaintext

                    string passPhrase = Utils.GetAppSetting("passPhrase");        // can be any string
                    string saltValue = Utils.GetAppSetting("saltValue");             // can be any string
                    string hashAlgorithm = Utils.GetAppSetting("hashAlgorithm");                   // can be "MD5"
                    int passwordIterations = Convert.ToInt32(Utils.GetAppSetting("passwordIterations"));                   // can be any number
                    string initVector = Utils.GetAppSetting("initVector");         // must be 16 bytes
                    int keySize = Convert.ToInt32(Utils.GetAppSetting("keySize"));                 // can be 192 or 128

                    string cipherText = RijndaelSimple.Encrypt(plainText,
                                                                passPhrase,
                                                                saltValue,
                                                                hashAlgorithm,
                                                                passwordIterations,
                                                                initVector,
                                                                keySize);


                    //plainText = RijndaelSimple.Decrypt(cipherText,
                    //                                            passPhrase,
                    //                                            saltValue,
                    //                                            hashAlgorithm,
                    //                                            passwordIterations,
                    //                                            initVector,
                    //                                            keySize);


                    user.Password = cipherText;
                    if (this.IsUpdateable)
                    {
                        user.UpdateBy = this.UserLoggedIn;
                        user.UpdateDate = DateTime.Now;
                        user.UpdateTerminal = this.UserHostAddress;
                        this.UserRepository.SaveOrUpdate(user);
                        isSuccess = true;
                        msg = "Data has been updated.";

                        bool IsSendEmail = Convert.ToBoolean(Utils.GetPostRequest("IsSendEmail"));
                        if (IsSendEmail)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("Your Login:{0}/{1}\n", user.Username, user.Email);
                            sb.AppendFormat("Your Password:{0}\n", plainText);
                            sb.AppendLine("Please dont reply this email!!!");

                            LNet.Utils.SendMail(user.Email, "Change Password PDSDA", sb.ToString(), credentialUserName: Utils.Decrypt(Utils.Email), credentialPassword: Utils.Decrypt(Utils.EmailPassword));
                        }
                    }

                    //return RedirectToAction("Index");
                }
            }
            catch (DataException de)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                msg = de.Message;
            }
            //return PartialView(usr);
            return Json(new { success = isSuccess, message = msg });

        }

        //
        // GET: /User/Delete/5

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            User usr = this.UserRepository.GetById(id);
            if (IsDeleteable && this.UserLoggedIn != usr.Username)
            {
                this.UserRepository.Delete(usr);
            }

            return Json(new { success = true, message = "User telah dihapus" });
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<User> collDomain = this.UserRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDomain = collDomain.Where(x => x.Firstname.Contains(param.sSearch) || x.Lastname.Contains(param.sSearch)
                    || x.Username.Contains(param.sSearch) || (x.BirthDate != null && x.BirthDate.ToString().Contains(param.sSearch))
                    || x.Groups.FirstOrDefault(p => p.Name.Contains(param.sSearch)) != null);
            }
            Count = collDomain.Count();

            var sortColumnIndex = iSortCol;
            Func<User, string> orderingFunction = (c => sortColumnIndex == 1 ? c.Firstname :
                                                                sortColumnIndex == 2 ? c.Username :
                                                                sortColumnIndex == 3 ? c.Groups == null ? "" : c.Groups.FirstOrDefault().Name :
                                                                sortColumnIndex == 4 ? c.BirthDate == null ? "" : c.BirthDate.ToString() :
                                                                c.Username);

            IList<User> collResult = new List<User>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }

            IList<UserDTO> collUserDTO = new List<UserDTO>();

            if (collResult != null)
            {

                collResult.ToList().ForEach(delegate(User usr)
                {
                    UserDTO UserDTO = new UserDTO();
                    UserDTO.BirthDate = usr.BirthDate;
                    UserDTO.Id = usr.Id;
                    UserDTO.Firstname = usr.Firstname;
                    UserDTO.Lastname = usr.Lastname;
                    UserDTO.Username = usr.Username;

                    if (usr.Groups != null)
                    {
                        usr.Groups.ToList().ForEach(delegate(Group grp)
                        {
                            if (grp != null)
                            {
                                if (string.IsNullOrEmpty(UserDTO.Groups))
                                {
                                    UserDTO.Groups = grp.Name;
                                }
                                else
                                {
                                    UserDTO.Groups = UserDTO.Groups + ", " + grp.Name;
                                }
                            }
                        });
                    }

                    collUserDTO.Add(UserDTO);
                });
            }


            var result = from c in collUserDTO
                         select new
                         {
                             KEY = c.Id,
                             NAME = c.Firstname + " " + c.Lastname,
                             c.Username,
                             c.Groups,
                             BirthDate = c.BirthDate == null ? "" : c.BirthDate.Value.ToString("dd-MMM-yyyy")
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangePassword()
        {
            if (string.IsNullOrWhiteSpace(this.UserLoggedIn))
            {
                return GenericView("~/Views/Shared/NotPermit");
            }
            User usr = this.UserRepository.Get(x => x.Username == this.UserLoggedIn || x.Email == this.UserLoggedIn).FirstOrDefault();
            ViewBag.Nama = usr.Firstname;
            ViewBag.Email = usr.Email;
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(FormCollection collection)
        {
            if (!string.IsNullOrEmpty(this.UserLoggedIn) && !this.UserLoggedIn.Contains("Guest"))
            {


                bool IsError = false;
                string message = "";
                User usr = this.UserRepository.Get(x => x.Username == this.UserLoggedIn || x.Email == this.UserLoggedIn).FirstOrDefault();

                if ((!string.IsNullOrEmpty(collection["newPassword"]) || !string.IsNullOrEmpty(collection["ConfirmNewPassword"])) && !string.IsNullOrEmpty(collection["currentPassword"]))
                {
                    if (collection["newPassword"].Equals(collection["ConfirmNewPassword"]))
                    {
                        try
                        {
                            // TODO: Add delete logic here
                            string plainPassword = Utils.Decrypt(usr.Password);
                            if (plainPassword.Equals(collection["currentPassword"]))
                            {
                                usr.Password = Utils.Encrypt(collection["newPassword"]);
                                //return View("ChangePassword", "Password Telah Diubah.");
                                //return View("ChangePassword");
                                message = "Data Telah Diubah.";
                                //Response.Redirect(String.Format("~/User/{0}/?message={1}", "ChangePassword", "Password Telah Diubah."), true);
                            }
                            else
                            {
                                IsError = true;
                                message = "Terjadi Kesalahan. Periksa password Anda.";
                                //Response.Redirect(String.Format("~/User/{0}/?message={1}", "ChangePassword", "Terjadi Kesalahan. Periksa password Anda."), true);
                            }
                        }
                        catch
                        {
                            IsError = true;
                            message = "Terjadi Kesalahan. Periksa password Anda.";
                            //return View("ChangePassword", "Terjadi Kesalahan. Periksa password Anda.");
                            //Response.Redirect(String.Format("~/User/{0}/?message={1}", "ChangePassword", "Terjadi Kesalahan. Periksa password Anda."), true);
                        }
                    }

                    else
                    {
                        IsError = true;
                        message = "Periksa Password Baru dan Konfirmasi Password Baru.";
                        //return View("ChangePassword", "Password Telah Diubah.");
                        //Response.Redirect(String.Format("~/User/{0}/?message={1}", "ChangePassword", "Periksa Password Baru dan Konfirmasi Password Baru."), true);
                    }
                }

                if (IsError)
                {
                    Response.Redirect(String.Format("~/User/{0}/?message={1}", "ChangePassword", message), true);
                }
                else
                {
                    usr.Firstname = collection["txtNama"];
                    usr.Email = collection["txtEmail"];
                    UserRepository.Update(usr);
                    Response.Redirect(String.Format("~/User/{0}/?message={1}", "ChangePassword", message), true);
                }

                return View();
            }
            else
            {
                return this.GenericViewDefault();
            }
        }

    }
}
