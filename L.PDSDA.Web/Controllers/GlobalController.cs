﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace L.PDSDA.Web.Controllers
{
    public class GlobalController : Controller
    {
        //
        // GET: /Global/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NotPermit(string url)
        {
            return View("NotPermit", url);
        }

        public ActionResult NotFound()
        {
            return View("NotFound");
        }

    }
}
