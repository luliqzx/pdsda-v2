﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using L.PDSDA.Web.Helpers;
//using SharpMap.Demo.Wms.Helpers;
//using SharpMap;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories.Base;

namespace L.PDSDA.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            //IDefaultRepository<SUNGAI> sungaiRepo = new DefaultRepository<SUNGAI>();
            ViewBag.Message = "Welcome to ASP.NET MVC!";
            //SampleMap.SimpleMap();
            //Map map = MapHelper.PolyMaps();
            //return View();

            //SUNGAI sungai = sungaiRepo.GetAll().FirstOrDefault();
            //TANGGUL tanggul = new TANGGUL();
            //tanggul.SUNGAI = sungai;
            //tanggul.URUT_TANGGUL = 1;
            //sungai.TANGGULs.Add(tanggul);
            //sungaiRepo.SaveOrUpdate(sungai);

            return GenericView("~/Areas/Peta/Views/GenericMap/HomeIndex2");

        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult GeoJSON()
        {
            return this.View();
        }

        public ActionResult HomeIndex()
        {
            return GenericView("~/Areas/Peta/Views/GenericMap/HomeIndex");
        }
    }
}
