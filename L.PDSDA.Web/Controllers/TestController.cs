﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using L.PDSDA.Web.Common;
using System.IO;

namespace L.PDSDA.Web.Controllers
{
    public class TestController : DefaultBaseController
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void Upload()
        {

            string path = Server.MapPath(@"~/uploads/");
            string filepath = Path.Combine(path, Request.Files[0].FileName);
            Request.Files[0].SaveAs(filepath);

            DataSet ds = Utils.ReadExcelAsDataSet(filepath, false);

        }

        public ActionResult vpIFrameIndex()
        {
            return View("vpIFrame");
        }

    }
}
