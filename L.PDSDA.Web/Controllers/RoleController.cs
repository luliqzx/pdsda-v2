﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories;
using L.PDSDA.DAL.Repositories.Implements;
using L.PDSDA.DAL.Entities;
using System.Data;
using L.Core.Utilities;
using L.Core.Utilities.Web;
using L.PDSDA.DAL.Repositories.Base;

namespace L.PDSDA.Web.Controllers
{
    public class RoleController : BaseController
    {
        //
        // GET: /Role/

        public ActionResult Index()
        {
            IQueryable<Group> collGroup = this.GroupRepository.GetAll();

            return View(collGroup);
        }

        private readonly IGroupRepository GroupRepository;
        private readonly IModuleRepository ModuleRepository;
        private readonly IDefaultRepository<BALAI> BALAIRepository;
        public RoleController()
        {
            this.ModuleRepository = new ModuleRepository();
            this.GroupRepository = new GroupRepository();
            this.GroupModuleRepository = new GroupModuleRepository();
            BALAIRepository = new DefaultRepository<BALAI>();
        }

        public ActionResult Seed()
        {
            var Contributor = new Group { Name = "Contributor" };
            var Viewer = new Group { Name = "Viewer" };
            var Operator = new Group { Name = "Operator" };
            return RedirectToAction("Index");
        }

        //
        // GET: /Role/Details/5

        public ActionResult Details(int id)
        {
            Group grp = this.GroupRepository.GetById(id);
            IList<GroupModule> collGroupModule = grp.GroupModules;
            ViewBag.Group = grp;
            ViewBag.collGroupModule = collGroupModule;
            IQueryable<Module> collModule = new ModuleRepository().GetAll();
            return PartialView("AssignModule", collModule);
        }

        private readonly IGroupModuleRepository GroupModuleRepository;

        [HttpPost]
        public ActionResult SetRoleToModule(string group, string module, string classes, bool ischecked)
        {

            int groupid = Convert.ToInt32(group);
            int moduleid = Convert.ToInt32(module);
            string permition = classes;
            bool assign = ischecked;
            Group grp = this.GroupRepository.GetById(groupid);
            Module mod = this.ModuleRepository.GetById(moduleid);

            if (grp.GroupModules != null)
            {
                GroupModule GroupModule = grp.GroupModules.FirstOrDefault(x => x.Module == mod);
                if (GroupModule != null)
                {
                    if (classes.Contains("viewable") && !ischecked)
                    {
                        grp.GroupModules.Remove(GroupModule);
                        this.GroupModuleRepository.Delete(GroupModule);
                    }
                    else
                        this.SetGroupModule(GroupModule, permition, assign);
                }
                else
                {
                    GroupModule = new GroupModule();
                    GroupModule.Module = mod;
                    GroupModule.Group = grp;
                    this.SetGroupModule(GroupModule, permition, assign);
                    grp.GroupModules.Add(GroupModule);
                }
            }
            else
            {
                GroupModule GroupModule = new GroupModule();
                GroupModule.Module = mod;
                GroupModule.Group = grp;
                this.SetGroupModule(GroupModule, permition, assign);
                grp.GroupModules.Add(GroupModule);
            }

            try
            {
                this.GroupRepository.SaveOrUpdate(grp);
            }
            catch (Exception ex)
            {

                return Json(new { IsSuccess = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSuccess = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }

        private void SetGroupModule(GroupModule GroupModule, string permition, bool assign)
        {
            if (permition.Equals("viewable"))
            {
                GroupModule.IsViewable = assign;
            }
            else if (permition.Equals("createable"))
            {
                GroupModule.IsCreateable = assign;
            }
            else if (permition.Equals("updateable"))
            {
                GroupModule.IsUpdateable = assign;
            }
            else if (permition.Equals("deleteable"))
            {
                GroupModule.IsDeleteable = assign;
            }
            //throw new NotImplementedException();
        }
        //
        // GET: /Role/Create

        public ActionResult Create()
        {
            return PartialView();
        }

        //
        // POST: /Role/Create

        [HttpPost]
        public ActionResult Create(Group Group)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Group.CreateBy = this.UserLoggedIn;
                    Group.CreateDate = DateTime.Now;
                    Group.CreateTerminal = this.UserHostAddress;
                    Group.UpdateBy = this.UserLoggedIn;
                    Group.UpdateDate = DateTime.Now;
                    Group.UpdateTerminal = this.UserHostAddress;
                    this.GroupRepository.SaveOrUpdate(Group);
                    //return RedirectToAction("Index");
                    return Json(new { success = true, message = "Data has been created." });
                }
                else
                {
                    return Json(new { success = false, message = "Data failed create." });
                }
            }
            catch (Exception ex)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Role/Edit/5

        public ActionResult Edit(int id)
        {
            Group grp = this.GroupRepository.GetById(id);
            return PartialView(grp);
        }

        //
        // POST: /Role/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Group grp)
        {
            Group Group = this.GroupRepository.GetById(grp.Id);
            Utils.SetTProperty<Group>(Group, grp);

            try
            {
                if (ModelState.IsValid)
                {
                    Group.UpdateBy = this.UserLoggedIn;
                    Group.UpdateDate = DateTime.Now;
                    Group.UpdateTerminal = this.UserHostAddress;
                    this.GroupRepository.SaveOrUpdate(Group);
                    //return RedirectToAction("Index");
                    return Json(new { success = true, message = "Data has been edit." });
                }
                else
                {
                    return Json(new { success = false, message = "Data failed edit." });
                }
            }
            catch (Exception ex)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                return Json(new { success = false, message = ex.StackTrace });
            }
        }

        //
        // GET: /Role/Delete/5

        public ActionResult Delete(int id)
        {
            Group grp = this.GroupRepository.GetById(id);
            if (grp != null)
            {
                this.GroupRepository.Delete(grp);
                return Json(new { success = true, message = "OK" });
            }
            return Json(new { success = false, message = "ERROR" });

        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<Group> collDomain = this.GroupRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDomain = collDomain.Where(x => x.Name.Contains(param.sSearch) || x.Code.Contains(param.sSearch) || x.Description.Contains(param.sSearch));
            }
            Count = collDomain.Count();

            var sortColumnIndex = iSortCol;
            Func<Group, string> orderingFunction = (c => sortColumnIndex == 1 ? c.Name :
                                                                sortColumnIndex == 2 ? c.Code :
                                                                sortColumnIndex == 3 ? c.Description :
                                                                c.Name);

            IList<Group> collResult = new List<Group>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.Id,
                             c.Name,
                             c.Code,
                             c.Description
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignBalaiRole(int id)
        {
            Group group = this.GroupRepository.Get(x => x.Id == id).FirstOrDefault();
            IQueryable<BALAI> collBalai = this.BALAIRepository.GetAll();
            ViewBag.Group = group;
            ViewBag.BalaiRole = group.BALAIs;
            return PartialView("AssignBalaiRole", collBalai);
        }

        public ActionResult SetBalaiToRole(string balai, string group, bool ischecked)
        {
            string KodeBalai = balai;
            int Group_Id = Convert.ToInt32(group);
            bool assign = ischecked;
            BALAI Balai = this.BALAIRepository.Get(x => x.KodeBalai == KodeBalai).SingleOrDefault();
            Group Group = this.GroupRepository.Get(x => x.Id == Group_Id).SingleOrDefault();

            if (Group.BALAIs != null)
            {
                if (Group.BALAIs.Contains(Balai))
                {
                    if (!ischecked)
                        Group.BALAIs.Remove(Balai);
                    else
                        Group.BALAIs.Add(Balai);
                }
                else
                {
                    Group.BALAIs.Add(Balai);
                }
            }
            else
            {
                Group.BALAIs = new List<BALAI>();
                Group.BALAIs.Add(Balai);
            }

            try
            {
                this.GroupRepository.SaveOrUpdate(Group);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSuccess = true, message = "OK" }, JsonRequestBehavior.AllowGet);
        }
    }
}
