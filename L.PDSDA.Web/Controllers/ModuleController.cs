﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Entities;
using L.PDSDA.DAL.Repositories;
using L.PDSDA.DAL.Repositories.Implements;
using System.Data;
using L.Core.Utilities;
using L.Core.Utilities.Web;

namespace L.PDSDA.Web.Controllers
{
    public class ModuleController : BaseController
    {
        //
        // GET: /Module/

        private readonly IModuleRepository ModuleRepository;
        public ModuleController()
        {
            this.ModuleRepository = new ModuleRepository();
        }

        public ActionResult Index()
        {
            IQueryable<Module> collModule = this.ModuleRepository.GetAll();
            return View(collModule);
        }

        public ActionResult Cheat(int id)
        {
            if (id == 123456)
            {
                ViewBag.IsCreateable = true;
                ViewBag.IsUpdateable = true;
                ViewBag.IsDeleteable = true;
            }
            IQueryable<Module> collModule = this.ModuleRepository.GetAll();
            return View("Index", collModule);
        }

        //
        // GET: /Module/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Module/Create

        public ActionResult Create()
        {
            IList<Module> collModule = this.ModuleRepository.GetAll().ToList();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collModule)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
            }
            ViewBag.Modules = collSelectListItem;
            return PartialView();
        }

        //
        // POST: /Module/Create

        [HttpPost]
        public ActionResult Create(Module module)
        {
            if (module.MainModule != null && module.MainModule.Id < 0)
            {
                module.MainModule = null;
            }
            //try
            //{
            //    // TODO: Add insert logic here

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}

            try
            {
                if (ModelState.IsValid)
                {
                    module.CreateBy = this.UserLoggedIn;
                    module.CreateDate = DateTime.Now;
                    module.CreateTerminal = this.UserHostAddress;
                    module.UpdateBy = this.UserLoggedIn;
                    module.UpdateDate = DateTime.Now;
                    module.UpdateTerminal = this.UserHostAddress;
                    this.ModuleRepository.SaveOrUpdate(module);
                    //return RedirectToAction("Index");
                    return Json(new { success = true, message = "Data '" + module.Name + "' has been created!." });
                }
            }
            catch (Exception ex)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                return Json(new { success = false, message = ex.Message });
            }

            return Json(new { success = false });
            //return PartialView(module);

        }

        //
        // GET: /Module/Edit/5

        public ActionResult Edit(int id)
        {
            IList<Module> collModule = this.ModuleRepository.GetAll().ToList();
            IList<SelectListItem> collSelectListItem = new List<SelectListItem>();
            collSelectListItem.Add(new SelectListItem { Text = "", Value = "-1" });
            foreach (var item in collModule)
            {
                collSelectListItem.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
            }
            ViewBag.collItem = collSelectListItem;

            Module module = collModule.FirstOrDefault(x => x.Id == id);
            return PartialView(module);// View();
        }

        //
        // POST: /Module/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Module module)
        {
            Module mod = this.ModuleRepository.GetById(id);

            if (module.MainModule != null && module.MainModule.Id > 0)
            {
                mod.MainModule = this.ModuleRepository.GetById(module.MainModule.Id);
            }
            else
            {
                mod.MainModule = null;
            }

            Utils.SetTProperty<Module>(mod, module);

            try
            {
                if (ModelState.IsValid)
                {
                    module.UpdateBy = this.UserLoggedIn;
                    module.UpdateDate = DateTime.Now;
                    module.UpdateTerminal = this.UserHostAddress;
                    this.ModuleRepository.SaveOrUpdate(mod);
                    //return RedirectToAction("Index");
                    return Json(new { success = true, message = "Data telah diubah" });
                }
            }
            catch (Exception ex)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                return Json(new { success = false, message = ex.StackTrace });
            }
            return Json(new { success = false, message = "UNDEFINED" });

        }

        //
        // GET: /Module/Delete/5
        public ActionResult Delete(int id)
        {
            Module module = this.ModuleRepository.GetById(id);
            module.GroupModules.Clear();
            module.ChildModules.Clear();

            bool isSuccess = false;
            string msg = "";

            try
            {
                isSuccess = true;
                msg = "Data has been deleted.";

                this.ModuleRepository.Delete(module);
            }
            catch (Exception ex)
            {

                msg = ex.Message;
            }
            return Json(new { success = isSuccess, message = msg }, JsonRequestBehavior.AllowGet);
        }

        protected Int32 iSortCol
        {
            get { return Convert.ToInt32(Utils.GetPostRequest("iSortCol_0")); }
        }

        protected string iSortDir
        {
            get { return Utils.GetPostRequest("sSortDir_0"); }
        }

        public ActionResult LoadAllData(jQueryDataTableParamModel param)
        {
            int Count = 0;
            IQueryable<Module> collDomain = this.ModuleRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                collDomain = collDomain.Where(x => x.Name.Contains(param.sSearch) || x.Code.Contains(param.sSearch) || x.Description.Contains(param.sSearch) || (x.MainModule != null && x.MainModule.Name.Contains(param.sSearch)));
            }
            Count = collDomain.Count();

            var sortColumnIndex = iSortCol;
            Func<Module, string> orderingFunction = (c => sortColumnIndex == 1 ? c.Code :
                                                                sortColumnIndex == 2 ? c.Name :
                                                                sortColumnIndex == 3 ? c.MainModule == null ? "" : c.MainModule.Name :
                                                                sortColumnIndex == 4 ? c.Path :
                                                                sortColumnIndex == 5 ? c.Position.ToString() :
                                                                c.Name);

            IList<Module> collResult = new List<Module>();// = collRawaDyn == null ? new List<RAWA_DYN>() : collRawaDyn.ToList();
            if (collDomain.Count() > 0)
            {
                collResult = collDomain.ToList();
                var sortDirection = iSortDir; // asc or desc
                if (sortDirection == "asc")
                    collResult = collResult.OrderBy(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                else
                    collResult = collResult.OrderByDescending(orderingFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            }


            var result = from c in collResult
                         select new
                         {
                             KEY = c.Id,
                             c.Code,
                             c.Name,
                             Parent = c.MainModule == null ? "" : c.MainModule.Name,
                             c.Path,
                             c.Position,
                             Public = c.IsPublic ? "Ya" : "Tidak",
                             Active = c.IsActive ? "Ya" : "Tidak"
                         };

            int iTotalDisplayRecords = result.Skip(param.iDisplayStart).Take(param.iDisplayLength).Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalDisplayRecords,
                iTotalDisplayRecords = Count,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
