﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace L.PDSDA.Web.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {
            return View();
        }


        // GET: /Error/HttpError404
        public ActionResult HttpError404(string message)
        {
            object msg = message;
            return View("HttpError404", msg);
        }

        public ActionResult HttpError500(string message)
        {
            object msg = message;
            return View("HttpError500", msg);
        }

        public ActionResult ErrorDefault(string message)
        {
            object msg = message;
            return View("ErrorDefault", msg);
        }

    }
}
