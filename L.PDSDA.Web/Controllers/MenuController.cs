﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using L.PDSDA.DAL.Repositories;
using L.PDSDA.DAL.Repositories.Implements;
using L.PDSDA.DAL.Entities;
using L.Core.Utilities;

namespace L.PDSDA.Web.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/

        private readonly IModuleRepository ModuleRepository;
        private readonly IUserRepository UserRepository;
        public MenuController()
        {
            ModuleRepository = new ModuleRepository();
            this.UserRepository = new UserRepository();
        }

        public ActionResult Index()
        {
            User usr = this.Session["UserLoggedIn"] as User;
            IList<Module> Modules = new List<Module>();
            if (usr != null)
            {
                //if (usr.Username == "luliqzx" && usr.Password == "luliqzx-ok")
                //{
                //    Modules = this.ModuleRepository.GetAll().ToList();
                //}
                if (Utils.VerifyingUserLogin(usr.Username, usr.Password))
                {
                    Modules = this.ModuleRepository.GetAll().ToList();
                }
                else
                {

                    User user = UserRepository.GetById(usr.Id);
                    IList<Group> collGroup = user.Groups;
                    //IList<Group> collGroup = usr.Groups;
                    this.BuildDefaultMenu(Modules);
                    foreach (Group grp in collGroup)
                    {
                        if (grp != null)
                        {
                            if (grp.GroupModules != null && grp.GroupModules.Count > 0)
                            {
                                foreach (GroupModule gm in grp.GroupModules)
                                {
                                    if (!Modules.Contains(gm.Module) && gm.IsViewable)
                                    {
                                        if (/*grp.Code.Equals("ADM") || */gm.Module != null && gm.Module.IsActive)
                                        {
                                            Modules.Add(gm.Module);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //IQueryable<Module> collModule = this.ModuleRepository.GetAll();
                //ViewBag.Modules = collModule;
                //return PartialView("Index", collModule);
                //ViewBag.Modules = Modules;

                Modules = Modules.Where(x => x != null).OrderBy(x => x.Position).ToList();

                return PartialView("Index", Modules.AsQueryable());
            }
            else
            {
                BuildDefaultMenu(Modules);
                return PartialView("Index", Modules.AsQueryable());
            }
        }

        private void BuildDefaultMenu(IList<Module> Modules)
        {
            IList<Module> collModulePublic = this.ModuleRepository.GetAllByPublicViewable();
            foreach (Module module in collModulePublic.OrderBy(x => x.Position))
            {
                if (module.IsActive)
                {
                    Modules.Add(module);
                }
            }
        }

    }
}
