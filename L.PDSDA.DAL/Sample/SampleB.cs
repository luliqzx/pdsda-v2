﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Sample
{
    public class SampleB
    {
        public virtual int ID { get; set; }
        public virtual IList<SampleA> SampleAs { get; set; }
    }
}
