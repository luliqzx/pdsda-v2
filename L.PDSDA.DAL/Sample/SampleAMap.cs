﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Sample
{
    public class SampleAMap : ClassMap<SampleA>
    {
        public SampleAMap()
        {
            this.Id(x => x.ID);
            this.References(x => x.SampleB);
            this.Map(x => x.A);
        }
    }
}
