﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Sample
{
    public class SampleBMap : ClassMap<SampleB>
    {
        public SampleBMap()
        {
            this.Id(x => x.ID);
            this.HasMany(x => x.SampleAs).Cascade.All().Inverse();
        }
    }
}
