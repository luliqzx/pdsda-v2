﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Sample
{
    public class SampleA
    {
        public virtual int ID { get; set; }
        public virtual string A { get; set; }
        public virtual SampleB SampleB { get; set; }
    }
}
