﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.Core.Repositories;
using NHibernate;

namespace L.PDSDA.DAL.Repositories.Base
{
    public class DefaultRepository<T> : BaseRepositoryFactories<T>, IDefaultRepository<T>
    {
        #region IDefaultRepository<T> Members

        public T GetById(int Id)
        {
            return this.session.Get<T>(Id);
        }

        #endregion

        #region IDefaultRepository<T> Members


        public T GetById(string Id)
        {
            return this.session.Get<T>(Id);
        }

        #endregion


        public ICriteria GetAllWithCriteria()
        {
            return this.session.CreateCriteria(typeof(T));
        }
    }
}
