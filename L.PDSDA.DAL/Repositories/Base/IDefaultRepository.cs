﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using L.PDSDA.DAL.Entities;
using L.Core.Repositories;
using NHibernate;
namespace L.PDSDA.DAL.Repositories.Base
{
    public interface IDefaultRepository<T> : IBaseRepositoryFactories<T>
    {
        T GetById(int Id);
        T GetById(string Id);

        ICriteria GetAllWithCriteria();
    }
}
