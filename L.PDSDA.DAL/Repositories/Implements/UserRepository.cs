﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using L.Core.Repositories;
using NHibernate.Criterion;

namespace L.PDSDA.DAL.Repositories.Implements
{
    public class UserRepository : BaseRepositoryFactories<User>, IUserRepository
    {
        #region IUserRepository Members

        public User GetById(int Id)
        {
            var user = session.CreateCriteria<User>()
                    .Add(Restrictions.IdEq(Id))
                    .UniqueResult<User>();

            return user;// this.session.Get<User>(Id);
        }

        #endregion

        #region IUserRepository Members


        public User GetByUsername(string username)
        {
            var user = session.CreateCriteria<User>()
                    .Add(Restrictions.Eq("Username", username))
                    .UniqueResult<User>();

            return user;// this.session.Get<User>(Id);
        }

        #endregion
    }
}
