﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using L.Core.Repositories;
using NHibernate.Criterion;

namespace L.PDSDA.DAL.Repositories.Implements
{
    public class ModuleRepository : BaseRepositoryFactories<Module>, IModuleRepository
    {
        #region IModuleRepository Members

        public Module GetById(int Id)
        {
            var module = session.CreateCriteria<Module>().Add(Restrictions.IdEq(Id)).UniqueResult<Module>();
            return module;
        }

        #endregion

        #region IModuleRepository Members


        public Module GetByPath(string url)
        {
            var module = session.CreateCriteria<Module>().Add(Restrictions.Like("Path", url)).UniqueResult<Module>();
            return module;
        }

        public Module GetByUrlRelativePath(string url)
        {
            var module = this.GetAll().Where(x => url.Contains(x.Path) || x.Path.Contains(url)).FirstOrDefault();
            return module;
        }

        #endregion

        #region IModuleRepository Members


        public IList<Module> GetAllByPublicViewable()
        {
            var module = session.CreateCriteria<Module>().Add(Restrictions.Eq("IsPublic", true)).List<Module>();
            return module;
        }

        #endregion
    }
}
