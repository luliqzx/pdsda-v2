﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using L.Core.Repositories;
using NHibernate.Criterion;

namespace L.PDSDA.DAL.Repositories.Implements
{
    public class GroupRepository : BaseRepositoryFactories<Group>, IGroupRepository
    {
        #region IGroupRepository Members

        public Group GetById(int Id)
        {
            var Group = session.CreateCriteria<Group>().Add(Restrictions.IdEq(Id)).UniqueResult<Group>();
            return Group;
        }

        #endregion
    }
}
