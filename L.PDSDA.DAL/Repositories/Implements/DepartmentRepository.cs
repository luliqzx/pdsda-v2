﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using L.Core.Repositories;

namespace L.PDSDA.DAL.Repositories.Implements
{
    public class DepartmentRepository : BaseRepositoryFactories<Department>, IDepartmentRepository
    {
    }
}
