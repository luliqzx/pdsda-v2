﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using L.Core.Repositories;

namespace L.PDSDA.DAL.Repositories
{
    public interface IModuleRepository : IBaseRepositoryFactories<Module>
    {
        Module GetById(int Id);


        Module GetByPath(string url);
        Module GetByUrlRelativePath(string url);
        
        IList<Module> GetAllByPublicViewable();
    }
}
