﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using L.Core.Repositories;
using L.Core.Utilities.Web;

namespace L.PDSDA.DAL.Repositories.Data.SumberDayaAir.Implements
{
    using NHibernate.Linq;
    public class SungaiRepository : BaseRepositoryFactories<SUNGAI>, ISungaiRepository
    {
        public double GetMaxUrut()
        {
            if (this.Session.Query<SUNGAI>().Count() > 0)
            {
                double currentMaxUrut = this.Session.Query<SUNGAI>().ToList().Max(x => x.URUT);
                return currentMaxUrut + 1;
            }
            return 1;
        }
    }
}
