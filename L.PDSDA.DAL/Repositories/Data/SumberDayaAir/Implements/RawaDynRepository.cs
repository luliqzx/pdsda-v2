﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using L.Core.Repositories;
using L.Core.Utilities.Web;

namespace L.PDSDA.DAL.Repositories.Data.SumberDayaAir.Implements
{
    public class RawaDynRepository : BaseRepositoryFactories<RAWA_DYN>, IRawaDynRepository
    {
        public IList<RAWA_DYN> GetList(jQueryDataTableParamModel param, out int TotalRecord)
        {
            IQueryable<RAWA_DYN> collRAWA_DYN = this.GetAll();
            TotalRecord = collRAWA_DYN.Count();

            if (TotalRecord > 0)
            {
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    collRAWA_DYN = collRAWA_DYN.Where(c => c.TAHUN_DATA.Equals(Convert.ToInt32(param.sSearch))
                            || (c.RAWA != null && c.RAWA.N_RAWA.Equals(param.sSearch))
                        ).Skip(param.iDisplayStart).Take(param.iDisplayLength);
                }
            }

            return collRAWA_DYN.ToList();


            //IEnumerable<Company> filteredCompanies = allCompanies;

            //var displayedCompanies = filteredCompanies
            //                    .Skip(param.iDisplayStart)
            //                    .Take(param.iDisplayLength);

            //var result = from c in displayedCompanies
            //             select new[] { Convert.ToString(c.ID), c.Name,
            //              c.Address, c.Town };
            //return Json(new
            //{
            //    sEcho = param.sEcho,
            //    iTotalRecords = allCompanies.Count(),
            //    iTotalDisplayRecords = filteredCompanies.Count(),
            //    aaData = result
            //},
            //                    JsonRequestBehavior.AllowGet);
        }

        public override RAWA_DYN SaveOrUpdate(RAWA_DYN entity)
        {
            this.Session.SaveOrUpdate(entity);
            this.Session.Flush();
            return entity;
        }

        public override RAWA_DYN Save(RAWA_DYN entity)
        {
            this.Session.Save(entity);
            this.Session.Flush();
            return entity;
        }


    }
}
