﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using L.Core.Repositories;
using L.Core.Utilities.Web;

namespace L.PDSDA.DAL.Repositories.Data.SumberDayaAir
{
    public interface IRawaDynRepository : IBaseRepositoryFactories<RAWA_DYN>
    {
        IList<RAWA_DYN> GetList(jQueryDataTableParamModel param, out int TotalRecord);
    }
}
