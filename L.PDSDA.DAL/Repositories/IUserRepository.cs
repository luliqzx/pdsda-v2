﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Repositories
{
    using L.PDSDA.DAL.Entities;
    using L.Core.Repositories;
    public interface IUserRepository:IBaseRepositoryFactories<User>
    {
        User GetById(int Id);

        User GetByUsername(string username);
    }
}
