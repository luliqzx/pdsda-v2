﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.Collections;
using NHibernate.Type;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.DAL.Interceptors.SDA
{
    [Serializable]
    public class DanauInterceptor : EmptyInterceptor
    {
        public override bool? IsTransient(object entity)
        {
            if (entity is DANAU)
            {
                return !((DANAU)entity).IsSaved;
            }
            else
            {
                return null;
            }
        }

        public override bool OnLoad(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            if (entity is DANAU) ((DANAU)entity).OnLoad();
            return false;
        }

        public override bool OnSave(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            if (entity is DANAU) ((DANAU)entity).OnSave();
            return false;
        }
    }
}
