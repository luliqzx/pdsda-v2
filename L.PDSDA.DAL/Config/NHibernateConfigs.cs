﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.Core.Modules;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate;
using FluentNHibernate.Cfg;
using NHibernate.Context;
using System.Reflection;

namespace L.PDSDA.DAL.Config
{
    public class NHibernateConfigs : NHibernateSessionFactoriesPerRequest
    {
        protected override IDictionary<string, string> connectionStrings()
        {
            throw new NotImplementedException();
        }

        private static void UpdateSchema(Configuration cfg)
        {
            new SchemaUpdate(cfg)
                .Execute(false, true);
        }


        protected override ISessionFactory CreateSessionFactory()
        {
            try
            {
                return Fluently.Configure()
                    //.Database(CreateMySqlDbConfig)
                    //.Database(CreateDbConfig)
                    //.Mappings(m => m.AutoMappings.Add(CreateMappings()))
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("L.PDSDA.DAL")))
                .ExposeConfiguration(UpdateSchema)
                .CurrentSessionContext<WebSessionContext>()
                .BuildSessionFactory();
            }
            catch (Exception ex)
            {
                IsError = true;
                ErrorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return null;
            }

        }
    }
}
