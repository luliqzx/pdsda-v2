﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Config
{
    using L.Core;
    using L.Core.Modules;
    using FluentNHibernate.Automapping;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using FluentNHibernate.Conventions.Helpers;
    using NHibernate;
    using NHibernate.Cfg;
    using NHibernate.Context;
    using NHibernate.Tool.hbm2ddl;
    using FluentNHibernate;
    using L.PDSDA.DAL.Maps;
    using System.Reflection;
    using FluentNHibernate.Conventions;
    using FluentNHibernate.Conventions.Instances;
    using L.Core.Utilities.Enums;
    using L.PDSDA.DAL.Common;

    public class NHibernateConfig : NHibernateSessionFactoriesPerRequest
    {
        // Returns our session factory
        //protected override ISessionFactory CreateSessionFactory()
        //{
        //    try
        //    {
        //        return Fluently.Configure()
        //            .Database(CreateMySqlDbConfig)
        //            //.Database(CreateDbConfig)
        //            //.Mappings(m => m.AutoMappings.Add(CreateMappings()))
        //        .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("L.PDSDA.DAL")))
        //        .ExposeConfiguration(UpdateSchema)
        //        .CurrentSessionContext<WebSessionContext>()
        //        .BuildSessionFactory();
        //    }
        //    catch (Exception ex)
        //    {
        //        IsError = true;
        //        ErrorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
        //        return null;
        //    }

        //}

        private static MySQLConfiguration CreateMySqlDbConfig()
        {
            return MySQLConfiguration
                .Standard
                .ConnectionString(x => x.FromConnectionStringWithKey("mysqlConn"));
        }

        // Returns our database configuration
        private static MsSqlConfiguration CreateDbConfig()
        {
            return MsSqlConfiguration
                .MsSql2008
                .ConnectionString(c => c.FromConnectionStringWithKey("testConn"));
        }


        // Returns our mappings
        private static AutoPersistenceModel CreateMappings()
        {
            return AutoMap
                .Assembly(System.Reflection.Assembly.GetCallingAssembly())
                .Where(t => t.Namespace != null && t.Namespace.EndsWith("Entities"))
                .Conventions.Setup(c =>
                {
                    c.Add(DefaultCascade.All());
                    //c.Add<PrimaryKeyConvention>();
                    //c.Add<ForeignKeyConvention>();
                });


        }

        public class PrimaryKeyConvention : IIdConvention
        {
            public void Apply(IIdentityInstance instance)
            {
                instance.Column("K_" + instance.EntityType.Name);
            }
        }

        public class CustomForeignKeyConvention : ForeignKeyConvention
        {


            protected override string GetKeyName(Member property, Type type)
            {
                if (property == null)
                    return "K_" + type.Name;

                return "K_" + property.Name;
            }
        }

        // Updates the database schema if there are any changes to the model,
        // or drops and creates it if it doesn't exist
        private static void UpdateSchema(Configuration cfg)
        {
            new SchemaUpdate(cfg)
                .Execute(false, true);
        }

        public override ISessionFactory CreateSessionFactory(DefaultDB DefaultDB = DefaultDB.SQLServer, RunIn RunIn = RunIn.Web, bool IsUpdateSchema = false, string OtherDBs = "")
        {
            FluentConfiguration fc = Fluently.Configure();
            try
            {
                if (DefaultDB == DefaultDB.SQLServer)
                {
                    //fc.Database(CreateMSSqlDbConfig).Cache(c => c.UseSecondLevelCache().ProviderClass<SysCacheProvider>().UseQueryCache().UseMinimalPuts())
                    //    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("APL.DPH.DAL")));
                }
                else if (DefaultDB == DefaultDB.MySQL)
                {
                    fc.Database(CreateMySqlDbConfig)//.Cache(c => c.UseSecondLevelCache().ProviderClass<SysCacheProvider>().UseQueryCache())
                        .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("L.PDSDA.DAL")));
                }
                else if (DefaultDB == DefaultDB.MSAccess)
                {
                    //fc.Database(CreateMsAccessConfig).Cache(c => c.UseSecondLevelCache().ProviderClass<SysCacheProvider>().UseQueryCache())
                    //    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("APL.DPH.DAL")));
                }
                else if (DefaultDB == DefaultDB.Oracle)
                {
                    //fc.Database(CreateOracleConfig).Cache(c => c.UseSecondLevelCache().ProviderClass<SysCacheProvider>().UseQueryCache())
                    //    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("APL.DPH.DAL")));
                }
                else if (DefaultDB == DefaultDB.None && !string.IsNullOrEmpty(OtherDBs) && OtherDBs == "sqlite")
                {
                    //fc.Database(CreateSQLiteConfig).Cache(c => c.UseSecondLevelCache().ProviderClass<SysCacheProvider>().UseQueryCache())
                    //    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("APL.DPH.DAL")));
                }
                else if (DefaultDB == DefaultDB.None && !string.IsNullOrEmpty(OtherDBs) && OtherDBs == "mssqlserverreturn")
                {
                    //fc.Database(CreateMSSqlReturnDbConfig).Cache(c => c.UseSecondLevelCache().ProviderClass<SysCacheProvider>().UseQueryCache())
                    //    .Mappings(m => m.FluentMappings.Add<PlantMap>());
                }

#if debug
                
#endif
                if (IsUpdateSchema)
                    fc.ExposeConfiguration(UpdateSchema);

                if (RunIn == RunIn.Web)
                    fc.CurrentSessionContext<WebSessionContext>();
                else
                    fc.CurrentSessionContext<CallSessionContext>();

                fc.ExposeConfiguration(config =>
                {
                    config.SetInterceptor(new SqlStatementInterceptor());
                    config.SetProperty(NHibernate.Cfg.Environment.SqlExceptionConverter,
                        typeof(MsSqlExceptionConverter).AssemblyQualifiedName);
                });

                return fc.BuildSessionFactory();
            }
            catch (Exception ex)
            {
                IsError = true;
                ErrorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                NHibernateSessionFactoriesPerRequest.IsError = IsError;
                NHibernateSessionFactoriesPerRequest.ErrorMessage = ErrorMessage;
                return null;
            }

        }

    }
}
