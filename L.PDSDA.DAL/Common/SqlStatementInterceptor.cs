﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.Diagnostics;

namespace L.PDSDA.DAL.Common
{
    public class SqlStatementInterceptor : EmptyInterceptor, IInterceptor
    {
        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
            string qry = sql.ToString();
            Trace.WriteLine(qry);
            //return sql;
            return base.OnPrepareStatement(sql);
        }
    }
}
