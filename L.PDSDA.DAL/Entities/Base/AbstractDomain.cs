﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace L.PDSDA.DAL.Entities.Base
{
    public abstract class AbstractDomain<TID>
    {

        public virtual TID Id { get; set; }
        public virtual string CreateBy { get; set; }
        public virtual Nullable<DateTime> CreateDate { get; set; }
        public virtual string CreateTerminal { get; set; }
        public virtual string UpdateBy { get; set; }
        public virtual Nullable<DateTime> UpdateDate { get; set; }
        public virtual string UpdateTerminal { get; set; }
    }
}
