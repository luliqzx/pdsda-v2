﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Entities.Base
{
    public class LogDes
    {
        public virtual string CreateBy
        {
            get
            {
                return _CreateBy;
            }
            set
            {
                if (string.IsNullOrEmpty(_CreateBy) && value != "")
                {
                    _CreateBy = value;
                }
            }
        }
        private string _CreateBy { get; set; }

        public virtual Nullable<DateTime> CreateDate
        {
            get
            {
                return _CreateDate;
            }
            set
            {
                if (_CreateDate == null && value != null)
                {
                    _CreateDate = value;
                }
            }
        }
        private Nullable<DateTime> _CreateDate { get; set; }

        public virtual string CreateTerminal
        {
            get
            {
                return _CreateTerminal;
            }
            set
            {
                if (string.IsNullOrEmpty(_CreateTerminal) && value != "")
                {
                    _CreateTerminal = value;
                }
            }
        }
        private string _CreateTerminal { get; set; }


        public virtual string UpdateBy { get; set; }
        public virtual Nullable<DateTime> UpdateDate { get; set; }
        public virtual string UpdateTerminal { get; set; }
    }
}
