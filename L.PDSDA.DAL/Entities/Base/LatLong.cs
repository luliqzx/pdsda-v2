﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Entities.Base
{
    public class LatLong
    {
        public virtual string Latitude { get; set; }
        public virtual string Longitude { get; set; }
    }
}
