//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace L.PDSDA.DAL.Entities
{
    public class PID_DYN
    {
        #region Primitive Properties

        public virtual string K_PID
        {
            get { return PID == null ? "" : PID.K_PID; }
            set
            {
                if (PID != null && PID.K_PID != value)
                {
                    PID = null;
                }
                else
                {
                    PID = new PID();
                    PID.K_PID = value;
                }
            }
        }

        public virtual int TAHUN_DATA
        {
            get;
            set;
        }

        public virtual string SUMBER_DATA
        {
            get;
            set;
        }

        public virtual Nullable<double> LUAS_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT1_PD_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT1_PL_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT1_LAIN_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_PD_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_PL_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_LAIN_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_PD_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_PL_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_LAIN_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> IP_SBL
        {
            get;
            set;
        }

        public virtual Nullable<double> LUAS_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT1_PD_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT1_PL_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT1_LAIN_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_PD_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_PL_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT2_LAIN_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_PD_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_PL_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> MT3_LAIN_SSD
        {
            get;
            set;
        }

        public virtual Nullable<double> IP_SSD
        {
            get;
            set;
        }

        public virtual Nullable<int> P3A
        {
            get;
            set;
        }

        public virtual Nullable<int> ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> BADAN_HUKUM
        {
            get;
            set;
        }

        public virtual string STATUS
        {
            get;
            set;
        }

        public virtual string KETERANGAN
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_AKTIF
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_AKTIF_ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_AKTIF_BH
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_BERKEMBANG
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_BERKEMBANG_ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_BERKEMBANG_BH
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_TIDAKBERKEMBANG
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_TIDAKBERKEMBANG_ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_TIDAKBERKEMBANG_BH
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_PASIF
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_PASIF_ANGGOTA
        {
            get;
            set;
        }

        public virtual Nullable<int> JML_PASIF_BH
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties

        public virtual PID PID
        {
            get;
            set;
        }
        private PID _pID;

        #endregion

        public override bool Equals(object obj)
        {
            PID_DYN o = obj as PID_DYN;
            if (this == o)
            {
                return true;
            }
            if (this.K_PID == o.K_PID && o.TAHUN_DATA == this.TAHUN_DATA)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += this.K_PID.GetHashCode();
            hash += this.TAHUN_DATA.GetHashCode();

            return hash;
        }

    }
}
