﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.DAL.Entities
{
    public class Department : AbstractDomain<int>
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual Department MainDepartment { get; set; }

        #region Navigate
        public virtual IList<User> Users { get; set; }
        #endregion
    }
}
