﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.DAL.Entities

{
    public class Group : AbstractDomain<int>
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }

        #region Navigate
        public virtual Group MainGroup { get; set; }
        public virtual IList<User> Users { get; set; }
        public virtual IList<Group> Groups { get; set; }
        public virtual IList<GroupModule> GroupModules { get; set; }
        public virtual IList<BALAI> BALAIs { get; set; }

        #endregion
    }
}
