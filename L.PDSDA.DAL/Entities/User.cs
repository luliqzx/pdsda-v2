﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.DAL.Entities
{
    public class User : AbstractDomain<int>
    {
        public virtual string Firstname { get; set; }
        public virtual string Lastname { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual string Email { get; set; }
        public virtual Nullable<DateTime> BirthDate { get; set; }

        #region Navigate
        public virtual Department Department { get; set; }
        public virtual IList<Group> Groups { get; set; }
        //public virtual PROPINSI PROPINSI { get; set; }
        //public virtual IList<BALAI> BALAIs { get; set; }
        //public virtual BALAI BALAI { get; set; }
        #endregion
    }
}
