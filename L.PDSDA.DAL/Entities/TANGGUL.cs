//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace L.PDSDA.DAL.Entities
{
    public class TANGGUL
    {
        #region Primitive Properties

        public virtual int URUT_TANGGUL
        {
            get;
            set;
        }

        public virtual string DESA
        {
            get;
            set;
        }

        public virtual string KECAMATAN
        {
            get;
            set;
        }

        public virtual string PENGELOLA
        {
            get;
            set;
        }

        public virtual string POSISI
        {
            get;
            set;
        }

        public virtual Nullable<double> PANJANG
        {
            get;
            set;
        }

        public virtual string TITIK_AWAL
        {
            get;
            set;
        }

        public virtual string TITIK_AKHIR
        {
            get;
            set;
        }

        public virtual Nullable<double> TINGGI_DALAM
        {
            get;
            set;
        }

        public virtual Nullable<double> TINGGI_LUAR
        {
            get;
            set;
        }

        public virtual string LERENG_DALAM
        {
            get;
            set;
        }

        public virtual string LERENG_LUAR
        {
            get;
            set;
        }

        public virtual string PELINDUNG_DALAM
        {
            get;
            set;
        }

        public virtual string PELINDUNG_LUAR
        {
            get;
            set;
        }

        public virtual string TIPE
        {
            get;
            set;
        }

        public virtual string KONDISI
        {
            get;
            set;
        }

        public virtual string PUNCAK
        {
            get;
            set;
        }

        public virtual Nullable<short> THDIBANGUN
        {
            get;
            set;
        }

        public virtual string NM_PROYEK
        {
            get;
            set;
        }

        public virtual string KONSULTAN
        {
            get;
            set;
        }

        public virtual string BIAYA_KONSULTAN
        {
            get;
            set;
        }

        public virtual string KONTRAKTOR
        {
            get;
            set;
        }

        public virtual string BIAYA_KONTRAKTOR
        {
            get;
            set;
        }

        public virtual Nullable<short> THREHAB
        {
            get;
            set;
        }

        public virtual string BIAYA_REHAB
        {
            get;
            set;
        }

        public virtual Nullable<double> LEBAR_BANTARAN
        {
            get;
            set;
        }

        public virtual string PENGGUNAAN_BANTARAN
        {
            get;
            set;
        }

        public virtual Nullable<double> LEBAR_SEMPADAN
        {
            get;
            set;
        }

        public virtual string PENGGUNAAN_SEMPADAN
        {
            get;
            set;
        }

        public virtual string KOORDINAT_AWAL
        {
            get;
            set;
        }

        public virtual string KOORDINAT_AKHIR
        {
            get;
            set;
        }

        public virtual Nullable<double> ELEVASI
        {
            get;
            set;
        }

        public virtual Nullable<double> QDESIGN
        {
            get;
            set;
        }

        public virtual Nullable<short> TAHUN_DATA
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties

        public virtual SUNGAI SUNGAI
        {
            get;
            set;
        }

        public virtual KABUPATEN KABUPATEN
        {
            get;
            set;
        }

        #endregion


        public override bool Equals(object obj)
        {

            TANGGUL TANGGUL = obj as TANGGUL;
            if (TANGGUL != null && TANGGUL.URUT_TANGGUL == this.URUT_TANGGUL)
            {
                if (TANGGUL.SUNGAI != null && TANGGUL.SUNGAI == SUNGAI)
                {
                    return true;
                }
            }
            return false;
            //return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            int result = this.URUT_TANGGUL.GetHashCode() + this.SUNGAI.GetHashCode();
            return result;
            //return base.GetHashCode();
        }

    }
}
