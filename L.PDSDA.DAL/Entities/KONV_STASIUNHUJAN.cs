//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace L.PDSDA.DAL.Entities
{
    public  class KONV_STASIUNHUJAN
    {
        #region Primitive Properties
    
        public virtual int NO_URUT
        {
            get;
            set;
        }
    
        public virtual string DESKRIPSI
        {
            get;
            set;
        }
    
        public virtual Nullable<int> K_STASIUN
        {
            get;
            set;
        }
    
        public virtual Nullable<int> K_STASIUN1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> N_STASIUN
        {
            get;
            set;
        }
    
        public virtual Nullable<int> N_STASIUN1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> KADASTER
        {
            get;
            set;
        }
    
        public virtual Nullable<int> KADASTER1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> KECAMATAN
        {
            get;
            set;
        }
    
        public virtual Nullable<int> KECAMATAN1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DESA
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DESA1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DAS
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DAS1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> LOKASI
        {
            get;
            set;
        }
    
        public virtual Nullable<int> LOKASI1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DIBANGUNOLEH
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DIBANGUNOLEH1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> TINGGI_MUKA_LAUT
        {
            get;
            set;
        }
    
        public virtual Nullable<int> TINGGI_MUKA_LAUT1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> THPENDIRIAN
        {
            get;
            set;
        }
    
        public virtual Nullable<int> THPENDIRIAN1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> THDATA
        {
            get;
            set;
        }
    
        public virtual Nullable<int> THDATA1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DATA
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DATA1
        {
            get;
            set;
        }
    
        public virtual Nullable<int> K_LAMA
        {
            get;
            set;
        }
    
        public virtual Nullable<int> K_LAMA1
        {
            get;
            set;
        }

        #endregion
    }
}
