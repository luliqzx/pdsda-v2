﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.DAL.Entities
{
    public class GroupModule
    {
        //[Key]
        public virtual int Id { get; set; }
        public virtual bool IsViewable { get; set; }
        public virtual bool IsCreateable { get; set; }
        public virtual bool IsUpdateable { get; set; }
        public virtual bool IsDeleteable { get; set; }
        public virtual Nullable<bool> IsOptional { get; set; }
        public virtual Nullable<bool> IsActive { get; set; }

        #region Navigate
        public virtual Module Module { get; set; }
        public virtual Group Group { get; set; }
        public virtual IList<Privilege> Privileges { get; set; }
        #endregion

        //public override bool Equals(object obj)
        //{
        //    //return base.Equals(obj);
        //    if (obj == null)
        //        return false;
        //    var t = obj as GroupModule;
        //    if (t == null)
        //        return false;
        //    if (Module == t.Module && Group == t.Group)
        //        return true;
        //    return false;

        //}

        //public override int GetHashCode()
        //{
        //    //return base.GetHashCode();
        //    return (Group.Id + "|" + Module.Id).GetHashCode();
        //}
    }
}
