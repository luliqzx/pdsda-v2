﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace L.PDSDA.DAL.Entities.Sample
{
    public class sBlob
    {
        public virtual Int32 Id { get; set; }

        public virtual byte[] blobData { get; set; }
    }
}
