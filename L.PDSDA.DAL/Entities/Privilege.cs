﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.DAL.Entities
{
    public class Privilege : AbstractDomain<int>
    {

        public virtual string Description { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }

        public virtual IList<GroupModule> GroupModules { get; set; }
        //public virtual ICollection<GroupModulePrivilege> GroupModulePrivileges { get; set; }
    }
}
