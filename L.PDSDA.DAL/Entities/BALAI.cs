﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Entities
{
    public class BALAI
    {
        //public virtual int ID { get; set; }
        public virtual string NamaBalai { get; set; }
        public virtual string KodeBalai { get; set; }
        public virtual IList<PROPINSI> PROPINSIs { get; set; }
        public virtual IList<WS> WSs { get; set; }
        //public virtual IList<User> Users { get; set; }
        public virtual IList<Group> Groups { get; set; }
    }
}
