﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.DAL.Entities
{
    public class Module : AbstractDomain<int>
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Path { get; set; }
        public virtual int Position { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsPublic { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string ImageUrl { get; set; }

        #region Navigate
        public virtual Module MainModule { get; set; }
        public virtual IList<Module> ChildModules { get; set; }
        public virtual IList<GroupModule> GroupModules { get; set; }
        #endregion Navigate
    }
}
