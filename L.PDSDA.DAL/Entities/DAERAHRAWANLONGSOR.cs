//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using L.PDSDA.DAL.Entities.Base;

namespace L.PDSDA.DAL.Entities
{
    public class DAERAHRAWANLONGSOR : XYAudit
    {
        #region Primitive Properties
    
        public virtual string K_DAERAHRAWANLONGSOR
        {
            get;
            set;
        }
    
        public virtual string N_DAERAHRAWANLONGSOR
        {
            get;
            set;
        }
    
    
        public virtual string PENYEBAB
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DAMPAK_PENDUDUK
        {
            get;
            set;
        }
    
        public virtual Nullable<double> DAMPAK_LAHAN_PERTANIAN
        {
            get;
            set;
        }
    
        public virtual Nullable<double> DAMPAK_LAHAN_IRIGASI
        {
            get;
            set;
        }
    
        public virtual Nullable<double> DAMPAK_LAHAN_KEBUN
        {
            get;
            set;
        }
    
        public virtual Nullable<double> DAMPAK_LAHAN_IKAN
        {
            get;
            set;
        }
    
        public virtual Nullable<double> DAMPAK_JALAN
        {
            get;
            set;
        }
    
        public virtual Nullable<int> DAMPAK_JEMBATAN
        {
            get;
            set;
        }
    
        public virtual Nullable<double> DAMPAK_TANGGUL
        {
            get;
            set;
        }
    
        public virtual Nullable<double> DAMPAK_SALURAN
        {
            get;
            set;
        }
    
        public virtual Nullable<double> NILAI_KERUGIAN
        {
            get;
            set;
        }
    
        public virtual string KLASIFIKASI
        {
            get;
            set;
        }
    
        public virtual string PENANGGULANGAN
        {
            get;
            set;
        }
    
        public virtual string KETERANGAN
        {
            get;
            set;
        }
    
        public virtual string LOKASI
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual DAS DA
        {
            get;
            set;
        }
        private DAS _dA;
    
        public virtual PROPINSI PROPINSI
        {
            get;
            set;
        }
        private PROPINSI _pROPINSI;
    
        public virtual WS W
        {
            get;
            set;
        }
        private WS _w;

        #endregion
        
    }
}
