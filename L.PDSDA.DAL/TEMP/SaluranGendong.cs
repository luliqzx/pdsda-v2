﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.TEMP
{
    public class SaluranGendong
    {
        public string GEND_GOR_JML { get; set; }
        public string GEND_GOR_B { get; set; }
        public string GEND_GOR_RR { get; set; }
        public string GEND_GOR_RB { get; set; }
        public string GEND_TERJUN_JML { get; set; }
        public string GEND_TERJUN_B { get; set; }
        public string GEND_TERJUN_RR { get; set; }
        public string GEND_TERJUN_RB { get; set; }
        public string GEND_LAIN_JML { get; set; }
        public string GEND_LAIN_B { get; set; }
        public string GEND_LAIN_RR { get; set; }
        public string GEND_LAIN_RB { get; set; }
        public string GEND_LAINP_JML { get; set; }
        public string GEND_LAINP_B { get; set; }
        public string GEND_LAINP_RR { get; set; }
        public string GEND_LAINP_RB { get; set; }

    }
}
