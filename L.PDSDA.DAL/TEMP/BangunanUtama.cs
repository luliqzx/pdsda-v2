﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.TEMP
{
    public class BangunanUtama
    {
        public string WADUK_JML { get; set; }
        public string WADUK_B { get; set; }
        public string WADUK_RR { get; set; }
        public string WADUK_RB { get; set; }
        public string WADUKP_JML { get; set; }
        public string WADUKP_B { get; set; }
        public string WADUKP_RR { get; set; }
        public string WADUKP_RB { get; set; }
        public string BT_JML { get; set; }
        public string BT_B { get; set; }
        public string BT_RR { get; set; }
        public string BT_RB { get; set; }
        public string BTP_JML { get; set; }
        public string BTP_B { get; set; }
        public string BTP_RR { get; set; }
        public string BTP_RB { get; set; }
        public string BG_JML { get; set; }
        public string BG_B { get; set; }
        public string BG_RR { get; set; }
        public string BG_RB { get; set; }
        public string BGP_JML { get; set; }
        public string BGP_B { get; set; }
        public string BGP_RR { get; set; }
        public string BGP_RB { get; set; }
        public string POMPA_JML { get; set; }
        public string POMPA_B { get; set; }
        public string POMPA_RR { get; set; }
        public string POMPA_RB { get; set; }
        public string POMPAP_JML { get; set; }
        public string POMPAP_B { get; set; }
        public string POMPAP_RR { get; set; }
        public string POMPAP_RB { get; set; }
        public string BEBAS_JML { get; set; }
        public string BEBAS_B { get; set; }
        public string BEBAS_RR { get; set; }
        public string BEBAS_RB { get; set; }
        public string BEBASP_JML { get; set; }
        public string BEBASP_B { get; set; }
        public string BEBASP_RR { get; set; }
        public string BEBASP_RB { get; set; }
    }
}
