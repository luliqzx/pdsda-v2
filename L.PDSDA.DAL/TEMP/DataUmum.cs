﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.TEMP
{
    public class DataUmum
    {
        public string K_DI { get; set; }
        public string TAHUN_DATA { get; set; }
        public string SUMBER_DATA { get; set; }
        public string K_TINGKATAN { get; set; }
        public string LUAS_RENCANA { get; set; }
        public string JAR_OPTIMAL { get; set; }
        public string JAR_BOPTIMAL { get; set; }
        public string JAR_ALFUNG { get; set; }
        public string JAR_ALFUNGDRBS { get; set; }
        public string JAR_BSAWAH { get; set; }
        public string BJAR_SAWAH { get; set; }
        public string BJAR_BSAWAH { get; set; }
        public string P3A { get; set; }
        public string ANGGOTA { get; set; }
        public string BADAN_HUKUM { get; set; }
        public string DEBIT_RENCANA { get; set; }
        public string DEBIT_KENYATAAN { get; set; }
        public string JALAN_PJ { get; set; }
        public string JALAN_B { get; set; }
        public string JALAN_RR { get; set; }
        public string JALAN_RB { get; set; }
        public string JML_AKTIF { get; set; }
        public string JML_AKTIF_ANGGOTA { get; set; }
        public string JML_AKTIF_BH { get; set; }
        public string JML_BERKEMBANG { get; set; }
        public string JML_BERKEMBANG_ANGGOTA { get; set; }
        public string JML_BERKEMBANG_BH { get; set; }
        public string JML_TIDAKBERKEMBANG { get; set; }
        public string JML_TIDAKBERKEMBANG_ANGGOTA { get; set; }
        public string JML_TIDAKBERKEMBANG_BH { get; set; }
        public string JML_PASIF { get; set; }
        public string JML_PASIF_ANGGOTA { get; set; }
        public string JML_PASIF_BH { get; set; }

    }
}
