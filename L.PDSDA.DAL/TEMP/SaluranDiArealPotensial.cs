﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.TEMP
{
    public class SaluranDiArealPotensial
    {
        public string SAL_INDUK_PJ { get; set; }
        public string SAL_INDUK_B { get; set; }
        public string SAL_INDUK_RR { get; set; }
        public string SAL_INDUK_RB { get; set; }
        public string SAL_SEKUNDER_PJ { get; set; }
        public string SAL_SEKUNDER_B { get; set; }
        public string SAL_SEKUNDER_RR { get; set; }
        public string SAL_SEKUNDER_RB { get; set; }
        public string SAL_PEMBUANG_PJ { get; set; }
        public string SAL_PEMBUANG_B { get; set; }
        public string SAL_PEMBUANG_RR { get; set; }
        public string SAL_PEMBUANG_RB { get; set; }
        public string SAL_SUPLESI_PJ { get; set; }
        public string SAL_SUPLESI_B { get; set; }
        public string SAL_SUPLESI_RR { get; set; }
        public string SAL_SUPLESI_RB { get; set; }
        public string SAL_GENDONG_PJ { get; set; }
        public string SAL_GENDONG_B { get; set; }
        public string SAL_GENDONG_RR { get; set; }
        public string SAL_GENDONG_RB { get; set; }

    }
}
