﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.TEMP
{
    public class SaluranSuplesi
    {
        public string SUPL_TALANG_JML { get; set; }
        public string SUPL_TALANG_B { get; set; }
        public string SUPL_TALANG_RR { get; set; }
        public string SUPL_TALANG_RB { get; set; }
        public string SUPL_TALANGP_JML { get; set; }
        public string SUPL_TALANGP_B { get; set; }
        public string SUPL_TALANGP_RR { get; set; }
        public string SUPL_TALANGP_RB { get; set; }
        public string SUPL_SYPHON_JML { get; set; }
        public string SUPL_SYPHON_B { get; set; }
        public string SUPL_SYPHON_RR { get; set; }
        public string SUPL_SYPHON_RB { get; set; }
        public string SUPL_SYPHONP_JML { get; set; }
        public string SUPL_SYPHONP_B { get; set; }
        public string SUPL_SYPHONP_RR { get; set; }
        public string SUPL_SYPHONP_RB { get; set; }
        public string SUPL_JEMBATAN_JML { get; set; }
        public string SUPL_JEMBATAN_B { get; set; }
        public string SUPL_JEMBATAN_RR { get; set; }
        public string SUPL_JEMBATAN_RB { get; set; }
        public string SUPL_GOR_JML { get; set; }
        public string SUPL_GOR_B { get; set; }
        public string SUPL_GOR_RR { get; set; }
        public string SUPL_GOR_RB { get; set; }
        public string SUPL_GOT_JML { get; set; }
        public string SUPL_GOT_B { get; set; }
        public string SUPL_GOT_RR { get; set; }
        public string SUPL_GOT_RB { get; set; }
        public string SUPL_GOTP_JML { get; set; }
        public string SUPL_GOTP_B { get; set; }
        public string SUPL_GOTP_RR { get; set; }
        public string SUPL_GOTP_RB { get; set; }
        public string SUPL_TERJUN_JML { get; set; }
        public string SUPL_TERJUN_B { get; set; }
        public string SUPL_TERJUN_RR { get; set; }
        public string SUPL_TERJUN_RB { get; set; }
        public string SUPL_SUPLESI_JML { get; set; }
        public string SUPL_SUPLESI_B { get; set; }
        public string SUPL_SUPLESI_RR { get; set; }
        public string SUPL_SUPLESI_RB { get; set; }
        public string SUPL_SUPLESIP_JML { get; set; }
        public string SUPL_SUPLESIP_B { get; set; }
        public string SUPL_SUPLESIP_RR { get; set; }
        public string SUPL_SUPLESIP_RB { get; set; }
        public string SUPL_LAIN_JML { get; set; }
        public string SUPL_LAIN_B { get; set; }
        public string SUPL_LAIN_RR { get; set; }
        public string SUPL_LAIN_RB { get; set; }
        public string SUPL_LAINP_JML { get; set; }
        public string SUPL_LAINP_B { get; set; }
        public string SUPL_LAINP_RR { get; set; }
        public string SUPL_LAINP_RB { get; set; }

    }
}
