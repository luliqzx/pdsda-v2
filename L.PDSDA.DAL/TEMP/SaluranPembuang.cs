﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.TEMP
{
    public class SaluranPembuang
    {
        public string PEMB_JEMBATAN_JML { get; set; }
        public string PEMB_JEMBATAN_B { get; set; }
        public string PEMB_JEMBATAN_RR { get; set; }
        public string PEMB_JEMBATAN_RB { get; set; }
        public string PEMB_GOR_JML { get; set; }
        public string PEMB_GOR_B { get; set; }
        public string PEMB_GOR_RR { get; set; }
        public string PEMB_GOR_RB { get; set; }
        public string PEMB_KLEP_PIN_JML { get; set; }
        public string PEMB_KLEP_PIN_B { get; set; }
        public string PEMB_KLEP_PIN_RR { get; set; }
        public string PEMB_KLEP_PIN_RB { get; set; }
        public string PEMB_KLEP_PINP_JML { get; set; }
        public string PEMB_KLEP_PINP_B { get; set; }
        public string PEMB_KLEP_PINP_RR { get; set; }
        public string PEMB_KLEP_PINP_RB { get; set; }
        public string PEMB_TERJUN_JML { get; set; }
        public string PEMB_TERJUN_B { get; set; }
        public string PEMB_TERJUN_RR { get; set; }
        public string PEMB_TERJUN_RB { get; set; }
        public string PEMB_LAIN_JML { get; set; }
        public string PEMB_LAIN_B { get; set; }
        public string PEMB_LAIN_RR { get; set; }
        public string PEMB_LAIN_RB { get; set; }
        public string PEMB_LAINP_JML { get; set; }
        public string PEMB_LAINP_B { get; set; }
        public string PEMB_LAINP_RR { get; set; }
        public string PEMB_LAINP_RB { get; set; }

    }
}
