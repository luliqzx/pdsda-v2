﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.TEMP
{
    public class SaluranPembawa
    {
        public string BAGI_JML { get; set; }
        public string BAGI_B { get; set; }
        public string BAGI_RR { get; set; }
        public string BAGI_RB { get; set; }
        public string BAGIP_JML { get; set; }
        public string BAGIP_B { get; set; }
        public string BAGIP_RR { get; set; }
        public string BAGIP_RB { get; set; }
        public string BSADAP_JML { get; set; }
        public string BSADAP_B { get; set; }
        public string BSADAP_RR { get; set; }
        public string BSADAP_RB { get; set; }
        public string BSADAPP_JML { get; set; }
        public string BSADAPP_B { get; set; }
        public string BSADAPP_RR { get; set; }
        public string BSADAPP_RB { get; set; }
        public string SADAP_JML { get; set; }
        public string SADAP_B { get; set; }
        public string SADAP_RR { get; set; }
        public string SADAP_RB { get; set; }
        public string SADAPP_JML { get; set; }
        public string SADAPP_B { get; set; }
        public string SADAPP_RR { get; set; }
        public string SADAPP_RB { get; set; }

    }
}
