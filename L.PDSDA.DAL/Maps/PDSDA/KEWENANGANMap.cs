﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.PDSDA
{
    public class KEWENANGANMap : ClassMap<KEWENANGAN>
    {
        public KEWENANGANMap()
        {
            this.Map(x => x.N_KEWENANGAN);
            //this.Id(x => x.K_KABUPATEN);
            //this.References(x => x.PROPINSI).Column("K_PROPINSI").Nullable().NotFound.Ignore();
            this.Id(x => x.K_KEWENANGAN);
            this.HasMany(x => x.IRIGASIs).KeyColumn("K_KEWENANGAN");
        }
    }
}
