﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.PDSDA
{
    public class KABUPATENMap : ClassMap<KABUPATEN>
    {
        public KABUPATENMap()
        {
            this.Map(x => x.N_KABUPATEN);
            //this.Id(x => x.K_KABUPATEN);
            //this.References(x => x.PROPINSI).Column("K_PROPINSI").Nullable().NotFound.Ignore();
            this.CompositeId().KeyProperty(x => x.K_KABUPATEN)
                .KeyReference(x => x.PROPINSI, "K_PROPINSI");

            this.HasMany(x => x.AIRTANAHs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.BENDUNGANs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.BENDUNGs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.EMBUNG_POTENSI).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.EMBUNGs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.IRIGASIs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.PIDs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.RAWAs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.STASIUNDEBITs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.STASIUNHUJANs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.STASIUNKLIMATOLOGIs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.DANAUs).KeyColumns.Add("KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.TANGGULs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            this.HasMany(x => x.SLUICEs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            //this.HasMany(x => x.TADAH_HUJANs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
            //this.HasMany(x => x.POMPAs).KeyColumns.Add("K_KABUPATEN", "K_PROPINSI").Inverse();
        }
    }
}
