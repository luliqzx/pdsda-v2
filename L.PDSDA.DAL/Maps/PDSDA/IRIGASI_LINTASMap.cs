﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.DAL.Maps.PDSDA
{
    public class IRIGASI_LINTASMap : ClassMap<IRIGASI_LINTAS>
    {
        public IRIGASI_LINTASMap()
        {
            this.Id(x => x.K_DI_LINTAS);
            this.Map(x => x.N_DI_LINTAS);
            this.References(x => x.STATUS_IRIGASI, "K_STATUS").NotFound.Ignore();//;
            //this.References(x => x.STATUS_IRIGASI).PropertyRef(x => x.K_STATUS);
            this.HasMany(x => x.IRIGASIs).KeyColumn("K_DI_LINTAS");//.Cache.ReadWrite();
        }
    }
}
