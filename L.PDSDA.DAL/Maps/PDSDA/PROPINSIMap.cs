﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.PDSDA
{
    public class PROPINSIMap : ClassMap<PROPINSI>
    {
        public PROPINSIMap()
        {
            this.Id(x => x.K_PROPINSI).GeneratedBy.Assigned();
            this.Map(x => x.N_PROPINSI);
            this.HasMany(x => x.KABUPATENs).KeyColumn("K_PROPINSI").Inverse();
            //this.HasManyToMany(x => x.BALAIs);
        }

    }
}
