﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.PDSDA
{
    public class STATUS_WSMap : ClassMap<STATUS_WS>
    {
        public STATUS_WSMap()
        {
            this.Map(x => x.N_STATUS);
            this.Id(x => x.K_STATUS).GeneratedBy.Assigned();
            this.HasMany(x => x.WS).KeyColumn("K_STATUS").Cascade.SaveUpdate().Inverse();
            //this.HasMany(x => x.WS).KeyColumn("STATUS_WS");
        }
    }
}
