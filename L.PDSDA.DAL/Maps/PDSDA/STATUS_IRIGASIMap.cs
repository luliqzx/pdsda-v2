﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.PDSDA
{
    public class STATUS_IRIGASIMap : ClassMap<STATUS_IRIGASI>
    {
        public STATUS_IRIGASIMap()
        {
            this.Map(x => x.N_STATUS);
            this.Id(x => x.K_STATUS).GeneratedBy.Assigned();
            this.HasMany(x => x.IRIGASI_LINTAS).KeyColumn("K_STATUS").Inverse();
        }
    }
}
