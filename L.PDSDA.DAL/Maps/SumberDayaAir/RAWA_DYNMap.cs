﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class RAWA_DYNMap : ClassMap<RAWA_DYN>
    {
        public RAWA_DYNMap()
        {
            this.CompositeId()
             .KeyProperty(x => x.TAHUN_DATA, x => x.Length(4))
             .KeyReference(x => x.RAWA, "K_RAWA");
            this.Map(x => x.BANGUN_ATUR);
            this.Map(x => x.BANGUN_ATUR2);
            this.Map(x => x.BANGUN_LAIN);
            this.Map(x => x.BANGUN_LAIN2);
            this.Map(x => x.BIAYA_INVESTASI1);
            this.Map(x => x.BIAYA_INVESTASI2);
            this.Map(x => x.BLM_DESAIN);
            this.Map(x => x.JML_LAHAN1);
            this.Map(x => x.JML_LAHAN2);
            this.Map(x => x.JML_TANI1);
            this.Map(x => x.JML_TANI2);
            this.Map(x => x.LAIN_LAIN);
            this.Map(x => x.LRAWA_DESAIN);
            this.Map(x => x.LRAWA_FUNGSIONAL);
            this.Map(x => x.LRAWA_POTENSIAL);
            this.Map(x => x.LRAWA_REKLAMASI);
            this.Map(x => x.LTOTALRAWA_FUNGSIMANFAAT);
            this.Map(x => x.LTOTALRAWA_PEMERINTAH);
            this.Map(x => x.LTOTALRAWA_SWASTA);
            this.Map(x => x.LUASLAHAN_SWASTA);
            this.Map(x => x.NON_FUNGSI);
            this.Map(x => x.NON_POTENSIAL);
            this.Map(x => x.PERIKANAN);
            this.Map(x => x.PERKEBUNAN);
            this.Map(x => x.PERTANIAN);
            this.Map(x => x.S_IKAN1);
            this.Map(x => x.S_IKAN2);
            this.Map(x => x.S_KEBUN1);
            this.Map(x => x.S_KEBUN2);
            this.Map(x => x.S_LAIN1);
            this.Map(x => x.S_LAIN2);
            this.Map(x => x.S_TANI1);
            this.Map(x => x.S_TANI2);
            this.Map(x => x.SAL_PRIMER_SWASTA);
            this.Map(x => x.SALURAN_NAVIGASI_MGUNA1);
            this.Map(x => x.SALURAN_NAVIGASI_MGUNA2);
            this.Map(x => x.SALURAN_PRI_TAMBAK1);
            this.Map(x => x.SALURAN_PRI_TAMBAK2);
            this.Map(x => x.SALURAN_PRIMER1);
            this.Map(x => x.SALURAN_PRIMER2);
            this.Map(x => x.SALURAN_SEKUNDER1);
            this.Map(x => x.SALURAN_SEKUNDER2);
            this.Map(x => x.SALURAN_TERSIER1);
            this.Map(x => x.SALURAN_TERSIER2);
            this.Map(x => x.SIAP_REKLAMASI);
            this.Map(x => x.SUMBER_DANA1);
            this.Map(x => x.SUMBER_DANA2);
            this.Map(x => x.TANGGUL);
            this.Map(x => x.TANGGUL2);
            this.Map(x => x.TANI_GARAP_SWASTA);
            this.Map(x => x.THN_BANGUN_SWASTA);
            this.Map(x => x.THN_BANGUN1);
            this.Map(x => x.THN_BANGUN2);
            this.Map(x => x.TOTAL_TAHAP1);
            this.Map(x => x.TOTAL_TAHAP2);
            this.Map(x => x.TRANS_LOKALLAHAN1);
            this.Map(x => x.TRANS_LOKALLAHAN2);
            this.Map(x => x.TRANS_LOKALTANI1);
            this.Map(x => x.TRANS_LOKALTANI2);
            this.Map(x => x.TRANS_UMUMLAHAN1);
            this.Map(x => x.TRANS_UMUMLAHAN2);
            this.Map(x => x.TRANS_UMUMTANI1);
            this.Map(x => x.TRANS_UMUMTANI2);

            //this.References(x => x.RAWA, "K_RAWA").Not.Insert().Not.Update();

            //this.Version(x => x.LastModifiedOn);//.UnsavedValue("null");
        }
    }
}
