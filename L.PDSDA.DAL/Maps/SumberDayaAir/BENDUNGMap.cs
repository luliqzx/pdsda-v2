﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class BENDUNGMap : ClassMap<BENDUNG>
    {
        public BENDUNGMap()
        {
            this.Id(x => x.K_BENDUNG);
            this.References(x => x.DAS).Column("K_DAS").NotFound.Ignore();
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();
            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();

            this.Map(x => x.N_BENDUNG);
            this.Map(x => x.SUNGAI);
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.DESA);
            this.Map(x => x.K_JENIS);
            this.Map(x => x.TH_BANGUN);
            this.Map(x => x.TH_REHAB);
            this.Map(x => x.TINGGI);
            this.Map(x => x.LEBAR);
            this.Map(x => x.KONDISI);
            this.Map(x => x.DEBIT_HUJAN);
            this.Map(x => x.DEBIT_KEMARAU);
            this.Map(x => x.MANFAAT_IRIGASI);
            this.Map(x => x.MANFAAT_DMI);
            this.Map(x => x.APBN_BUAT);
            this.Map(x => x.PLN_BUAT);
            this.Map(x => x.APBN_REHAB);
            this.Map(x => x.PLN_REHAB);
            this.Map(x => x.SUMBER_DATA);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
