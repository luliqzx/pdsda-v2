﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class SUNGAIMap : ClassMap<SUNGAI>
    {
        public SUNGAIMap()
        {
            this.CompositeId()
                .KeyProperty(x => x.URUT)
                .KeyProperty(x => x.K_ORDE1)
                .KeyProperty(x => x.K_ORDE2)
                .KeyProperty(x => x.K_ORDE3)
                .KeyProperty(x => x.K_ORDE4)
                .KeyProperty(x => x.K_ORDE5);
            //this.Map(x => x.K_DAS);
            //this.Map(x => x.K_ORDE1);
            //this.Map(x => x.K_ORDE2);
            //this.Map(x => x.K_ORDE3);
            //this.Map(x => x.K_ORDE4);
            //this.Map(x => x.K_ORDE5);
            this.Map(x => x.K_STATUSSUNGAI);
            //this.Map(x => x.K_WS);
            this.Map(x => x.KETERANGAN);
            this.Map(x => x.LEBAR_M);
            this.Map(x => x.LERENG);
            this.Map(x => x.LUAS_DPS_KM2);
            this.Map(x => x.N_SUNGAI);
            this.Map(x => x.ORDE);
            this.Map(x => x.PANJANG_KM);
            this.Map(x => x.QBANJIR_M3PD);

            this.HasMany(x => x.TANGGULs).KeyColumns.Add(new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" }).Inverse().Cascade.All();
            this.HasMany(x => x.SLUICEs).KeyColumns.Add(new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" }).Inverse();
            this.HasMany(x => x.REVETMENTs).KeyColumns.Add(new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" });
            this.HasMany(x => x.POMPAs).KeyColumns.Add(new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" }).Inverse();
            this.HasMany(x => x.KRIBs).KeyColumns.Add(new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" });
            this.HasMany(x => x.GROUNDSILLs).KeyColumns.Add(new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" });

            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();//).Column("").Nullable().NotFound.Ignore();
            this.References(x => x.DAS).Column("K_DAS").NotFound.Ignore();//).Column("").Nullable().NotFound.Ignore();

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
