﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class AIRTANAH_DYNMap : ClassMap<AIRTANAH_DYN>
    {
        public AIRTANAH_DYNMap()
        {
            //this.References(x => x.AIRTANAH);
            this.Map(x => x.ANGGOTA);
            this.Map(x => x.BADAN_HUKUM);
            this.Map(x => x.DEBIT_POMPA);
            this.Map(x => x.IP);
            this.Map(x => x.JENIS_POMPA);
            this.Map(x => x.KETERANGAN);
            this.Map(x => x.MANF_AIR);
            this.Map(x => x.MANF_IRIGASI);
            this.Map(x => x.MT1_LAIN);
            this.Map(x => x.MT1_PD);
            this.Map(x => x.MT1_PL);
            this.Map(x => x.MT2_LAIN);
            this.Map(x => x.MT2_PD);
            this.Map(x => x.MT2_PL);
            this.Map(x => x.MT3_LAIN);
            this.Map(x => x.MT3_PD);
            this.Map(x => x.MT3_PL);
            this.Map(x => x.P3A);
            this.Map(x => x.PENGGUNA);
            this.Map(x => x.RENC_IRIGASI);
            this.Map(x => x.RENC_RT);
            this.Map(x => x.STATUS);
            this.Map(x => x.SWL);
            //this.Map(x => x.TAHUN_DATA);
            this.CompositeId()
                .KeyReference(x => x.AIRTANAH, "K_AIRTANAH")
                .KeyProperty(x => x.TAHUN_DATA, x => x.Length(4));
        }
    }
}
