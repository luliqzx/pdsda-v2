﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class EMBUNGMap : ClassMap<EMBUNG>
    {
        public EMBUNGMap()
        {
            this.Map(x => x.APBN);
            this.References(x => x.DAS).Column("K_DAS").NotFound.Ignore();
            this.Map(x => x.DESA);
            this.Map(x => x.DMI_SBLM);
            this.Map(x => x.DMI_SSDH);
            this.Map(x => x.IRIGASI_SBLM);
            this.Map(x => x.IRIGASI_SSDH);
            this.Map(x => x.JENIS_PENANGANAN);
            this.Id(x => x.K_EMBUNG).GeneratedBy.Assigned();
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();
            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();
            this.Map(x => x.KAPASITAS_SBLM);
            this.Map(x => x.KAPASITAS_SSDH);
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.N_EMBUNG);
            this.Map(x => x.PLN);
            this.Map(x => x.TAHUN_PEMBUATAN);
            this.Map(x => x.TERNAK_SBLM);
            this.Map(x => x.TERNAK_SSDH);
            this.Map(x => x.TMA_SBLM);
            this.Map(x => x.TMA_SSDH);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
