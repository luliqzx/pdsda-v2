﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class DASMap : ClassMap<DAS>
    {
        public DASMap()
        {
            this.Id(x => x.K_DAS);//.GeneratedBy.Assigned();
            //this.Map(x => x.AIRTANAHs);
            //this.Map(x => x.BENDUNGANs);
            //this.Map(x => x.BENDUNGs);
            this.Map(x => x.CHECKDAM_B);
            this.Map(x => x.CHECKDAM_RB);
            this.Map(x => x.CHECKDAM_RR);
            this.Map(x => x.CHECKDAM_RS);
            //this.Map(x => x.DAERAHRAWANBANJIRs);
            //this.Map(x => x.DAERAHRAWANKEKERINGANs);
            //this.Map(x => x.DAERAHRAWANLONGSORs);
            //this.Map(x => x.DANAUs);
            //this.Map(x => x.EMBUNG_POTENSI);
            //this.Map(x => x.EMBUNGs);
            this.Map(x => x.GROUNDSILL_B);
            this.Map(x => x.GROUNDSILL_RB);
            this.Map(x => x.GROUNDSILL_RR);
            this.Map(x => x.GROUNDSILL_RS);
            //this.Map(x => x.GUNUNGBERAPIs);
            //this.Map(x => x.IRIGASIs);
            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();//.Nullable();//).Column("K_WS").NotFound.Ignore();//.Nullable().NotFound.Ignore();
            this.Map(x => x.KRIB_B);
            this.Map(x => x.KRIB_RB);
            this.Map(x => x.KRIB_RR);
            this.Map(x => x.KRIB_RS);
            this.Map(x => x.N_DAS);
            this.Map(x => x.PARAPET_B);
            this.Map(x => x.PARAPET_RB);
            this.Map(x => x.PARAPET_RR);
            this.Map(x => x.PARAPET_RS);
            //this.Map(x => x.PIDs);
            this.Map(x => x.POMPA_B);
            this.Map(x => x.POMPA_RB);
            this.Map(x => x.POMPA_RR);
            this.Map(x => x.POMPA_RS);
            //this.Map(x => x.POS_PENGAMAT);
            //this.Map(x => x.POSKO_BANJIR);
            //this.Map(x => x.RAWAs);
            this.Map(x => x.REVETMENT_B);
            this.Map(x => x.REVETMENT_RB);
            this.Map(x => x.REVETMENT_RR);
            this.Map(x => x.REVETMENT_RS);
            this.Map(x => x.SLUICE_B);
            this.Map(x => x.SLUICE_RB);
            this.Map(x => x.SLUICE_RR);
            this.Map(x => x.SLUICE_RS);
            //this.Map(x => x.STASIUNDEBITs);
            //this.Map(x => x.STASIUNHUJANs);
            //this.Map(x => x.STASIUNKLIMATOLOGIs);
            this.Map(x => x.TANGGUL_B);
            this.Map(x => x.TANGGUL_RB);
            this.Map(x => x.TANGGUL_RR);
            this.Map(x => x.TANGGUL_RS);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit

            this.HasMany(x => x.AIRTANAHs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.BENDUNGANs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.BENDUNGs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.DAERAHRAWANBANJIRs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.DAERAHRAWANKEKERINGANs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.DAERAHRAWANLONGSORs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.DANAUs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.EMBUNG_POTENSI).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.EMBUNGs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.GUNUNGBERAPIs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.IRIGASIs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.PIDs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.POS_PENGAMAT).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.POSKO_BANJIR).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.RAWAs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.STASIUNDEBITs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.STASIUNHUJANs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.STASIUNKLIMATOLOGIs).KeyColumn("K_DAS").Inverse();
            this.HasMany(x => x.SUNGAIs).KeyColumn("K_DAS").Inverse();
        }
    }
}
