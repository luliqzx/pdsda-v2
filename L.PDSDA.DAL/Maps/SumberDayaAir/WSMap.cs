﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class WSMap : ClassMap<WS>
    {
        public WSMap()
        {
            this.Map(x => x.N_WS);
            this.Map(x => x.ANAK_SUNGAI);
            this.Map(x => x.AREA);
            this.Map(x => x.DANAU);
            this.Map(x => x.GEOLOGI);
            this.Id(x => x.K_WS);
            this.Map(x => x.KOTA);
            this.Map(x => x.LANDUSE);
            this.Map(x => x.LOKASI);
            this.Map(x => x.LOKASI_GEOGRAFIS);
            this.Map(x => x.MARSHES);
            this.Map(x => x.ORIGIN);
            this.Map(x => x.OUTLET);
            this.Map(x => x.PJG_SUNGAI_UTAMA);
            this.Map(x => x.POPULASI);
            this.Map(x => x.PRECIPITATION);
            this.Map(x => x.RUNOFF);
            this.References(x => x.STATUS_WS).Column("K_STATUS").NotFound.Ignore();
            this.Map(x => x.TERENDAH);
            this.Map(x => x.TERTINGGI);
            this.Map(x => x.WADUK);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit

            this.HasMany(x => x.AIRTANAHs).KeyColumn("K_WS");
            this.HasMany(x => x.BENDUNGANs).KeyColumn("K_WS");
            this.HasMany(x => x.BENDUNGs).KeyColumn("K_WS");
            this.HasMany(x => x.DAERAHRAWANBANJIRs).KeyColumn("K_WS");
            this.HasMany(x => x.DAERAHRAWANKEKERINGANs).KeyColumn("K_WS");
            this.HasMany(x => x.DAERAHRAWANLONGSORs).KeyColumn("K_WS");
            this.HasMany(x => x.DANAUs).KeyColumn("K_WS");
            this.HasMany(x => x.EMBUNG_POTENSI).KeyColumn("K_WS");
            this.HasMany(x => x.EMBUNGs).KeyColumn("K_WS");
            this.HasMany(x => x.GUNUNGBERAPIs).KeyColumn("K_WS");
            this.HasMany(x => x.IRIGASIs).KeyColumn("K_WS");
            this.HasMany(x => x.PIDs).KeyColumn("K_WS");
            this.HasMany(x => x.POS_PENGAMAT).KeyColumn("K_WS");
            this.HasMany(x => x.POSKO_BANJIR).KeyColumn("K_WS");
            this.HasMany(x => x.RAWAs).KeyColumn("K_WS");
            this.HasMany(x => x.STASIUNDEBITs).KeyColumn("K_WS");
            this.HasMany(x => x.STASIUNHUJANs).KeyColumn("K_WS");
            this.HasMany(x => x.STASIUNKLIMATOLOGIs).KeyColumn("K_WS");
            this.HasMany(x => x.DASes).KeyColumn("K_WS");
            this.HasMany(x => x.SUNGAIs).KeyColumn("K_WS");
            this.HasManyToMany(x => x.BALAIs).Table("WS_BALAI").NotFound.Ignore();
        }
    }
}
