﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class DANAUMap : ClassMap<DANAU>
    {
        public DANAUMap()
        {
            this.Map(x => x.CURAH_HUJAN);
            this.References(x => x.DAS).Column("K_DAS").NotFound.Ignore();
            this.Map(x => x.DTA);
            this.Map(x => x.JENIS);
            this.Id(x => x.K_DANAU).GeneratedBy.Assigned();
            this.References(x => x.KABUPATEN).Columns("KABUPATEN", "K_PROPINSI").NotFound.Ignore();
            this.Map(x => x.KAPASITAS_LAYANAN);
            this.Map(x => x.KUALITAS_AIR);
            this.Map(x => x.LAJU_SEDIMENTASI);
            this.Map(x => x.LUAS);
            this.Map(x => x.MANF_DMI);
            this.Map(x => x.MANF_IRIGASI);
            this.Map(x => x.MANF_PLTA);
            this.Map(x => x.MANF_TERNAK);
            this.Map(x => x.N_DANAU);
            this.Map(x => x.SEDIMENTASI);
            this.Map(x => x.VOLUME_TAMPUNG);
            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit

            //this.Map(x => x.K_KABUPATEN);
        }
    }
}
