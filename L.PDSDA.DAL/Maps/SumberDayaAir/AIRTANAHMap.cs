﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    using L.PDSDA.DAL.Entities;
    using FluentNHibernate.Mapping;
    public class AIRTANAHMap : ClassMap<AIRTANAH>
    {
        public AIRTANAHMap()
        {
            this.Map(x => x.DESA);
            this.Map(x => x.DI);
            this.Id(x => x.K_AIRTANAH).GeneratedBy.Assigned();
            this.References(x => x.DAS).Column("K_DAS").NotFound.Ignore();
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();
            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.NO_SUMUR);
            this.Map(x => x.TH_BANGUN);
            this.HasMany(x => x.AIRTANAH_DYN).KeyColumn("K_AIRTANAH").Cascade.All();
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
        }
    }
}
