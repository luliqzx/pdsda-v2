﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class RAWAMap : ClassMap<RAWA>
    {
        public RAWAMap()
        {
            this.References(x => x.DAS).Column("K_DAS").LazyLoad().NotFound.Ignore();//.Nullable();//.Cascade.SaveUpdate();
            this.Map(x => x.DESA);
            this.Map(x => x.JENIS);
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").LazyLoad().NotFound.Ignore();//.Nullable();//;
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.N_RAWA);
            this.Id(x => x.K_RAWA);//.GeneratedBy.Assigned();
            this.References(x => x.WS).Column("K_WS").LazyLoad().NotFound.Ignore();//.Nullable();
            //this.HasMany(x => x.RAWA_DYN).KeyColumn("K_RAWA").LazyLoad();
            this.HasMany(x => x.RAWA_DYNs).KeyColumn("K_RAWA").LazyLoad().Cascade.AllDeleteOrphan();//.AsBag().Inverse();
            //this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit

            //Bag(x => x.RawaDyns, colmap => { colmap.Key(x => x.Column("K_RAWA")); colmap.Inverse(true); }, map => { map.OneToMany(); });
        }
    }
}
