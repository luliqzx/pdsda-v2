﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    using L.PDSDA.DAL.Entities;
    using FluentNHibernate.Mapping;
    public class EMBUNG_POTENSIMap : ClassMap<EMBUNG_POTENSI>
    {
        public EMBUNG_POTENSIMap()
        {
            this.References(x => x.DAS).Column("K_DAS").NotFound.Ignore();//.Column("K_DAS").NotFound.Ignore().Nullable().NotFound.Ignore();
            this.Map(x => x.DESA);
            this.Map(x => x.IRIGASI);
            this.Map(x => x.JENIS_PENANGANAN);
            this.Id(x => x.K_EMBUNG_POTENSI);
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();//.Column("K_KABUPATEN").Nullable().NotFound.Ignore();
            //this.References(x => x.PROPINSI).Column("K_PROPINSI").Nullable().NotFound.Ignore();
            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();//).Column("K_WS").NotFound.Ignore().Nullable().NotFound.Ignore();
            this.Map(x => x.KAPASITAS);
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.KETERANGAN);
            this.Map(x => x.KK);
            this.Map(x => x.N_EMBUNG_POTENSI);
            this.Map(x => x.RENCANA_ANGGARAN);
            this.Map(x => x.TERNAK);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit

        }
    }
}
