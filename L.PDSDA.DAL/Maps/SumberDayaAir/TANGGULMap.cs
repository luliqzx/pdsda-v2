﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class TANGGULMap : ClassMap<TANGGUL>
    {
        public TANGGULMap()
        {
            this.CompositeId()
                .KeyProperty(x => x.URUT_TANGGUL)
                .KeyReference(x => x.SUNGAI, new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" });
            this.Map(x => x.BIAYA_KONSULTAN);
            this.Map(x => x.BIAYA_KONTRAKTOR);
            this.Map(x => x.BIAYA_REHAB);
            this.Map(x => x.DESA);
            this.Map(x => x.ELEVASI);
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.KONDISI);
            this.Map(x => x.KONSULTAN);
            this.Map(x => x.KONTRAKTOR);
            this.Map(x => x.KOORDINAT_AKHIR);
            this.Map(x => x.KOORDINAT_AWAL);
            this.Map(x => x.LEBAR_BANTARAN);
            this.Map(x => x.LEBAR_SEMPADAN);
            this.Map(x => x.LERENG_DALAM);
            this.Map(x => x.LERENG_LUAR);
            this.Map(x => x.NM_PROYEK);
            this.Map(x => x.PANJANG);
            this.Map(x => x.PELINDUNG_DALAM);
            this.Map(x => x.PELINDUNG_LUAR);
            this.Map(x => x.PENGELOLA);
            this.Map(x => x.PENGGUNAAN_BANTARAN);
            this.Map(x => x.PENGGUNAAN_SEMPADAN);
            this.Map(x => x.POSISI);
            this.Map(x => x.PUNCAK);
            this.Map(x => x.QDESIGN);
            //this.References(x => x.SUNGAI).Columns(new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" }).Cascade.None();
            this.Map(x => x.TAHUN_DATA);
            this.Map(x => x.THDIBANGUN);
            this.Map(x => x.THREHAB);
            this.Map(x => x.TINGGI_DALAM);
            this.Map(x => x.TINGGI_LUAR);
            this.Map(x => x.TIPE);
            this.Map(x => x.TITIK_AKHIR);
            this.Map(x => x.TITIK_AWAL);
            //this.References(x => x.PROPINSI).Columns("K_PROPINSI");
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();
        }
    }
}
