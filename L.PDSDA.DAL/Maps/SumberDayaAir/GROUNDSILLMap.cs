﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class GROUNDSILLMap : ClassMap<GROUNDSILL>
    {
        public GROUNDSILLMap()
        {
            this.CompositeId()
                .KeyProperty(x => x.URUT_GROUNDSILL)
                .KeyReference(x => x.SUNGAI, new string[] { "URUT", "K_ORDE1", "K_ORDE2", "K_ORDE3", "K_ORDE4", "K_ORDE5" });
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();
            this.Map(x => x.BIAYA_KONSULTAN);
            this.Map(x => x.BIAYA_KONTRAKTOR);
            this.Map(x => x.BIAYA_REHAB);
            this.Map(x => x.DALAM);
            this.Map(x => x.DESA);
            this.Map(x => x.JEMBATAN);
            this.Map(x => x.KEADAAN);
            this.Map(x => x.KEADAAN1);
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.KONDISI);
            this.Map(x => x.KONSTRUKSI);
            this.Map(x => x.KONSULTAN);
            this.Map(x => x.KONTRAKTOR);
            this.Map(x => x.KOORDINAT);
            this.Map(x => x.LEBAR);
            this.Map(x => x.NAMA);
            this.Map(x => x.NM_PROYEK);
            this.Map(x => x.PENGELOLA);
            this.Map(x => x.POSISI);
            this.Map(x => x.TAHUN_DATA);
            this.Map(x => x.THDIBANGUN);
            this.Map(x => x.THREHAB);
            this.Map(x => x.TIPE);
        }
    }
}
