﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.SumberDayaAir
{
    public class BENDUNGANMap : ClassMap<BENDUNGAN>
    {
        public BENDUNGANMap()
        {
            this.Id(x => x.K_BENDUNGAN);
            this.References(x => x.DAS).Column("K_DAS").NotFound.Ignore();
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();
            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.DESA);
            this.Map(x => x.ANAK);
            this.Map(x => x.INDUK);
            this.Map(x => x.N_BENDUNGAN);
            this.Map(x => x.MANF_IRIGASI);
            this.Map(x => x.MANF_PLTA);
            this.Map(x => x.MANF_DMI);
            this.Map(x => x.MANF_LAIN);
            this.Map(x => x.TH_MULAI);
            this.Map(x => x.TH_SELESAI);
            this.Map(x => x.BIAYA);
            this.Map(x => x.PENGELOLA);
            this.Map(x => x.KONSULTAN);
            this.Map(x => x.KONTRAKTOR);
            this.Map(x => x.DTA);
            this.Map(x => x.HUJAN_TAHUNAN);
            this.Map(x => x.HUJAN_DESAIN);
            this.Map(x => x.DESAIN_PENGELAK);
            this.Map(x => x.K_BENDTIPE);
            this.Map(x => x.TINGGI_DASAR_SUNGAI).Precision(10).Scale(7);
            this.Map(x => x.TINGGI_DASAR_GALIAN);
            this.Map(x => x.PJG_PUNCAK);
            this.Map(x => x.LEBAR_PUNCAK);
            this.Map(x => x.ELEVASI_PUNCAK);
            this.Map(x => x.VOL_BENDUNG);
            this.Map(x => x.PELIMPAH_TIPE);
            this.Map(x => x.PELIMPAH_BANJIR);
            this.Map(x => x.PELIMPAH_KALA);
            this.Map(x => x.PELIMPAH_KAPASITAS);
            this.Map(x => x.PELIMPAH_PINTU);
            this.Map(x => x.PELIMPAH_TIPEPINTU);
            this.Map(x => x.PELIMPAH_LEBAR_MERCU);
            this.Map(x => x.PELIMPAH_PJG_MERCU);
            this.Map(x => x.PELIMPAH_ELEVASI_MERCU);
            this.Map(x => x.HEAD_TIPE);
            this.Map(x => x.HEAD_BENTUK);
            this.Map(x => x.HEAD_DIAMETER);
            this.Map(x => x.HEAD_JUMLAH);
            this.Map(x => x.HEAD_PJG);
            this.Map(x => x.HEAD_TIPEALAT);
            this.Map(x => x.HEAD_KAPASITAS);
            this.Map(x => x.TANGKI_TIPE);
            this.Map(x => x.TANGKI_JUMLAH);
            this.Map(x => x.TANGKI_DIAMETER);
            this.Map(x => x.TANGKI_TINGGI);
            this.Map(x => x.PIPA_TIPE);
            this.Map(x => x.PIPA_JUMLAH);
            this.Map(x => x.PIPA_BENTUK);
            this.Map(x => x.PIPA_PJG);
            this.Map(x => x.PIPA_DIAMETER);
            this.Map(x => x.PIPA_KAPASITAS);
            this.Map(x => x.IRIGASI_TIPE);
            this.Map(x => x.IRIGASI_BENTUK);
            this.Map(x => x.IRIGASI_UKURAN);
            this.Map(x => x.IRIGASI_PJG);
            this.Map(x => x.IRIGASI_TIPEALAT);
            this.Map(x => x.IRIGASI_JUMLAH);
            this.Map(x => x.IRIGASI_KAPASITAS);
            this.Map(x => x.LISTRIK_TIPE);
            this.Map(x => x.LISTRIK_KAPASITAS);
            this.Map(x => x.LISTRIK_DIMENSI);
            this.Map(x => x.LISTRIK_TURBIN);
            this.Map(x => x.BAKU_TIPE);
            this.Map(x => x.BAKU_TINGGI);
            this.Map(x => x.BAKU_BENTUK);
            this.Map(x => x.BAKU_DIAMETER);
            this.Map(x => x.BAKU_JUMLAH);
            this.Map(x => x.BAKU_KAPASITAS);
            this.Map(x => x.BAKU_ENERGI);
            this.Map(x => x.PISOMETER);
            this.Map(x => x.PENURUNAN);
            this.Map(x => x.REMBESAN);
            this.Map(x => x.INKLINOMETER);
            this.Map(x => x.TEKANAN);
            this.Map(x => x.ALIRAN);
            this.Map(x => x.PATOK);
            this.Map(x => x.ASELEROGRAF);
            this.Map(x => x.EXTENSOMETER);
            this.Map(x => x.PENGGUNA);
            this.Map(x => x.WADUK_ELEVASI_BANJIR);
            this.Map(x => x.WADUK_MUKAAIR_BANJIR);
            this.Map(x => x.WADUK_ELEVASI_NORMAL);
            this.Map(x => x.WADUK_MUKAAIR_NORMAL);
            this.Map(x => x.WADUK_ELEVASI_MINIMUM);
            this.Map(x => x.WADUK_MUKAAIR_MINIMUM);
            this.Map(x => x.WADUK_VOL_BANJIR);
            this.Map(x => x.WADUK_VOL_NORMAL);
            this.Map(x => x.WADUK_VOL_MATI);
            this.Map(x => x.WADUK_VOL_EFEKTIF);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }

    }
}
