﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Maps
{
    using FluentNHibernate;
    using FluentNHibernate.Mapping;
    using L.PDSDA.DAL.Entities;
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            this.Id(x => x.Id).GeneratedBy.Increment();
            this.Map(x => x.BirthDate).Nullable();
            this.References(x => x.Department).Nullable().NotFound.Ignore();
            this.Map(x => x.Firstname).Not.Nullable();
            this.Map(x => x.Lastname);
            this.Map(x => x.Password);
            this.Map(x => x.Username).Unique().Not.Nullable();
            this.Map(x => x.Email).Unique().Nullable();
            this.HasManyToMany(x => x.Groups).NotFound.Ignore();
            //this.References(x => x.PROPINSI, "K_PROPINSI").LazyLoad(Laziness.False);

            //this.HasManyToMany(x => x.BALAIs);
            //this.References(x => x.BALAI).Column("Balai").Nullable();

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
        }
    }
}
