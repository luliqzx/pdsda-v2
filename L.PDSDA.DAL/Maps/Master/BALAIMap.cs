﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.DAL.Maps.Master
{
    public class BALAIMap : ClassMap<BALAI>
    {
        public BALAIMap()
        {
            //this.Id(x => x.ID);
            this.Id(x => x.KodeBalai).GeneratedBy.Assigned(); 
            this.Map(x => x.NamaBalai);
            this.HasManyToMany(x => x.PROPINSIs).Table("PROPINSI_BALAI");
            this.HasManyToMany(x => x.WSs).Table("WS_BALAI").NotFound.Ignore();
            this.HasManyToMany(x => x.Groups).Table("Role_BALAI");
            //this.HasMany(x => x.Users);
        }
    }
}
