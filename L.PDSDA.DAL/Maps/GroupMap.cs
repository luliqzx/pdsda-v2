﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps
{
    public class GroupMap : ClassMap<Group>
    {
        public GroupMap()
        {
            this.Id(x => x.Id).GeneratedBy.Increment();
            this.Map(x => x.Code).Unique();
            this.Map(x => x.Description);
            this.Map(x => x.Name);
            this.References(x => x.MainGroup).NotFound.Ignore();
            this.HasMany(x => x.Groups);
            this.HasManyToMany(x => x.Users);
            this.HasMany(x => x.GroupModules).Cascade.All();
            this.HasManyToMany(x => x.BALAIs).Table("Role_BALAI").NotFound.Ignore();


            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
        }

    }
}
