﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Maps
{
    using L.PDSDA.DAL.Entities;
    using FluentNHibernate.Mapping;
    public class DepartmentMap : ClassMap<Department>
    {
        public DepartmentMap()
        {
            this.Id(x => x.Id).GeneratedBy.Increment();
            this.Map(x => x.Code).Unique().Not.Nullable();
            this.Map(x => x.Name);
            this.References(x => x.MainDepartment).NotFound.Ignore();
            this.HasMany(x => x.Users);

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
        }
    }
}
