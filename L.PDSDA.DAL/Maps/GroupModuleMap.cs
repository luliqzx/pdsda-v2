﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Maps
{
    using FluentNHibernate;
    using FluentNHibernate.Mapping;
    using L.PDSDA.DAL.Entities;

    public class GroupModuleMap : ClassMap<GroupModule>
    {
        public GroupModuleMap()
        {
            this.Id(x => x.Id).GeneratedBy.Increment();
            this.Map(x => x.IsActive);
            this.Map(x => x.IsCreateable);
            this.Map(x => x.IsDeleteable);
            this.Map(x => x.IsOptional);
            this.Map(x => x.IsUpdateable);
            this.Map(x => x.IsViewable);
            this.References(x => x.Module).NotFound.Ignore();
            this.References(x => x.Group).NotFound.Ignore();
            this.HasManyToMany(x => x.Privileges).Cascade.All();
            //CompositeId().KeyReference(x => x.Module).KeyReference(x => x.Group);
        }
    }
}
