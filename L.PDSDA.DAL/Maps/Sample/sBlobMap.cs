﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities.Sample;
using FluentNHibernate.Mapping;
using NHibernate.Type;

namespace L.PDSDA.DAL.Maps.Sample
{
    public class sBlobMap : ClassMap<sBlob>
    {
        public sBlobMap()
        {
            this.Id(x => x.Id).GeneratedBy.Increment();
            this.Map(x => x.blobData).CustomType<BinaryBlobType>().Not.Nullable(); //.CustomType<StringClobType>();
        }
    }
}
