﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Bencana
{
   public class POS_PENGAMATMap:ClassMap<POS_PENGAMAT>
    {
       public POS_PENGAMATMap()
       {
           this.Map(x=>x.ALAT);
           this.Map(x=>x.AWAS);
           this.References(x => x.DA, "K_DAS").NotFound.Ignore();//).Column("K_DAS").NotFound.Ignore().Nullable().NotFound.Ignore();
           this.References(x => x.PROPINSI, "K_PROPINSI").NotFound.Ignore();//).Column("K_PROPINSI").Nullable().NotFound.Ignore();
           this.References(x => x.W, "K_WS").NotFound.Ignore();//).Column("K_WS").NotFound.Ignore().Nullable().NotFound.Ignore();
           this.Id(x => x.K_POSPENGAMAT);//.GeneratedBy.Assigned();
           this.Map(x=>x.N_POSPENGAMAT);
           this.Map(x=>x.SIAGA);
           this.Map(x=>x.SIAP);
           this.Map(x=>x.SUNGAI);

           #region XYAudit
           this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

           this.Map(x => x.CreateBy);
           this.Map(x => x.CreateDate).Nullable();
           this.Map(x => x.CreateTerminal);
           this.Map(x => x.UpdateBy);
           this.Map(x => x.UpdateDate).Nullable();
           this.Map(x => x.UpdateTerminal);
           #endregion XYAudit
       }
    }
}
