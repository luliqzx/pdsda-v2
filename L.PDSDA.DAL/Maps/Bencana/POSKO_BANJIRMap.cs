﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.DAL.Maps.Bencana
{
    public class POSKO_BANJIRMap : ClassMap<POSKO_BANJIR>
    {
        public POSKO_BANJIRMap()
        {
            this.Map(x => x.ALAMAT);
            this.Map(x => x.ALAT_BERAT);
            this.Map(x => x.ALAT_KOMUNIKASI);
            this.Map(x => x.BAHAN_BANJIRAN);
            this.Id(x => x.K_POSKOBANJIR);
            this.References(x => x.DA).Column("K_DAS").NotFound.Ignore();//.Nullable().NotFound.Ignore();
            this.References(x => x.PROPINSI).Column("K_PROPINSI").NotFound.Ignore();//.Nullable().NotFound.Ignore();
            this.References(x => x.W).Column("K_WS").NotFound.Ignore();//.Nullable().NotFound.Ignore();
            this.Map(x => x.KETERANGAN);
            this.Map(x => x.KOORDINATOR);
            this.Map(x => x.N_POSKOBANJIR);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
