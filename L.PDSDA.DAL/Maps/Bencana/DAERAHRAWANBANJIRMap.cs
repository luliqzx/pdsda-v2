﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Bencana
{
  public  class DAERAHRAWANBANJIRMap:ClassMap<DAERAHRAWANBANJIR>
    {
      public DAERAHRAWANBANJIRMap()
      {
          this.Map(x => x.BANDARA);
          this.Map(x => x.HUNIAN_DESA_HA);
          this.Map(x => x.HUNIAN_DESA_PDDK);
          this.Map(x => x.HUNIAN_KOTA_HA);
          this.Map(x => x.HUNIAN_KOTA_PDDK);
          this.Map(x => x.JALANKA);
          this.Map(x => x.JALANKABUPATEN);
          this.Map(x => x.JALANNEGARA);
          this.Map(x => x.JALANPROPINSI);
          this.Map(x => x.JEMBATAN);

          this.Id(x => x.K_DAERAHRAWANBANJIR);//.GeneratedBy.Assigned();
          this.References(x => x.DA, "K_DAS").NotFound.Ignore();//).Column("K_DAS").NotFound.Ignore().Nullable().NotFound.Ignore();
          this.References(x => x.PROPINSI, "K_PROPINSI").NotFound.Ignore();//).Column("K_PROPINSI").Nullable().NotFound.Ignore();
          this.References(x => x.W, "K_WS").NotFound.Ignore();//).Column("K_WS").NotFound.Ignore().Nullable().NotFound.Ignore();
          this.Map(x => x.KAWASAN_INDUSTRI);
          this.Map(x => x.KEBUN);
          this.Map(x => x.KETERANGAN);
          this.Map(x => x.KETERANGAN1);
          this.Map(x => x.KLASIFIKASI);
          this.Map(x => x.KONDISI_PINTU);
          this.Map(x => x.LAMA_GENANGAN_HARI);
          this.Map(x => x.LAMA_GENANGAN_JAM);
          this.Map(x => x.LOKASI);
          this.Map(x => x.LUAS);
          this.Map(x => x.N_DAERAHRAWANBANJIR);
          this.Map(x => x.PELABUHAN);
          this.Map(x => x.PENYEBAB);
          this.Map(x => x.POLDER_KAPASITAS);
          this.Map(x => x.POLDER_KONDISI);
          this.Map(x => x.POMPA_KAPASITAS);
          this.Map(x => x.POMPA_KONDISI);
          this.Map(x => x.POSKOBANJIR);
          this.Map(x => x.POSPENGAMAT);
          this.Map(x => x.SAWAH);
          this.Map(x => x.SODETAN_KAPASITAS);
          this.Map(x => x.SODETAN_PANJANG);
          this.Map(x => x.STASIUNKA);
          this.Map(x => x.SUNGAI);
          this.Map(x => x.TAMBAK);
          this.Map(x => x.TANGGUL_KAPASITAS);
          this.Map(x => x.TANGGUL_KRITIS);
          this.Map(x => x.TANGGUL_PANJANG);
          this.Map(x => x.TERMINAL);
          this.Map(x => x.WADUK_KAPASITAS);
          this.Map(x => x.WADUK_KONDISI);

          #region XYAudit
          this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

          this.Map(x => x.CreateBy);
          this.Map(x => x.CreateDate).Nullable();
          this.Map(x => x.CreateTerminal);
          this.Map(x => x.UpdateBy);
          this.Map(x => x.UpdateDate).Nullable();
          this.Map(x => x.UpdateTerminal);
          #endregion XYAudit
      }
    }
}
