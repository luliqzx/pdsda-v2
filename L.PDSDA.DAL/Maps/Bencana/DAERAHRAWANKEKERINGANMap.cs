﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.DAL.Maps.Bencana
{
    public class DAERAHRAWANKEKERINGANMap : ClassMap<DAERAHRAWANKEKERINGAN>
    {
        public DAERAHRAWANKEKERINGANMap()
        {
            this.Map(x => x.DAMPAK_LAHAN_KEBUN);
            this.Map(x => x.DAMPAK_LAHAN_LAIN2);
            this.Map(x => x.DAMPAK_LAHAN_SAWAH);
            this.Map(x => x.DAMPAK_PENDUDUK);
            this.Id(x => x.K_DAERAHRAWANKEKERINGAN);//.GeneratedBy.Assigned();
            this.References(x => x.DA, "K_DAS").NotFound.Ignore();//).Column("K_DAS").NotFound.Ignore().Nullable().NotFound.Ignore();
            this.References(x => x.PROPINSI, "K_PROPINSI").NotFound.Ignore();//).Column("K_PROPINSI").Nullable().NotFound.Ignore();
            this.References(x => x.W, "K_WS").NotFound.Ignore();//).Column("K_WS").NotFound.Ignore().Nullable().NotFound.Ignore();
            this.Map(x => x.KETERANGAN);
            this.Map(x => x.KLASIFIKASI);
            this.Map(x => x.LOKASI);
            this.Map(x => x.N_DAERAHRAWANKEKERINGAN);
            this.Map(x => x.NILAI_KERUGIAN);
            this.Map(x => x.PENANGGULANGAN);
            this.Map(x => x.PENYEBAB);
            this.Map(x => x.TAHUN_DATA).Length(4);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
