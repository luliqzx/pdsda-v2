﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Daerah_Irigasi
{
    public class IRIGASI_TANAMMap : ClassMap<IRIGASI_TANAM>
    {
        public IRIGASI_TANAMMap()
        {
            this.CompositeId().KeyReference(x => x.IRIGASI, "K_DI").KeyProperty(x => x.TAHUN_DATA, x => x.Length(4));
            //this.Map(x => x.K_DI);
            //this.Map(x => x.TAHUN_DATA);

            this.Map(x => x.MT1_PD);
            this.Map(x => x.MT1_PL);
            this.Map(x => x.MT1_LAIN);
            this.Map(x => x.MT2_PD);
            this.Map(x => x.MT2_PL);
            this.Map(x => x.MT2_LAIN);
            this.Map(x => x.MT3_PD);
            this.Map(x => x.MT3_PL);
            this.Map(x => x.MT3_LAIN);
            this.Map(x => x.IP);
            this.Map(x => x.PRED_MT1_PD);
            this.Map(x => x.PRED_MT1_PL);
            this.Map(x => x.PRED_MT1_LAIN);
            this.Map(x => x.PRED_MT2_PD);
            this.Map(x => x.PRED_MT2_PL);
            this.Map(x => x.PRED_MT2_LAIN);
            this.Map(x => x.PRED_MT3_PD);
            this.Map(x => x.PRED_MT3_PL);
            this.Map(x => x.PRED_MT3_LAIN);
            this.Map(x => x.PRED_IP);

        }
    }
}
