﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using L.PDSDA.DAL.Entities;

namespace L.PDSDA.DAL.Maps.Daerah_Irigasi
{
    public class IRIGASIMap : ClassMap<IRIGASI>
    {
        public IRIGASIMap()
        {
            this.Id(x => x.K_DI).GeneratedBy.Assigned();
            this.References(x => x.DAS, "K_DAS").Fetch.Join().NotFound.Ignore();
            this.Map(x => x.DESA);
            this.HasMany(x => x.IRIGASI_DYN).KeyColumn("K_DI").Cascade.All();//.ExtraLazyLoad();
            this.References(x => x.IRIGASI_LINTAS).Column("K_DI_LINTAS").NotFound.Ignore();
            this.HasMany(x => x.IRIGASI_TANAM).KeyColumn("K_DI").Fetch.Join().Cascade.All();//.ExtraLazyLoad();
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").Fetch.Join().NotFound.Ignore();
            this.Map(x => x.KECAMATAN);
            this.References(x => x.KEWENANGAN).Column("K_KEWENANGAN").NotFound.Ignore();
            this.Map(x => x.N_DI);
            this.References(x => x.WS).Column("K_WS").NotFound.Ignore();

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
