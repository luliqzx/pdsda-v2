﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Daerah_Irigasi
{
    public class TADAH_HUJANMap : ClassMap<TADAH_HUJAN>
    {
        public TADAH_HUJANMap()
        {
            //this.Map(x => x.K_PROPINSI);
            //this.Map(x => x.K_KABUPATEN);
            //this.Map(x => x.TAHUN_DATA);

            this.CompositeId().KeyReference(x => x.KABUPATEN, "K_KABUPATEN", "K_PROPINSI").KeyProperty(x => x.TAHUN_DATA, x => x.Length(4));

            this.Map(x => x.TADAH_HUJAN1).Column("TADAH_HUJAN");
            this.Map(x => x.LAHAN_KERING);
            this.Map(x => x.MT1_PD);
            this.Map(x => x.MT1_PL);
            this.Map(x => x.MT1_LAIN);
            this.Map(x => x.MT2_PD);
            this.Map(x => x.MT2_PL);
            this.Map(x => x.MT2_LAIN);
            this.Map(x => x.MT3_PD);
            this.Map(x => x.MT3_PL);
            this.Map(x => x.MT3_LAIN);
            this.Map(x => x.IP);
            this.Map(x => x.P3A);
            this.Map(x => x.ANGGOTA);
            this.Map(x => x.BADAN_HUKUM);
            this.Map(x => x.STATUS);
            this.Map(x => x.KETERANGAN);
            this.Map(x => x.JML_AKTIF);
            this.Map(x => x.JML_AKTIF_ANGGOTA);
            this.Map(x => x.JML_AKTIF_BH);
            this.Map(x => x.JML_BERKEMBANG);
            this.Map(x => x.JML_BERKEMBANG_ANGGOTA);
            this.Map(x => x.JML_BERKEMBANG_BH);
            this.Map(x => x.JML_TIDAKBERKEMBANG);
            this.Map(x => x.JML_TIDAKBERKEMBANG_ANGGOTA);
            this.Map(x => x.JML_TIDAKBERKEMBANG_BH);
            this.Map(x => x.JML_PASIF);
            this.Map(x => x.JML_PASIF_ANGGOTA);
            this.Map(x => x.JML_PASIF_BH);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
