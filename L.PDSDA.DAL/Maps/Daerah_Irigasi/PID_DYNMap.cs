﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Daerah_Irigasi
{
    public class PID_DYNMap : ClassMap<PID_DYN>
    {
        public PID_DYNMap()
        {
            this.CompositeId().KeyReference(x => x.PID, "K_PID").KeyProperty(x => x.TAHUN_DATA, x => x.Length(4));
            this.Map(x => x.SUMBER_DATA);
            this.Map(x => x.LUAS_SBL);
            this.Map(x => x.MT1_PD_SBL);
            this.Map(x => x.MT1_PL_SBL);
            this.Map(x => x.MT1_LAIN_SBL);
            this.Map(x => x.MT2_PD_SBL);
            this.Map(x => x.MT2_PL_SBL);
            this.Map(x => x.MT2_LAIN_SBL);
            this.Map(x => x.MT3_PD_SBL);
            this.Map(x => x.MT3_PL_SBL);
            this.Map(x => x.MT3_LAIN_SBL);
            this.Map(x => x.IP_SBL);
            this.Map(x => x.LUAS_SSD);
            this.Map(x => x.MT1_PD_SSD);
            this.Map(x => x.MT1_PL_SSD);
            this.Map(x => x.MT1_LAIN_SSD);
            this.Map(x => x.MT2_PD_SSD);
            this.Map(x => x.MT2_PL_SSD);
            this.Map(x => x.MT2_LAIN_SSD);
            this.Map(x => x.MT3_PD_SSD);
            this.Map(x => x.MT3_PL_SSD);
            this.Map(x => x.MT3_LAIN_SSD);
            this.Map(x => x.IP_SSD);
            this.Map(x => x.P3A);
            this.Map(x => x.ANGGOTA);
            this.Map(x => x.BADAN_HUKUM);
            this.Map(x => x.STATUS);
            this.Map(x => x.KETERANGAN);
            this.Map(x => x.JML_AKTIF);
            this.Map(x => x.JML_AKTIF_ANGGOTA);
            this.Map(x => x.JML_AKTIF_BH);
            this.Map(x => x.JML_BERKEMBANG);
            this.Map(x => x.JML_BERKEMBANG_ANGGOTA);
            this.Map(x => x.JML_BERKEMBANG_BH);
            this.Map(x => x.JML_TIDAKBERKEMBANG);
            this.Map(x => x.JML_TIDAKBERKEMBANG_ANGGOTA);
            this.Map(x => x.JML_TIDAKBERKEMBANG_BH);
            this.Map(x => x.JML_PASIF);
            this.Map(x => x.JML_PASIF_ANGGOTA);
            this.Map(x => x.JML_PASIF_BH);

        }
    }
}
