﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Daerah_Irigasi
{
    public class PIDMap : ClassMap<PID>
    {
        public PIDMap()
        {
            this.Id(x => x.K_PID);
            this.Map(x => x.N_PID).Unique().Not.Nullable();
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.DESA);
            this.References(x => x.DA, "K_DAS").NotFound.Ignore();//;
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();//;
            this.HasMany(x => x.PID_DYN).KeyColumn("K_PID").Cascade.All();//.Cache.ReadWrite();
            this.References(x => x.W).Column("K_WS").NotFound.Ignore();

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
