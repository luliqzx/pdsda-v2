﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Maps
{
    using FluentNHibernate;
    using FluentNHibernate.Mapping;
    using L.PDSDA.DAL.Entities;

    public class PrivilegeMap : ClassMap<Privilege>
    {
        public PrivilegeMap()
        {
            this.Id(x => x.Id).GeneratedBy.Increment();
            this.Map(x => x.Code).Unique();
            this.Map(x => x.Description);
            this.Map(x => x.Name);
            this.HasManyToMany(x => x.GroupModules);

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
        }
    }
}
