﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.PDSDA.DAL.Maps
{
    using FluentNHibernate;
    using FluentNHibernate.Mapping;
    using L.PDSDA.DAL.Entities;

    public class ModuleMap : ClassMap<Module>
    {
        public ModuleMap()
        {
            this.Id(x => x.Id).GeneratedBy.Increment();
            this.Map(x => x.Code).Unique();
            this.Map(x => x.Description);
            this.Map(x => x.Name);
            this.Map(x => x.Path);
            this.Map(x => x.Position);
            this.Map(x => x.IsPublic);
            this.Map(x => x.ImageUrl);
            this.Map(x => x.IsActive);

            this.References(x => x.MainModule).Nullable().NotFound.Ignore();
            this.HasMany(x => x.GroupModules).Cascade.All();
            this.HasMany(x => x.ChildModules).Cascade.All();

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
        }
    }
}
