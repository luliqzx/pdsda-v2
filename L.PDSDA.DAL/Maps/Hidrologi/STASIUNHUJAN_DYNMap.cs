﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;
using NHibernate.Type;

namespace L.PDSDA.DAL.Maps.Hidrologi
{
    public class STASIUNHUJAN_DYNMap : ClassMap<STASIUNHUJAN_DYN>
    {
        public STASIUNHUJAN_DYNMap()
        {
            this.CompositeId().KeyReference(x => x.STASIUNHUJAN, "K_STASIUN").KeyProperty(x => x.TAHUN,x=>x.Length(4));
            this.Map(x => x.HARIAN);//.CustomType<BinaryBlobType>().Nullable();
            this.Map(x => x.JAMJAMAN);//.CustomType<BinaryBlobType>().Nullable();
        }
    }
}
