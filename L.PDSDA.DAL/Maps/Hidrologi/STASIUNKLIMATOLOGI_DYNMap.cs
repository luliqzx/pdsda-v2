﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;
using NHibernate.Type;

namespace L.PDSDA.DAL.Maps.Hidrologi
{
    public class STASIUNKLIMATOLOGI_DYNMap : ClassMap<STASIUNKLIMATOLOGI_DYN>
    {
        public STASIUNKLIMATOLOGI_DYNMap()
        {
            this.CompositeId().KeyReference(x => x.STASIUNKLIMATOLOGI, "K_STASIUN").KeyProperty(x => x.TAHUN, x => x.Length(4));
            this.Map(x => x.HARIAN);//.CustomType<BinaryBlobType>().Not.Nullable();
        }
    }
}
