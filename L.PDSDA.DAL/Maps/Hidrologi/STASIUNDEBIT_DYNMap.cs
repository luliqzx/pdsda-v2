﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Hidrologi
{
    public class STASIUNDEBIT_DYNMap : ClassMap<STASIUNDEBIT_DYN>
    {
        public STASIUNDEBIT_DYNMap()
        {
            this.CompositeId().KeyReference(x => x.STASIUNDEBIT, "K_STASIUN").KeyProperty(x => x.TAHUN, x => x.Length(4));

            this.Map(x => x.D_MAKSIMUM_EKSTRIM);
            this.Map(x => x.D_MINIMUM_EKSTRIM);
            this.Map(x => x.D_MAKSIMUM_EKSTRIM_KUMULATIF);
            this.Map(x => x.D_MINIMUM_EKSTRIM_KUMULATIF);
            this.Map(x => x.D_PERIODE_PENCATATAN);
            this.Map(x => x.D_PENENTUAN);
            this.Map(x => x.D_CATATAN);
            this.Map(x => x.D_JENIS_ALAT);
            this.Map(x => x.D_PELAKSANA);
            this.Map(x => x.T_MAKSIMUM_EKSTRIM);
            this.Map(x => x.T_MINIMUM_EKSTRIM);
            this.Map(x => x.T_MAKSIMUM_EKSTRIM_KUMULATIF);
            this.Map(x => x.T_MINIMUM_EKSTRIM_KUMULATIF);
            this.Map(x => x.T_PERIODE_PENCATATAN);
            this.Map(x => x.T_PENENTUAN);
            this.Map(x => x.T_CATATAN);
            this.Map(x => x.T_JENIS_ALAT);
            this.Map(x => x.T_PELAKSANA);
            this.Map(x => x.DEBIT);
            this.Map(x => x.TMA);

        }
    }
}
