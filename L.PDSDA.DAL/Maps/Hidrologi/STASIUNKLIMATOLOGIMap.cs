﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Hidrologi
{
    public class STASIUNKLIMATOLOGIMap : ClassMap<STASIUNKLIMATOLOGI>
    {
        public STASIUNKLIMATOLOGIMap()
        {
            this.References(x => x.DA, "K_DAS");
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();//;

            this.References(x => x.W, "K_WS").NotFound.Ignore();

            this.Id(x => x.K_STASIUN);
            this.Map(x => x.N_STASIUN).Unique();
            this.Map(x => x.KADASTER);
            this.Map(x => x.KECAMATAN);
            this.Map(x => x.DESA);
            this.Map(x => x.LOKASI);
            this.Map(x => x.DIBANGUNOLEH);
            this.Map(x => x.TINGGI_MUKA_LAUT);
            this.Map(x => x.THPENDIRIAN);
            this.Map(x => x.K_STASIUN1);

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
            this.HasMany(x => x.STASIUNKLIMATOLOGI_DYN).KeyColumn("K_STASIUN").Inverse().Cascade.All();

        }
    }
}
