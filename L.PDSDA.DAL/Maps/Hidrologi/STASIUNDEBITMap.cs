﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using L.PDSDA.DAL.Entities;
using FluentNHibernate.Mapping;

namespace L.PDSDA.DAL.Maps.Hidrologi
{
    public class STASIUNDEBITMap : ClassMap<STASIUNDEBIT>
    {
        public STASIUNDEBITMap()
        {
            this.References(x => x.DA).Column("K_DAS").NotFound.Ignore();
            this.References(x => x.KABUPATEN).Columns("K_KABUPATEN", "K_PROPINSI").NotFound.Ignore();//;

            this.References(x => x.W).Column("K_WS").NotFound.Ignore();

            this.Map(x => x.DIBANGUNOLEH);
            this.Map(x => x.ELEVASI);
            this.Map(x => x.INDUK_SUNGAI);
            this.Map(x => x.JENIS);
            this.Id(x => x.K_STASIUN);
            this.Map(x => x.K_STASIUN1);
            this.Map(x => x.LOKASI);
            this.Map(x => x.LOKASI_GEOGRAFIS);
            this.Map(x => x.LUAS_DAS);
            this.Map(x => x.N_STASIUN).Unique();
            this.Map(x => x.PELAKSANA);
            this.Map(x => x.SUNGAI);
            this.Map(x => x.TGLDIBANGUN);
            this.HasMany(x => x.STASIUNDEBIT_DYN).KeyColumn("K_STASIUN").Inverse().Cascade.All();

            #region XYAudit
            this.Component(x => x.LatLong, latlong => { latlong.Map(x => x.Latitude); latlong.Map(x => x.Longitude); });

            this.Map(x => x.CreateBy);
            this.Map(x => x.CreateDate).Nullable();
            this.Map(x => x.CreateTerminal);
            this.Map(x => x.UpdateBy);
            this.Map(x => x.UpdateDate).Nullable();
            this.Map(x => x.UpdateTerminal);
            #endregion XYAudit
        }
    }
}
